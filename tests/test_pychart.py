import copy
import os
import unittest

import numpy as np
import numpy.testing as nptest

from src.pychart.core import pychart as pc
from src.pychart.core import error as pcerror

class TestTimeUnit (unittest.TestCase):
    
    def test_classmethod (self):
        fs = pc.TimeUnit.FemtoSec
        ps = pc.TimeUnit.PicoSec
        ms = pc.TimeUnit.MilliSec
        sec = pc.TimeUnit.Sec
        minute = pc.TimeUnit.Minute
        
        self.assertEqual(pc.TimeUnit.conversion_factor(fs, sec), 1E-15)
        self.assertEqual(pc.TimeUnit.conversion_factor(fs, sec), fs.magnitude)
        self.assertEqual(pc.TimeUnit.conversion_factor(sec, fs), 1/fs.magnitude)
        self.assertEqual(pc.TimeUnit.conversion_factor(fs, fs), 1)
        self.assertEqual(pc.TimeUnit.conversion_factor(minute, sec), 60)
        self.assertEqual(pc.TimeUnit.conversion_factor(fs, ps), ms.magnitude)
        self.assertEqual(pc.TimeUnit.conversion_factor(ps, ms), 1E-9)
        self.assertEqual(pc.TimeUnit.conversion_factor(sec, ms), 1000)
    
    def test_instancemethod (self):
        fs = pc.TimeUnit.FemtoSec
        ps = pc.TimeUnit.PicoSec
        ms = pc.TimeUnit.MilliSec
        sec = pc.TimeUnit.Sec
        minute = pc.TimeUnit.Minute
        
        self.assertEqual(fs.conversion_factor_to(sec), 1E-15)
        self.assertEqual(fs.conversion_factor_to(sec), fs.magnitude)
        self.assertEqual(sec.conversion_factor_to(fs), 1 / fs.magnitude)
        self.assertEqual(fs.conversion_factor_to(fs), 1)
        self.assertEqual(minute.conversion_factor_to(sec), 60)
        self.assertEqual(fs.conversion_factor_to(ps), ms.magnitude)
        self.assertEqual(ps.conversion_factor_to(ms), 1E-9)
        self.assertEqual(sec.conversion_factor_to(ms), 1000)

class TestSpectroscopy (unittest.TestCase):
    
    def test_absorption (self):
        # test for special values
        self.assertEqual(pc.absorption(0.0), 0)
        self.assertEqual(pc.absorption(3), 0.999)
        self.assertEqual(pc.absorption(np.inf), 1.0)
        self.assertRaises(pcerror.InputError, pc.absorption, -2)
    
    def test_transmission (self):
        # test for special values
        self.assertEqual(pc.transmission(0.0), 1)
        self.assertEqual(pc.transmission(3), 0.001)
        self.assertEqual(pc.transmission(np.inf), 0.0)
        self.assertRaises(pcerror.InputError, pc.transmission, -2)
    
    def test_solvfactor (self):
        # test for special values
        self.assertEqual(pc.solvent_factor(0), 0)
        self.assertEqual(pc.solvent_factor(np.inf), 0.0)
        self.assertRaises(pcerror.InputError, pc.solvent_factor, -2)
    
    def test_factor_function (self):
        # test for regular values
        self.assertEqual(pc.calculate_solvent_factor(0.8, 'free'), 0.8)
        self.assertAlmostEqual(pc.calculate_solvent_factor(0.8, 'absorption'), 0.8415106, places=5)
        self.assertAlmostEqual(pc.calculate_solvent_factor(0.8, 'transmission'), 0.1584893, places=5)
        self.assertAlmostEqual(pc.calculate_solvent_factor(0.8, 'factor'), 0.0860387, places=5)
        with self.assertRaises(pcerror.RangeError):
            pc.calculate_solvent_factor(0.8, 'foobar')
    
    def test_black_body_function (self):
        wlns = np.array([500,501,502])
        a = pc.black_body_curve(3000, wlns)
        b = pc.black_body_curve(5000, wlns)
        
        # should increase within test wavelength range
        self.assertLess(a[1], b[1])
        self.assertLess(a[0], a[-1])
        
        # not defined at absolute zero
        self.assertRaises(pcerror.RangeError, pc.black_body_curve, 0.0, wlns)
        
        # test for certain value (500nm, 3000K)
        self.assertAlmostEqual(a[0], 1.0927426e-15)
    
class TestUtilities (unittest.TestCase):
    
    def setUp(self):
        self.arr_a = np.array([-69, np.nan,  16, np.inf,   42,  21, np.inf,   16, -2, np.NINF])
        self.arr_b = np.array([-69, np.nan, -16, np.inf,  -42, -21, np.inf,  -16, -2, np.NINF])
        self.arr_c = np.array([-69,         -16,          -42, -21,          -16, -2, np.NINF])
        self.arr_d = np.array([ 69, np.nan, -16, np.NINF, -42, -21, np.NINF,  16, -2, np.inf])
        self.arr_e = np.array([ 69, np.nan,  16, np.NINF,  42,  21, np.NINF,  16,  2, np.inf])
        self.arr_f = np.array([ 69,          16,           42,  21,           16,  2, np.inf])
    
    def test_replacement (self):
        a, b, c = copy.deepcopy(self.arr_a), copy.deepcopy(self.arr_b), copy.deepcopy(self.arr_c)
        d, e, f = copy.deepcopy(self.arr_d), copy.deepcopy(self.arr_e), copy.deepcopy(self.arr_f)
        
        # basic function tests
        self.assertEqual(pc.replace_nan(a, 0), 1)
        self.assertEqual(pc.replace_nan(c, 0), 0)
        
        self.assertEqual(pc.replace_inf(a, 0, False, False), 0)
        self.assertEqual(pc.replace_inf(a, 0),               3)
        self.assertEqual(pc.replace_inf(b, 0, True, False),  2)
        self.assertEqual(pc.replace_inf(e, 0, False, True),  2)
        self.assertEqual(pc.replace_inf(f, 0, False, True),  0)
        
        # test that originals are changed
        self.assertEqual(pc.replace_inf(a, 0), 0)
        
        pc.replace_inf(c, -33)
        pc.replace_inf(f, +33)
        # -> (c + f) should now even out each other to [0, ..., 0]
        self.assertFalse(np.any(c + f))
        
        # test for interferences
        self.assertEqual(pc.replace_inf(d, np.nan), 3)
        self.assertEqual(pc.replace_nan(d, 0),      4)
        
        n_pos = pc.replace_inf(e, np.NINF)
        n_neg = pc.replace_inf(e, np.inf)
        self.assertEqual(n_pos, n_neg)
        
        n1 = pc.replace_nan(b, np.nan)
        n2 = pc.replace_nan(b, 0)
        self.assertEqual(n1, n2)
    
    def test_finite_max (self):
        # highest positive value ignoring +inf
        self.assertEqual(pc.finite_maximum(self.arr_a), 42)
        # highest possible value with +inf is zero
        self.assertEqual(pc.finite_maximum(self.arr_b), 0)
        # highest value below zero
        self.assertEqual(pc.finite_maximum(self.arr_c), -2)
    
    def test_finite_min (self):
        # lowest negative value ignoring -inf
        self.assertEqual(pc.finite_minimum(self.arr_d), -42)
        # lowest possible value with -inf is zero
        self.assertEqual(pc.finite_minimum(self.arr_e), 0)
        # lowest value above zero
        self.assertEqual(pc.finite_minimum(self.arr_f), 2)
    
    def test_lookup_ind (self):
        # exact match
        self.assertEqual(pc.lookup_index(21, self.arr_a), 5)
        # closest value, unambiguous
        self.assertEqual(pc.lookup_index(.8, self.arr_f), 5)
        # closest value, ambiguous
        ind = pc.lookup_index(7, self.arr_a)
        self.assertTrue(ind == 2 or ind == 7 or ind == 8)
    
    def test_consistent_axes (self):
        # empty arrays
        self.assertTrue(pc.consistent_axes(np.ones(0), np.ones(0)))
        self.assertFalse(pc.consistent_axes(np.ones(0), np.ones((0,0))))
        
        # multidimensional axes
        e = np.linspace(1,48,48)
        self.assertFalse(pc.consistent_axes(e.reshape((12,4)), e.reshape((4,12))))
        self.assertFalse(pc.consistent_axes(e.reshape((2,24)), e.reshape((8,3,2))))
        self.assertTrue(pc.consistent_axes(e.reshape((6,2,4)), e.reshape((6,2,4))))
        self.assertTrue(pc.consistent_axes(e, e.reshape((16,3)).flatten()))
        
        # partially matching axes
        a = np.arange(-10, 10, 1)
        b = np.arange(-10, 20, 1)
        self.assertFalse(pc.consistent_axes(a, b))
        self.assertTrue(pc.consistent_axes(a, b[:-10]))
        
        # inconsistent values
        c = np.linspace(0, 0.9, 10)
        d = np.linspace(0,   1, 10)
        self.assertFalse(pc.consistent_axes(c, d))
        self.assertFalse(pc.consistent_axes(c, d, rtol=0.1))
        self.assertFalse(pc.consistent_axes(c, d, atol=0.05))
        self.assertTrue(pc.consistent_axes(c, d, rtol=0.2))
        self.assertTrue(pc.consistent_axes(c, d, atol=0.05, rtol=0.1))

class TestMathFunctions (unittest.TestCase):
    
    def test_gaussian (self):
        amp, offset = 1, 0.5
        gauss = pc.gaussian(np.arange(280,360), amp, 320, 6, offset)
        
        # test center value
        self.assertEqual(gauss[40], amp + offset)
        # test offset
        self.assertAlmostEqual(gauss[-1], offset)
        for i in range(40):
            # test for continuous raising and falling values
            self.assertLessEqual(gauss[i], gauss[i+1])
            # test for symmetry (implicitly test for full range coverage)
            self.assertEqual(gauss[i], gauss[-i])
    
    def test_smooth (self):
        data = np.array([9,7,5,3,1])
        self.assertTrue(np.all(pc.smooth(data,2) == [4.5,8,6,4,2]))
        self.assertRaises(pcerror.RangeError, pc.smooth, data, 2.)
        self.assertRaises(pcerror.RangeError, pc.smooth, data, 1)

class TestMPLFunctions (unittest.TestCase):
    
    def test_mpl_color (self):
        # named colors
        for color in pc.MPL_COLOR_STRINGS:
            self.assertEqual(pc.mpl_color(color), color)
        self.assertEqual(pc.mpl_color('tab:blue'), 'tab:blue')
        self.assertEqual(pc.mpl_color('xkcd:dust'), 'xkcd:dust')
        
        # RGB tuple as string and floats
        nptest.assert_almost_equal(pc.mpl_color('0.255,0.255,0.255'), (0.001,0.001,0.001))
        nptest.assert_almost_equal(pc.mpl_color('127.5,63.75,0'), (0.5,0.25,0))
        nptest.assert_almost_equal(pc.mpl_color((0.255,0.255,0.255)), (0.255,0.255,0.255))
        nptest.assert_almost_equal(pc.mpl_color((127.5,63.75,0)), (0.5,0.25,0))
        
        # RGBA tuple as string and floats
        nptest.assert_almost_equal(pc.mpl_color('0.255,0.255,0.255,0.1275'), (0.001,0.001,0.001,0.0005))
        nptest.assert_almost_equal(pc.mpl_color('127.5,63.75,0,191.25'), (0.5,0.25,0,0.75))
        nptest.assert_almost_equal(pc.mpl_color((0.255,0.255,0.255,0.5)), (0.255,0.255,0.255,0.5))
        nptest.assert_almost_equal(pc.mpl_color((127.5,63.75,0,191.25)), (0.5,0.25,0,0.75))
        
        # grayscale values as string and ints or floats
        self.assertEqual(pc.mpl_color('0.8'), '0.8')
        self.assertEqual(pc.mpl_color('255'), '1.0')
        self.assertEqual(pc.mpl_color(0.8), '0.8')
        self.assertEqual(pc.mpl_color(127.5), '0.5')
        
        # string handling
        nptest.assert_almost_equal(pc.mpl_color(' 255, 255,255   '), (1,1,1))
        nptest.assert_almost_equal(pc.mpl_color('   00  , 0, 0.0 '), (0,0,0))
        
        # no color acceptance
        self.assertEqual(pc.mpl_color(''), 'none')
        self.assertEqual(pc.mpl_color('    '), 'none')
        self.assertEqual(pc.mpl_color('none'), 'none')
        self.assertEqual(pc.mpl_color(None), 'none')
        
        # unknown named colors and values above 255
        self.assertRaises(pcerror.RangeError, pc.mpl_color, 'redish')
        self.assertRaises(pcerror.RangeError, pc.mpl_color, '666')
        self.assertRaises(pcerror.RangeError, pc.mpl_color, 666)
        self.assertRaises(pcerror.RangeError, pc.mpl_color, '128,256,0')
        self.assertRaises(pcerror.RangeError, pc.mpl_color, (128,256,0))
        self.assertRaises(pcerror.RangeError, pc.mpl_color, '-1')
        self.assertRaises(pcerror.RangeError, pc.mpl_color, -0.12)

class TestIO (unittest.TestCase):
    
    def test_file_import (self):
        # point as decimal separator
        ds = pc.import_numpy_file('benchmark-data/plotting/fsTA_data_1.dat')
        self.assertEqual(ds.shape, (142,397))
        self.assertEqual(ds[1,1], 3.17866)
        
        # comma as decimal separator + scientific notation + no EOL at end of file
        ds = pc.import_numpy_file('benchmark-data/plotting/nsIR_data_steadystate.dat')
        self.assertEqual(ds.shape, (3629, 2))
        self.assertEqual(ds[0,0], 3998.1387)
        self.assertEqual(ds[1,1], 1.43754E-4)
        
        # precision
        ds = pc.import_numpy_file('benchmark-data/plotting/fsTA_data_1_fl.dat')
        self.assertEqual(ds.dtype, np.float64)
        self.assertEqual(ds[0,1], 5.1251799898174E-5)
        
        # test exception handling
        self.assertRaises(pcerror.InputError, pc.import_numpy_file, 'foobar.dat')
    
    def test_file_export (self):
        ds_out = np.linspace(-2,16,num=42).reshape((6,7))
        filename = 'test_export'
        
        # test export itself
        pc.export_pychart_file(ds_out, np.arange(7), np.arange(6), filename)
        self.assertTrue(os.path.exists(filename))
        
        # test conformity to pychart by reloading
        ds_in = pc.import_numpy_file(filename)
        self.assertEqual(ds_in[0,0], 0)
        self.assertEqual(ds_in.shape, (7,8))
        
        os.remove(filename)
    
    def test_z20_export (self):
        ds_out = np.arange(12).reshape((3, 4))
        filename = 'test_export'
        
        # test export itself
        pc.export_z20_file(ds_out, np.arange(200,204), np.arange(100,103), filename)
        self.assertTrue(os.path.exists(filename))
        
        # test conformity to Z20
        with open(filename, 'r') as file:
            lines = file.readlines()
        self.assertEqual(int(lines[3][9:]), 3)
        self.assertEqual(int(lines[5][12:]), 4)
        wlns = lines[6][14:].split()
        self.assertEqual(len(wlns), 4)
        
        # test integrity of data
        txt = '0 '
        txt += lines[6][14:]
        for i in range(10,len(lines)):
            txt += lines[i]
        ds_in = np.fromstring(txt, sep=' ')
        ds_expected = np.array([[0,200,201,202,203],[100,0,1,2,3],[101,4,5,6,7],[102,8,9,10,11]])
        ds_actual = ds_in.reshape(4,5) - ds_expected
        self.assertEqual(np.sum(ds_actual), 0)
        
        os.remove(filename)

if __name__ == '__main__':
    unittest.main()