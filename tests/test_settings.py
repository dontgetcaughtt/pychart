import unittest
from enum import Enum
import os

import matplotlib as mpl

from PyQt5.QtCore import QDir

from src.pychart.utils import settings as pcsettings
from src.pychart.core import error as pcerror
from src.pychart.utils import i18n

class TestPychartSettings (unittest.TestCase):
    
    def setUp (self):
        self.translator = i18n.Translator(i18n.LanguageCode.en)
    
    def test_properties (self):
        s = pcsettings.PyChartSettings(self.translator)
        
        # language
        class TestLanguageCode (Enum):
            tlh = 'tlh'
        
        for i in ['en', 'English', i18n.LanguageCode.en]:
            s.language = i
            self.assertEqual(s.language, 'English')
            self.assertIs(s.language_i18n_identifier(), i18n.LanguageCode.en)
        
        for i in ['de', 'Deutsch', i18n.LanguageCode.de]:
            s.language = i
            self.assertEqual(s.language, 'Deutsch')
            self.assertIs(s.language_i18n_identifier(), i18n.LanguageCode.de)
        
        for i in ['tlh', 'tlhIngan Hol', TestLanguageCode.tlh]:
            s.language = i
            self.assertEqual(s.language, 'Deutsch')
            self.assertIs(s.language_i18n_identifier(), i18n.LanguageCode.de)
        
        # startup tab
        s.language = 'en'
        tabs = {
            'internal': ['PlotTab', 'TD-Fit', 'NetCDF', 'HDF', 'ChooseNew'],
            'i18n': ['SETTINGS_TAB_PLOT', 'SETTINGS_TAB_TDFIT', 'SETTINGS_TAB_NETCDF', 'SETTINGS_TAB_HDF', 'SETTINGS_TAB_CHOOSE'],
            'en': ['Contour Plot', 'TD-Fit', 'NetCDF Import', 'HDF Import', 'New Dialog']
        }
        
        for i, o in zip(tabs['internal'], tabs['en']):
            s.startup_tab = i
            self.assertEqual(s.startup_tab, o)
        
        for i, o in zip(tabs['i18n'], tabs['en']):
            s.startup_tab = self.translator._(i)
            self.assertEqual(s.startup_tab, o)
        
        for i in ['Choose', 'Auswahldialog', None, False]:
            s.startup_tab = i
            self.assertEqual(s.startup_tab, self.translator._('SETTINGS_TAB_PLOT'))
        
        # default directory
        s.default_dir = 'benchmark-data/plotting/'
        self.assertEqual(s.default_dir, 'benchmark-data/plotting/')
        s.default_dir = '../'
        self.assertEqual(s.default_dir, '../')
        
        # default plot tab settings
        s.default_plot_settings = 'benchmark-data/plotting/nsTA.json'
        self.assertEqual(s.default_plot_settings, 'benchmark-data/plotting/nsTA.json')
        
        # plot export resolution
        s.export_resolution = '1280'
        self.assertEqual(s.export_resolution, 1280)
        s.export_resolution = 600
        self.assertEqual(s.export_resolution, 600)
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_DPI_ERROR'):
            s.export_resolution = '1280dpi'
        
        # matplotlib's rcparams
        params = {'ytick.major.size':  20, 'xtick.minor.size': 11, 'ytick.minor.size': 2, 'xtick.major.width': 7}
        s.rcparams = params
        for param, value in params.items():
            self.assertEqual(s.rcparams[param], value)
            self.assertEqual(mpl.rcParams[param], value)
        
        s.rcparams = {'xtick.minor.width': 4.2}
        self.assertEqual(s.rcparams['xtick.minor.width'], 4.2)
        self.assertEqual(s.rcparams['xtick.major.width'], 7)
        self.assertEqual(mpl.rcParams['xtick.minor.width'], 4.2)
        
        s.rcparams = {}
        self.assertEqual(s.rcparams['xtick.major.width'], 7)
        self.assertEqual(mpl.rcParams['xtick.major.width'], 7)
        
        s.rcparams = {'ztick.minor.width': 0.666}
        self.assertEqual(s.rcparams['ztick.minor.width'], 0.666)
        with self.assertRaises(KeyError):
            _ = mpl.rcParams['ztick.minor.width']
        
        # (pychart | z20) export factors for (netcdf | hdf)
        inputs =  [100, 1E6, -5, -0.001, '42', '1.4e-3', True]
        outputs = [100, 1E6, -5, -0.001,  42,    1.4E-3,    1]
        for i, o in zip(inputs, outputs):
            s.netcdf_to_pychart_factor = i
            self.assertAlmostEqual(s.netcdf_to_pychart_factor, o)
            self.assertIsInstance(s.netcdf_to_pychart_factor, float)
            
            s.netcdf_to_z20_factor = i
            self.assertAlmostEqual(s.netcdf_to_z20_factor, o)
            self.assertIsInstance(s.netcdf_to_z20_factor, float)
        
            s.hdf_to_pychart_factor = i
            self.assertAlmostEqual(s.hdf_to_pychart_factor, o)
            self.assertIsInstance(s.hdf_to_pychart_factor, float)
        
            s.hdf_to_z20_factor = i
            self.assertAlmostEqual(s.hdf_to_z20_factor, o)
            self.assertIsInstance(s.hdf_to_z20_factor, float)
        
        none_inputs = [None, 0, '', False, []]
        for i in none_inputs:
            s.netcdf_to_pychart_factor = i
            self.assertIsNone(s.netcdf_to_pychart_factor)
            
            s.netcdf_to_z20_factor = i
            self.assertIsNone(s.netcdf_to_z20_factor)
            
            s.hdf_to_pychart_factor = i
            self.assertIsNone(s.hdf_to_pychart_factor)
            
            s.hdf_to_z20_factor = i
            self.assertIsNone(s.hdf_to_z20_factor)
        
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR'):
            s.netcdf_to_pychart_factor = 'foobar'
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR'):
            s.netcdf_to_z20_factor = 'foobar'
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR'):
            s.hdf_to_pychart_factor = 'foobar'
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR'):
            s.hdf_to_z20_factor = 'foobar'
        
        # (wavelength | time) tolerances for (netcdf | hdf)
        inputs  = [1, 0, 0.05, -1E-6, 12.3E2, '42', '1.4e-3']
        outputs = [1, 0, 0.05, -1E-6, 1230.0,  42,   1.4E-3]
        for i, o in zip(inputs, outputs):
            s.netcdf_wln_tolerance = i
            self.assertAlmostEqual(s.netcdf_wln_tolerance, o)
            self.assertIsInstance(s.netcdf_wln_tolerance, float)
            
            s.hdf_wln_tolerance = i
            self.assertAlmostEqual(s.hdf_wln_tolerance, o)
            self.assertIsInstance(s.hdf_wln_tolerance, float)
            
            s.netcdf_time_tolerance = i
            self.assertAlmostEqual(s.netcdf_time_tolerance, o)
            self.assertIsInstance(s.netcdf_time_tolerance, float)
            
            s.hdf_time_tolerance = i
            self.assertAlmostEqual(s.hdf_time_tolerance, o)
            self.assertIsInstance(s.hdf_time_tolerance, float)
        
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR'):
            s.netcdf_wln_tolerance = 'foobar'
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR'):
            s.hdf_wln_tolerance = 'foobar'
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR'):
            s.netcdf_time_tolerance = 'foobar'
        with self.assertRaises(pcerror.RangeError, msg='ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR'):
            s.hdf_time_tolerance = 'foobar'
        
        # gate time ranges
        s.gate_time_range = (2.2, -0.8)
        self.assertEqual(s.gate_time_range, (2.2,-0.8))
        s.gate_time_range = (-1.2, 1,2, 3)
        self.assertEqual(s.gate_time_range, (2.2,-0.8))
        s.gate_time_range = [-1.2, -0.8]
        self.assertEqual(s.gate_time_range, (2.2,-0.8))
        
        # photodiode plot layout
        s.pd_plot_layout = 'combined'
        self.assertEqual(s.pd_plot_layout, 'combined')
        s.pd_plot_layout = 'separated'
        self.assertEqual(s.pd_plot_layout, 'separated')
        s.pd_plot_layout = 'kombiniert'
        self.assertEqual(s.pd_plot_layout, 'separated')
        s.pd_plot_layout = False
        self.assertEqual(s.pd_plot_layout, 'separated')
    
    def check_defaults (self, settings):
        self.assertEqual(settings.language, self.translator._('de'))
        self.assertEqual(settings.startup_tab, self.translator._('SETTINGS_TAB_PLOT'))
        self.assertEqual(settings.default_dir, QDir.currentPath())
        
        self.assertEqual(settings.default_plot_settings, '../settings/default_fsTA.json')
        self.assertEqual(settings.export_resolution, 300)
        rcparamsdefaults= {
            'figure.titlesize':  15, 'xtick.labelsize': 11, 'ytick.labelsize': 11, 'xtick.major.size': 5,
            'ytick.major.size':  5, 'xtick.minor.size': 3, 'ytick.minor.size': 3, 'xtick.major.width': 1,
            'ytick.major.width': 1, 'axes.labelsize': 12, 'axes.linewidth': 1.25, 'lines.linewidth': 1.25,
            'font.family': 'sans-serif'}
        self.assertEqual(settings.rcparams, rcparamsdefaults)
        
        self.assertEqual(settings.netcdf_to_pychart_factor, 1000)
        self.assertEqual(settings.hdf_to_pychart_factor, None)
        self.assertEqual(settings.netcdf_to_z20_factor, None)
        self.assertEqual(settings.hdf_to_z20_factor, None)
        self.assertEqual(settings.netcdf_wln_tolerance, 0.01)
        self.assertEqual(settings.hdf_wln_tolerance, 0.01)
        self.assertEqual(settings.netcdf_time_tolerance, 0.005)
        self.assertEqual(settings.hdf_time_tolerance, 0.005)
        self.assertEqual(settings.gate_time_range, (-1.2, 1.2))
        self.assertEqual(settings.pd_plot_layout, 'separated')
    
    def test_load_save (self):
        s = pcsettings.PyChartSettings(self.translator)
        
        # always initialize with defaults
        self.check_defaults(s)
        
        # load valid settings
        s.load_settings_from_file('test-data/misc/pychart-settings.json')
        self.assertEqual(s.language, self.translator._('en'))
        self.assertEqual(s.startup_tab, self.translator._('SETTINGS_TAB_CHOOSE'))
        self.assertEqual(s.default_dir, '')
        self.assertEqual(s.default_plot_settings, '../settings/default_fsTA.json')
        self.assertEqual(s.export_resolution, 123)
        self.assertEqual(s.rcparams['figure.titlesize'], 32)
        self.assertEqual(s.rcparams['xtick.major.size'], 3)
        self.assertEqual(s.rcparams['ytick.minor.size'], 1)
        self.assertEqual(s.rcparams['lines.linewidth'], 2)
        self.assertEqual(s.rcparams['font.family'], 'serif')
        self.assertEqual(s.hdf_to_z20_factor, 0.3)
        self.assertEqual(s.netcdf_wln_tolerance, 1)
        self.assertEqual(s.hdf_time_tolerance, 0.5)
        self.assertEqual(s.gate_time_range, (-1.2, 1))
        self.assertEqual(s.pd_plot_layout, 'combined')
        
        # if only irrelevant keys exists, all previous settings should be kept
        s.load_settings_from_file('test-data/misc/pychart-settings-empty.json')
        self.assertEqual(s.language, self.translator._('en'))
        self.assertEqual(s.startup_tab, self.translator._('SETTINGS_TAB_CHOOSE'))
        self.assertEqual(s.default_dir, '')
        self.assertEqual(s.default_plot_settings, '../settings/default_fsTA.json')
        self.assertEqual(s.export_resolution, 123)
        rcparamsdefaults = {
            'figure.titlesize':  32, 'xtick.labelsize': 11, 'ytick.labelsize': 11, 'xtick.major.size': 3,
            'ytick.major.size':  5, 'xtick.minor.size': 3, 'ytick.minor.size': 1, 'xtick.major.width': 1,
            'ytick.major.width': 1, 'axes.labelsize': 12, 'axes.linewidth': 1.25, 'lines.linewidth': 2,
            'font.family':'serif'}
        self.assertEqual(s.rcparams, rcparamsdefaults)
        self.assertEqual(s.netcdf_to_pychart_factor, 1000)
        self.assertEqual(s.hdf_to_pychart_factor, None)
        self.assertEqual(s.netcdf_to_z20_factor, None)
        self.assertEqual(s.hdf_to_z20_factor, 0.3)
        self.assertEqual(s.netcdf_wln_tolerance, 1)
        self.assertEqual(s.hdf_wln_tolerance, 0.01)
        self.assertEqual(s.netcdf_time_tolerance, 0.005)
        self.assertEqual(s.hdf_time_tolerance, 0.5)
        self.assertEqual(s.gate_time_range, (-1.2, 1))
        self.assertEqual(s.pd_plot_layout, 'combined')
        
        # completely invalid file
        with self.assertRaises(pcerror.InputError):
            s.load_settings_from_file('test-data/misc/tlh.yaml')
        
        # settings are changed until errors occur
        with self.assertRaises(pcerror.InputError):
            s.load_settings_from_file('test-data/misc/pychart-settings-invalid.json')
        self.assertEqual(s.export_resolution, 0) # valid change
        self.assertEqual(s.rcparams['font.family'], 'monospace') # ignore invalid rcparam 'ztick'
        self.assertEqual(s.pd_plot_layout, 'combined') # keep previous after error
        
        # catch json decode error (here on single quotes)
        f = """{ "general": {"language": 'en', "startup_tab": "ChooseNew", "default_dir": ""} }"""
        fn = 'test-invalid-settings-export'
        with open(fn, 'w') as file:
            file.write(f)
        self.assertTrue(os.path.exists(fn))
        
        with self.assertRaises(pcerror.InputError, msg='ERROR_PYCHART_SETTINGS_JSON_ERROR'):
            s.load_settings_from_file(fn)
        os.remove(fn)
        
        # test reset function
        s.default_settings()
        self.check_defaults(s)
        
        # save slightly changed settings to file and check reload
        s.startup_tab = 'TD-Fit'
        s.gate_time_range = (0, 1)
        s.hdf_to_pychart_factor = 42
        
        fn = 'test-settings-export'
        s.save_settings_to_file(fn)
        self.assertTrue(os.path.exists(fn))
        s.load_settings_from_file(fn)
        
        self.assertEqual(s.startup_tab, self.translator._('SETTINGS_TAB_TDFIT'))
        self.assertEqual(s.gate_time_range, (0, 1))
        self.assertEqual(s.hdf_to_pychart_factor, 42)
        
        os.remove(fn)
    
    def test_backwards_compatibility (self):
        s = pcsettings.PyChartSettings(self.translator)
        
        # version 4.0
        s.load_settings_from_file('test-data/backwards-compatibility/pychart-settings-v4.json')
        self.assertEqual(s.export_resolution, 300)       # new in v5.1
        self.assertEqual(s.netcdf_to_pychart_factor, None)
        self.assertEqual(s.hdf_to_pychart_factor, None)  # new in v5.0
        self.assertEqual(s.netcdf_to_z20_factor, 1000)
        self.assertEqual(s.hdf_to_z20_factor, None)      # new in v5.0
        self.assertEqual(s.netcdf_wln_tolerance, 0.01)   # new in v5.1
        self.assertEqual(s.hdf_wln_tolerance, 0.01)      # new in v5.1
        self.assertEqual(s.netcdf_time_tolerance, 0.005) # new in v5.1
        self.assertEqual(s.hdf_time_tolerance, 0.005)    # new in v5.1
        self.assertEqual(s.gate_time_range, (-1.2, 1.2)) # new in v5.0
        self.assertEqual(s.pd_plot_layout, 'separated')  # new in v5.1
        
        # version 5.0
        s.load_settings_from_file('test-data/backwards-compatibility/pychart-settings-v5.json')
        self.assertEqual(s.export_resolution, 300)       # new in v5.1
        self.assertEqual(s.netcdf_to_pychart_factor, 1000)
        self.assertEqual(s.hdf_to_pychart_factor, 1E21)
        self.assertEqual(s.netcdf_to_z20_factor, None)
        self.assertEqual(s.hdf_to_z20_factor, 1E21)
        self.assertEqual(s.netcdf_wln_tolerance, 0.01)   # new in v5.1
        self.assertEqual(s.hdf_wln_tolerance, 0.01)      # new in v5.1
        self.assertEqual(s.netcdf_time_tolerance, 0.005) # new in v5.1
        self.assertEqual(s.hdf_time_tolerance, 0.005)    # new in v5.1
        self.assertEqual(s.gate_time_range, (-1.0, 1.0))
        self.assertEqual(s.pd_plot_layout, 'separated')  # new in v5.1
        
        # version 5.1
        s.load_settings_from_file('test-data/backwards-compatibility/pychart-settings-v5_1.json')
        self.assertEqual(s.export_resolution, 600)
        self.assertEqual(s.netcdf_wln_tolerance, 1)
        self.assertEqual(s.hdf_wln_tolerance, 0.1)
        self.assertEqual(s.netcdf_time_tolerance, 0.01)
        self.assertEqual(s.hdf_time_tolerance, 0.001)
        self.assertEqual(s.pd_plot_layout, 'combined')
        
        # current production file
        s.load_settings_from_file('../settings/pychart-settings.json')
    