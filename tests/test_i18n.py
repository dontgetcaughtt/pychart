import unittest
from enum import Enum

from src.pychart.utils import i18n
from src.pychart.core import error as pcerror

class TestInternationalization (unittest.TestCase):
    
    def test_translation_files (self):
        translator = i18n.Translator()
        
        translator.load_lang(i18n.LanguageCode.de)
        reference = set(translator._strings.keys())
        
        # check all languages against German
        for lang in i18n.LanguageCode:
            translator.load_lang(lang)
            self.assertEqual(reference, set(translator._strings.keys()), lang)
    
    def test_translation (self):
        
        class TestLanguageCode (Enum):
            tlh = 'tlh'
        
        translator = i18n.Translator()
        
        # test fallback error
        self.assertRaises(pcerror.InputError, translator.load_lang, TestLanguageCode.tlh)
        
        # test language import
        self.assertTrue(translator.load_lang_from_file('test-data/misc/tlh.yaml'))
        
        # test (re)translation
        self.assertEqual(translator._('THE_SEARCH_FOR_SPOCK'), 'SIpoq nejlu')
        self.assertEqual(translator._('THE_FORCE_AWAKENS'), 'THE_FORCE_AWAKENS')
        self.assertEqual(translator.retranslate(translator._('THE_VOYAGE_HOME')), 'THE_VOYAGE_HOME')

if __name__ == '__main__':
    unittest.main()