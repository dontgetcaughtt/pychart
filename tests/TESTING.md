# Pychart Testing

Pychart might be tested in two ways:

1. Standard unittests at the code level
2. Integration tests using benchmark data and comparing the overall output to the expected result.

## Unittest on Code Level

After every change committed to pychart, all unittests found at `test/test_*.py` should be run and pass. Only then 
changes can be considered satisfactorily and included in a future version.

The `test/test-data/` directory contains several sample files used for general source code tests, which are used as 
described in the following. For detailed information on the unittests themselves, please refer to the docstrings in the 
code files. 

### Testing Pychart Utilitiy Functions

For testing the sellmeier correction implemented in pychart, sample coefficients are provided in the `.json` files. They 
contain imaginary data for excited materials and are used by [`test_sellmeier.py`](test_sellmeier.py).

The internationalization functions are tested against a klingon translation file [`tlh.yaml`](test-data/pychart/tlh.yaml) 
and used by [`test_i18n.py`](test_i18n.py)

### Testing the NetCDF Module

For testing the NetCDF import features, a bunch of example netcdf files with imaginary data are included. The following 
table gives an overview on these. Note that currently only the "pure" `NetCDFFile` and `NetCDFSingleFile` are tested; 
the actual import class is not.

All files can be generated using [`netcdf-test-files.py`](../scripts/netcdf-test-files.py) in the `/scripts/` directory.

|File|Shape|Description|
|---|---|---|
|`4cycle.arr`|(3,7,5,1,4,32)|Fully valid DataArray with four chopper modes and three scans. Scan 1 provides certain, non-random values for all different chopper positions. This scan, thus, might be used for testing the calculation against distinct expectation values, more precisely the correct chopper position indexing.|
|`2cycle.arr`|(1,7,5,1,2,32)|Fully valid DataArray with two chopper modes and one scan. It provides certain, non-random values for both chopper positions, and might be used for testing the calculation against distinct expectation values - also in combination with the `bg2.arr` background file.|
|`5scans.arr`|(5,7,5,1,4,32)|Fully valid DataArray with four chopper modes and five scans. They provide certain, non-random values at one specific chopper position. This position, thus, might be used for testing the averaging against a distinct expectation value of 2.|
|`20shots.arr`|(4,7,20,1,4,32)|Fully valid DataArray with four chopper modes and 20 shots per measurement. Scan 1 provides the same values as `4cycle.arr`. This scan, thus, might be used for testing the calculation against distinct expectation values, more precisely the correct chopper position indexing.|
|`bg-2cycle.arr`|(5,1,2,32)|Fully valid background file DataArray with two chopper modes. Values are carefully chosen to work as an appropriate background for the `2cycle.arr` test file.|
|`bg-3cycle.arr`|(5,1,3,32)|Regular background file DataSet, but with invalid three chopper modes. Should cause an exception when loading/initialising.|
|`bg-4cycle.arr`|(5,1,4,32)|Fully valid background file DataArray with four chopper modes.|
|`altered-axes.arr`|(3,7,5,1,4,32)|Same DataArray as in the `4cycle.arr` test file, but the wavelength axis is shift by 0.1 and the time axis by 50%. This file might be used for testing consistency checks, e.g. prior to correction.|
|`one-dimensional.arr`|(1,1,1,1,1,1)|Regular, but empty data array of shape (1,1,1,1,1,1). Thus, provides specifically an invalid ChopperMode.|
|`missing-axes.arr`|(3,7,5,1,4,32)|Regular DataArray, but axis arrays `ccd_pixel_var` (wavelength values) and `scan_var` (delay times) are missing. Should cause an exception when loading/initializing.|
|`wrong-dimensions.arr`|(14,32,5)|DataArray with only three dimensions. Should cause exceptions for both, main and background, files when loading.|
|`wrong-identifier.arr`|(1,7,5,1,2,32)|Regular DataSet, but dimension `exp_step_nr` is renamed to `shot_nr`. Should cause an exception while calculating/averaging.|
|`bg-wrong-identifier`|(5,1,4,32)|Regular background file DataSet, but with dimension `ccd_mess` (`exp_step_nr`) is renamed to `shot_nr`. Should cause an exception while initializing.|

## Benchmark Tests Using Real Datasets

The directory `tests/benchmark-data/` contains real datasets related to the plotting, import as well as fitting feature 
of pychart. These files are intended to serve for benchmarking and regular tests. Thus, the selection of datasets shall 
provide all possible kinds of data, such as small and large sets, absorption and fluorescence data, small and large 
signal amplitudes, etc.

When developing new code, these datasets can be used as benchmark data. On the other hand, after fixing bugs or making 
smaller changes to the code, they can be used as references to check for the integrity of code as described below.

Of course, datasets retrieved from own measurements can also be used when developing new code or improvements. However, 
the interaction with other developers gets more difficult, when everyone uses their own files for testing. Therefore, 
the benchmark datasets provided with pychart should be preferred over individual ones, when the discussion with others 
might be necessary.

### Testing Plotting Feature by Reference Datasets 

Pychart's plotting feature can be checked by using the benchmark datasets and plotting these according to the settings 
provided in the corresponding .json files. The rendered output should match those of the reference .png files as much 
as possible. The following table indicates those combinations of data and settings files, which can be used as 
benchmarks.

|dataset + external files|settings file|expected output|
|------------------------|-------------|---------------|
|`fsTA_data_1.dat`|`default_fsTA.json`|`fsTA_data_1.png`|
|`fsTA_data_1.dat` <br />+ `fsTA_data_1_1MN.dat` <br />+ `fsTA_data_1_abs.dat` <br />+ `fsTA_data_1_fl.dat`|`fsTA_data_1_settings.json`|`fsTA_data_1_ext.png`|
|`nsTA_data.dat`|`default_nsTA.json`|`nsTA_data.png`|
|`nsTA_data.dat` <br />+ `nsTA_data_fsOffset.dat`|`nsTA_data_settings.json`|`nsTA_data_ext.png`|
|`nsIR_data.dat`|`default_nsIR.json`|`nsIR_data.png`|
|`nsIR_data.dat` <br />+ `nsIR_data_offset.dat` <br />+ `nsIR_data_steadystate.dat`|`nsIR_data_settings.json`|`nsIR_data_ext.png`|

### Benchmark Datasets

**_Datasets for Plotting_**

`tests/benchmark-data/plotting/`

* `fsTA_data_1.dat` Exemplary fsTA data set of _N_-methylacridone (NMA) in 
acetonitrile for high difference amplitudes. Data is given in (nm | ps | mOD).

  * `fsTA_data_1_1MN.dat` Time trace of NMA in acetonitrile after 
  adding 1-methylnapthalene probed at 605 nm. Data given in (ps | mOD).

  * `fsTA_data_1_abs.dat` Steady-state absorption spectrum of NMA in
  acetonitrile. Data given in (nm | M<sup>-1</sup>cm<sup>-1</sup>).

  * `fsTA_data_1_fl.dat` Steady-state fluorescence spectrum of NMA in
  acetonitrile. Data given in (nm | counts).


* `fsTA_data_2.dat` Exemplary fsTA data set for rather low difference 
amplitudes. Data is given in (nm | ps | mOD).


* `nsTA_data.dat` Exemplary laser flash photolysis data set of 
_N_-methylphthalimide in acetonitrile. Data is given in (nm | µs | OD). 

  * `nsTA_data_fsOffset.dat` Offset spectrum of _N_-methylphthalimide obtained 
  by fsTA measurement after "infinite" delay time. Data given in (nm | mOD).

  >Further information and discussion of these data is given in Reiffers et al.: 
  "On the large apparent Stokes shift of phthalimides", _Phys. Chem. Chem. Phys._, 
  **2019**, _21_, 4839-4853, [doi:10.1039/C8CP07795A](https://doi.org/10.1039/C8CP07795A).


* `nsIR_data.dat` Exemplary nanosecond UV pump IR probe data set of
4'-aminomethyl-4,5',8-trimethylpsoralen (AMT) in AT-DNA. Data given in (cm⁻¹ | 
ns | mOD). 

  * `nsIR_data_offset.dat` Offset spectrum of AMT in AT-DNA for "infinite" delay 
  time of the nsIR measurement. Data given in (cm⁻¹ | mOD).

  * `nsIR_data_steadystate.dat` Difference spectrum of AMT in AT-DNA after 
  illumination at 370 nm obtained from steady-state IR-spectroscopy. Data given
  in (cm⁻¹ | mOD).

  >Further information and discussion of these data is given in Diekmann et al.:
  "The Photoaddition of a Psoralen to DNA Proceeds via the Triplet State", 
  _J. Am. Chem. Soc._ **2019**, 141, 34, 13643-13653,
  [doi:10.1021/jacs.9b06521](https://doi.org/10.1021/jacs.9b06521).

**_Datasets for Fitting_**

`tests/benchmark-data/fitting/`

* `TCSPC_data.dat` Exemplary time-correlated single photon counting data set of 
_N_-methylacridone in methanol excited at 385 nm. Data given in (ns | counts).

  * `TCSPC_data_IRF.dat` Intrumental response data set measured from scattered light. Data given in (ns | counts).
