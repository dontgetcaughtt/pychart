import unittest

import numpy as np

from src.pychart.core import netcdfimport as pcnetcdf
from src.pychart.core import error as pcerror


def _fp (name):
    return 'test-data/netcdf/' + name + '.arr'


class TestChopperMode (unittest.TestCase):
    
    def test_lut (self):
        # value => number of array dimensions
        mode_2 = pcnetcdf.ChopperMode.OneChopper
        mode_4 = pcnetcdf.ChopperMode.TwoChopper
        self.assertEqual(mode_2.value, 2)
        self.assertEqual(mode_4.value, 4)
        
        # lookup table => array index
        lut_2 = mode_2.lut()
        lut_4 = mode_4.lut()
        self.assertEqual(lut_2['pumpprobe'], 0)
        self.assertEqual(lut_2['probe'],     1)
        self.assertEqual(lut_4['pumpprobe'], 0)
        self.assertEqual(lut_4['probe'],     2)
        self.assertEqual(lut_4['pump'],      1)
        self.assertEqual(lut_4['bg'],        3)


class TestNetCDFFile (unittest.TestCase):
    
    def test_load_file (self):
        netcdf = pcnetcdf.NetCDFFile()
        
        # four cycle mode files
        netcdf.load_data(_fp('4cycle'))
        self.assertEqual(netcdf['data'].shape, (3,7,5,4,32))
        self.assertTrue(np.array_equal(netcdf['wlns'], np.arange(32, dtype='int32')))
        self.assertTrue(np.array_equal(netcdf['times'], np.arange(7, dtype='int32')))
        self.assertEqual(netcdf['nshots'], 5)
        self.assertEqual(netcdf['nscans'], 3)
        self.assertEqual(netcdf['mode'], pcnetcdf.ChopperMode.TwoChopper)
        self.assertEqual(netcdf['data'].dims, ('scan_nr', 'delay_time', 'exp_step_nr', 'ccd_line', 'ccd_row'))
        
        # two cycle mode files
        netcdf.load_data(_fp('2cycle'))
        self.assertEqual(netcdf['mode'], pcnetcdf.ChopperMode.OneChopper)
        
        # invalid files
        self.assertRaises(pcerror.RangeError, netcdf.load_data, _fp('one-dimensional'))
        self.assertRaises(pcerror.InputError, netcdf.load_data, _fp('missing-axes'))
        self.assertRaises(pcerror.InputError, netcdf.load_data, _fp('wrong-dimensions'))
        
        netcdf.load_data(_fp('wrong-identifier'))
        self.assertEqual(netcdf['data'].dims, ('scan_nr', 'delay_time', 'shot_nr', 'ccd_line', 'ccd_row'))
        
        # background files
        netcdf.load_bg(_fp('bg-2cycle'))
        self.assertEqual(netcdf['bgdata'].shape, (32,))
        self.assertRaises(pcerror.InputError, netcdf.load_bg, _fp('one-dimensional'))
        self.assertRaises(pcerror.InputError, netcdf.load_bg, _fp('wrong-dimensions'))
    
    def test_retrieve_information (self):
        
        info = pcnetcdf.retrieve_netcdf_information(_fp('4cycle'))
        self.assertEqual(info['nscans'], 3)
        self.assertEqual(info['ntimes'], 7)
        self.assertEqual(info['nshots'], 5)
        self.assertEqual(info['nchopper'], 4)
        self.assertEqual(info['nwlns'], 32)
        self.assertTrue(np.array_equal(info['wlns'], np.arange(32)))
        self.assertTrue(np.array_equal(info['times'], np.arange(7)))
        
        info = pcnetcdf.retrieve_netcdf_information(_fp('2cycle'))
        self.assertEqual(info['nscans'], 1)
        self.assertEqual(info['ntimes'], 7)
        self.assertEqual(info['nshots'], 5)
        self.assertEqual(info['nchopper'], 2)
        self.assertEqual(info['nwlns'], 32)
        self.assertTrue(np.array_equal(info['wlns'], np.arange(32)))
        self.assertTrue(np.array_equal(info['times'], np.arange(7)))
        
        info = pcnetcdf.retrieve_netcdf_information(_fp('missing-axes'))
        self.assertFalse(info)
        
        info = pcnetcdf.retrieve_netcdf_information(_fp('one-dimensional'))
        self.assertEqual(info['nscans'], 1)
        self.assertEqual(info['ntimes'], 1)
        self.assertEqual(info['nshots'], 1)
        self.assertEqual(info['nchopper'], 1)
        self.assertEqual(info['nwlns'], 1)
        self.assertTrue(np.array_equal(info['wlns'], np.asarray([0])))
        self.assertTrue(np.array_equal(info['times'], np.asarray([0])))
        
        info = pcnetcdf.retrieve_netcdf_information(_fp(('wrong-identifier')))
        self.assertEqual(info['nshots'], 5)
    
    def test_extract_data (self):
        netcdf = pcnetcdf.NetCDFFile()
        
        
        netcdf.load_data(_fp('5scans'))
        # expected outcome, np.array_equal can test for value and shape
        result = np.full((7,32), 2 + 2**15)
        
        # proper scan handling
        data = netcdf.extract_data('probe', [0,1,2,3,4])
        self.assertTrue(np.array_equal(data, result))
        
        data = netcdf.extract_data('probe', [0,2,9,4])
        self.assertTrue(np.array_equal(data, result))
        
        data = netcdf.extract_data('probe', [0,2])
        self.assertTrue(np.array_equal(data, result))
        
        data = netcdf.extract_data('probe', [])
        self.assertEqual(data.shape, result.shape)
        self.assertTrue(np.all(np.isnan(data)))
        
        # proper chopper mode indexing
        data = netcdf.extract_data('pump', [0,1,2,3,4])
        self.assertFalse(np.array_equal(data, result))
        
        with self.assertRaises(pcerror.RangeError):
            netcdf.extract_data('foobar', [0,1,2,3,4])
        
        
        netcdf.load_data(_fp('2cycle'))
        
        # shift real-world data 2**15 upwards to make positive
        data_p = netcdf.extract_data('probe', [0])
        self.assertEqual(data_p.shape, (7,32))
        self.assertTrue(np.all(np.greater(data_p, 0)))
        
        # all positive difference absorption in '2cycle' test array
        data_pp = netcdf.extract_data('pumpprobe', [0])
        self.assertTrue(np.all(np.greater(data_p, data_pp)))
        
        with self.assertRaises(pcerror.RangeError):
            netcdf.extract_data('bg', [0])
        
        
        netcdf.load_data(_fp('wrong-identifier'))
        with self.assertRaises(ValueError):
            netcdf.extract_data('pumpprobe', [0])
    
    def test_diffabs (self):
        netcdf = pcnetcdf.NetCDFFile()
        
        
        netcdf.load_data(_fp('4cycle'))
        
        # proper indexing: all positive values for '4cycle' array, negative otherwise
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [1])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.all(np.greater(netcdf['diffabs'], 0)))
        
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.OneChopper, [1])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.all(np.greater(netcdf['diffabs'], 0)))
        
        # ref corr: yields zero values at [0,:]
        netcdf.calculate_diffabs(False, True, pcnetcdf.ChopperMode.TwoChopper, [0,1,2])
        ref_corr_on = netcdf['diffabs'][1,:]
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertEqual(np.count_nonzero(netcdf['diffabs'][0,:]), 0)
        
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [0,1,2])
        ref_corr_off = netcdf['diffabs'][1,:]
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertEqual(np.count_nonzero(netcdf['diffabs'][0,:]), 32)
        
        self.assertFalse(np.array_equal(ref_corr_on, ref_corr_off))
        
        
        netcdf.load_data(_fp('5scans'))
        
        # proper scan handling
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [0,1,2,3,4])
        all_scans = netcdf['diffabs'][1,:]
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [1,2,4])
        fewer_scans = netcdf['diffabs'][1,:]
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertFalse(np.array_equal(fewer_scans, all_scans))
        
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [1,9,2])
        invalid_scans = netcdf['diffabs'][1,:]
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.array_equal(invalid_scans, fewer_scans))
        
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.all(np.isnan(netcdf['diffabs'])))

        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [4])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.all(np.isnan(netcdf['diffabs'])))
        
        # bg corr: irrelevant in four cycle mode
        netcdf.calculate_diffabs(True, False, pcnetcdf.ChopperMode.TwoChopper, [0,1,2,3,4])
        self.assertTrue(np.array_equal(netcdf['diffabs'][1,:], all_scans))
        
        
        netcdf.load_data(_fp('2cycle'))
        netcdf.load_bg(_fp('bg-2cycle'))
        
        with self.assertRaises(pcerror.RangeError):
            netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [0])
        
        # bg corr: values around zero, if bg file used (because crazy correction func)
        # + 2**15: all positive otherwise with '2cycle' array
        netcdf.calculate_diffabs(True, False, pcnetcdf.ChopperMode.OneChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertEqual(np.count_nonzero(netcdf['diffabs'][1:,:]), 192)
        bg_on_ref_off = netcdf['diffabs'][1,:]
        
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.OneChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.all(np.greater(netcdf['diffabs'], 0)))
        bg_off_ref_off = netcdf['diffabs'][1,:]
        
        # ref corr: yields zero values at [0,:]
        netcdf.calculate_diffabs(True, True, pcnetcdf.ChopperMode.OneChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        # TODO: understand why the following line sometimes fails
        self.assertEqual(np.count_nonzero(netcdf['diffabs'][0,:]), 0)
        self.assertEqual(np.count_nonzero(netcdf['diffabs'][1:,:]), 192)
        bg_on_ref_on = netcdf['diffabs'][1,:]
        
        netcdf.calculate_diffabs(False, True, pcnetcdf.ChopperMode.OneChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertEqual(np.count_nonzero(netcdf['diffabs'][0,:]), 0)
        self.assertEqual(np.count_nonzero(netcdf['diffabs'][1:,:]), 192)
        bg_off_ref_on = netcdf['diffabs'][1,:]
        
        # ref corr and bg corr should both make a difference
        # proven for arbitrary test slice [1,:]
        self.assertFalse(np.array_equal(bg_off_ref_on, bg_off_ref_off))
        self.assertFalse(np.array_equal(bg_on_ref_on,  bg_on_ref_off))
        
        self.assertFalse(np.array_equal(bg_off_ref_on,  bg_on_ref_on))
        self.assertFalse(np.array_equal(bg_off_ref_off, bg_on_ref_off))
        
        # chopper mode: irrelevant for bg file
        netcdf.load_bg(_fp('bg-4cycle'))
        netcdf.calculate_diffabs(True, False, pcnetcdf.ChopperMode.OneChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        
        
        netcdf.load_data(_fp('wrong-identifier'))
        with self.assertRaises(ValueError):
            netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.OneChopper, [0])
    
    def test_sigdiff (self):
        netcdf = pcnetcdf.NetCDFFile()
        
        
        netcdf.load_data(_fp('4cycle'))
        
        # proper indexing: all negative values for '4cycle' array, negative otherwise
        netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.TwoChopper, [1])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.all(np.less(netcdf['diffabs'], 0)))
        
        netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.OneChopper, [1])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.all(np.less(netcdf['diffabs'], 0)))

        # proper scan handling
        netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.TwoChopper, [0,1,2])
        more_scans = netcdf['diffabs'][1,:]
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        
        netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.TwoChopper, [1,2])
        fewer_scans = netcdf['diffabs'][1,:]
        self.assertFalse(np.array_equal(fewer_scans, more_scans))
        
        netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.TwoChopper, [1,9,2])
        invalid_scans = netcdf['diffabs'][1,:]
        self.assertTrue(np.array_equal(invalid_scans, fewer_scans))
        
        netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.TwoChopper, [])
        self.assertEqual(netcdf['diffabs'].shape, (7, 32))
        self.assertTrue(np.all(np.isnan(netcdf['diffabs'])))
        
        
        netcdf.load_data(_fp('2cycle'))
        
        with self.assertRaises(pcerror.RangeError):
            netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.TwoChopper, [0])
        
        # bg shift irrelevant: all negative values for '2cycle' test array
        netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.OneChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (7,32))
        self.assertTrue(np.all(np.less(netcdf['diffabs'], 0)))
        
        
        netcdf.load_data(_fp('wrong-identifier'))
        with self.assertRaises(ValueError):
            netcdf.calculate_signaldiff(pcnetcdf.ChopperMode.OneChopper, [0])


class TestNetCDFSingleFile (unittest.TestCase):
    
    def test_load_file (self):
        netcdf = pcnetcdf.NetCDFSingleFile()
        
        # four cycle mode files
        netcdf.load_data(_fp('bg-4cycle'))
        self.assertEqual(netcdf['data'].shape, (1,1,5,4,32))
        self.assertTrue(np.array_equal(netcdf['wlns'], np.arange(32, dtype='int32')))
        self.assertIsNone(netcdf['times'])
        self.assertEqual(netcdf['nshots'], 5)
        self.assertEqual(netcdf['nscans'], 1)
        self.assertEqual(netcdf['mode'], pcnetcdf.ChopperMode.TwoChopper)
        self.assertEqual(netcdf['data'].dims, ('scan_nr', 'delay_time', 'exp_step_nr', 'ccd_line', 'ccd_row'))
        
        # two cycle mode files
        netcdf.load_data(_fp('bg-2cycle'))
        self.assertEqual(netcdf['data'].shape, (1,1,5,2,32))
        self.assertEqual(netcdf['mode'], pcnetcdf.ChopperMode.OneChopper)
        
        # invalid shapes and identifiers
        self.assertRaises(pcerror.RangeError, netcdf.load_data, _fp('bg-3cycle'))
        self.assertRaises(pcerror.RangeError, netcdf.load_data, _fp('one-dimensional'))
        self.assertRaises(pcerror.InputError, netcdf.load_data, _fp('wrong-dimensions'))
        self.assertRaises(pcerror.InputError, netcdf.load_data, _fp('bg-wrong-identifier'))
        
        # do not load bg files, not even valid ones
        self.assertRaises(NotImplementedError, netcdf.load_bg, _fp('bg-4cycle'))

    def test_retrieve_information (self):
        
        info = pcnetcdf.retrieve_netcdf_information(_fp('bg-4cycle'))
        self.assertEqual(info['nscans'], 0)
        self.assertEqual(info['ntimes'], 1)
        self.assertEqual(info['nshots'], 5)
        self.assertEqual(info['nchopper'], 4)
        self.assertEqual(info['nwlns'], 32)
        self.assertTrue(np.array_equal(info['wlns'], np.arange(32)))
        self.assertTrue(np.array_equal(info['times'], np.asarray([])))
        
        info = pcnetcdf.retrieve_netcdf_information(_fp('bg-2cycle'))
        self.assertEqual(info['nscans'], 0)
        self.assertEqual(info['ntimes'], 1)
        self.assertEqual(info['nshots'], 5)
        self.assertEqual(info['nchopper'], 2)
        self.assertEqual(info['nwlns'], 32)
        self.assertTrue(np.array_equal(info['wlns'], np.arange(32)))
        self.assertTrue(np.array_equal(info['times'], np.asarray([])))
        
        info = pcnetcdf.retrieve_netcdf_information(_fp('wrong-dimensions'))
        self.assertFalse(info)
    
    def test_extract_data (self):
        netcdf = pcnetcdf.NetCDFSingleFile()
        
        
        netcdf.load_data(_fp('bg-4cycle'))
        # expected outcome, np.array_equal can test for value and shape
        result = np.full((1,32), 42 + 2**15)
        
        # scans: ignore
        data = netcdf.extract_data('probe', [0])
        self.assertTrue(np.array_equal(data, result))
        
        data = netcdf.extract_data('probe', [0,-1,9])
        self.assertTrue(np.array_equal(data, result))
        
        data = netcdf.extract_data('probe', [])
        self.assertTrue(np.array_equal(data, result))
        
        # proper chopper mode indexing
        data = netcdf.extract_data('pump', [0])
        self.assertFalse(np.array_equal(data, result))
        
        with self.assertRaises(pcerror.RangeError):
            netcdf.extract_data('foobar', [0])
        
        
        netcdf.load_data(_fp('bg-2cycle'))
        
        with self.assertRaises(pcerror.RangeError):
            netcdf.extract_data('bg', [0])
        
        # shift real-world data 2**15 upwards to make positive
        data_p = netcdf.extract_data('probe', [0])
        self.assertEqual(data_p.shape, (1,32))
        self.assertTrue(np.all(np.greater(data_p, 0)))
    
    def test_diffabs (self):
        netcdf = pcnetcdf.NetCDFSingleFile()
        
        
        netcdf.load_data(_fp('bg-4cycle'))
        
        # proper indexing: all positive values for 'bg-4cycle' array, negative otherwise
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (1,32))
        self.assertTrue(np.all(np.greater(netcdf['diffabs'], 0)))
        mode4 = netcdf['diffabs']
        
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.OneChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (1,32))
        self.assertTrue(np.all(np.greater(netcdf['diffabs'], 0)))
        mode2 = netcdf['diffabs']
        
        self.assertFalse(np.array_equal(mode4, mode2))
        
        # proper scan handling
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [0])
        a = netcdf['diffabs']
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [0,-1,9])
        b = netcdf['diffabs']
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [])
        c = netcdf['diffabs']
        self.assertTrue(np.array_equal(a, b))
        self.assertTrue(np.array_equal(a, c))
        
        # ref corr, bg corr: ignore
        netcdf.calculate_diffabs(False, True, pcnetcdf.ChopperMode.TwoChopper, [0])
        bg_off_ref_on = netcdf['diffabs']
        self.assertEqual(netcdf['diffabs'].shape, (1,32))
        
        netcdf.calculate_diffabs(True, True, pcnetcdf.ChopperMode.TwoChopper, [0])
        bg_on_ref_on = netcdf['diffabs']
        netcdf.calculate_diffabs(True, False, pcnetcdf.ChopperMode.TwoChopper, [0])
        bg_on_ref_off = netcdf['diffabs']
        
        self.assertTrue(np.array_equal(bg_off_ref_on, bg_on_ref_off))
        self.assertTrue(np.array_equal(bg_on_ref_on, bg_on_ref_off))
        
        
        netcdf.load_data(_fp('bg-2cycle'))
        
        with self.assertRaises(pcerror.RangeError):
            netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.TwoChopper, [0])
        
        netcdf.calculate_diffabs(False, False, pcnetcdf.ChopperMode.OneChopper, [0])
        self.assertEqual(netcdf['diffabs'].shape, (1,32))
        self.assertEqual(np.count_nonzero(netcdf['diffabs']), 32)
    
    def test_unused_methods (self):
        netcdf = pcnetcdf.NetCDFSingleFile()
        self.assertRaises(NotImplementedError, netcdf.correct_for_bg)
        self.assertRaises(NotImplementedError, netcdf.calculate_signaldiff, pcnetcdf.ChopperMode.TwoChopper, [])


class TestNetCDFMerge (unittest.TestCase):

    def test_merge (self):
        
        flist = [(_fp('4cycle'), [0,1,2]), (_fp('5scans'), [0,1,2,3,4])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.dims, ('scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'))
        self.assertEqual(ds.ccd_data.shape, (8,7,5,1,4,32))
        self.assertTrue(np.array_equal(ds.ccd_pixel_var, np.arange(32, dtype='int32')))
        self.assertTrue(np.array_equal(ds.scan_var, np.arange(7, dtype='int32')))
        a = ds.ccd_data[:,0,0,0,0,0]

        flist = [(_fp('4cycle'), [0,2,1]), (_fp('5scans'), [0,1,2,3,4])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.shape, (8,7,5,1,4,32))
        self.assertFalse(np.array_equal(a, ds.ccd_data[:,0,0,0,0,0]))
        self.assertTrue(np.array_equal(a[1], ds.ccd_data[2,0,0,0,0,0])) # 14.4
        
        flist = [(_fp('5scans'), [0,1,2,3,4]), (_fp('4cycle'), [0,1,2])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.shape, (8,7,5,1,4,32))
        self.assertFalse(np.array_equal(a, ds.ccd_data[:,0,0,0,0,0]))
        self.assertTrue(np.array_equal(a[2], ds.ccd_data[-1,0,0,0,0,0]))
        
        # proper scan list handling
        flist = [(_fp('4cycle'), [0,1,2]), (_fp('2cycle'), []), (_fp('5scans'), [0,1,2,3,4])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.shape, (8,7,5,1,4,32))
        b = ds.ccd_data[:,0,0,0,0,0]
        self.assertTrue(np.array_equal(a, b))
        
        flist = [(_fp('4cycle'), [0,2]), (_fp('5scans'), [4])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.shape, (3,7,5,1,4,32))
        self.assertTrue(np.array_equal(ds.ccd_pixel_var, np.arange(32, dtype='int32')))
        self.assertTrue(np.array_equal(ds.scan_var, np.arange(7, dtype='int32')))
        
        flist = [(_fp('4cycle'), [1])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.shape, (1,7,5,1,4,32))
        first_scan = ds.ccd_data[0,0,0,0,0,0]
        
        flist = [(_fp('4cycle'), [1,1])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.shape, (2,7,5,1,4,32))
        self.assertTrue(np.array_equal(ds.ccd_data[0,0,0,0,0,0], ds.ccd_data[1,0,0,0,0,0]))
        
        flist = [(_fp('4cycle'), [1]), (_fp('4cycle'), [1])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.shape, (2,7,5,1,4,32))
        self.assertTrue(np.array_equal(ds.ccd_data[0,0,0,0,0,0], first_scan))
        self.assertTrue(np.array_equal(ds.ccd_data[1,0,0,0,0,0], first_scan))
        
        ds = pcnetcdf.merge_netcdf_files([])
        self.assertIsNone(ds)
        
        # inconsistent wln and time axes
        flist = [(_fp('4cycle'), [0,1,2]), (_fp('5scans'), [1,3,4]), (_fp('altered-axes'), [0,1,2])]
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_WLNS_ERROR'):
            pcnetcdf.merge_netcdf_files(flist)
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_WLNS_ERROR'):
            pcnetcdf.merge_netcdf_files(flist, time_tol=0.55)
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_TIME_ERROR'):
            pcnetcdf.merge_netcdf_files(flist, wln_tol=0.3)
        
        ds = pcnetcdf.merge_netcdf_files(flist, wln_tol=0.3, time_tol=0.55)
        self.assertEqual(ds.ccd_data.shape, (9,7,5,1,4,32))
        
        # incompatible shot counts and chopper mode
        flist = [(_fp('4cycle'), [0,1,2]), (_fp('5scans'), [1,3,4]), (_fp('2cycle'), [0,1,2])]
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_MODE_ERROR'):
            pcnetcdf.merge_netcdf_files(flist)
        
        flist = [(_fp('4cycle'), [0,1,2]), (_fp('5scans'), [1,3,4]), (_fp('20shots'), [0,1,2])]
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_SHOTS_ERROR'):
            pcnetcdf.merge_netcdf_files(flist)
        
        # empty master still defines shape and axes
        flist = [(_fp('4cycle'), []), (_fp('5scans'), [1,3,4])]
        ds = pcnetcdf.merge_netcdf_files(flist)
        self.assertEqual(ds.ccd_data.shape, (3,7,5,1,4,32))
        self.assertTrue(np.array_equal(ds.ccd_pixel_var, np.arange(32, dtype='int32')))
        self.assertTrue(np.array_equal(ds.scan_var, np.arange(7, dtype='int32')))
        
        flist = [(_fp('4cycle'), []), (_fp('5scans'), [1,3,4]), (_fp('altered-axes'), [0,1,2])]
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_WLNS_ERROR'):
            pcnetcdf.merge_netcdf_files(flist)
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_WLNS_ERROR'):
            pcnetcdf.merge_netcdf_files(flist, time_tol=0.55)
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_TIME_ERROR'):
            pcnetcdf.merge_netcdf_files(flist, wln_tol=0.3)
        
        flist = [(_fp('4cycle'), []), (_fp('20shots'), [0,1,2])]
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_MODE_ERROR'):
            pcnetcdf.merge_netcdf_files(flist)
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INCOMPATIBLE_SHOTS_ERROR'):
            pcnetcdf.merge_netcdf_files(flist)
        
        # invalid file lists
        self.assertRaises(ValueError, pcnetcdf.merge_netcdf_files, [_fp('4cycle')])
        self.assertRaises(ValueError, pcnetcdf.merge_netcdf_files, [(_fp('4cycle'),)])
        self.assertRaises(ValueError, pcnetcdf.merge_netcdf_files, [(_fp('4cycle'),[0]), _fp('2cycle')])
        
        # invalid scans
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INVALID_SCAN_ERROR'):
            pcnetcdf.merge_netcdf_files([(_fp('4cycle'),[0,1,9]), (_fp('5scans'),[0,1,2,3,4])])
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INVALID_SCAN_ERROR'):
            pcnetcdf.merge_netcdf_files([(_fp('4cycle'),[0,1,2]), (_fp('5scans'),[0,1,9,3,4])])
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INVALID_SCAN_ERROR'):
            pcnetcdf.merge_netcdf_files([(_fp('4cycle'),[0,1,2]), (_fp('5scans'),[-1,1,2,3,4])])
        with self.assertRaises(pcerror.RangeError, msg='ERROR_IMPORT_MERGE_INVALID_SCAN_ERROR'):
            pcnetcdf.merge_netcdf_files([(_fp('4cycle'),[1,-5]), (_fp('5scans'),[0,1,2,3,4])])


if __name__ == '__main__':
    unittest.main()
