import unittest

import numpy as np

from src.pychart.core import sellmeier

class TestSellmeier (unittest.TestCase):
    
    def test_basic_functions (self):
        self.assertEqual(sellmeier.t_from_n(1,sellmeier.SPEED_OF_LIGHT * 1000), 1)
        self.assertAlmostEqual(sellmeier.t_from_n(1.33, 1.5), 6.6546037e-12)
        self.assertRaises(ZeroDivisionError, sellmeier.speed_of_light, 0)
    
    def test_timeshift_calculation (self):
        cls = sellmeier.SellmeierCoefficients()
        
        # test load function
        cls.load_coeffs_from_files('test-data/misc/')
        self.assertIn('Dilithium', cls.materials)
        self.assertEqual(len(cls.materials), 5)
        
        # test loaded values (for sodium D-line at 589 nm)
        self.assertAlmostEqual(float(cls.sellmeier('Dilithium', np.array([589]))), 2.830648639)
        self.assertAlmostEqual(float(cls.cauchy('Dilithium', np.array([589]))), 2.836316775)
        self.assertAlmostEqual(cls.permittivity('Dilithium'), 2.8301943)
        self.assertEqual(cls.refractive_index('Dilithium'), 2.83)
        
        # test search function
        wlns = np.array([266,355,532])
        layers = [('Unbinilium', 2), ('Unbiunium', 0.8), ('Dilithium', 0.5), ('Unbibium', 1.2)]
        timezeros = cls.calculate_time_shifts(wlns,layers)
        self.assertAlmostEqual(float(np.sum(timezeros)), 91.17883127, places=5)
        
        # ignore unknown materials (-> dt = 0)
        self.assertEqual(np.sum(cls.calculate_time_shifts(wlns,[('Unobtainium', 0)])), 0)
    
    def test_sellmeierclass_integration (self):
        cls = sellmeier.SellmeierCoefficients()
        
        # test for error flags
        errors = cls.load_coeffs_from_files('')
        self.assertIn(('load', 'sellmeier-coefficients.json'), errors)
        self.assertFalse(cls.materials)
        
        # test loading of production data
        errors = cls.load_coeffs_from_files('../data/')
        self.assertFalse(errors)
        
        # test production data for consistent values
        wlns = np.arange(250,800,0.8)
        materials = cls.materials
        layers=zip(materials, range(len(materials)))
        timezeros = cls.calculate_time_shifts(wlns,layers)
        
        # -> one time for each wavelength
        self.assertTrue(wlns.shape == timezeros.shape)
        # -> increasing values with increasing λ
        self.assertGreater(timezeros[0], timezeros[1])
        self.assertGreater(timezeros[0], timezeros[344])

if __name__ == '__main__':
    unittest.main()