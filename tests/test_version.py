import unittest

from src.pychart.version import version_is_newer, version_is_newer_eq, version_from_string
from src.pychart.version import PYCHART_VERSION, PYCHART_VERSION_NAME, PYCHART_HISTORY

class TestPychartVersion (unittest.TestCase):
    
    def test_comparison (self):
        self.assertTrue(version_is_newer((1,), (2,)))
        self.assertTrue(version_is_newer((1,2), (2,)))
        self.assertTrue(version_is_newer((1,2), (1,3)))
        self.assertTrue(version_is_newer((1,2), (1,2,9)))
        self.assertTrue(version_is_newer((1,2,3), (1,2,9)))
        
        self.assertFalse(version_is_newer((3,), (2,)))
        self.assertFalse(version_is_newer((1,2), (1,)))
        self.assertFalse(version_is_newer((1,2), (0,9)))
        self.assertFalse(version_is_newer((1,2), (1,1,3)))
        self.assertFalse(version_is_newer((1,3,3), (1,2,9)))
        self.assertFalse(version_is_newer((1,2), (1,2)))
        
        self.assertTrue(version_is_newer_eq((1,), (2,)))
        self.assertTrue(version_is_newer_eq((1,2), (2,)))
        self.assertTrue(version_is_newer_eq((1,2), (1,3)))
        self.assertTrue(version_is_newer_eq((1,2), (1,2,9)))
        self.assertTrue(version_is_newer_eq((1,2,3), (1,2,9)))
        self.assertTrue(version_is_newer_eq((1,), (1,)))
        self.assertTrue(version_is_newer_eq((1,2), (1,2)))
        self.assertTrue(version_is_newer_eq((1,2,9), (1,2,9)))
        
        self.assertFalse(version_is_newer_eq((3,), (2,)))
        self.assertFalse(version_is_newer_eq((1,2), (1,)))
        self.assertFalse(version_is_newer_eq((1,2), (0,9)))
        self.assertFalse(version_is_newer_eq((1,2), (1,1,3)))
        self.assertFalse(version_is_newer_eq((1,3,1), (1,2,9)))
    
    def test_version_strings (self):
        self.assertEqual(version_from_string('v1.0'), (1,0))
        self.assertEqual(version_from_string('v1.3'), (1,3))
        self.assertEqual(version_from_string('v1.3.4'), (1,3,4))
        self.assertEqual(version_from_string('v1'), (1,))
        
        self.assertEqual(version_from_string('1.3'), (0,0))
        self.assertEqual(version_from_string('1..0'), (0,0))
        self.assertEqual(version_from_string('2'), (0,0))
        self.assertEqual(version_from_string('v1.0b'), (0,0))
    
    def test_history_and_current (self):
        self.assertNotEqual(PYCHART_HISTORY, [])
        for i in range(len(PYCHART_HISTORY) - 1):
            self.assertTrue(version_is_newer(PYCHART_HISTORY[i+1], PYCHART_HISTORY[i]))
        
        self.assertTrue(version_is_newer(PYCHART_HISTORY[0]))
        
        self.assertFalse(version_is_newer(PYCHART_VERSION))
        self.assertTrue(version_is_newer_eq(PYCHART_VERSION))
        
        self.assertEqual(version_from_string(PYCHART_VERSION_NAME), PYCHART_VERSION)
    