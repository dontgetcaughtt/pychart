from enum import Enum

import numpy as np
import xarray

from . import pychart as pc
from . import error as pcerror

#-----------------------------------------------------------------
#   CHOPPER MODE ENUM
#-----------------------------------------------------------------

class ChopperMode (Enum):
    """
    An Enum defining different chopper modes.
    
    Members
    -------
    OneChopper : 2
        one chopper used to gain a) pump + probe and b) probe
    
    TwoChopper : 4
        two choppers used to gain a) pump + probe, b) pump,
        c) probe and d) nothing
    """
    
    OneChopper, TwoChopper = 2, 4
    
    @classmethod
    def lut_OneChopper (cls):
        """ Return lookup table to assign a chopper position (as string) to the corresponding array dimension. """
        return {'pumpprobe': 0, 'probe': 1}
    
    @classmethod
    def lut_TwoChopper (cls):
        """ Return lookup table to assign a chopper position (as string) to the corresponding array dimension. """
        return {'pumpprobe': 0, 'pump': 1, 'probe': 2, 'bg': 3}
    
    def lut (self):
        """ Return the correct lookup table for instance's chopper mode. """
        return self.lut_TwoChopper() if self.value == 4 else self.lut_OneChopper()

#-----------------------------------------------------------------
#   NETCDF FILE CLASS
#-----------------------------------------------------------------

class NetCDFFile (dict):
    """
    A dictionary containing all information regarding an fsTA NetCDF file.
    Extended by functions to calculate the difference absorption.
    
    Attributes
    ----------
    ['data'] : xarray.DataArray
        raw data
    
    ['bgdata'] : xarray.DataArray
        raw background data

    ['wlns'] : numpy.ndarray
        wavelength axis
    
    ['times'] : numpy.ndarray
        time axis
    
    ['diffabs'] : xarray.DataArray
        calculated difference absorption
    
    ['mode'] : ChopperMode
        chopper mode of acquisition
    
    ['nscans'] : int
        number of delay line scans
    
    ['nshots'] : int
        number of single shots per delay time position
    """
    
    def __init__ (self):
        
        super().__init__()
        self['data']     = None
        self['bgdata']   = None
        self['wlns']     = None
        self['times']    = None
        self['diffabs']  = None
        self['mode']     = None
        self['nscans']   = 0
        self['nshots']   = 0
    
    def load_data (self, filepath):
        """
        Load data file.
        
        Loads the data from file and reduces not necessary dimensions. Time 
        and wavelength axis are extracted as well as the choppermode and 
        number of scans.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Raises
        ------
        pcerror.InputError
            if the file can not be opened or read
            if not enough memory could be allocated
        """
        try:
            dataset = xarray.open_dataset(filepath)
            self['data'] = dataset['ccd_data'][:,:,:,0,:,:]
            self['wlns'] = dataset.ccd_pixel_var.values
            self['times'] = dataset.scan_var.values
        except MemoryError:
            raise pcerror.InputError('ERROR_IMPORT_MEMORY_ERROR')
        except:
            raise pcerror.InputError('ERROR_IMPORT_IMPORT_DATA_ERROR', input=filepath)
        else:
            if len(self['data'][0,0,0,:,0]) == 4:
                self['mode'] = ChopperMode.TwoChopper
            elif len(self['data'][0,0,0,:,0]) == 2:
                self['mode'] = ChopperMode.OneChopper
            else:
                raise pcerror.RangeError('ERROR_IMPORT_INVALID_MODE_ERROR', input=len(self['data'][0,0,0,:,0]), ranges=[2,4])
            self['nscans'] = len(self['data'][:,0,0,0,0])
            self['nshots'] = len(self['data'][0,0,:,0,0])
    
    def load_bg (self, filepath):
        """
        Load background file.
        
        Loads the background data from file and reduces not necessary dimensions.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Raises
        ------
        pcerror.InputError
            if the file can not be opened or read
            if not enough memory could be allocated
        """
        try:
            self['bgdata'] = xarray.open_dataset(filepath).ccd_data[:,0,:,:].mean(dim='ccd_line').mean(dim='ccd_mess')
        except MemoryError:
            raise pcerror.InputError('ERROR_IMPORT_MEMORY_ERROR')
        except:
            raise pcerror.InputError('ERROR_IMPORT_IMPORT_DATA_ERROR', input=filepath)
    
    def correct_for_bg (self):
        """
        Correct data for background.
        
        Returns
        -------
        xarray.DataArray
            background corrected data
        """
        left_bg, right_bg = self['bgdata'][1:6].mean(), self['bgdata'][-5:].mean()
        left = self['data'][:,:,:,:,1:6].mean(dim='ccd_row')
        right = self['data'][:,:,:,:,-5:].mean(dim='ccd_row')
        delta_bg = xarray.DataArray(np.empty(self['data'].shape), dims=self['data'].dims)
        # The following seems to be a correction for a sloped background.
        # However, I don't really get it. It's simply taken from the original IDL routine, which is not documented.
        for px in range(len(self['bgdata'])):
            delta_bg[:,:,:,:,px] = (left - left_bg) + (px * (right - left + left_bg - right_bg) / (len(self['bgdata'])-1) )
        return self['data'] - delta_bg - self['bgdata']
    
    def extract_data (self, mode, scans):
        """
        Extract part (pumpprobe, pump, probe or background) of data set.
        
        Extracts the desired `mode` (one chopper position) of data and averages
        of single measurements and scans according to `scans`.
        
        Parameters
        ----------
        mode : ['pumpprobe', 'pump', 'probe', 'bg']
            which part of the data to extract
        
        scans :  list of int
            list of scans to average
        
        Returns
        -------
        xarray.DataArray
            averaged part of data
        
        Raises
        ------
        pcerror.RangeError
            if an invalid mode (part of data) was asked for
        """
        try:
            x = self['mode'].lut()[mode]
        except KeyError:
            raise pcerror.RangeError('ERROR_IMPORT_UNKNOWN_MODE_ERROR', input=mode, ranges="['pump', 'pumpprobe', 'probe', 'bg']")
        reduced_data = self['data'][:,:,:,x,:].mean(dim='exp_step_nr', skipna=True)
        
        for scan in range(self['nscans']):
            if scan not in scans:
                reduced_data[scan,:,:] = np.nan
        return reduced_data.mean(dim='scan_nr', skipna=True) + 2**15
    
    @pc.suppress_warnings
    def calculate_diffabs (self, bgcorr, refcorr, choppermode, scans):
        """
        Calculate difference absorption.
        
        Calculates the difference absorption of the data set and applies 
        corrections according to user settings. The difference absorption 
        is calculated based on the mode defined by `choppermode` regardless
        of the actual mode of acquisition. Finally, the data is averaged 
        over single measurements and `scans`.
        
        Parameters
        ----------
        bgcorr : bool
            whether or not to apply a background correction
        
        refcorr : bool
            whether or not to apply a reference point correction
        
        choppermode : ChopperMode
            which choppermode to use
        
        scans : list of int
            list of scans to average
        
        Raises
        ------
        pcerror.RangeError
            if a correction based on ChopperMode.TwoChopper was asked for at a ChopperMode.OneChopper measurement
        """
        if choppermode is ChopperMode.TwoChopper:
            if self['mode'] is not ChopperMode.TwoChopper:
                raise pcerror.RangeError('ERROR_IMPORT_MODE_UNAVAILABLE_ERROR')
            data = self['data']
            ref = -np.log10( (data[:,0,:,0,:] - data[:,0,:,1,:]) / (data[:,0,:,2,:] - data[:,0,:,3,:]) ) if refcorr else 0
            diffabs = -np.log10( (data[:,:,:,0,:] - data[:,:,:,1,:]) / (data[:,:,:,2,:] - data[:,:,:,3,:]) ) - ref
        else:
            data = self.correct_for_bg() if bgcorr else self['data'] + 2**15
            probe = self['mode'].lut()['probe']
            ref = -np.log10( (data[:,0,:,0,:]) / (data[:,0,:,probe,:]) ) if refcorr else 0
            diffabs = -np.log10((data[:,:,:,0,:]) / (data[:,:,:,probe,:])) - ref
        reduced_diffabs = diffabs.mean(dim='exp_step_nr', skipna=True)
        
        for scan in range(self['nscans']):
            if scan not in scans:
                reduced_diffabs[scan,:,:] = np.nan
        self['diffabs'] = reduced_diffabs.mean(dim='scan_nr', skipna=True)
    
    def calculate_signaldiff (self, choppermode, scans):
        """
        Calculate signal difference.
        
        Calculates the difference of signal of the data set. The
        calculation based on the mode defined by `choppermode`
        regardless of the actual mode of acquisition. Finally,
        the data is averaged over single measurements and `scans`.
        
        Parameters
        ----------
        choppermode : ChopperMode
            which choppermode to use
        
        scans : list of int
            list of scans to average
        
        Raises
        ------
        pcerror.RangeError
            if a correction based on ChopperMode.TwoChopper was asked for at a ChopperMode.OneChopper measurement
        """
        if choppermode is ChopperMode.TwoChopper:
            if self['mode'] is not ChopperMode.TwoChopper:
                raise pcerror.RangeError('ERROR_IMPORT_MODE_UNAVAILABLE_ERROR')
            sigdiff = (self['data'][:,:,:,0,:] - self['data'][:,:,:,1,:]) - (self['data'][:,:,:,2,:] - self['data'][:,:,:,3,:])
        else:
            probe = self['mode'].lut()['probe']
            sigdiff = self['data'][:,:,:,0,:] - self['data'][:,:,:,probe,:]
        reduced_sigdiff = sigdiff.mean(dim='exp_step_nr', skipna=True)
        
        for scan in range(self['nscans']):
            if scan not in scans:
                reduced_sigdiff[scan,:,:] = np.nan
        self['diffabs'] = reduced_sigdiff.mean(dim='scan_nr', skipna=True)

#-----------------------------------------------------------------
#   MAJOR IMPORT CLASS
#-----------------------------------------------------------------

class NetCDFImport (dict):
    """
    A dict holding all information regarding an fsTA measurement including
    solvent, gate and background data.
    
    Attributes
    ----------
    ['maindata'] : NetCDFFile
        main sample data
    
    ['solvdata'] : NetCDFFile
        solvent data
    
    ['gatedata'] : NetCDFFile
        gate measurement data
    
    ['timezeros'] : numpy.ndarray
        time zero points
    
    observer : callable
        observer function which is called to update current plotting progress
    """
    
    def __init__ (self):
        
        super().__init__()
        self['maindata']  = NetCDFFile()
        self['solvdata']  = NetCDFFile()
        self['gatedata']  = NetCDFFile()
        self['timezeros'] = None
        self.observer     = None
    
    def update_progress (self, progress, message):
        """ Call observer function to inform about current progress. """
        if self.observer is not None:
            self.observer(progress, message)
    
    def load_main_data (self, filepath):
        """
        Load main measurement from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Returns
        -------
        (ChopperMode, int)
            choppermode and available number of scans of the loaded file
        """
        self['maindata'].load_data(filepath)
        return self['maindata']['mode'], self['maindata']['nscans']
    
    def load_solv_data (self, filepath):
        """
        Load solvent measurement from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Returns
        -------
        (ChopperMode, int)
            choppermode and available number of scans of the loaded file
        """
        self['solvdata'].load_data(filepath)
        return self['solvdata']['mode'], self['solvdata']['nscans']

    def load_gate_data (self, filepath):
        """
        Load gate measurement from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Returns
        -------
        (ChopperMode, int)
            choppermode and available number of scans of the loaded file
        """
        self['gatedata'].load_data(filepath)
        return self['gatedata']['mode'], self['gatedata']['nscans']
    
    def load_bg_data (self, filepath):
        """
        Load background for main data from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        """
        self['maindata'].load_bg(filepath)
    
    def load_solv_bg_data (self, filepath):
        """
        Load background for solvent data from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        """
        self['solvdata'].load_bg(filepath)
    
    def load_gate_bg_data (self, filepath):
        """
        Load background for gate data from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        """
        self['gatedata'].load_bg(filepath)
    
    def check_solv_dimensions (self, wln_tol, time_tol):
        """
        Check wavelength and time axes of main and solvent data for consistency.
        
        Wavelength axes need to be fully identical within a tolerance range of
        `wln_tol` wavelength units.
        Time axes values need to be identical with a tolerance of `time_tol`. The time
        axis of the solvent data might be only a subset of the main data time axis.
        
        Parameters
        ----------
        wln_tol : float
            absolute tolerance of wavelength axes values
        
        time_tol : float
            relative tolerance of time axes values
        
        Returns
        -------
        bool
            True, if dimensions are compatible
        
        Raises
        -------
        pcerror.RangeError
            if wavelength axes of main and solvent data do not match
            if the solvent's time axis can not be broadcast to the main data time axis
        """
        if not pc.consistent_axes(self['maindata']['wlns'], self['solvdata']['wlns'], atol=wln_tol, rtol=0):
            raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_WLNS_NETCDF_ERROR')
        
        d = len(self['maindata']['times']) - len(self['solvdata']['times'])
        if d == 0 and pc.consistent_axes(self['maindata']['times'], self['solvdata']['times'], atol=0, rtol=time_tol):
            return True
        elif d > 0 and pc.consistent_axes(self['maindata']['times'][:-d], self['solvdata']['times'], atol=0, rtol=time_tol):
            return True
        else:
            raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_TIMES_NETCDF_ERROR')
    
    def check_gate_dimensions (self, wln_tol):
        """
        Check wavelength axes of main and gate data for consistency.
        
        Wavelength axes need to be fully identical within a tolerance range of `wln_tol` wavelength units.
        
        Parameters
        ----------
        wln_tol : float
            absolute tolerance of wavelength axes values
        
        Returns
        -------
        bool
            True, if dimensions are compatible
        
        Raises
        -------
        pcerror.RangeError
            if wavelength axes of main and solvent data do not match
        """
        if not pc.consistent_axes(self['maindata']['wlns'], self['gatedata']['wlns'], atol=wln_tol):
            raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_WLNS_NETCDF_ERROR')
        else:
            return True
    
    def check_bg_dimensions (self, data_component):
        """
        Check the wavelength axes of `data_component` and its background file for consistency.
        
        Parameters
        ----------
        data_component : ['maindata', 'solvdata', 'gatedata']
            data component, which bg should be checked for compatibility
        
        Returns
        -------
        bool
            True, if dimensions are compatible
            False, if unregocnized `data_component` parameter is given
        
        Raises
        ------
        pcerror.RangeError
            if wavelength axes `data_component` and its bg file do not match in shape
        """
        if data_component not in ['maindata', 'solvdata', 'gatedata']:
            return False
        
        if self[data_component]['bgdata'] is None:
            raise pcerror.InputError('ERROR_IMPORT_MISSING_BG_ERROR', input=data_component)
        
        if self[data_component]['bgdata'].shape != self[data_component]['wlns'].shape:
            raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_BG_NETCDF_ERROR', input=data_component)
        else:
            return True
    
    def extract_gate_peaks (self, gatedict):
        """
        Extract peaks from gate measurement.
        
        Parameters
        ----------
        gatedict : dict
            dictionary containing information on the main data correction
            ('bgcorr': bool, 'mode': ChopperMode, 'scans': list of int, 'time_range': tuple of float,
            'method': ['gauss' or 'maxima'], 'type': ['sigdiff' or 'diffabs'],
            'settings': dict (cf. pychart.determine_time_zeros()))
        
        Returns
        -------
        (numpy.ndarray, numpy.ndarray)
            wavelengths axis values and time zeros
        
        Raises
        ------
        pcerror.InputError
            if not enough memory could be allocated
        """
        try:
            if gatedict['type'] == 'sigdiff':
                self['gatedata'].calculate_signaldiff(gatedict['mode'], gatedict['scans'])
            else:
                if gatedict['bgcorr']:
                    self.check_bg_dimensions('gatedata')
                self['gatedata'].calculate_diffabs(gatedict['bgcorr'], False, gatedict['mode'], gatedict['scans'])
            
            t_start_i = pc.lookup_index(gatedict['time_range'][0], self['gatedata']['times'])
            t_stop_i = pc.lookup_index(gatedict['time_range'][1], self['gatedata']['times'])
            
            return self['gatedata']['wlns'], pc.determine_time_zeros(self['gatedata']['diffabs'][t_start_i:t_stop_i,:].values,
                                                self['gatedata']['times'][t_start_i:t_stop_i], self['gatedata']['wlns'],
                                                gatedict['method'], gatedict['processing'], gatedict['settings'])
        except MemoryError:
            raise pcerror.InputError('ERROR_IMPORT_MEMORY_ERROR')
        except pcerror.RangeError:
            raise
    
    def extract_main_data (self, maindict):
        """
        Extract desired part (pumpprobe, pump, probe or background) of main data.
        
        Parameters
        ----------
        maindict : dict
            dictionary containing information on the main data correction
            ('mode': ChopperMode, 'scans': list of int)
        
        Returns
        -------
        (numpy.ndarray, numpy.ndarray, numpy.ndarray)
            corrected part of main data, its wavelength and time axis values
        
        Raises
        ------
        pcerror.InputError
            if not enough memory could be allocated
        
        pcerror.InternalError
            if something unexpected happens
        """
        try:
            corrdata = self['maindata'].extract_data(maindict['mode'], maindict['scans'])
            return corrdata.values, self['maindata']['wlns'], self['maindata']['times']
        except (pcerror.InputError, pcerror.RangeError):
            raise
        except MemoryError:
            raise pcerror.InputError('ERROR_IMPORT_MEMORY_ERROR')
        except:
            raise pcerror.InternalError('ERROR_NCDF_INTERNAL_ERROR_1')
    
    def correct_main_data (self, maindict, observer_func=None, **kwargs):
        """
        Correct main data according to user settings.
        
        Parameters
        ----------
        maindict : dict
            dictionary containing information on the main data correction
            ('bgcorr': bool, 'refcorr': bool, 'mode': ChopperMode, 'scans': list of int)
        
        observer_func : callable
            function to call if progress has changed
        
        **kwargs
            keyword arguments, described as follows:
            
            solvdict : dict or None
                dictionary containing information on the solvent data correction
                ('bgcorr': bool, 'refcorr': bool, 'mode': ChopperMode, 'scans': list of int,
                 'absorption': float, 'shift': float,
                 'method': ['factor' or 'absorption' or 'transmission' or 'free'])
            
            gatedict : dict or None
                dictionary containing information on the gate data correction
                ('bgcorr': bool, 'mode': ChopperMode, 'scans': list of int, 'time_range': tuple of float,
                 'method': ['gauss' or 'maxima'], 'type': ['sigdiff' or 'diffabs'],
                 'processing': ['off' or 'smooth' or 'fit'], 'settings': dict (cf. pychart.determine_time_zeros()),
                 'shift': float)
            
            sellmeierdict : dict or None
                dictionary containing information on the time zero correction
                ('list': list of tuple of str (material) and float (thickness), 'offset': float,
                 'handle': pcsellmeier.SellmeierCoefficients)
            
            wln_tolerance : float or None
                absolute tolerance of wavelength axes values
            
            time_tolerance : float or None
                relative tolerance of time axes values
        
        Returns
        -------
        (numpy.ndarray, numpy.ndarray, numpy.ndarray)
            corrected main data values, their wavelength and time axis values
        
        Raises
        ------
        pcerror.InputError
            if not enough memory could be allocated
        
        pcerror.InternalError
            if something unexpected happens
        """
        self.observer = observer_func
        solvdict = kwargs.get('solvdict', None)
        gatedict = kwargs.get('gatedict', None)
        sellmeierdict = kwargs.get('sellmeierdict', None)
        wln_tol = kwargs.get('wln_tolerance', None)
        time_tol = kwargs.get('time_tolerance', None)
        try:
            
            # pre-flight checks
            if maindict['bgcorr']:
                self.check_bg_dimensions('maindata')
            if solvdict is not None:
                self.check_solv_dimensions(wln_tol, time_tol)
                if solvdict['bgcorr']:
                    self.check_bg_dimensions('solvdata')
            if gatedict is not None:
                self.check_gate_dimensions(wln_tol)
                if gatedict['bgcorr']:
                    self.check_bg_dimensions('gatedata')
            
            # main data calculation
            self.update_progress(0, 'STATUS_IMPORT_CALC_MAIN')
            self['maindata'].calculate_diffabs(maindict['bgcorr'], maindict['refcorr'], maindict['mode'], maindict['scans'])
            
            # solvent correction
            if solvdict is not None:
                self.update_progress(25, 'STATUS_IMPORT_CALC_SOLV')
                solvfactor = pc.calculate_solvent_factor(solvdict['absorption'], solvdict['method'])
                self['solvdata'].calculate_diffabs(solvdict['bgcorr'], solvdict['refcorr'], solvdict['mode'], solvdict['scans'])
                if len(self['maindata']['diffabs']) > len(self['solvdata']['diffabs']):
                    solvent = np.zeros_like(self['maindata']['diffabs'])
                    solvent[:len(self['solvdata']['diffabs'])] = self['solvdata']['diffabs']
                else:
                    solvent = self['solvdata']['diffabs']
            else:
                solvent = self['solvdata']['diffabs'] = 0
                solvfactor = 0
            
            # time zero correction
            if (gatedict is not None) or (sellmeierdict is not None):
                self.update_progress(50, 'STATUS_IMPORT_PREPARE_TIMEZERO')
                
                # prepare
                shape = (self['maindata']['times'].size, self['maindata']['wlns'].size)
                shifted_data        = np.empty(shape)
                shifted_time_axes   = np.empty(shape)
                if solvdict is not None:
                    shifted_solvent = np.empty(shape)
                else:
                    shifted_solvent = 0
                
                # calculate time zeros
                if gatedict is not None:
                    self.update_progress(60, 'STATUS_IMPORT_GATE_CORR')
                    if gatedict['type'] == 'sigdiff':
                        self['gatedata'].calculate_signaldiff(gatedict['mode'], gatedict['scans'])
                    else:
                        self['gatedata'].calculate_diffabs(gatedict['bgcorr'], False, gatedict['mode'], gatedict['scans'])
                    t_start_i = pc.lookup_index(gatedict['time_range'][0], self['gatedata']['times'])
                    t_stop_i = pc.lookup_index(gatedict['time_range'][1], self['gatedata']['times'])
                    self['timezeros'] = pc.determine_time_zeros(self['gatedata']['diffabs'][t_start_i:t_stop_i,:].values,
                                            self['gatedata']['times'][t_start_i:t_stop_i], self['gatedata']['wlns'],
                                            gatedict['method'], gatedict['processing'], gatedict['settings'])
                    if gatedict['shift']:
                        self['timezeros'] += gatedict['shift']
                else:
                    self.update_progress(60, 'STATUS_IMPORT_SELLMEIER_CORR')
                    self['timezeros'] = sellmeierdict['handle'].calculate_time_shifts(self['maindata']['wlns'], sellmeierdict['list'])
                    self['timezeros'] = - (self['timezeros'] - np.min(self['timezeros']) - sellmeierdict['offset'])
                
                # shift time axis and interpolate data
                for wln in range(self['maindata']['wlns'].size):
                    shifted_time_axes[:,wln] = self['maindata']['times'] - self['timezeros'][wln]
                    shifted_data[:,wln] = np.interp(self['maindata']['times'], shifted_time_axes[:,wln], self['maindata']['diffabs'][:,wln])
                    if solvdict is not None:
                        shifted_solvent[:,wln] = np.interp(self['maindata']['times'], shifted_time_axes[:,wln] + solvdict['shift'], solvent[:,wln])
                
                self.update_progress(85, 'STATUS_IMPORT_SUBSTRACT_SOLV')
                corrdata = shifted_data - shifted_solvent * solvfactor
            
            else:
                
                if solvdict is not None and solvdict['shift']:
                    solvent = np.zeros((self['solvdata']['times'].size, self['solvdata']['wlns'].size))
                    for wln in range(self['maindata']['wlns'].size):
                        solvent[:,wln] = np.interp(self['maindata']['times'], self['maindata']['times'] + solvdict['shift'], solvent[:,wln])
                
                self.update_progress(85, 'STATUS_IMPORT_SUBSTRACT_SOLV')
                corrdata = (self['maindata']['diffabs'] - solvent * solvfactor).values
            
            self.update_progress(100, '')
            return corrdata, self['maindata']['wlns'], self['maindata']['times']
        
        except (pcerror.InputError, pcerror.RangeError):
            raise
        
        except MemoryError:
            raise pcerror.InputError('ERROR_IMPORT_MEMORY_ERROR')
        
        except:
            raise pcerror.InternalError('ERROR_NCDF_INTERNAL_ERROR_2')

#-----------------------------------------------------------------
#   NETCDF SINGLE FILE CLASS
#-----------------------------------------------------------------

class NetCDFSingleFile (NetCDFFile):
    """
    Inherits `NetCDFFile`.
    
    A dictionary containing all information regarding an fsTA NetCDF
    single delay time file. Extended by functions to calculate the
    difference absorption.
    
    Attributes
    ----------
    ['data'] : xarray.DataArray
        raw data
    
    ['wlns'] : numpy.ndarray
        wavelength axis
    
    ['diffabs'] : xarray.DataArray
        calculated difference absorption
    
    ['mode'] : ChopperMode
        chopper mode of acquisition
    
    ['nshots'] : int
        number of single shots per delay time position
    """
    
    def __init__ (self):
        super().__init__()
        self['nscans'] = 1
    
    def load_data(self, filepath):
        """
        Load a single spectrum file in background format.
        
        Loads a single spectrum from background file. The wavelength axis is
        extracted as well as the choppermode. Dimensions are filled to
        match the shape of a normal NetCDFFile. Thus all corrections and
        calculations can be performed as usual.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Raises
        ------
        pcerror.InputError
            if the file can not be opened or read
            if not enough memory could be allocated
        """
        try:
            dataset = xarray.open_dataset(filepath)
            
            self['wlns'] = dataset.ccd_pixel_var.values
            if len(dataset.ccd_data[0,0,:,0]) == 4:
                self['mode'] = ChopperMode.TwoChopper
            elif len(dataset.ccd_data[0,0,:,0]) == 2:
                self['mode'] = ChopperMode.OneChopper
            else:
                raise pcerror.RangeError('ERROR_IMPORT_INVALID_MODE_ERROR', input=len(dataset.ccd_data[0,0,:,0]), ranges=[2,4])
            self['nshots'] = len(dataset.ccd_data[:,0,0,0])
            
            self['data'] = dataset.ccd_data
            self['data'] = self['data'].expand_dims(dim={'delay_time': 1}, axis=0)
            self['data'] = self['data'].expand_dims(dim={'scan_nr': 1}, axis=0)
            self['data'] = self['data'].rename({'ccd_mess':'exp_step_nr'})
            self['data'] = self['data'][:,:,:,0,:,:]
        
        except MemoryError:
            raise pcerror.InputError('ERROR_IMPORT_MEMORY_ERROR')
        except pcerror.RangeError:
            raise
        except:
            raise pcerror.InputError('ERROR_IMPORT_IMPORT_DATA_ERROR', input=filepath)
    
    def load_bg (self, filepath):
        raise NotImplementedError
    
    def correct_for_bg (self):
        raise NotImplementedError
    
    def extract_data (self, mode, scans):
        return super(NetCDFSingleFile, self).extract_data(mode, [0])
    
    def calculate_diffabs (self, bgcorr, refcorr, choppermode, scans):
        super(NetCDFSingleFile, self).calculate_diffabs(False, False, choppermode, [0])
    
    def calculate_signaldiff (self, choppermode, scans):
        raise NotImplementedError

#-----------------------------------------------------------------
#   NETCDF SINGLE IMPORT CLASS
#-----------------------------------------------------------------

class NetCDFSingleImport (NetCDFImport):
    """
    Inherits `NetCDFImport`.
    
    A dict holding all information regarding an fsTA single delay time measurement.
    This includes only main and solvent data. Background as well as gate data are
    not considered.
    
    Attributes
    ----------
    ['maindata'] : NetCDFSingleFile
        main sample data
    
    ['solvdata'] : NetCDFSingleFile
        solvent data
    
    observer : callable
        observer function which is called to update current plotting progress
    """
    
    def __init__ (self):
        super().__init__()
        self['maindata'] = NetCDFSingleFile()
        self['solvdata'] = NetCDFSingleFile()
        self['gatedata'] = None
    
    def load_gate_data(self, filepath):
        raise NotImplementedError
    
    def load_bg_data(self, filepath):
        raise NotImplementedError
    
    def load_solv_bg_data(self, filepath):
        raise NotImplementedError
    
    def load_gate_bg_data(self, filepath):
        raise NotImplementedError
    
    def check_solv_dimensions(self, wln_tol, time_tol):
        raise NotImplementedError
    
    def check_gate_dimensions(self, wln_tol):
        raise NotImplementedError
    
    def extract_gate_peaks(self, gatedict):
        raise NotImplementedError
    
    def extract_main_data (self, maindict):
        """
        Extract desired part (pumpprobe, pump, probe or background) of main data.
        
        Parameters
        ----------
        maindict : dict
            dictionary containing information on the main data correction ('mode': ChopperMode)
        
        Returns
        -------
        (numpy.ndarray, numpy.ndarray)
            corrected part of main data and its wavelength axis values
        
        Raises
        ------
        pcerror.InputError
            if not enough memory could be allocated

        pcerror.InternalError
            if something unexpected happens
        """
        maindict.setdefault('scans', [0])
        data, wlns, times = super(NetCDFSingleImport, self).extract_main_data(maindict)
        return data[0,:], wlns
    
    def correct_main_data(self, maindict, observer_func=None, **kwargs):
        """
        Correct main data according to user settings.
        
        Parameters
        ----------
        maindict : dict
            dictionary containing information on the main data correction
            ('mode': ChopperMode)
        
        observer_func : callable
            function to call if progress has changed; currently dismissed
        
        **kwargs
            keyword arguments, described as follows:
            
            refcorr : bool or None
                whether the solvent data should be interpreted as a reference point correction;
                if true, keyword argument `solvdict` needs to be given with key `'mode'` while
                other keys are dismissed
                
            solvdict : dict or None
                dictionary containing information on the solvent data correction
                ('mode': ChopperMode, 'absorption': float,
                 'method': ['factor' or 'absorption' or 'transmission' or 'free'])
            
            wln_tolerance : float or None
                absolute tolerance of wavelength axes values
        
        Returns
        -------
        (numpy.ndarray, numpy.ndarray)
            corrected main data values and their wavelength axis values
        
        Raises
        ------
        pcerror.InputError
            if not enough memory could be allocated
        
        pcerror.InternalError
            if something unexpected happens
        
        pcerror.RangeError
            if wavelength axes of main and solvent data do not match
        """
        self.observer = observer_func
        refcorr = kwargs.get('refcorr', None)
        solvdict = kwargs.get('solvdict', None)
        wln_tol = kwargs.get('wln_tolerance', None)
        
        try:
            if solvdict and not pc.consistent_axes(self['maindata']['wlns'], self['solvdata']['wlns'], atol=wln_tol, rtol=0):
                raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_WLNS_NETCDF_ERROR')
            
            self['maindata'].calculate_diffabs(False, False, maindict['mode'], [0])
            
            if refcorr:
                self['solvdata'].calculate_diffabs(False, False, solvdict['mode'], [0])
                solvfactor = 1
                
            elif solvdict is not None:
                self['solvdata'].calculate_diffabs(False, False, solvdict['mode'], [0])
                solvfactor = pc.calculate_solvent_factor(solvdict['absorption'], solvdict['method'])
                
            else:
                self['solvdata']['diffabs'] = 0
                solvfactor = 0
            
            corrdata = (self['maindata']['diffabs'] - self['solvdata']['diffabs'] * solvfactor)
            return corrdata[0,:].values, self['maindata']['wlns']
        
        except (pcerror.InputError, pcerror.RangeError):
            raise
        except MemoryError:
            raise pcerror.InputError('ERROR_IMPORT_MEMORY_ERROR')
        except:
            raise pcerror.InternalError('ERROR_NCDF_INTERNAL_ERROR_2')
    

#-----------------------------------------------------------------
#   NETCDF MERGE FUNCTIONS
#-----------------------------------------------------------------

def retrieve_netcdf_information (filepath):
    """
    Extract shape information about dimensions of a NetCDF file and
    its axis values.
    
    Depending on the nature of the file - regular or background type
    file - the dimensions of all axes are determined. For background
    files, the number of scans will be returned as 0, the number of
    times as 1. Note that in this case, the time axes values are yet
    returned as an empty list, as no information about the actual
    time is included in such files.
    
    Parameters
    ----------
    filepath : str
         path of the file to be read
    
    Returns
    -------
    dict or bool
        Returns a dictionary containg the number ('n') of 'wlns', 'times', 'shots',
        and 'chopper' positions as well as the wavelength and time axis values.
        Returns False, if file could not be loaded or has an invalid format.
    """
    info = {
        'nwlns': 0, 'ntimes': 0, 'nscans': 0, 'nshots': 0, 'nchopper': 0,
        'wlns': np.asarray([]), 'times': np.asarray([])
    }
    
    try:
        ds = xarray.open_dataset(filepath)
        if ds.ccd_data.ndim == 6:
            info['nscans'], info['ntimes'], info['nshots'], _, info['nchopper'], info['nwlns'] = ds.ccd_data.shape
            info['wlns'] = ds.ccd_pixel_var.values
            info['times'] = ds.scan_var.values
        elif ds.ccd_data.ndim == 4:
            info['nshots'], _, info['nchopper'], info['nwlns'] = ds.ccd_data.shape
            info['wlns'] = ds.ccd_pixel_var.values
            info['ntimes'] = 1
        else:
            raise
    except:
        return False
    else:
        return info

def merge_netcdf_files (files, wln_tol=0, time_tol=0):
    """
    Merge NetCDF raw data files into one.
    
    Merges selected scans of files with identical shape into a new file.
    Other data and meta information are discarded. The returned file
    is guaranteed to be compatible with the NetCDF import module.
    
    This function might also be used to extract scans from one file.
    
    Parameters
    ----------
    files : list of tuples
        Files to be merged as a list of tuples. Each tuple needs to
        consist of the file path and a list of scans as strings and
        integers, respectively. Note that scan count starts with 0.
    
    wln_tol, time_tol : float
        Tolerances for wavelength (absolute) and time (relative) axes.
    
    Returns
    -------
    xarray.DataArray
        Merged scans as new raw netcdf file.
    
    Raises
    ------
    pcerror.RangeError
        if a file does not contain a specifed scans
        if a incompatible wavelength axis is encountered
        if a incompatible time axis is encountered
        if inconsistent number of shots are encountered
        if incompatible chopper modes are encountered
    """
    if not files:
        return
    else:
        master_filename, master_scans = files[0]
    
    scan_sum = 0
    for file in files:
        fn, fscans = file
        scan_sum += len(fscans)
    
    master = NetCDFFile()
    master.load_data(master_filename)
    nscans, ntimes, nshots, mode, nwlns = master['data'].shape
    
    ccd_data = np.empty((scan_sum,ntimes,nshots,1,mode,nwlns))
    scan_count = 0
    try:
        for scan in master_scans:
            ccd_data[scan_count,:,:,0,:,:] = master['data'][scan,:,:,:,:]
            scan_count += 1
    except IndexError:
        raise pcerror.RangeError('ERROR_IMPORT_MERGE_INVALID_SCAN_ERROR', input=master_filename, ranges=master_scans)
    
    for file in files[1:]:
        filename, scans = file
        if not scans:
            continue
        else:
            ds = NetCDFFile()
            ds.load_data(filename)
        if min(scans) < 0 or max(scans) > ds['nscans']:
            raise pcerror.RangeError('ERROR_IMPORT_MERGE_INVALID_SCAN_ERROR', input=filename, ranges=scans)
        
        if not pc.consistent_axes(master['wlns'], ds['wlns'], atol=wln_tol):
            raise pcerror.RangeError('ERROR_IMPORT_MERGE_INCOMPATIBLE_WLNS_ERROR')
        if not pc.consistent_axes(master['times'], ds['times'], rtol=time_tol):
            raise pcerror.RangeError('ERROR_IMPORT_MERGE_INCOMPATIBLE_TIMES_ERROR')
        if nshots != ds['nshots']:
            raise pcerror.RangeError('ERROR_IMPORT_MERGE_INCOMPATIBLE_SHOTS_ERROR', ranges=nshots)
        if mode != ds['mode'].value:
            raise pcerror.RangeError('ERROR_IMPORT_MERGE_INCOMPATIBLE_MODE_ERROR', ranges=mode)
        
        try:
            for scan in scans:
                ccd_data[scan_count,:,:,0,:,:] = ds['data'][scan,:,:,:,:]
                scan_count += 1
        except IndexError:
            raise pcerror.RangeError('ERROR_IMPORT_MERGE_INVALID_SCAN_ERROR', input=filename, ranges=scans)
        finally:
            del ds
    
    return xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], ccd_data),
        ccd_pixel_var=(['ccd_row'], master['wlns']), scan_var=(['delay_time'], master['times']))
    )
    