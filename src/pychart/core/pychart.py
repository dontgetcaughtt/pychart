import copy
from enum import Enum
import warnings
import functools

import numpy as np
import lmfit

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as mplColors
from matplotlib.colors import LinearSegmentedColormap as mplLinearColormap

from .sellmeier import sellmeier_func
from . import error as pcerror

#-----------------------------------------------------------------
#   UNITS
#-----------------------------------------------------------------

class WavelengthUnit (Enum):
    """
    Enumeration for units of energy-equivalent quantities
    like wavelength or wavenumbers.
    
    For compatibility with older JSON setting files direct
    string representations are used. However, i18n identifiers
    such as `"NANO_METER"` should be preferred.
    """
    NanoMeter = 'nm'
    PerCentiMeter = 'cm⁻¹'
    ElectronVolt = 'eV'

class AbsorptionUnit (Enum):
    """
    Enumeration for absorption-like units including arbitrary units.
    
    For compatibility with older JSON setting files direct
    string representations are used. However, i18n identifiers
    such as `"MILLI_OD"` should be preferred.
    """
    MilliOD = 'mOD'
    OD = 'OD'
    Counts = 'counts'
    Arbitrary = 'a.u.'
    
class TimeUnit (Enum):
    """
    Enumeration for time units.
    
    For compatibility with older JSON setting files direct
    string representations are used. However, i18n identifiers
    such as `"FEMTO_SEC"` should be preferred.
    """
    FemtoSec = ('fs',  1E-15)
    PicoSec  = ('ps',  1E-12)
    NanoSec  = ('ns',  1E-9)
    MicroSec = ('µs',  1E-6)
    MilliSec = ('ms',  1E-3)
    Sec      = ('s',   1)
    Minute   = ('min', 60)
    
    def __new__(cls, value, factor):
        obj = object.__new__(cls)
        obj._value_ = value
        obj.magnitude = factor
        return obj
    
    def conversion_factor_to (self, target_unit):
        """
        Calculate conversion factor from instance's unit to `target_unit`.
        
        Parameters
        ----------
        target_unit : TimeUnit
            target unit
        
        Returns
        -------
        float
            factor to multiply a time with to yield its value in `target_unit`
        """
        return self.magnitude / target_unit.magnitude
    
    @staticmethod
    def conversion_factor (base_unit, target_unit):
        """
        Calculate conversion factor from `base_unit` to `target_unit`.
        
        Parameters
        ----------
        base_unit :
            unit of start value
            
        target_unit :
            unit of target value
        
        Returns
        -------
        float
            factor to multiply a time in `base_unit` with to yield its value in `target_unit`
        """
        return base_unit.magnitude / target_unit.magnitude
    
#-----------------------------------------------------------------
#   SPECTROSCOPY
#-----------------------------------------------------------------

def absorption (a):
    """
    Calculate fraction of light absorbed by a sample with absorption `a`.
    
    Parameters
    ----------
    a : float
        sample absorption
    
    Returns
    -------
    float
        fraction of light absorbed (value between 0 and 1)
    
    Raises
    ------
    pcerror.InputError
        if absorption `a` is negative
    """
    if a < 0:
        raise pcerror.InputError('ERROR_PYCHART_INVALID_ABSORPTION_ERROR')
    else:
        return 1 - 10**(-a)

def transmission (a):
    """
    Calculate fraction of light transmitted through a sample with absorption `a`.
    
    Parameters
    ----------
    a : float
        sample absorption
    
    Returns
    -------
    float
        fraction of light transmitted (value between 0 and 1)
    
    Raises
    ------
    pcerror.InputError
        if absorption `a` is negative
    """
    if a < 0:
        raise pcerror.InputError('ERROR_PYCHART_INVALID_ABSORPTION_ERROR')
    else:
        return 10**(-a)

def solvent_factor (a):
    """
    Calculate factor for proper subtraction of solvent measurements based
    on the sample absorption `a`. Returns zero if absorption is zero.
    
    Parameters
    ----------
    a : float
        sample absorption
    
    Returns
    -------
    float
    
    Notes
    -----
    Calculation is based on the equation
    
        1 - 10**(-absorption)
    f = ---------------------.
         ln(10) * absorption
    
    See https://doi.org/10.1007/s003400100750 for further explanations.
    """
    if a != 0:
        return transmission(a) / (np.log(10) * a)
    else:
        return 0

def calculate_solvent_factor (a, method):
    """
    Determine solvent factor based on `method` and given sample `a`.
    
    Raises
    ------
    pcerror.RangeError
        if an invalid type of solvent subtraction was asked for
    """
    if method == 'factor':
        return solvent_factor(a)
    elif method == 'transmission':
        return transmission(a)
    elif method == 'absorption':
        return absorption(a)
    elif method == 'free':
        return a
    else:
        raise pcerror.RangeError('ERROR_PYCHART_UNKNOWN_ABS_FACTOR_ERROR', input=method,
                               ranges="['factor', 'transmission', 'absorption', 'free']")

def black_body_curve (temperature, wlns):
    """
    Calculate the spectrum of an ideal black body radiator with a given `temperature`.
    
    Calculation is based on Planck's law of radiation
    
                     2 * π * c_0
    BB(λ) = ------------------------------
                         h_p * c_0
            λ^4 * ( exp(-----------) - 1 )
                        λ * k_B * T
    
    where c_0 is the speed of light, h_p Planck's constant, k_B the Boltzmann constant
    and T the temperature.
    
    Parameters
    ----------
    temperature : float
        temperature of the black body radiator in K
        
    wlns : np.ndarray
        wavelength axis of the spectrum
    
    Returns
    -------
    np.ndarray
        spectrum of a black body
    
    Raises
    ------
    pcerror.RangeError
        if a negative or zero temperature is encountered
    """
    if temperature <= 0:
        raise pcerror.RangeError('ERROR_PYCHART_INVALID_TEMPERATURE_ERROR')
    else:
        wlns = wlns.astype(np.float64)
        return 1 / ((wlns**4) * (np.exp(14.38748E6 / temperature / wlns) - 1))

#-----------------------------------------------------------------
#   MATH
#-----------------------------------------------------------------

def gaussian (x, amp, center, width, offset):
    """
    Implementation of a 4-term Gaussian to be used in fitting routines.
    
    Parameters
    ----------
    x :
        independent variable(s)
    
    amp :
        height of the Gaussian
    
    center :
        center of the Gaussian
    
    width :
        standard deviation
    
    offset :
        additional constant term
    
    Returns
    -------
        dependent variable(s)
    """
    return amp * np.exp(- ((x-center)**2) / (2 * width**2)) + offset

def smooth (y, n):
    """
    Smooth data by moving average.
    
    Parameters
    ----------
    y : numpy.ndarray
        input data
    
    n : int
        number of averaged points

    Returns
    -------
    numpy.ndarray
        smooth data
    
    Raises
    ------
    pcerror.RangeError
        if n is not an integer or n < 2
    """
    if (type(n) is not int) or (n < 2):
        raise pcerror.RangeError('ERROR_PYCHART_INVALID_SMOOTH_ERROR')
    av = np.ones(n) / n
    return np.convolve(y, av, mode='same')

#-----------------------------------------------------------------
#   MATPLOTLIB STUFF
#-----------------------------------------------------------------

""" List of line styles available in matplotlib. """
MPL_LINESTYLES = ['-.', '--', ':', '-']

""" List of predefined colors in matplotlib. """
MPL_COLOR_STRINGS = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']

""" List of matplotlib available font families. """
MPL_FONT_FAMILIES = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']

mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['ytick.direction'] = 'in'
mpl.rcParams['xtick.top']       = True
mpl.rcParams['xtick.bottom']    = True
mpl.rcParams['ytick.left']      = True
mpl.rcParams['ytick.right']     = True

""" List of matplotlib rcParams which should be somehow user-defined. """
MPL_RCPARAMS = ['figure.titlesize', 'axes.labelsize', 'xtick.labelsize', 'ytick.labelsize', 'xtick.major.size',
    'xtick.minor.size', 'ytick.major.size', 'ytick.minor.size', 'xtick.major.width', 'ytick.major.width',
    'axes.linewidth', 'lines.linewidth']

def mpl_color (color):
    """
    Convert `color` to a valid matplotlib input, which can be directly
    called as `color=` parameter in matplotlib functions.
    
    The valid return value might be a string of predefined colors (e.g.
    'r', 'tab:blue'), a grayscale value string (e.g. '0.8') or a RGB
    or RGBA tuple of floats in the closed interval [0,1].
    
    Parameters
    ----------
    color : Any
        input to be checked, and possibly converted
    
    Returns
    -------
    str or tuple of float or tuple of int
        valid matplotlib color input
    """
    if mplColors.is_color_like(color):
        return color
    
    if isinstance(color, str):
        try:
            c = color.strip().split(',')
            match c:
                case [r, g, b]:
                    rgb = tuple(map(lambda x: float(x) / 255, c))
                    if mplColors.is_color_like(rgb):
                        return rgb
                case [r, g, b, a]:
                    rgba = tuple(map(lambda x: float(x) / 255, c))
                    if mplColors.is_color_like(rgba):
                        return rgba
                case [gray] if gray != '':
                    gray = float(gray)
                    if 1 < gray <= 255:
                        return str(gray/255)
                case ['']:
                    return 'none'
        except ValueError:
            pass
    
    if isinstance(color, tuple):
        if len(color) == 3 or len(color) == 4:
            try:
                c = tuple(map(lambda x: x / 255, color))
                if mplColors.is_color_like(c):
                    return c
            except (TypeError, ValueError):
                pass
    
    if isinstance(color, int) or isinstance(color, float):
        if 0 <= color <= 1:
            return str(color)
        if 1 < color <= 255:
            return str(color/255)
    
    if color is None:
        return 'none'
    
    raise pcerror.RangeError('ERROR_PYCHART_INVALID_MPL_COLOR_ERROR', input=color)

def cmap_from_rgb_colors (rgb, name=''):
    """
    Create colormap from list of colors.
    
    Creates a linear segmented colormap from a list of colors.
    
    Parameters
    ----------
    rgb : list of tuples of float
        list of colors as RGB tuples (0 to 255)
    
    name : str
        name of the colormap
    
    Returns
    -------
    matplotlib.colors.LinearSegmentedColormap
    """
    temp = []
    for a, b in enumerate(rgb):
        temp.append(tuple(x / 255 for x in b))
    cmap = mplLinearColormap.from_list(name, temp)
    return cmap

def cmap_from_file (filename, name):
    """
    Load colormap from data file.
    
    Imports list of colors from text file to colormap.
    
    Parameters
    ----------
    filename : str
        file path to be imported
    
    name : str
        name of the colormap
    
    Returns
    -------
    matplotlib.colors.LinearSegmentedColormap
    
    Raises
    ------
    pcerror.InputError
        if cmap file could not be opened, features syntax errors or
        contains no rgb tuples
    
    Notes
    -----
    The file must contain one color per line as an non-normalized RGB-tuple.
    Comments marked with `#` are possible.
    
    An example for a valid cmap file is given here:
    
        # blue - white - yellow - red
        ( 85,   0, 214)
        (255, 255, 255)
        (255, 255,   0)
        (255,   0,   0)
    """
    try:
        with open(filename, 'r') as file:
            content = file.read().splitlines()
        rgb = []
        for line in content:
            if line.startswith('#'):
                continue
            else:
                temp = line[:line.rfind('#')].strip().strip('(),[]=')
                if not temp.isalpha():
                    rgb.append(tuple(float(x) for x in temp.split(',')))
        return cmap_from_rgb_colors(rgb, name)
    except:
        raise pcerror.InputError('ERROR_PYCHART_CMAP_IMPORT_ERROR', input=filename)

def generate_custom_cmap (name, reverse=False, over=False, under=False, nan=(1,1,1)):
    """
    Generate a colormap with over and under values.
    
    Creates an instance of `LinearSegmentedColormap`, which copies an existing 
    colormap but uses the given colors for under, over and NaN values.
    
    Parameters
    ----------
    name : str
        name of an existing colormap
    
    reverse : bool
        whether to flip the colormap
    
    over : tuple or False
        Color for over extending values. False (default) if the highest defined 
        color should be used.
        
    under : tuple or False
        Color for under extending values. False (default) if the lowest defined 
        color should be used.
    
    nan : tuple
        Color for undefined values. Defaults to white ((1,1,1)).
    
    Returns
    -------
    matplotlib.colors.LinearSegmentedColormap
        colormap with applied over, under and nan value colors
    """
    cmap = copy.copy(plt.get_cmap(name))
    if reverse:
        cmap = cmap.reversed()
    if over:
        cmap.set_over(over)
    if under:
        cmap.set_under(under)
    cmap.set_bad(nan)
    return cmap

def sci_format_func (value, tick_number):
    """
    Fix function to display '1' as '10^0' if scientific labeling is enabled.
    
    Returns all other values as rounded integers, therefore scientific labeling
    should only be activated if ticks in linear part are 0, 1 or -1.
    """
    if value == 1:
        return '$\mathregular{10^0}$'
    else:
        return f'{int(value)}'

#-----------------------------------------------------------------
#   UTILITIES
#-----------------------------------------------------------------

def replace_nan (data, value):
    """
    Replace all NaNs in a data array.
    
    Replaces all NaNs in `data` by given `value`. Infinite values are not
    changed. Changes are conducted directly to the data array itself, not
    a copy. Returns the number of NaNs replaced.
    
    Parameters
    ----------
    data : numpy.ndarray
        input and output data array
    
    value : float
        value to replace NaNs with
    
    Returns
    -------
    int :
        number of replacements
    """
    count = np.count_nonzero(np.isnan(data))
    np.nan_to_num(data, nan=value, posinf=np.inf, neginf=np.NINF, copy=False)
    return count

def replace_inf (data, value, pos=True, neg=True):
    """
    Replace all infinite numbers in a data array.
    
    Replaces all infinite numbers in `data` by `value`. Can be applied for positive
    and negative inf independently by `pos` and `neg`. NaNs are not changed. Changes
    are conducted directly to the data array itself, not a copy. Returns the number
    of values replaced.
    
    Parameters
    ----------
    data : numpy.ndarray
        input and output data array
    
    value : float
        value to replace infs with
    
    pos : bool
        whether to replace positive inf
    
    neg : bool
        whether to replace negative inf
    
    Returns
    -------
    int :
        number of conducted replacements
    """
    posinf, neginf = np.inf, np.NINF
    if pos and neg:
        posinf = neginf = value
        count_func = np.isinf
    elif pos:
        posinf = value
        count_func = np.isposinf
    elif neg:
        count_func = np.isneginf
        neginf = value
    else:
        return 0
    
    count = np.count_nonzero(count_func(data))
    np.nan_to_num(data, nan=np.nan, posinf=posinf, neginf=neginf, copy=False)
    return count

def finite_minimum (data):
    """
    Determine the lowest finite value within `data`.
    
    Determines the highest value after replacing NaNs and -inf
    by zero. Thus, if `data` contains non-finite values, the
    lowest possible value to be returned is 0.
    
    Parameters
    ----------
    data : numpy.ndarray
        data to find minimum value in
    
    Returns
    -------
    float
        lowest finite value in data
    """
    return np.amin(np.nan_to_num(data, nan=0.0, neginf=0.0))

def finite_maximum (data):
    """
    Determine the highest finite value within `data`.
    
    Determines the highest value after replacing NaNs and +inf
    by zero. Thus, if `data` contains non-finite values, the
    lowest possible value to be returned is 0.
    
    Parameters
    ----------
    data : numpy.ndarray
        data to find maximum value in
    
    Returns
    -------
    float
        highest finite value in data
    """
    return np.amax(np.nan_to_num(data, nan=0.0, posinf=0.0))

def lookup_index (value, data):
    """
    Look up the index of `value` in `data`.
    
    Looks up the index of the value closest to the given one within
    an array or list of data. If the the value is not found in the list
    and the values before and after have the same difference, any one of
    these two will be returned.
    
    Parameters
    ----------
    value : float
        value to look up
    
    data : list of float
        values in which `value` should be searched for
    
    Returns
    -------
    int
        index of the value closest to `value`
    """
    temp = [abs(i - value) for i in data]
    return temp.index(min(temp))

def consistent_axes (a, b, atol=0, rtol=0):
    """
    Check axis arrays for consistent shape and values.
    
    Check if given arrays `a` and `b` have the same length and identical values.
    Values only need to be identical within the given tolerances.
    
    Parameters
    ----------
    a,b : numpy.ndarray
        1-dimensional array to compare
    
    atol, rtol : float
        absolute and relative tolerances for value comparison
    
    Returns
    -------
    bool
        True, if all axes values are identical within given tolerances; False otherwise.
    """
    return a.shape == b.shape and np.allclose(a, b, atol=atol, rtol=rtol)

def suppress_warnings (func):
    """
    Decorator to suppress floods of numeric warnings, such as 'divide by zero',
    log(x<0) or 'all-NaN' values.
    """
    @functools.wraps(func)
    def wrapper (*args, **kwargs):
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore', r'divide by zero encountered in log10')
            warnings.filterwarnings('ignore', r'invalid value encountered in log10')
            warnings.filterwarnings('ignore', r'All-NaN (slice|axis) encountered')
            return func(*args, **kwargs)
    
    return wrapper

# -----------------------------------------------------------------
#   I/O OPERATIONS
# -----------------------------------------------------------------

def import_numpy_file (filepath):
    """
    Import data from file.
    
    Imports 2-dimensional data from `filepath` into a numpy array.
    If first attempt to load data fails, all commas will be replaced
    by periods through string replacement.
    
    Parameters
    ----------
    filepath : str
        file path from where the file should be loaded
    
    Returns
    -------
    numpy.ndarray
    
    Raises
    ------
    pcerror.InputError
        if the file can not be opened or read
    """
    try:
        data = np.loadtxt(filepath, dtype=float)
    except:
        try:  # try comma as decimal point
            temp = np.loadtxt(filepath, dtype=str)
            data = np.char.replace(temp, ',', '.').astype(float)
        except:
            raise pcerror.InputError('ERROR_PYCHART_IMPORT_DATA_ERROR', input=filepath)
    return data

def export_pychart_file (data, wlndata, timedata, filepath):
    """
    Export pychart compatible file.
    
    Exports 2-dimensional `data` with axis `wlndata` and axis 
    `timedata` in a format guaranteed to be read by pychart for
    plotting.

    Parameters
    ----------
    data : numpy.ndarray
        2-dimensional data
    
    wlndata : numpy.ndarray
        wavelength axis values
    
    timedata : numpy.ndarray
        time axis values
    
    filepath : str
        filepath to save the exported file in
    
    Returns
    -------
    str :
        returns file path if export was successful
    
    Raises
    ------
    pcerror.OutputError
        if the file could not be exported
    """
    try:
        np.savetxt('../temp/pychart-export', data, header='0 ' + ' '.join([str(wln) for wln in wlndata]), comments='', fmt='%.6g', delimiter=' ')
        with open('../temp/pychart-export', 'r') as file:
            lines = file.readlines()
        for n in range(timedata.size):
            lines[n+1] = str(timedata[n]) + ' ' + lines[n+1]
        with open(filepath, 'w') as file:
            file.writelines(lines)
        return filepath
    except:
        raise pcerror.OutputError('ERROR_PYCHART_EXPORT_DATA_ERROR', output=filepath)

def export_z20_file (data, wlndata, timedata, filepath):
    """
    Export Z20 compatible file.
    
    Exports 2-dimensional `data` with axis `wlndata` and axis
    `timedata` in a format hopefully to be read by Z20 for
    fitting.
    
    Parameters
    ----------
    data : numpy.ndarray
        2-dimensional data
    
    wlndata : numpy.ndarray
        wavelength axis values
    
    timedata : numpy.ndarray
        time axis values
    
    filepath : str
        filepath to save the exported file in
    
    Returns
    -------
    str :
        returns file path if export was successful
    
    Raises
    ------
    pcerror.OutputError
        if the file could not be exported
    """
    s = '#\n#\nKommentar=IDL-free export from pychart'
    s += '\nschritte=' + str(timedata.size)
    s += '\nlschritte=' + str(timedata.size)
    s += '\nKanalanzahl=' + str(wlndata.size)
    s += '\nWellenlaengen='
    s += ' '.join([str(wln) for wln in wlndata])
    s += '\nXMode=SingleX\nData=\n#'
    try:
        np.savetxt('../temp/pychart-export', data, header=s, comments='', fmt='%.6g', delimiter=' ')
        with open('../temp/pychart-export', 'r') as file:
            lines = file.readlines()
        for n in range(timedata.size):
            lines[n+10] = str(timedata[n]) + ' ' + lines[n+10]
        with open(filepath, 'w') as file:
            file.writelines(lines)
        return filepath
    except:
        raise pcerror.OutputError('ERROR_PYCHART_EXPORT_DATA_ERROR', output=filepath)

def export_external_pychart_file (data, axisdata, filepath):
    """
    Export single spectrum/trace as pychart compatible file.
    
    Exports 1-dimensional `data` with axis `axisdata` in a format
    guaranteed to be read by pychart for plotting as an _external_
    file.
    
    Parameters
    ----------
    data : numpy.ndarray
        one-dimensional data
    
    axisdata : numpy.ndarray
        one-dimensional axis values
    
    filepath : str
        filepath to save the exported file in
    
    Returns
    -------
    str :
        returns file path if export was successful
    
    Raises
    ------
    pcerror.OutputError
        if the file could not be exported
    """
    try:
        np.savetxt(filepath, np.asarray((axisdata,data)).transpose(), header='', fmt='%.6g', delimiter=' ')
        return filepath
    except:
        raise pcerror.OutputError('', output=filepath)

# -----------------------------------------------------------------
#   TIME ZERO DETERMINATION VIA GATE MEASUREMENT
# -----------------------------------------------------------------

def determine_time_zeros (traces, times, wlns, method, processing, settings):
    """
    Extract peak maxima from gate measurement and do some post-processing.
    
    Determines the time zero points for each wavelength by the `method` chosen by the user.
    Either a gauss fit can be applied, using the center of the fit result, or simply the
    maximum signal position of each time trace.
    
    If `processing` is not 'off', the resulting time zeros are either smoothed or fitted
    according to a hypothetical Sellmeier function.
    
    Parameters
    ----------
    traces : numpy.ndarray
        gate measurement data
        
    times : numpy.ndarray
        time axis values
    
    wlns : numpy.ndarray
        wavelength axis values
    
    method : 'gauss' or 'minima'
        how to determine the time zero points, either by a gaussian fit or by taking maximum values
    
    processing : 'off' or 'smooth' or 'fit'
        how to to post-process time zero curve, either by smoothing (moving average) or by a
        Sellmeier fit or not at all
    
    settings : dict (optional)
        dictionary containing fit and post-processing settings as
            ['amp'] : amplitude start value
            ['center'] : center start value
            ['center_min'] : center minimum value
            ['center_max'] : center maximum value
            ['width'] : width start value
            ['offset'] : offset start value
            ['amp_auto'] : whether to use `['amp']` or minimum amplitude value in trace as start value
            ['center_auto'] : whether to use `['center']` or time of minimum amplitude value within trace as start value
            ['smooth']: number of points for smoothing
            ['smooth_on']: whether or not to smooth time zero curve
            ['fit_start']: first wavelength to include in the fit
            ['fit_end']: last wavelength to include in the fit
            ['fit_thickness']: material thickness start value
            ['fit_thickness_fix'] whether or not to keep thickness fixed
            ['fit_offset']: offset start value
            ['fit_offset_fix'] whether or not to keep offset fixed
    
    Returns
    -------
    numpy.ndarray
        time zero for each wavelength
    
    Raises
    ------
    pcerror.RangeError
        if an invalid method of determination (besides gauss fit and minima) was asked for
        if the wavelength range for a Sellmeier fit is to short
        if the start wavelength for a Sellmeier fit is above than the end wavelength
    """
    nwlns = np.size(traces,1)
    nan_free_traces = np.nan_to_num(traces, nan=0.0, posinf=0.0, neginf=0.0)
    peaks = np.zeros(nwlns)
    
    if method == 'gauss':
        _min, _max = settings['center_min'], settings['center_max']

        for wln in range(nwlns):
            if settings['center_auto']:
                index = np.argmax(nan_free_traces[:, wln])
                center = times[index]
            else:
                center = settings['center']
            if settings['amp_auto']:
                amp = np.max(nan_free_traces[:, wln])
            else:
                amp = settings['amp']
            
            # The normalization (which is an division) will throw lots of "invalid value encountered" warnings.
            # That's why we convert NaNs to numbers afterwards. We can, thus, dismiss all the warnings.
            with warnings.catch_warnings():
                warnings.simplefilter('ignore', RuntimeWarning)
                norm_traces = np.nan_to_num(nan_free_traces / np.nanmax(nan_free_traces, axis=0))
            
            model = lmfit.Model(gaussian, nan_policy='omit')
            params = model.make_params(amp=amp, center=center, width=settings['width'], offset=settings['offset'])
            params['center'].set(min=_min, max=_max)
            
            # Levenberg-Marquardt ('leastsq') is fast but results are sometimes stepped.
            # Nelder-Mead algorithm ('nelder') is about seven times slower but gives much more smooth results.
            # However, since a fit according to Sellmeier is usually performed anyway leastsq might be good enough.
            result = model.fit(norm_traces[:, wln], x=times, params=params, method='leastsq')
            peaks[wln] = result.params['center'].value

    elif method == 'maxima':
        peaks = times[nan_free_traces.argmax(axis=0)]
    
    else:
        raise pcerror.RangeError('ERROR_IMPORT_UNKNOWN_FIT_ERROR', input=method, ranges="['gauss', 'maxima']")
    
    if processing == 'smooth':
        return smooth(peaks, settings['smooth'])
    
    elif processing == 'fit':
        a = lookup_index(settings['fit_start'], wlns)
        b = lookup_index(settings['fit_end'], wlns)
        if not a < b:
            raise pcerror.RangeError('ERROR_IMPORT_INVALID_GATE_WLN_RANGE_ERROR')
        if (b - a) < 2:
            raise pcerror.RangeError('ERROR_IMPORT_SMALL_GATE_WLN_RANGE_ERROR')
        
        # wlns and peaks must be of the same dtype for the fit to work properly
        # usually peaks.dtype == np.float64, while peaks.dtype == np.float32
        λ = wlns.astype(dtype=peaks.dtype)
        
        # Reminder: When changing the post processing procedure (e.g. the nan policy
        # or the fitting method) apply the same changes to the preview routine in
        # `NetCDFImportTabDialogs.show_preview()`, too.
        model = lmfit.Model(sellmeier_func, independent_vars=['λ'], nan_policy='omit')
        params = model.make_params(d=settings['fit_thickness'], offset=settings['fit_offset'])
        params['d'].set(vary=not settings['fit_thickness_fix'])
        params['offset'].set(vary=not settings['fit_offset_fix'])
        result = model.fit(peaks[a:b], λ=λ[a:b], params=params, method='leastsq')
        time_zeros = sellmeier_func(λ, result.best_values['d'], result.best_values['offset'])
        return time_zeros
    
    else:
        return peaks

#-----------------------------------------------------------------
#   FILE EXTENSIONS
#-----------------------------------------------------------------

FILE_TYPE_ALL  = 'All files (*.*)'
FILE_TYPE_TEXT = 'Text files (*.txt)'
FILE_TYPE_DATA = 'Data files (*.dat)'
FILE_TYPE_NCDF = 'NetCDF file (*.dat *.arr)'
FILE_TYPE_HDF  = 'Hierarchical Data Format (*.hdf *.h5)'
FILE_TYPE_JSON = 'JSON file (*.json)'
FILE_TYPE_PNG  = 'Portable Network Graphic (*.png)'
FILE_TYPE_PDF  = 'Portable Document Format (*.pdf)'
FILE_TYPE_JPG  = 'JPG image (*.jpg)'
FILE_TYPE_EPS  = 'Encapsulated Postscript (*.eps)'
FILE_TYPE_SVG  = 'Scalable Vector Graphics (*.svg)'
FILE_TYPE_TIF  = 'TIFF image (*.tiff)'

""" File type filters for open/save and import/export dialogs. """
FILE_TYPE_FILTER_DATA = (
    FILE_TYPE_DATA + ';;' +
    FILE_TYPE_TEXT
)
FILE_TYPE_FILTER_PLOT = (
    FILE_TYPE_PNG + ';;' +
    FILE_TYPE_PDF + ';;' +
    FILE_TYPE_SVG + ';;' +
    FILE_TYPE_EPS + ';;' +
    FILE_TYPE_JPG + ';;' +
    FILE_TYPE_TIF
)