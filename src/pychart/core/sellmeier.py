import json

import numpy as np

SPEED_OF_LIGHT = 299792458 # m/s

def sellmeier_func (λ, d, offset):
    """
    Sellmeier equation with constant coefficients (B = 0.6, C = 0.005,
    D = 0.5, E = 0.01) for fitting.
    
    `d` is assumed to be given in mm; `offset` in ps. Returned is the
    time shift in ps.
    
    Parameters
    ----------
    λ :
        independent variable (aka wavelength)
    
    d :
        transmission path length of the hypothetical material
    
    offset :
        constant offset
    
    Returns
    -------
        dependent variable (aka time shift)
    """
    # these are meant to be reasonable values for a hypothetical optical glass
    B, C, D, E = 0.6, 0.005, 0.5, 0.01
    
    m = -1E12 * d / 1000 / SPEED_OF_LIGHT
    λ = λ / 1000  # for some reason λ /= 1000 does not work
    
    return m * np.sqrt(1 + ((B * λ**2) / (λ**2 - C)) + ((D * λ**2) / (λ**2 - E))) + offset

def speed_of_light (n):
    """
    Calculate the speed of light in a material of given refractive index
    
    Parameters
    ----------
    n : float
        refractive index
    
    Returns
    -------
    float :
        speed of light in material with `n`
    """
    return SPEED_OF_LIGHT / n

def t_from_n (n, d):
    """
    Calulate time of travel for a material with refractive index `n`
    through a path of length `d`.
    
    Parameters
    ----------
    n : float or numpy.ndarray
        refractive index of material
    
    d : float or numpy.ndarray
        path length which is traveled with the speed of light (in the material
        with refractive index `n`), in mm
    
    Returns
    -------
    float or numpy.ndarray :
        time the light need to travel over distance `d` in a material with `n` (in s)
    """
    return (d / 1000) / speed_of_light(n)

# -----------------------------------------------------------------
#   SELLMEIER DATA CLASS
# -----------------------------------------------------------------

class SellmeierCoefficients (object):
    """
    A class which stores data that might be used for calculating
    wavelength-dependent refractive indices. For Sellmeier and
    Cauchy coefficients are stored. Additionally, refractive
    indices (D-line of sodium) and relative permittivities are
    available.
    
    Attributes
    ----------
    sellmeiercoeffs : dict
        Sellmeier coefficients B and C for different materials.
        Approximation is based on three terms!
            { "material": {"B": [float, float, float], "C": [float, float, float]}, ... }
    
    cauchycoeffs : dict
        cauchy coeffcients for different materials; [A]=nm^0, [B]=nm^2, [C]=nm^4, ...
            { "material": list of float, ... }
    
    refractiveindices : dict
        single-wavelength refractive indices for different materials
            { "material": float, ... }
    
    permittivities : dict
        relative permittivities for different materials
            { "material": float, ... }
    
    materials : bool
        list  of available materials (union of materials of all coefficients lists)
    """
    
    def __init__(self):
        self._sellmeiercoeffs = {}
        self._cauchycoeffs = {}
        self._permittivities = {}
        self._refractiveindices = {}
        self._materials = []
    
    @property
    def materials (self):
        return self._materials
    
    def make_materials_list (self):
        # TODO: user ordered dict and the new merge / update operator |=
        materials = []
        materials += list(self._sellmeiercoeffs.keys())   if self._sellmeiercoeffs   else []
        materials += list(self._cauchycoeffs.keys())      if self._cauchycoeffs      else []
        materials += list(self._refractiveindices.keys()) if self._refractiveindices else []
        materials += list(self._permittivities.keys())    if self._permittivities    else []
        self._materials = list(dict.fromkeys(materials))
    
    def load_coeffs_from_files (self, path):
        """
        Load coefficients or other data for time zero corrections from JSON files.
        
        Parameters
        ----------
        path :  str
            directory to search coefficient data files in
        
        Returns
        -------
        list of tuple
            list coefficient files, which could not be loaded or decoded:
            [('decode', <filepath>), ('load', <filepath>)]
        """
        # TODO: status for splash screen
        
        def load_json (filepath):
            try:
                with open(filepath, 'r', encoding='utf-8') as file:
                    return json.load(file)
            except json.JSONDecodeError:
                errors.append( ('decode',filepath) )
            except:
                errors.append( ('load',filepath) )
        
        errors = []
        self._sellmeiercoeffs   = load_json(path + 'sellmeier-coefficients.json')
        self._cauchycoeffs      = load_json(path + 'cauchy-coefficients.json')
        self._refractiveindices = load_json(path + 'refractive-indices.json')
        self._permittivities    = load_json(path + 'relative-permittivities.json')
        self.make_materials_list()
        return errors
    
    # -----------------------------------------------------------------
    #   CALCULATION
    # -----------------------------------------------------------------
    
    def sellmeier (self, material, wlndata):
        """
        Calculate the refractive indices n of `material` for wavelengths λ in `wlndata`.
        
        Calculation is based on Sellmeier's equation:
        
                             B_i λ^2           B_1 λ^2     B_2 λ^2     B_3 λ^2
        n^2(λ) = 1 + sum_i( --------- ) = 1 + --------- + --------- + --------- + ...
                            λ^2 - C_i         λ^2 - C_1   λ^2 - C_2   λ^2 - C_3
        
        The group velocity index is given by (n - λ * dndλ).
        
        Deviation of the Sellmeier equation yields dndλ as (validated by K.A.T. at 2021-01-10):
        
                 1             B_i C_i λ
        dndλ = - - * sum_i ( ------------- )
                 n           (λ^2 - C_i)^2
        
        Parameters
        ----------
        material : str
            name of the transmitted material
        
        wlndata : numpy.ndarray
            wavelengths to calculate n for
        
        Returns
        -------
        numpy.ndarray
            wavelength dependent refractive indices
        """
        λ = wlndata / 1000  # sellmeier coefficients usually given in µm²
        n2, dndλ = 1, 0
        for i in range(3):
            n2 += (self._sellmeiercoeffs[material]['B'][i] * (λ**2) / ((λ**2) - self._sellmeiercoeffs[material]['C'][i]))
        n = np.sqrt(n2)
        for i in range(3):
            dndλ -= ((λ * self._sellmeiercoeffs[material]['B'][i] * self._sellmeiercoeffs[material]['C'][i])
                     / (n * ((λ**2) - self._sellmeiercoeffs[material]['C'][i])**2))
        return n - λ * dndλ
    
    def cauchy (self, material, wlndata):
        """
        Calculate the refractive indices n of `material` for wavelengths λ in `wlndata`.
        
        Calculation is based on Cauchy's transmission equation:
        
                    B     C
        n(λ) = A + --- + --- + ...
                   λ^2   λ^4
        
        The group velocity index is given by (n - λ * dndλ).
        
        Deviation of the Cauchy equation yields dndλ as (validated by K.A.T. at 2021-01-10):
        
                          2i * A_i
        dndλ = - sum_i ( --------- )
                         λ^(2i +1)
        
        Parameters
        ----------
        material : str
            name of the transmitted material
        
        wlndata : numpy.ndarray
            wavelengths in nm to calculate n for
        
        Returns
        -------
        numpy.ndarray
            wavelength dependent refractive indices
        """
        n, dndλ = 0, 0
        wlndata = wlndata.astype(np.float64)
        for i in range(len(self._cauchycoeffs[material])):
            n +=  self._cauchycoeffs[material][i] / (wlndata**(i * 2))
            dndλ -= (i * 2) * self._cauchycoeffs[material][i] / (wlndata**((i * 2) + 1))
        return n - wlndata * dndλ
    
    def permittivity (self, material):
        """
        Estimate refractive index n of `material` from relative permittivity as n = sqrt(ε).
        
        Parameters
        ----------
        material : str
            name of the transmitted material
        
        Returns
        -------
        float
            (wavelength independent) refractive index
        """
        return np.sqrt(self._permittivities[material])
    
    def refractive_index (self, material):
        """
        Returns refractive index n of `material` at Sodium's D-line.
        
        Parameters
        ----------
        material : str
            name of the transmitted material
        
        Returns
        -------
        float
            (wavelength independent) refractive index
        """
        return self._refractiveindices[material]
    
    def calculate_time_shifts (self, wlns, layerthicknesses):
        """
        Calculate time shifts for wavelengths `wlns` through `layerthicknesses`.
        
        Tries to calculate the time shifts from group velocity based on Sellmeier's
        or Cauchy's equation. If no coefficients are given, the wavelength-independent
        refractive index is used. As a final fall-back an estimation based on the relative
        permittivity is undertaken. If given materials are neither found in any of
        these respective lists, their time shift is assumed to be zero.
        
        The thickness of each material has to be given in mm, the wavelengths in nm,
        while the resulting time shifts are returned in ps.
        
        Parameters
        ----------
        wlns : numpy.ndarray
            wavelengths to calculate the time shift for (in nm)
        
        layerthicknesses :  list of (str,float)
            list of materials with certain thickness (in mm)
        
        Returns
        -------
        numpy.ndarray
            time zero shifts (in ps)
        """
        dt = np.zeros(wlns.size)
        
        for (material, d) in layerthicknesses:
            if material in self._sellmeiercoeffs:
                dt += t_from_n(self.sellmeier(material, wlns), d)
                
            elif material in self._cauchycoeffs:
                dt += t_from_n(self.cauchy(material, wlns), d)
                
            elif material in self._refractiveindices:
                dt += t_from_n(self.refractive_index(material), d)
            
            elif material in self._permittivities:
                dt += t_from_n(self.permittivity(material), d)
            
            else:
                dt += 0
            
        return dt * 10**12
    