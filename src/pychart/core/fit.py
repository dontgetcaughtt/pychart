# TODO: EXPERIMENTAL: WIP - CODE REVIEW needed
# code is very ugly and constantly changed on-the-fly

from enum import Enum
import numpy as np
from scipy.signal import fftconvolve
import math
from lmfit import CompositeModel, Model, report_fit
from lmfit.models import ExponentialModel, ConstantModel

def GaussModel (x, center, fwhm, amplitude, offset):
    return offset + amplitude * math.exp((-math.log(4) / (fwhm ** 2)) * (x - center)**2)

def Convolution (data, irf):
    p = np.fft.fft(data)
    q = np.fft.fft(irf)
    #s = np.real(q)
    #print(s)
    #q /= s
    return np.real(np.fft.ifft(p * q))
    #return np.convolve(data, irf, mode='same')
    #return fftconvolve(data, irf, mode='same')
    #npts = min(len(data), len(irf))
    #pad = np.ones(npts)
    #tmp = np.concatenate((pad * data[0], data, pad * data[-1]))
    #out = np.convolve(tmp, irf, mode='valid')
    #noff = int((len(out) - npts) / 2)
    #return (out[noff:])[:npts]

def IRFModel (x, baseline, irf=None):
    return irf - baseline



class FitTime (dict):
    
    def __init__ (self):
        self['start'] = {'time': 0, 'amp': 0}
        self['range'] = {'min': -np.inf, 'max': np.inf}
        self['fit']   = {'time': 0, 'amp': 0}
        self['fix']   =  False

class TDFit (dict):
    
    class Conv (Enum):
        IRF    = 0,
        Gauss  = 1,
        NoConv = 2 
    
    def __init__ (self):
        
        self['name'] = ''
        
        self['datafile'] = ''
        self['irffile']  = ''
        
        self['data'] = None
        self['axis'] = None
        self['unit'] = None
        
        self['convolution'] = TDFit.Conv.IRF
        self['irfdata']     = None
        self['fwhm']        = 0
        
        self['ranges'] = {
            'start': 0,
            'end':   0,
            'step':  0
        }
        
        self['params'] = {
            'shift':   {'start': 0, 'fix': False, 'fit': 0},
            'bg':      {'start': 0, 'fix': False, 'fit': 0},
            'bgirf':   {'start': 0, 'fix': False, 'fit': 0},
            'cuttoff': {'start': 0, 'fix': True,  'fit': 0},
            'scatter': {'start': 0, 'fix': False, 'fit': 0}
        }
        
        self['timescount'] = 1
        self['times'] = [
            FitTime(), FitTime(), FitTime(), FitTime(),
            FitTime(), FitTime(), FitTime(), FitTime()
        ]
        
        self['fit'] = None
        self['success'] = True # DEV ONLY
    
    def load_data (self, filename):
        try:
            self['data'] = np.loadtxt(filename, dtype=float)
        except:
            print(filename + ': ' + 'Data file could not be loaded.')
    
    def load_irf_data (self, filename):
        try:
            self['irfdata'] = np.loadtxt(filename, dtype=float)
        except:
            print('IRF data file could not be loaded.')
    
    def run_fit (self):
        
        def lookup_index (time, axis):
            temp = [abs(i - time) for i in axis.tolist()]
            return temp.index(min(temp))
        
        def prefix_str (index):
            return 't' + str(index + 1) + '_'
        
        t = self['params']['shift']['start']
        a = lookup_index(self['ranges']['start'], self['data'][:,0]-t)
        b = lookup_index(self['ranges']['end'], self['data'][:,0]-t)
        self['axis'] = self['data'][a:b,0]
        
        # initialize model
        i = 0
        exp = ExponentialModel(prefix=prefix_str(i))
        params = exp.make_params()
        #params = exp.make_params(decay=self['times'][i]['start']['time'], amplitude=self['times'][i]['start']['amp'])
        params[prefix_str(i) + 'decay'].set(value=self['times'][i]['start']['time'],
                                            vary= not self['times'][i]['fix'],
                                            min=self['times'][i]['range']['min'],
                                            max=self['times'][i]['range']['max'])
        params[prefix_str(i) + 'amplitude'].set(value=self['times'][i]['start']['amp'],vary= not self['times'][i]['fix'])
        i += 1
        while i < self['timescount']:
            temp_model = ExponentialModel(prefix=prefix_str(i))
            exp += temp_model
            temp_params = temp_model.make_params()
            temp_params[prefix_str(i) + 'decay'].set(value=self['times'][i]['start']['time'],
                                                     vary= not self['times'][i]['fix'],
                                                     min=self['times'][i]['range']['min'],
                                                     max=self['times'][i]['range']['max'])
            temp_params[prefix_str(i) + 'amplitude'].set(value=self['times'][i]['start']['amp'],vary= not self['times'][i]['fix'])
            #params += temp_model.make_params(decay=self['times'][i]['start']['time'], amplitude=self['times'][i]['start']['amp'])
            params += temp_params
            i += 1
        
        const = ConstantModel(prefix='c_')
        const_params = const.make_params()
        const_params['c_c'].set(value=self['params']['bg']['start'], 
                              vary= not self['params']['bg']['fix'])
        exp += const
        params += const_params
        if self['convolution'] == TDFit.Conv.IRF:
            shape = self['irfdata'][a:b,1].shape
            irf_list = []
            for x in np.nditer(self['irfdata'][a:b,1]):
                irf_list.append(x if x > self['params']['cuttoff']['start'] else 0)
            irf = np.reshape(np.array(irf_list), shape)
            irf_model = Model(IRFModel)
            irf_params = irf_model.make_params()
            irf_params['baseline'].set(value=self['params']['bgirf']['start'],
                                       vary= not self['params']['bgirf']['fix'])
            #irf_params['irf'].set(value=irf, vary=False)
            model = CompositeModel(exp, irf_model, Convolution)
            params += const_params + irf_params
        elif self['convolution'] == TDFit.Conv.Gauss:
            # TODO: Faltung mit Gauss
            irf = self['irfdata'][a:b,1]
            model = CompositeModel(exp, Model(IRFModel), Convolution)
        else:
            irf = None
            model = exp
        print('wait for it')
        try:
            self['fit'] = model.fit(self['data'][a:b,1], params, x=self['axis'], irf=irf, method='nelder')
            print(self['fit'].fit_report())
        except:
            print('error in fit')
        
        try:
            for i in range(self['timescount']):
                self['times'][i]['fit']['time'] = self['fit'].params[prefix_str(i) + 'decay'].value
                self['times'][i]['fit']['amp']  = self['fit'].params[prefix_str(i) + 'amplitude'].value
        except:
            print('error line 176')
        try:
            self['params']['bg']['fit'] = self['fit'].params['c_c'].value
            self['params']['bgirf']['fit'] = self['fit'].params['baseline'].value
        except:
            self['params']['bg']['fit'] = 0
            self['params']['bgirf']['fit'] = 0
        #self['params']['shift']['fit']    = self['fit'].params['offset'].value
