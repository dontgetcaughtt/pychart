import numpy as np
import h5py

from . import pychart as pc
from . import error as pcerror

# -----------------------------------------------------------------
#   H5 FILE CLASS
# -----------------------------------------------------------------

class H5File (dict):
    """
    A dictionary containing all necessary information regarding a Kerr-Gate
    HDF file. Extended by correction routines. If single spectra for these
    corrections are stored in another HDF file, they are loaded in and
    considered to be part of this file.
    
    Attributes
    ----------
    ['data'] : numpy.ndarray
        raw data
    
    ['signal'] : numpy.ndarray
        corrected data
    
    ['wlns'] : numpy.ndarray
        wavelength axis
    
    ['times'] : numpy.ndarray
        time axis
    
    ['pd_gate'] : numpy.ndarray
        raw gate photodiode values
    
    ['pd_pump'] : numpy.ndarray
        raw pump photodiode values
    
    ['pd_gate_n'] : numpy.ndarray
        normalized gate photodiode values
    
    ['pd_pump_n'] : numpy.ndarray
        normalized pump photodiode values
    
    ['g0'] : numpy.ndarray
        thg spectrum gate photodiode value
    
    ['p0'] : numpy.ndarray
        leak spectrum pump photodiode value
    
    ['nwln'] : int
        number of wavelengths
    
    ['ntimes'] : int
        number of delay time points
    
    ['nscans'] : int
        number of delay line scans
    
    ['leak'] : numpy.ndarray
        leak spectrum (polarizers closed)
    
    ['thg'] : numpy.ndarray
        third harmonic spectrum of gate pulse (gate only)
    
    ['dark'] : numpy.ndarray
        background spectrum (all blocked)
    
    ['lamp'] : numpy.ndarray
        black body spectrum for sensitivity correction (lamp)
    """
    
    def __init__ (self):
        
        super().__init__()
        self['data'], self['signal'] = None, None
        self['wlns'], self['times']  = None, None
        
        self['pd_gate'],   self['pd_pump']   = None, None
        self['pd_gate_n'], self['pd_pump_n'] = None, None
        self['g0'],        self['p0']        = 0,    0
        
        self['nwln'], self['ntimes'], self['nscans'] = 0, 0, 0
        
        self['leak'], self['thg'], self['dark'], self['lamp'] = None, None, None, None
    
    def load_data (self, filepath):
        """
        Load data from file.
        
        Extracts data from data set. Only those data necessary for the
        functions implemented here are imported.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Raises
        ------
        pcerror.InputError
            if the file can not be opened or read
        """
        try:
            with h5py.File(filepath, 'r') as f:
                self['data'] = f['data_raw'][:, :]
                self['pd_gate'] = f['reference'][:, 0]
                self['pd_pump'] = f['reference'][:, 2]
                self['wlns'] = f['ccd_pixel_var'][:-1]
                self['times'] = f['delay_time_ps'][:]
                self['nscans'] = f['scan_duration'].size
                if f['scan_duration'][0] == 0.0:
                    self['nscans'] -= 1
        except:
            raise pcerror.InputError('ERROR_IMPORT_IMPORT_DATA_ERROR', input=filepath)
    
    def load_bg (self, filepath, wln_tolerance):
        """
        Load background spectra from file.
        
        Extracts dark, leak and thg spectra from data set and checks
        their axes for consistency with main data.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        wln_tolerance : float
            absolute tolerance of wavelength axes values
        
        Raises
        ------
        pcerror.InputError
            if the file can not be opened or read
            if no main data set was loaded before
        
        pcerror.RangeError
            if wavelength axes of background and master data do not match
        """
        try:
            with h5py.File(filepath, 'r') as f:
                
                if not pc.consistent_axes(self['wlns'], f['ccd_pixel_var'][:-1], atol=wln_tolerance, rtol=0):
                    raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_WLNS_BG_HDF_ERROR')
                
                self['leak'] = f['single_spectra'][1, :-1]
                self['thg']  = f['single_spectra'][2, :-1]
                self['dark'] = f['single_spectra'][4, :-1]
                try:
                    self['g0'] = f['singles_pd'][1,0]
                    self['p0'] = f['singles_pd'][2,0]
                except (ValueError, IndexError, KeyError):
                    self['g0'] = self['p0'] = 1
                
        except pcerror.RangeError:
            raise
        except:
            raise pcerror.InputError('ERROR_IMPORT_IMPORT_DATA_ERROR', input=filepath)
    
    def initialize_dataset (self):
        """
        Initialize dataset.
        
        Extracts information from loaded data and checks for consistency. If
        the number of saved spectra does not match the expected number based
        on the number of scans and delay times, the following attempts are
        undertaken to resolve that issue:
            
            - If the number of spectra is exactly one spectrum short of the
              expected number, it is assumed that the very first spectrum
              is missing and the (remaining) first scan is discarded.
            
                        n(spectra) = n(scans) * n(times) - 1
            
            - If the number of spectra is an integer multiple of the number
              of delay times, it is assumed that only full scans are missing
              and no data is dismissed.
              
                        n(spectra) = n(times) * x,    x = 1,2,3,4,...
              
        In case that the inconsistency can not be resolved, the data is
        considered corrupted and an exception raised.
        
        For non-corrupted data, arrays are reshaped from 2-dimensional to three
        dimensions (scans, times, wavelengths).
        
        Returns
        -------
        int
            Returns 0 if file is well-formed; returns -1 if first scan was discarded;
            returns number of scans actually found if that is an integer multiple of
            the number of delay times.
        
        Raises
        ------
        pcerror.InputError
            if file is not well-formed and delay times can not be clearly assigned to spectra
        """
        self['nwln'] = self['wlns'].size
        self['ntimes'] = self['times'].size
        
        nspectra_expect = self['nscans'] * self['ntimes']
        nspectra_actual = self['data'][:, 0].size
        
        if nspectra_expect == nspectra_actual:
            status = 0
        elif nspectra_expect-1 == nspectra_actual:
            second_scan_i = self['ntimes'] - 1
            self['data'] = self['data'][second_scan_i:, :]
            self['pd_gate'] = self['pd_gate'][second_scan_i:]
            self['pd_pump'] = self['pd_pump'][second_scan_i:]
            self['nscans'] -= 1
            status = -1
        elif nspectra_actual % self['ntimes'] == 0:
            self['nscans'] = nspectra_actual // self['ntimes']
            status = nspectra_expect // self['ntimes']
        else:
            raise pcerror.InputError('ERROR_IMPORT_CORRUPTED_FILE_ERROR')
        
        self['data'] = (np.reshape(self['data'], (self['nscans'], self['ntimes'], self['nwln']))).astype(np.float)
        self['pd_gate'] = np.reshape(self['pd_gate'], (self['nscans'], self['ntimes'], 1))
        self['pd_pump'] = np.reshape(self['pd_pump'], (self['nscans'], self['ntimes'], 1))
        
        return status
    
    def initialize_photodiodes (self, pd_correction):
        """
        Initialize photodiode arrays.
        
        If `pd_correction` is true, photodiode values are normalized by
        dividing them by their reference (or mean, see Notes) value. If
        not, all values are discarded from further corrections.
        
        Parameters
        ----------
        pd_correction : bool
            whether it is planned to use photodiodes for data correction
        
        Notes
        -----
        Before July 2017 the photodiode values during acquisition of THG and leak
        spectra were not (properly) recorded. The values stored in these old datasets are
        either 0 or 1. This function checks for appropriate floats and, if not found,
        calculates the mean value to use for normalization.
        """
        if pd_correction:
            temp = self['g0'] - self['p0']
            if abs(temp) == 1 or temp == 0:
                self['g0'] = (self['pd_gate']**3).mean()
                self['p0'] = self['pd_pump'].mean()
                # TODO: somehow include ErrorDialog w/ translation of `ERROR_IMPORT_INCOMPLETE_BG_ERROR`
                print('*** WARNING: Photodiode values incomplete. Background correction might be corrupted. ***')
            self['pd_gate_n'] = self['pd_gate']**3 / self['g0']
            self['pd_pump_n'] = self['pd_pump'] / self['p0']
        else:  # Keep it short and simple! Set all values to 1.
            self['pd_gate_n'] = np.ones_like(self['pd_gate'])
            self['pd_pump_n'] = np.ones_like(self['pd_pump'])
    
    def background_correction (self):
        """
        Correct data from calculated background.
        
        Estimates background signal from steady-state THG and leaking fluorescence
        spectra with scaling based on photodiodes. It is then subtracted from
        raw data along with the dark spectrum.
        """
        thg = self['thg'] - self['dark']
        leak = self['leak'] - self['dark']
        
        for scan in range(self['nscans']):
            for time in range(self['ntimes']):
                bg = thg * self['pd_gate_n'][scan, time] + leak * self['pd_pump_n'][scan, time]
                self['signal'][scan, time, :] = self['signal'][scan, time, :] - self['dark'] - bg
    
    def reference_point_correction (self):
        """
        Correct data for reference time point.
        
        Subtracts signal from each scan's reference time point.
        """
        for scan in range(self['nscans']):
            for time in reversed(range(self['ntimes'])):
                self['signal'][scan, time, :] = self['signal'][scan, time, :] - self['signal'][scan, 0, :]
    
    def intensity_correction (self):
        """
        Correct data for intensity dependent processes.
        
        Accounts for the dependence of the signal intensity from the excitation
        intensity as well as the one of the gate efficiency.
        """
        self['signal'] = self['signal'] / self['pd_pump_n'] / self['pd_gate_n']**2
    
    def calculate_corrected_signal (self, bgcorr, refcorr, pdcorr, scans):
        """
        Generate corrected data set.
        
        Calculates the final signal intensities from raw data based on the
        corrections chosen by the user. Finally, the data is averaged
        over `scans`.
        
        Parameters
        ----------
        bgcorr : bool
            whether to apply a background correction
        
        refcorr : bool
            whether to apply a reference point correction
        
        pdcorr : bool
            whether to include photodiode values in corrections
        
        scans : list of int
            list of scans to average
        
        Returns
        -------
        np.ndarray
            averaged part of corrected data
        """
        self['signal'] = np.copy(self['data'])
        self.initialize_photodiodes(pdcorr)
        
        if bgcorr:
            self.background_correction()
        if refcorr:
            self.reference_point_correction()
        if pdcorr:
            self.intensity_correction()
        
        for scan in range(self['nscans']):
            if scan not in scans:
                self['signal'][scan, :, :] = np.nan
        
        self['signal'] = np.nanmean(self['signal'], axis=0)
    
# -----------------------------------------------------------------
#   MAJOR IMPORT CLASS
# -----------------------------------------------------------------

class HDFImport (dict):
    """
    A dict holding all information regarding a Kerr-Gate measurement
    including solvent and background data.
    
    Attributes
    ----------
    ['maindata'] : HDFFile
        main sample data
    
    ['solvdata'] : HDFFile
        solvent data
    
    ['lampdata'] : numpy.ndarray
        black body spectrum
    
    observer : callable
        observer function which is called to update current plotting progress
    """
    
    def __init__ (self):
        
        super().__init__()
        self['maindata'] = H5File()
        self['solvdata'] = H5File()
        self['lampdata'] = {'wlns': None, 'spectrum': None}
        self.observer    = None
    
    def update_progress (self, progress, message):
        """ Call observer function to inform about current progress. """
        if self.observer is not None:
            self.observer(progress, message)
    
    def load_main_data (self, filepath):
        """
        Load main measurement from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Returns
        -------
        (int, int)
            available number of scans and status flag regarding first scan
        """
        self['maindata'].load_data(filepath)
        flag = self['maindata'].initialize_dataset()
        return self['maindata']['nscans'], flag
    
    def load_solv_data (self, filepath):
        """
        Load solvent measurement from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Returns
        -------
        (int, int)
            available number of scans and status flag regarding first scan
        """
        self['solvdata'].load_data(filepath)
        flag = self['solvdata'].initialize_dataset()
        return self['solvdata']['nscans'], flag

    def load_lamp_data (self, filepath):
        """
        Load black body measurement from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        Returns
        -------
        (np.ndarray, np.ndarray)
            lamp spectrum, wln axis
        """
        with h5py.File(filepath, 'r') as f:
            self['lampdata']['wlns'] = f['ccd_pixel_var'][:-1]
            self['lampdata']['spectrum'] = f['single_spectra'][0, :-1]
        return self['lampdata']['spectrum'], self['lampdata']['wlns']
    
    def load_bg_data (self, filepath, wln_tolerance=0):
        """
        Load background for main data from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        wln_tolerance : float
            absolute tolerance of wavelength axes values
        """
        self['maindata'].load_bg(filepath, wln_tolerance)
    
    def load_solv_bg_data (self, filepath, wln_tolerance=0):
        """
        Load background for solvent data from file.
        
        Parameters
        ----------
        filepath : str
            path of the file to be loaded
        
        wln_tolerance : float
            absolute tolerance of wavelength axes values
        """
        self['solvdata'].load_bg(filepath, wln_tolerance)
    
    def check_solv_dimensions (self, wln_tol, time_tol):
        """
        Check wavelength and time axes of main and solvent data for consistency.
        
        Wavelength axes need to be fully identical within a tolerance range of
        `wln_tol` wavelength units.
        Time axes values need to be identical with a tolerance of `time_tol`.
        
        Parameters
        ----------
        wln_tol : float
            absolute tolerance of wavelength axes values
        
        time_tol : float
            relative tolerance of time axes values
        
        Returns
        -------
        bool
            True, if dimensions are compatible
        
        Raises
        -------
        pcerror.RangeError
            if wavelength axes of main and solvent data do not match
            if time axes of main and solvent data do not match
        """
        if not pc.consistent_axes(self['maindata']['wlns'], self['solvdata']['wlns'], atol=wln_tol, rtol=0):
            raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_WLNS_HDF_ERROR')
        
        if not pc.consistent_axes(self['maindata']['times'], self['solvdata']['times'], atol=0, rtol=time_tol):
            raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_TIMES_HDF_ERROR')
        
        return True
    
    def check_lamp_dimensions (self, wln_tol):
        """
        Check wavelength axes of main and lamp data for consistency.
        
        Wavelength axes need to be fully identical within a tolerance range of `wln_tol` wavelength units.
        
        Parameters
        ----------
        wln_tol : float
            absolute tolerance of wavelength axes values
        
        Returns
        -------
        bool
            True, if dimensions are compatible
        
        Raises
        -------
        pcerror.RangeError
            if wavelength axes of main and solvent data do not match
        """
        if not pc.consistent_axes(self['maindata']['wlns'], self['lampdata']['wlns'], atol=wln_tol, rtol=0):
            raise pcerror.RangeError('ERROR_IMPORT_INCOMPATIBLE_WLNS_LAMP_HDF_ERROR')
        else:
            return True
    
    def correct_main_data (self, maindict, solvdict, sensdict, sellmeierdict, axes_tolerances, observer_func):
        """
        Correct main data according to user settings.
        
        Parameters
        ----------
        maindict : dict
            dictionary containing information on the main data correction
            ('bgcorr': bool, 'refcorr': bool, 'pdcorr': bool, 'scans': list of int)
        
        solvdict : dict or None
            dictionary containing information on the main data correction
            ('bgcorr': bool, 'refcorr': bool, 'pdcorr': bool, 'scans': list of int
             'absorption': float, 'method': ['factor' or 'absorption' or 'transmission' or 'free'])
        
        sensdict : dict or None
            dictionary containing informatiomn on the spectral correction
            ('temperature': float, 'offset': float)
        
        sellmeierdict : dict or None
            dictionary containing information on the time zero correction
            ('list': list of tuple of str (material) and float (thickness), 'offset': float,
             'handle': pcsellmeier.SellmeierCoefficients)
        
        axes_tolerances : tuple of float
            wavelength and time axes tolerances
        
        observer_func : callable
            function to call if progress has changed
        
        Returns
        -------
        (numpy.ndarray, numpy.ndarray, numpy.ndarray)
            corrected main data values, their wavelength and time axis values
        
        Raises
        ------
        pcerror.InternalError
            if something unexpected happens
        """
        self.observer = observer_func
        try:
            self.update_progress(0, 'STATUS_IMPORT_PREPARE_CORRECTION')
            
            # pre-flight checks
            if solvdict is not None:
                self.check_solv_dimensions(*axes_tolerances)
            if sensdict is not None:
                self.check_lamp_dimensions(axes_tolerances[0])
            
            # prepare sensitivity correction
            if sensdict is not None:
                bb = pc.black_body_curve(sensdict['temperature'], self['lampdata']['wlns'])
                lamp = self['lampdata']['spectrum'] - self['maindata']['dark'] - sensdict['offset']
                sens = lamp / bb
            else:
                sens = np.ones_like(self['maindata']['wlns'])
            
            # prepare gate efficiency correction
            center_wln = np.median(self['maindata']['wlns'])
            gate_efficiency = self['maindata']['wlns']**2 / center_wln**2
            
            # main data calculation
            self.update_progress(10, 'STATUS_IMPORT_CORR_MAIN')
            self['maindata'].calculate_corrected_signal(maindict['bgcorr'], maindict['refcorr'], maindict['pdcorr'], maindict['scans'])
            main = np.empty_like(self['maindata']['signal'])
            for time in range(self['maindata']['ntimes']):
                for wln in range(self['maindata']['nwln']):
                    main[time,wln] = self['maindata']['signal'][time,wln] / sens[wln] * gate_efficiency[wln]
            
            # solvent correction
            if solvdict is not None:
                self.update_progress(30, 'STATUS_IMPORT_CORR_SOLV')
                self['solvdata'].calculate_corrected_signal(solvdict['bgcorr'], solvdict['refcorr'], solvdict['pdcorr'], solvdict['scans'])
                solv = np.empty_like(self['solvdata']['signal'])
                for time in range(self['solvdata']['ntimes']):
                    for wln in range(self['solvdata']['nwln']):
                        solv[time,wln] = self['solvdata']['signal'][time, wln] / sens[wln] * gate_efficiency[wln]
                solvfactor = pc.calculate_solvent_factor(solvdict['absorption'], solvdict['method'])
            else:
                solv = 0
                solvfactor = 0
            
            # time zero correction
            if sellmeierdict is not None:
                self.update_progress(50, 'STATUS_IMPORT_SELLMEIER_CORR')
                
                # prepare
                shape = (self['maindata']['times'].size, self['maindata']['wlns'].size)
                shifted_data        = np.empty(shape)
                shifted_time_axes   = np.empty(shape)
                if solvdict is not None:
                    shifted_solvent = np.empty(shape)
                else:
                    shifted_solvent = 0
                
                # calculate time zeros
                timezeros = sellmeierdict['handle'].calculate_time_shifts(self['maindata']['wlns'], sellmeierdict['list'])
                timezeros = timezeros - np.min(timezeros) - sellmeierdict['offset']
                
                # shift time axis and interpolate data
                for wln in range(self['maindata']['wlns'].size):
                    shifted_time_axes[:, wln] = self['maindata']['times'] - timezeros[wln]
                    shifted_data[:, wln] = np.interp(self['maindata']['times'], shifted_time_axes[:,wln], main[:,wln])
                    if solvdict is not None:
                        shifted_solvent[:, wln] = np.interp(self['solvdata']['times'], shifted_time_axes[:,wln], solv[:,wln])
                
                self.update_progress(85, 'STATUS_IMPORT_SUBSTRACT_SOLV')
                corrdata = shifted_data - shifted_solvent * solvfactor
            
            else:
                self.update_progress(85, 'STATUS_IMPORT_SUBSTRACT_SOLV')
                corrdata = main - solv * solvfactor

            self.update_progress(100, '')
            return corrdata, self['maindata']['wlns'], self['maindata']['times']
        
        except (pcerror.InputError, pcerror.RangeError):
            raise
        
        except:
            raise pcerror.InternalError('ERROR_HDF_INTERNAL_ERROR')
