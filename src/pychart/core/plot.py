import math
from enum import Enum

import numpy as np

import matplotlib as mpl
from matplotlib.gridspec import GridSpec        as mplGridSpec
from matplotlib.text     import Annotation      as mplAnnotation
from matplotlib.patches  import ConnectionPatch as mplConnectionPatch
from matplotlib.patches  import Rectangle       as mplRectangle
from matplotlib.ticker   import ScalarFormatter as mplScalarFormatter
from matplotlib.ticker   import FuncFormatter   as mplFuncFormatter

from . import pychart as pc
from . import error as pcerror

""" Special identifiers for auto labeling and last delay time. """
PLOT_AUTO   = 'auto'
PLOT_OFFSET = 'offset'

""" Threshold to evaluate small floats as zero. """
# cf. docstring of Plot.get_time_cut
PLOT_ZERO_THRESHOLD = 0.00001

""" Scaling ranges of spectra in linear mode. """
# cf. docstring of Plot.calculate_spectrum_offset
PLOT_SPECTRA_SCALING_MIN = -100
PLOT_SPECTRA_SCALING_MAX =  200

#-----------------------------------------------------------------
#   LAYOUT ENUMS
#-----------------------------------------------------------------

class PlotType (Enum):
    """
    An Enum defining different plot types.
    
    Members
    -------
    LinLog : 1
        combined linear and logarithmic time axis
    
    Lin : 2
        linear-only time axis
    
    Log : 3
        logarithmic-only time axis
    """
    LinLog, Lin, Log = 1, 2, 3

class PlotLayout (Enum):
    """
    An Enum defining different kinds of plot layouts.
    
    Members
    -------
    Linear : 1
        traces - contour - transient spectra
    
    LType : 2
        traces - contour | transient spectra @ top
    
    TType : 3
        traces - contour - transient spectra | DAS @ top
    """
    Linear, LType, TType = 1, 2, 3

class ColorbarPosition (Enum):
    """
    An Enum defining the position of the colorbar.
    
    Members
    -------
    Top : 1
        colorbar above contour representation
    
    Bottom : 2
        colorbar below contour representation
        
    Right : 3
        colorbar next to very right plot, either spectra or contour
    """
    Top, Bottom, Right = 1, 2, 3

class ColorbarStyle (Enum):
    """
    An Enum defining the style of the colorbar regarding out-of-range values.
    
    Members
    -------
    NoneStyle : 0
        colorbar with no indications for out-of-range colors
    
    Triangular : 1
        colorbar with triangular ends for out-of-range colors
        
    Rectangular : 2
        colorbar with rectangular ends for out-of-range colors
    """
    NoneStyle, Triangular, Rectangular = 0, 1, 2

#-----------------------------------------------------------------
#   MAJOR PLOT CLASS
#-----------------------------------------------------------------

class Plot (dict):
    """
    A dict holding all setup information necessary to plot a contour, 
    wavelength traces as well as transient spectra. Extended by corresponding
    plotting functions.
    
    Attributes
    ----------
    figure : matplotlib.Figure
        the figure to which the plot is drawn
        
    grid : matplotlib.GridSpec
        subplot grid
        
    contour_log : matplotlib.subplot
        the subplot for the logarithmic part of the contour representation
        
    log_map : matplotlib.ContourSet
        the contour set containing the logarithmic contour representation
        
    contour_lin : matplotlib.subplot
        the subplot for the linear part of the contour representation
        
    lin_map : matplotlib.ContourSet
        the contour set containing the linear contour representation
        
    contour : matplotlib.subplot
        the subplot for an lin or log only contour representation
        
    map : matplotlib.ContourSet
        the contour set containing the lin or log only contour representation
        
    traces_log : matplotlib.subplot
        the subplot for the logarithmic part of the time traces plot
        
    traces_lin : matplotlib.subplot
        the subplot for the linear part of the time traces plot
    
    traces : matplotlib.subplot
        the subplot for an lin or log only time traces plot
    
    spectra : matplotlib.subplot
        the subplot for the transient spectra
        
    colorbar : matplotlib.subplot
        the subplot for the colorbar
    
    ['title'] : str
        the title of the plot
        
    ['datafile'] : str
        data file path
        
    ['data'] : numpy.ndarray
        2-dim array containing the absorption data
        
    ['datafactor'] : float
        factor to multiply the data prior to plotting
        
    ['wlnaxis'] : numpy.ndarray
        1-dim array containing the wavelength data
        
    ['timeaxis'] : numpy.ndarray
        1-dim array containing the time data
        
    ['units'] : dict
        dictionary containing units
        
        ['wln'] : pychart.WavelengthUnit
            the wavelength unit
            
        ['time'] : pychart.TimeUnit
            the time unit
            
        ['abs'] : pychart.AbsorptionUnit
            the absorption unit
            
    ['ranges'] : dict
        dictionary containing the ranges for plotting
    
        ['wln'] : dict
            dictionary containing wavelength ranges
        
            ['min'] : float
                lower wavelength bound
                
            ['max'] : float
                upper wavelength bound
                
        ['time'] : dict
            dictionary containing time ranges
            
            ['min'] : float
                lower time bound
                
            ['max'] : float
                upper time bound
                
            ['cut'] : float
                position of time axis break between lin and log part
            
        ['abs'] :  dict
            dictionary containing absorption ranges
        
            ['min'] : float
                lower absorption bound
                
            ['max'] : float
                upper absorption bound
                
            ['step'] : float
                step width between absorption levels of contour representation
    
    wln_min_i : int
        index of lower wavelength bound
    
    wln_max_i : int
        index of upper wavelength bound
    
    time_min_i : int
        index of lower time bound
    
    time_cut_i : int
        index of time cut
    
    time_max_i : int
        index of upper time bound
    
    observer : callable
        observer function which is called to update current plotting progress
    
    ['contour'] : dict
        dictionary containing settings about contour plots
    
        ['contourlevel'] : numpy.ndarray
            array containing the contour levels
        
        ['contourticks'] : list of float
            list of wavelength ticks for contour plot
        
        ['colormap'] : dict
            dictionary regarding the colormap
            
            ['name'] : str
                name of the colormap used for plotting
                
            ['map'] : mpl.LinearSegmentedColormap
                colormap used for plotting
            
            ['flip'] : bool
                whether or not to use a reversed colormap
            
            ['over'] : tuple
                color for high out-of-range values
            
            ['under'] : tuple
                color for low out-of-range values
            
            ['nan'] : tuple
                color for undefined values
            
            ['overon'] : bool
                whether to use the given color for over extending values
            
            ['underon'] : bool
                whether to use the given color for under extending values
            
            ['colorbarstyle'] : plot.ColorbarStyle
                how colors for extending values should be displayed
            
            ['tickstep'] : float
                step width between colorbar ticks
    
    ['traces'] : dict
        dictionary containing settings about time traces
    
        ['list'] : list of Plot.WavelengthTrace
            list of all traces to be drawn
            
        ['xlim'] : list of float
            absorption axis limits
            
        ['xticks'] : list of float
            list of ticks for absorption axis
        
        ['labelsize'] : float
            size of wavelength labels
        
        ['lw'] : float
            wavelength lines' line width
            
        ['ls'] : ['-.', '--', ':', '-']
            wavelength lines' line style
        
        ['baseline'] : dict
            dictionary regarding the traces' baseline
            
                ['on'] : bool
                    whether or not to draw the baseline
            
                ['value'] : float
                    absorption value for baseline
                    
                ['color'] : tuple
                    color of baseline
                    
                ['ls'] : ['-.', '--', ':', '-']
                    baseline's line style
                    
                ['lw'] : float
                    baseline's line width
                    
                ['zorder'] : int
                    baseline's zorder
    
    ['spectra'] = dict
        dictionary containing settings about transient spectra    
    
        ['list'] : list of Plot.TransientSpectrum
            list of all spectra to be drawn
            
        ['scaling'] : [float, list of float]
            scaling factor for transient spectra if they are plotted on the
            right; absorption limits if they are plotted above the contour
            
        ['xticks'] : list of float
            list of ticks for wavelength axis
        
        ['yticks'] : list of float
            list of ticks for amplitude axis
        
        ['labelsize'] : float
            size of time labels
        
        ['lw'] : float
            time lines' line width
            
        ['ls'] : ['-.', '--', ':', '-']
            time lines' line style
            
        ['label]' : bool
            whether or not to display time labels within contour plot
            
        ['longtimeline'] : bool
            whether or not to expand time lines into traces plot
            
        ['scalebar']: dict
            dictionary containing settings about scalebar
            
            ['value'] : float
                height of scalebar
                
            ['label'] : str
                labeling of scalebar
                
            ['poswln'] : float
                scalebar's position on wavelength axis
                
            ['posy'] : float
                scalebar's position in y-dimension
                
            ['on'] : bool
                whether or not to display the scalebar
        
        ['baseline'] : dict
            dictionary regarding the spectras' baseline
            
                ['on'] : bool
                    whether or not to draw the baseline
            
                ['value'] : float
                    absorption value for baseline
                    
                ['color'] : tuple
                    color of baseline
                    
                ['ls'] : ['-.', '--', ':', '-']
                    baseline's line style
                    
                ['lw'] : float
                    baseline's line width
                    
                ['zorder'] : int
                    baseline's zorder
    
    ['misc'] : dict
        dictionary containing some additional settings
        
        ['timetickslin'] : list of float
            list of ticks for linear part of time axis
        
        ['timetickslog'] : list of int
            list of ticks for logarithmic part of time axis
    
    ['axeslabel'] : dict
        dictionary containing axes labels
            
        ['titleon'] : bool
            whether or not to show the plot's title
            
        ['wlncontour'] : str
            label of the contour's wavelength axis
            
        ['wlntransients'] : str
            label of the spectra's wavelength axis
            
        ['time'] : str
            label of the time axis
            
        ['timedec'] : bool
            whether or not to display the time axis decimal or scientific
        
        ['abstraces'] : str
            label of the traces' absorption axis
            
        ['abscolorbar'] : str
            label of the colorbar
    
    ['layout'] : dict
        dictionary specifying the overall layout
        
        ['widths'] : list of int
            relative widths of traces, contour and spectra plots
            
        ['heights'] : list of int
            relative heights of all six plot parts
        
        ['type'] : plot.PlotType
            time axis type
        
        ['design'] : plot.PlotLayout
            overall layout
        
        ['colorbarpos'] : plot.ColorbarPosition
            position of the colorbar
        
        translator : i18n.Translator
            reference to the applications translator instance
    
    ['artists'] : dict
        dictionary containing settings about additional artists
    """
    
    def __init__ (self, translator):
        
        super().__init__()
        self['title'] = ''
        self['datafile'] = ''
        self['data'] = None
        self['datafactor'] = 1
        self['wlnaxis'] = None
        self['timeaxis'] = None
        self['units'] = {'wln': None, 'time': None, 'abs': None}
        self['ranges'] = {
            'wln':  {'min': 0, 'max': 0},
            'time': {'min': 0, 'max': 0, 'cut':  0},
            'abs':  {'min': 0, 'max': 0, 'step': 0}
        }
        
        self.wln_min_i, self.wln_max_i = 0, 0
        self.time_min_i, self.time_cut_i, self.time_max_i = 0, 0, 0
        
        self.figure = None
        self.grid = None
        self.contour_log, self.contour_lin, self.contour = None, None, None
        self.log_map,     self.lin_map,     self.map     = None, None, None
        self.traces_log,  self.traces_lin,  self.traces  = None, None, None
        self.spectra,     self.colorbar                  = None, None
        
        self.observer = None
        
        self['contour'] = {
            'contourlevel': None, 'contourticks': [],
            'colormap': {
                'name': '',       'map': None,      'flip': False,
                'over': None,     'under': None,    'nan': None,
                'overon': False,  'underon': False, 'colorbarstyle': False,
                'tickstep': 0}
        }
        self['traces'] = {
            'list': [],     'xlim': [], 'xticks': [],
            'labelsize': 0, 'lw': 0,    'ls': '',
            'baseline': {
                'on': False, 'value': 0, 'color': (0,0,0),
                'ls': '',    'lw': 0,    'zorder': 0}
        }
        self['spectra'] = {
            'list': [],      'scaling': 0,   'xticks': [],
            'yticks': [],    'labelsize': 0, 'lw': 0,
            'ls': '',        'label': False, 'longtimeline': False,
            'scalebar': {
                'value': 0,  'label': '',    'poswln': 0,
                'posy': 0,   'on': True},
            'baseline': {
                'on': False, 'value': 0,     'color': (0,0,0),
                'ls': '',    'lw': 0,        'zorder': 0}
        }
        self['misc'] = {
            'timetickslin': [], 'timetickslog': []
        }
        self['axeslabel'] = {
            'titleon': True, 'wlncontour': '', 'wlntransients': '',
            'time': '',      'timedec': True,  'abstraces': '',
            'abscolorbar': ''
        }
        self['layout'] = {
            'widths': [],   'heights': [], 'type': None,
            'design': None, 'colorbarpos': None
        }
        self['artists'] = []
        
        self.translator = translator
    
    def data_loaded (self):
        """ Returns True if some data is currently loaded, otherwise False. """
        return self['data'] is not None
    
    def update_progress (self, progress, message):
        """ Call observer function to inform about current progress. """
        if self.observer is not None:
            self.observer(progress, message)
    
    #-----------------------------------------------------------------
    #   HELPER CLASSES
    #-----------------------------------------------------------------
    
    class WavelengthTrace (dict):
        """
        A dictionary storing all information
        about one wavelength and its trace.
        
        Attributes
        ----------
        ['wln'] : float
            wavelength
        
        ['index'] : int
            wavelength's index within the data array
            
        ['factor'] : float
            optional scaling factor
        
        ['color'] : tuple
            color to be drawn in
            
        ['label'] : str
            label text
        
        ['label_traces'] : bool
            whether or not to display the label within traces plot
        
        ['trace_x'] : float
            label's position on the linear trace's absorption axis
            
        ['trace_y'] : float
            label's position on the linear trace's time axis
        
        ['traces_rot'] : int
            rotation of label within traces plot
        
        ['label_contour'] : bool
            whether or not to display the label within contour plot
        
        ['contour_x'] : float
            label's position on the linear contour's absorption axis
            
        ['contour_y'] : float
            label's position on the linear contour's time axis
        
        ['contour_rot'] : int
            rotation of label within contour plot
        
        Notes
        -----
        Wavelength traces relying on external file use `ExtWavelengthTrace`.
        """
        
        def __init__ (self):
            super().__init__()
            self['wln']           = 0
            self['index']         = 0
            self['factor']        = 1
            self['color']         = (0,0,0)
            self['label']         = ''
            self['label_traces']  = True
            self['trace_x']       = 0
            self['trace_y']       = 0
            self['trace_rot']     = 90
            self['label_contour'] = False
            self['contour_x']     = 0
            self['contour_y']     = 0
            self['contour_rot']   = 0
    
    class ExtWavelengthTrace (WavelengthTrace):
        """
        A dictionary storing all information about one wavelength and its 
        trace, which data is taken from an additional file.
        
        Attributes
        ----------
        ['path'] : str
            file path in which the wavelength trace data is stored
            
        ['data'] : numpy.ndarray
            2-dim array containing the time axis (first column) and absorption
            values (second column)
        
        ['lim_amax'] : float
            upper absorption axis limit
        
        ['lim_amin'] : float
            lower absorption axis limit
        
        Notes
        -----
        The following attributes inherited from `WavelengthTrace` have 
        different meanings:
        
        ['index'] : int
            index of time cut
        """
        
        def __init__ (self):
            super(Plot.WavelengthTrace, self).__init__()
            self['path']     = ''
            self['data']     = None
            self['lim_amax'] = 0
            self['lim_amin'] = 0
    
    class TransientSpectrum (dict):
        """
        A dictionary storing all information
        about one transient spectrum and its trace.
        
        Attributes
        ----------
        ['time'] : float
            time
            
        ['index'] : int
            time's index within the data array
            
        ['factor'] : float
            optional scaling factor
            
        ['color'] : tuple
            color to be drawn in
        
        ['offset'] : float
            offset the spectrum has to be shifted of 
            (cf. `Plot.calculate_spectrum_offset` for more information)
        
        ['label'] : str
            label text
        
        ['label_on'] : bool
            whether or not to display the label
        
        ['labelcolor'] : str
            label's text color
            
        ['contour_x'] : float
            label's position on the contour's absorption axis
            
        ['contour_y'] : float
            label's position on the contour's time axis
        """
        
        def __init__ (self):
            super().__init__()
            self['time']       = 0
            self['index']      = 0
            self['factor']     = 1
            self['color']      = (0,0,0)
            self['offset']     = 0
            self['label']      = ''
            self['label_on']   = False
            self['labelcolor'] = (0,0,0)
            self['contour_x']  = 0
            self['contour_y']  = 0
            
    class ExtTransientSpectrum (TransientSpectrum):
        """
        A dictionary storing all information about one transient spectrum and 
        its trace, which data is taken from an additional file.
        
        Attributes
        ----------
        ['path'] : str
            file path in which the transient spectrum data is stored
            
        ['data'] : numpy.ndarray
            2-dim array containing the wavelength axis (first column) and 
            absorption values (second column)
        
        ['lim_amax'] : float
            upper absorption axis limit
        
        ['lim_amin'] : float
            lower absorption axis limit
        
        Notes
        -----
        The attribute ['offset'] inherited from `TransientSpectrum` has
        no more meaning.
        """
        
        def __init__ (self):
            super(Plot.TransientSpectrum, self).__init__()
            self['time']     = 0
            self['path']     = ''
            self['data']     = None
            self['lim_amin'] = 0
            self['lim_amax'] = 0
    
    #-----------------------------------------------------------------
    #   INITIALIZATION FUNCTIONS
    #-----------------------------------------------------------------
    
    def calculate_spectrum_offset (self, time, minB, maxB):
        """
        Calculate the offset for transient spectrum needed to place the 
        baseline (y=0) at the same level as the respective time in the
        contour plot (y=time).
        
        Parameters
        ----------
        time : float
            delay time of the the spectrum which offset should be calculated
        
        minB : float
            lower bound of vertical axis in which the spectrum will be drawn
        
        maxB : float
            upper bound of vertical axis in which the spectrum will be drawn
        
        Returns
        -------
        float
            offset value of which the spectrum has to be shifted
        
        Notes
        -----
        Algorithm explanation:
        
        - The scaling ranges for plotting the transient spectra are arbitrary.
          They are set by global variables PLOT_SPECTRA_SCALING_MIN and
          PLOT_SPECTRA_SCALING_MAX, respectively. The scaling factor defined
          by the user is applied by multiplying it with these ranges. This yields
          `minB` and `maxB`.
          
          By default PLOT_SPECTRA_SCALING_MIN and PLOT_SPECTRA_SCALING_MAX are
          -100 and +200.
          
                                                          |-------------------| maxB
                                                          |                   |
         max |--------------|-----------------------------|                   | topB
             |              |                             |                   |
             |              |                             |                   |
             |  traces_log  |         contour_log         |      spectra      |
             |              |                             |                   |
             |              |                             |                   |
         cut |--------------|-----------------------------|                   | cutB
             |              |                             |                   |
             |  traces_lin  |         contour_lin         |                   |
        time.|. . . . . . . |. . . . . . . . . . . . . . .| . . . . . . . . . |.offset
             |              |                             |                   |
         min |--------------|-----------------------------|-------------------| minB
        
        - According to the definition of the plot heights and the above diagram
            a = self['layout']['heights'][1] = maxB |<- - ->| topB ,
            b = self['layout']['heights'][2] = topB |<- - ->| cutB ,
            c = self['layout']['heights'][3] = cutB |<- - ->| minB .
        
        - This results in the following equations, which allow to calculate the
          values cutB as well as topB within [minB, maxB].
        
            cutB - minB    c                 maxB * c + minB * (a+b)
            ----------- = ---   <=>   cutB = -----------------------
            maxB - cutB   a+b                        a+b+c
            
            maxB - topB    a                 minB * a + maxB * (b+c)
            ----------- = ---   <=>   topB = -----------------------
            topB - minB   b+c                        a+b+c
        
        - The offset describes the value of the scaled axis, which is at the same
          height as the time the spectrum belongs to. By shifting the spectrum by
          this offset its baseline (ΔA = 0) will be at the corresponding time line.
          
        - The final calculation is based on known time points (min, cut, time) as
          well as scaling-dependent points (minB, cutB, maxB).
        
            offset - minB   time - min                  cutB * (time - min) + minB * (cut - time)
            ------------- = ----------   <=>   offset = -----------------------------------------
             cutB - minB    cut - min                                   cut - min
        
        - In case of a single-axis plot (linear or logarithmic only) cut and cutB
          do not exist. The determination then requires topB.
        
            offset - minB   time - min                  topB * (time - min) + minB * (max - time)
            ------------- = ----------   <=>   offset = -----------------------------------------
             topB - minB    max - min                                   max - min
        
          For logarithmic plots the same equation can be used when the logarithm of
          the points on the time axis (min, max, cut) is taken.
        
        - If the time is on the logarithmic part of a LinLog plot, min might have
          a negative value prohibiting the use of the log function. Instead,
          another relation relying on points guaranteed to be positive is needed.
          In fact, min can simply be replaced by cut since time (and the offset)
          are now above the time cut, which can serve as a new anchor.
          
            offset - cutB   lg time - lg cut                  topB * (lg time - lg cut) + cutB * (lg max - lg time)
            ------------- = ----------------   <=>   offset = -----------------------------------------------------
             topB - cutB    lg max - lg cut                                     lg max - lg cut
        """
        a, b, c = self['layout']['heights'][1], self['layout']['heights'][2], self['layout']['heights'][3]
        _min, _max, _cut = self['ranges']['time']['min'], self['ranges']['time']['max'], self['ranges']['time']['cut']
        
        cutB = (maxB * c + minB * (a + b)) / (a + b + c)
        topB = (minB * a + maxB * (b + c)) / (a + b + c)
        if self['layout']['type'] is PlotType.LinLog:
            if time < self['ranges']['time']['cut']:
                return ((cutB * (time - _min)) + (minB * (_cut - time))) / (_cut - _min)
            else:
                return ((topB * (math.log10(time) - math.log10(_cut))) + (cutB * (math.log10(_max) - math.log10(time)))) / (math.log10(_max) - math.log10(_cut))
        elif self['layout']['type'] is PlotType.Lin:
            return ((topB * (time - _min)) + (minB * (_max - time))) / (_max - _min)
        else:
            return ((topB * (math.log10(time) - math.log10(_min))) + (minB * (math.log10(_max) - math.log10(time)))) / (math.log10(_max) - math.log10(_min))
        
    def load_data (self, file):
        """
        Load data from file and extract axes.
        
        Imports data from given `file`. The first line has to contain the 
        wavelengths with a leading 0 to even the grid; first column has to 
        consist of the delay times.
        
        The data will be multiplied by `Plot['datafactor']` after import.
        
        Parameters
        ----------
        file : str
            path of the file to be loaded
        
        Raises
        ------
        pcerror.InputError
            if imported data is not of shape (ntimes, nwlns) with ntimes, nwlns >= 3
        """
        rawdata = pc.import_numpy_file(file)
        self['datafile'] = file
        if rawdata.ndim != 2 or rawdata.shape[1] < 3 or rawdata.shape[0] < 3:
            raise pcerror.InputError('ERROR_PLOT_IMPORT_DATA_ERROR', input=file)
        self['wlnaxis'] = rawdata[0,1:]
        self['timeaxis'] = rawdata[1:,0]
        self['data'] = np.delete(np.delete(rawdata,0,0), 0, 1) * self['datafactor']
    
    def initialize (self):
        """
        Initialize figure, grid and certain values.
        
        Creates all necessary subplots bases on the chosen layout. Axes ranges
        and other given values are checked and errors raised when values are 
        invalid.
        
        Raises
        ------
        pcerror.RangeError
            if index of minimum and maximum wavelength are identical
        
        pcerror.RangeError
            if minimum time is not lower than cut time or cut time is not lower 
            than maximum time
        
        pcerror.RangeError
            if cut time is zero
            
        pcerror.RangeError
            if less than two absorption levels are defined (minimum absporption 
            plus two absorption steps has to be lower than maximum absorption)
        
        pcerror.RangeError
            if absorption step is zero
        
        pcerror.RangeError
            if scaling factor for transient spectra is zero
        """
        
        def initialize_external_trace (trace):
            """
            Load and prepare time trace data from additional file.
            
            Imports data for `trace` and scales it to the traces plot limits. 
            Index position of time cut is looked up.
            
            Parameters
            ----------
            trace : Plot.ExtWavelengthTrace
                external trace to be loaded
            
            Raises
            ------
            pcerror.InputError
                if imported data is not of shape (ntimes, 2)
            """
            trace['data'] = pc.import_numpy_file(trace['path'])
            if trace['data'].ndim != 2 or trace['data'].shape[1] != 2:
                raise pcerror.InputError('ERROR_PLOT_EXT_IMPORT_DATA_ERROR', input=trace['path'])
            rawamps = trace['data'][:,1]
            trace['data'][:,1] = self['traces']['xlim'][0] - ((self['traces']['xlim'][0] - self['traces']['xlim'][1]) * (trace['lim_amax'] - rawamps) / (trace['lim_amax'] - trace['lim_amin']))
            trace['index'] = pc.lookup_index(self['ranges']['time']['cut'], trace['data'][:,0].tolist())
        
        def initialize_external_spectrum (spectrum):
            """
            Load and prepare transient spectrum from additional file.
            
            Imports data for `spectrum` and scales it to the spectra plot limits. 
            
            Parameters
            ----------
            spectrum : Plot.ExtTransientSpectrum
                external spectrum to be loaded
            
            Raises
            ------
            pcerror.InputError
                if imported data is not of shape (nwlns, 2)
            """
            spectrum['data'] = pc.import_numpy_file(spectrum['path'])
            if spectrum['data'].ndim != 2 or spectrum['data'].shape[1] != 2:
                raise pcerror.InputError('ERROR_PLOT_EXT_IMPORT_DATA_ERROR', input=spectrum['path'])
            rawamps = spectrum['data'][:,1]
            if self['layout']['design'] is not PlotLayout.LType:
                amin = PLOT_SPECTRA_SCALING_MIN / self['spectra']['scaling']
                amax = PLOT_SPECTRA_SCALING_MAX / self['spectra']['scaling']
            else:
                amin = self['spectra']['scaling'][0]
                amax = self['spectra']['scaling'][1]
            spectrum['data'][:,1] = amax - ((amax - amin) * (spectrum['lim_amax'] - rawamps) / (spectrum['lim_amax'] - spectrum['lim_amin']))
        
        self['contour']['colormap']['map'] = pc.generate_custom_cmap(
            self['contour']['colormap']['name'], self['contour']['colormap']['flip'],
            self['contour']['colormap']['over'], self['contour']['colormap']['under'],
            self['contour']['colormap']['nan'])
        
        # generate subplots from grid
        if self['layout']['type'] is PlotType.LinLog:
            self.contour_log = self.figure.add_subplot(self.grid[2,1])
            self.contour_lin = self.figure.add_subplot(self.grid[3,1], sharex=self.contour_log)
            self.traces_log  = self.figure.add_subplot(self.grid[2,0])
            self.traces_lin  = self.figure.add_subplot(self.grid[3,0], sharex=self.traces_log)
        else:
            self.contour = self.figure.add_subplot(self.grid[2:4,1])
            self.traces  = self.figure.add_subplot(self.grid[2:4,0])
        if self['layout']['design'] is PlotLayout.LType:
            self.spectra = self.figure.add_subplot(self.grid[:2,1], sharex=self.contour_log)
        else:
            self.spectra = self.figure.add_subplot(self.grid[1:4,2])
        colorbarpos = self['layout']['colorbarpos']
        if colorbarpos is ColorbarPosition.Top:
            self.colorbar = self.figure.add_subplot(self.grid[:2,1])
        elif colorbarpos is ColorbarPosition.Right:
            self.colorbar = self.figure.add_subplot(self.grid[1:3,2])
        else:
            self.colorbar = self.figure.add_subplot(self.grid[5,1])
        
        # look up indices for wlns and check for consistent values
        self.wln_min_i = pc.lookup_index(self['ranges']['wln']['min'], self['wlnaxis'].tolist())
        self.wln_max_i = pc.lookup_index(self['ranges']['wln']['max'], self['wlnaxis'].tolist())
        if self.wln_min_i == self.wln_max_i:
            raise pcerror.RangeError('ERROR_PLOT_WLN_RANGE_ERROR')
        for trace in self['traces']['list']:
            if type(trace) is Plot.ExtWavelengthTrace:
                initialize_external_trace(trace)
            else:
                trace['index'] = pc.lookup_index(trace['wln'], self['wlnaxis'].tolist())
        
        # check amplitudes and scaling factor for consistent values
        if (self['ranges']['abs']['min'] + 2 * self['ranges']['abs']['step']) > self['ranges']['abs']['max']:
            raise pcerror.RangeError('ERROR_PLOT_ABS_RANGE_ERROR')
        elif self['ranges']['abs']['step'] == 0:
            raise pcerror.RangeError('ERROR_PLOT_ABS_STEP_ERROR')
        else:
            self['contour']['contourlevel'] = np.arange(self['ranges']['abs']['min'],
                self['ranges']['abs']['max'] + self['ranges']['abs']['step'],
                self['ranges']['abs']['step'])
        if self['spectra']['scaling'] == 0:
            raise pcerror.RangeError('ERROR_PLOT_SCALING_ERROR')
        
        # look up indices for times and check for consistent values
        self.time_min_i = pc.lookup_index(self['ranges']['time']['min'], self['timeaxis'].tolist())
        self.time_cut_i = pc.lookup_index(self['ranges']['time']['cut'], self['timeaxis'].tolist())
        self.time_max_i = pc.lookup_index(self['ranges']['time']['max'], self['timeaxis'].tolist())
        if ((self.time_min_i >= self.time_cut_i) or (self.time_cut_i >= self.time_max_i)) and (self['layout']['type'] is PlotType.LinLog):
            raise pcerror.RangeError('ERROR_PLOT_TIME_RANGES_ERROR')
        elif self['ranges']['time']['cut'] == 0:
            raise pcerror.RangeError('ERROR_PLOT_TIME_CUT_ERROR')
        for spectrum in self['spectra']['list']:
            if type(spectrum) is Plot.ExtTransientSpectrum:
                initialize_external_spectrum(spectrum)
            else:
                if spectrum['time'] == PLOT_OFFSET:
                    spectrum['time'] = self['timeaxis'][-1]
                spectrum['index'] = pc.lookup_index(spectrum['time'], self['timeaxis'].tolist())
                if self['layout']['design'] is not PlotLayout.LType:
                    spectrum['offset'] = self.calculate_spectrum_offset(spectrum['time'],
                        PLOT_SPECTRA_SCALING_MIN / self['spectra']['scaling'],
                        PLOT_SPECTRA_SCALING_MAX / self['spectra']['scaling'])
    
    #-----------------------------------------------------------------
    #   AUTO-RANGE FUNCTIONS
    #-----------------------------------------------------------------
    
    def get_lowest_wln (self):
        """
        Get smallest wavelength in data set.
        
        Returns
        -------
        float
            first value of wavelength axis
        
        Raises
        ------
        pcerror.InputError
            if no data is loaded yet
        """
        if self['data'] is not None:
            return self['wlnaxis'][0]
        else:
            raise pcerror.InputError('ERROR_PLOT_MISSING_DATA_ERROR')
    
    def get_largest_wln (self):
        """
        Get largest wavelength in data set.
        
        Returns
        -------
        float
            last value of wavelength axis
        
        Raises
        ------
        pcerror.InputError
            if no data is loaded yet
        """
        if self['data'] is not None:
            return self['wlnaxis'][-1]
        else:
            raise pcerror.InputError('ERROR_PLOT_MISSING_DATA_ERROR')
    
    def get_lowest_time (self):
        """
        Get shortest delay time in data set.
        
        Returns
        -------
        float
            first value of time axis
        
        Raises
        ------
        pcerror.InputError
            if no data is loaded yet
        """
        if self['data'] is not None:
            return self['timeaxis'][0]
        else:
            raise pcerror.InputError('ERROR_PLOT_MISSING_DATA_ERROR')
    
    def get_largest_time (self):
        """
        Get longest delay time in data set.
        
        Returns
        -------
        float
            last value of time axis
        
        Raises
        ------
        pcerror.InputError
            if no data is loaded yet
        """
        if self['data'] is not None:
            return self['timeaxis'][-1]
        else:
            raise pcerror.InputError('ERROR_PLOT_MISSING_DATA_ERROR')
    
    def get_cut_time (self):
        """
        Get time at which the time axis changes from linear to logarithmic.
        
        Returns
        -------
        float
            last time of constant step width
        
        Notes
        -----
        Algorithm explanation:
            
            1. dismiss negative time axis values
            2. calculate differences to next neighbour (aka time step)
            3. calculate differences between time steps
            4. dismiss sign
            5. set all values below PLOT_ZERO_THRESHOLD to zero, all values above to infinity
            6. determine index of first occurrence of highest value (np.inf)
            7. take value at that index + 1
        
        The zero threshold should not be larger than the time resolution of the delay time,
        otherwise small logarithmic steps might not be detected.
        
        Index of interest must be increased by one, since differences are calculated as
        time[i+1] - time[i].
        
        Raises
        ------
        pcerror.InputError
            if no data is loaded yet
        """
        if self['data'] is not None:
            pos_times = [time for time in self['timeaxis'] if time >= 0]
            diffs = np.abs(np.diff(np.diff(pos_times)))
            return pos_times[np.argmax(np.where(diffs < PLOT_ZERO_THRESHOLD, 0, np.inf)) + 1]
        else:
            raise pcerror.InputError('ERROR_PLOT_MISSING_DATA_ERROR')
    
    def get_lowest_abs (self):
        """
        Get lowest absorption value in data set.
        
        Returns
        -------
        float
            minimum value of absorption axis ignoring NINF and NaN
        
        Raises
        ------
        pcerror.InputError
            if no data is loaded yet
        """
        if self['data'] is not None:
            return pc.finite_minimum(self['data'])
        else:
            raise pcerror.InputError('ERROR_PLOT_MISSING_DATA_ERROR')
    
    def get_largest_abs (self):
        """
        Get highest absorption value in data set.
        
        Returns
        -------
        float
            maximum value of absorption axis ignoring NaN and inf
        
        Raises
        ------
        pcerror.InputError
            if no data is loaded yet
        """
        if self['data'] is not None:
            return pc.finite_maximum(self['data'])
        else:
            raise pcerror.InputError('ERROR_PLOT_MISSING_DATA_ERROR')
    
    def get_abs_step_width (self):
        """
        Get reasonable step width between absorption values for use as colorbar 
        step width.
        
        Returns
        -------
        float
            approximate step width of absorption values
        
        Notes
        -----
        Algorithm explanation: 
            
            1. calculate differences to next neighbour
            2. dismiss sign
            3. take mean value (ignoring all NaNs)
            4. round it to one decimal
        
        The result can not be zero except all absorption changes are below 0.1.
        
        Raises
        ------
        pcerror.InputError
            if no data is loaded yet
        """
        if self['data'] is not None:
            return np.around(np.nanmean(np.abs(np.diff(self['data']))), 1)
        else:
            raise pcerror.InputError('ERROR_PLOT_MISSING_DATA_ERROR')
    
    #-----------------------------------------------------------------
    #   PLOT FUNCTIONS
    #-----------------------------------------------------------------
    
    def create_wln_line (self, trace):
        """
        Create vertical line through plots for the given trace.
        
        If `Plot['traces']['longwlnline']` is enabled and the design is
        LType, the line will end at the maximum (upper bound) of the spectra
        plot, otherwise at the maximum time within the contour plot. Line
        always starts from minimum time of the contour plot.
        User settings regarding line width, line style and color are applied.
        
        Parameters
        ----------
        trace : Plot.WavelengthTrace
            wavelength trace for which the wavelength line should be drawn
            
        Returns
        -------
        matplotlib.ConnectionPatch
            the patch which can be added to an existing axis
        """
        expand = self['traces']['longwlnline'] and (self['layout']['design'] is PlotLayout.LType)
        
        if self['layout']['type'] is PlotType.LinLog:
            ax1 = self.contour_lin
            ax2 = self.spectra if expand else self.contour_log
        else:
            ax1 = self.contour
            ax2 = self.spectra if expand else self.contour
        
        end = self['spectra']['scaling'][1] if expand else self['ranges']['time']['max']
        
        line = mplConnectionPatch((trace['wln'], self['ranges']['time']['min']),
            (trace['wln'], end), "data", "data", ax1, ax2)
        line.set_linewidth(self['traces']['lw'])
        line.set_linestyle(self['traces']['ls'])
        line.set_color(trace['color'])
        line.set_annotation_clip(False)
        return line
    
    def create_time_line (self, time):
        """
        Create horizontal line through plots for the given time.
        
        If `Plot['spectra']['longtimeline']` is enabled the line will start 
        at the maximum (left bound) of the traces plot, otherwise from the 
        minimum wavelength within the contour plot.
        Line ends at maximum wavelength either of the spectra plot or of the 
        contour plot, if spectra are drawn above the contour.
        User settings regarding line width, line style and color are applied.
        
        Parameters
        ----------
        time : Plot.TransientSpectrum
            transient spectrum for which the time line should be drawn
        
        Returns
        -------
        matplotlib.ConnectionPatch
            the patch which can be added to an existing axis
        """
        expand = self['spectra']['longtimeline']
        ltype = (self['layout']['design'] is PlotLayout.LType)
        
        if self['layout']['type'] != PlotType.LinLog:
            ax1 = self.traces      if expand else self.contour
            ax2 = self.contour     if ltype  else self.spectra
        elif time['time'] < self['ranges']['time']['cut']:
            ax1 = self.traces_lin  if expand else self.contour_lin
            ax2 = self.contour_lin if ltype  else self.spectra
        else:
            ax1 = self.traces_log  if expand else self.contour_log
            ax2 = self.contour_log if ltype  else self.spectra
        
        start = self['traces']['xlim'][0] if expand else self['ranges']['wln']['min']
        end = time['time']                if ltype  else time['offset']
        
        line = mplConnectionPatch((start, time['time']), 
            (self['ranges']['wln']['max'], end), "data", "data", ax1, ax2)
        line.set_linewidth(self['spectra']['lw'])
        line.set_linestyle(self['spectra']['ls'])
        line.set_color(time['color'])
        line.set_annotation_clip(False)
        return line
    
    def draw_colorbar (self):
        """
        Create a colorbar.
        
        The colorbar is created with some scaling offset and a vertical shift 
        depending on whether the colorbar is displayed at top/bottom or at the 
        right of the contour to avoid overlapping with labels or ticks.
        """
        
        def determine_colorbar_ticks ():
            """
            Return a useful list of colorbar ticks.
            
            Returns a list of integers for use as colorbar ticks, which are 
            centered around zero and spaced by `Plot['contour']['tickstep']`. 
            Minimum and maximum absorptions values will only be the start and 
            end values, if they are multiples of the step width.
            
            Returns
            -------
            list
                list of integers spaced by `Plot['contour']['tickstep']`
            """
            max_ = np.arange(0, abs(self['ranges']['abs']['max']) + self['ranges']['abs']['step'], self['contour']['tickstep'])
            min_ = np.arange(0, abs(self['ranges']['abs']['min']) + self['ranges']['abs']['step'], self['contour']['tickstep'])
            ticks_pos = set(max_ * -1) if self['ranges']['abs']['max'] < 0 else set(max_)
            ticks_neg = set(min_ * -1) if self['ranges']['abs']['min'] < 0 else set(min_)
            
            if (self['ranges']['abs']['max'] >= 0) ^ (self['ranges']['abs']['min'] < 0):  # same sign
                if abs(self['ranges']['abs']['max']) > abs(self['ranges']['abs']['min']):
                    ticks = ticks_pos - ticks_neg
                    if self['ranges']['abs']['min'] in ticks_neg:
                        ticks.add(self['ranges']['abs']['min'])
                else:
                    ticks = ticks_neg - ticks_pos
                    if self['ranges']['abs']['max'] in ticks_pos:
                        ticks.add(self['ranges']['abs']['max'])
            else:  # different sign
                ticks = ticks_pos | ticks_neg
            
            return list(ticks)
        
        l, b, w, h = self.colorbar.get_position().bounds
        if self['layout']['colorbarpos'] is ColorbarPosition.Right:
            ax = self.figure.add_axes((l + 0.2*w, b + 0.1*h, 0.7*w, 0.8*h))
            orientation = 'vertical'
        else:
            ax = self.figure.add_axes((l + 0.05*w, b + 0.875*h, 0.9*w, 0.25*h))
            orientation = 'horizontal'
        m = self.lin_map if self['layout']['type'] is PlotType.LinLog else self.map
        if self['contour']['colorbarstyle'] is ColorbarStyle.NoneStyle:
            rect, frac = False, 0
        elif self['contour']['colorbarstyle'] is ColorbarStyle.Triangular:
            rect, frac = False, None
        else:
            rect, frac = True, None
            
        colorbar = self.figure.colorbar(m, cax=ax, orientation=orientation, 
            extendrect=rect, extendfrac=frac)
        colorbar.set_label(self['axeslabel']['abscolorbar'])
        for i in [0,1]:
            colorbar.ax.get_children()[i].set_linewidth(mpl.rcParams['axes.linewidth'])
        ax.tick_params(width=mpl.rcParams['xtick.major.width'])
        colorbar.set_ticks(determine_colorbar_ticks())
        self.colorbar.set_visible(False)
    
    def draw_baselines (self):
        """
        Draw baselines.
        
        Creates baselines within the traces and / or spectra plot according
        to user settings. User settings regarding line width, line style and
        color are applied.
        """
        if self['traces']['baseline']['on']:
            if self['layout']['type'] is PlotType.LinLog:
                ax1 = self.traces_lin
                ax2 = self.traces_log
            else:
                ax1 = ax2 = self.traces
            baseline = mplConnectionPatch(
                (self['traces']['baseline']['value'], self['ranges']['time']['min']),
                (self['traces']['baseline']['value'], self['ranges']['time']['max']),
                "data", "data", ax1, ax2, zorder=self['traces']['baseline']['zorder'],
                ls=self['traces']['baseline']['ls'], lw=self['traces']['baseline']['lw'],
                color=self['traces']['baseline']['color'])
            baseline.set_annotation_clip(True)
            ax1.add_patch(baseline)
        
        if self['spectra']['baseline']['on'] and self['layout']['design'] is PlotLayout.LType:
            self.spectra.plot([self['ranges']['wln']['min'], self['ranges']['wln']['max']],
                [self['spectra']['baseline']['value'], self['spectra']['baseline']['value']],
                ls=self['spectra']['baseline']['ls'], lw=self['spectra']['baseline']['lw'], 
                color=self['spectra']['baseline']['color'],
                zorder=self['spectra']['baseline']['zorder'])
    
    def draw_artists (self):
        """
        Draw addition artists onto plot.
        
        Creates artists according to user settings and plots them. Axes are determined from
        `['contour', 'traces', 'spectra']` depending on the current layout. If necessary,
        the y-position within spectra is calculated from the offset of the given time.
        """
        if self['layout']['design'] is PlotLayout.Linear:
            scaling_min = PLOT_SPECTRA_SCALING_MIN / self['spectra']['scaling']
            scaling_max = PLOT_SPECTRA_SCALING_MAX / self['spectra']['scaling']
        
        for artist in self['artists']:
            x1, x2 = artist[1], artist[4]
            y1, y2 = artist[2], artist[5]
            
            # The following is not satisfying, but using a local function is not
            # possible (due to self) and I don't want define a global one (since
            # there are several methods which need to determine an axis but
            # sometimes have to consider expansion of lines and sometimes not).
            # TODO: Come up with an nice way to do this.
            
            if self['layout']['type'] is PlotType.LinLog:
                cut = self['ranges']['time']['cut']
                
                if artist[0] == 'traces':
                    ax1 = self.traces_lin if artist[2] < cut else self.traces_log
                elif artist[0] == 'spectra':
                    ax1 = self.spectra
                    if self['layout']['design'] is PlotLayout.Linear:
                        y1 = self.calculate_spectrum_offset(artist[2], scaling_min, scaling_max)
                else:
                    ax1 = self.contour_lin if artist[2] < cut else self.contour_log
                
                if artist[3] == 'traces':
                    ax2 = self.traces_lin if artist[5] < cut else self.traces_log
                elif artist[3] == 'spectra':
                    ax2 = self.spectra
                    if self['layout']['design'] is PlotLayout.Linear:
                        y2 = self.calculate_spectrum_offset(artist[5], scaling_min, scaling_max)
                else:
                    ax2 = self.contour_lin if artist[5] < cut else self.contour_log
            
            else: # not lin-log
                if artist[0] == 'traces':
                    ax1 = self.traces
                elif artist[0] == 'spectra':
                    ax1 = self.spectra
                    if self['layout']['design'] is PlotLayout.Linear:
                        y1 = self.calculate_spectrum_offset(artist[2], scaling_min, scaling_max)
                else:
                    ax1 = self.contour
                
                if artist[3] == 'traces':
                    ax2 = self.traces
                elif artist[3] == 'spectra':
                    ax2 = self.spectra
                    if self['layout']['design'] is PlotLayout.Linear:
                        y2 = self.calculate_spectrum_offset(artist[5], scaling_min, scaling_max)
                else:
                    ax2 = self.contour
            
            if artist[9] is None: # artist is a line
                a = mplConnectionPatch((x1,y1), (x2,y2), "data", "data", ax1, ax2,
                                       color=artist[6], ls=artist[7], lw=artist[8], zorder=artist[10])
                a.set_annotation_clip(False)
            else: # artist is a rect
                tmp = ax1.transData.transform((x1,y1))
                start = ax2.transData.inverted().transform(tmp)
                width, height = start - (x2,y2)
                a = mplRectangle((x2,y2), width, height, facecolor=artist[9], zorder=artist[10],
                                 edgecolor=artist[6], ls=artist[7], lw=artist[8])
            a.set_clip_on(False)
            ax2.add_patch(a)
    
    def finalize (self):
        """
        Finalize plot layout.
        """
        if self['axeslabel']['titleon']:
            self.figure.suptitle(self['title'])
            
        # align x-labels manually since build-in method
        # align_xlabels doesn't work properly
        if self['layout']['type'] is PlotType.LinLog:
            self.traces_lin.xaxis.get_label().set_va('baseline')
            self.contour_lin.xaxis.get_label().set_va('baseline')
            self.contour_lin.xaxis.labelpad = 15
            self.traces_lin.xaxis.labelpad = 15
        else:
            self.traces.xaxis.get_label().set_va('baseline')
            self.contour.xaxis.get_label().set_va('baseline')
            self.contour.xaxis.labelpad = 15
            self.traces.xaxis.labelpad = 15
        self.spectra.xaxis.get_label().set_va('baseline')
        self.spectra.xaxis.labelpad = 15
    
    def draw_contour (self):
        """
        Draw linear and/or logarithmic contour plots
        and apply corresponding user settings.
        """
        if self['contour']['colormap']['overon'] ^ self['contour']['colormap']['underon']:
            extend = 'max' if self['contour']['colormap']['overon'] else 'min'
        else:
            extend = 'both' if self['contour']['colormap']['overon'] and self['contour']['colormap']['underon'] else 'neither'
        
        if self['layout']['type'] is PlotType.LinLog:
            
            self.log_map = self.contour_log.contourf(self['wlnaxis'][self.wln_min_i:self.wln_max_i+1],
                self['timeaxis'][self.time_cut_i:self.time_max_i+1],
                self['data'][self.time_cut_i:self.time_max_i+1, self.wln_min_i:self.wln_max_i+1],
                cmap=self['contour']['colormap']['map'], levels=self['contour']['contourlevel'],
                extend=extend)
            self.lin_map = self.contour_lin.contourf(self['wlnaxis'][self.wln_min_i:self.wln_max_i+1],
                self['timeaxis'][self.time_min_i:self.time_cut_i+1],
                self['data'][self.time_min_i:self.time_cut_i+1, self.wln_min_i:self.wln_max_i+1],
                cmap=self['contour']['colormap']['map'], levels=self['contour']['contourlevel'],
                extend=extend)
            self.contour_log.set_yscale('log')
            self.contour_lin.set_yscale('linear')
            
            self.contour_log.set_xlim([self['ranges']['wln']['min'],  self['ranges']['wln']['max']])
            self.contour_log.set_ylim([self['ranges']['time']['cut'], self['ranges']['time']['max']])
            self.contour_lin.set_ylim([self['ranges']['time']['min'], self['ranges']['time']['cut']])
            self.contour_log.set_xticks(self['contour']['contourticks'])
            self.contour_log.set_yticklabels([])
            self.contour_lin.set_yticklabels([])
            
            self.contour_log.tick_params(bottom=False, labelbottom=False)
            self.contour_lin.tick_params(top=False, labeltop=False)
            self.contour_log.spines['bottom'].set_visible(False)
            self.contour_lin.spines['top'].set_visible(False)
            
            self.contour_lin.set_xlabel(self['axeslabel']['wlncontour'])
            
        else: # lin or log only
            
            self.map = self.contour.contourf(self['wlnaxis'][self.wln_min_i:self.wln_max_i+1],
                self['timeaxis'][self.time_min_i:self.time_max_i+1],
                self['data'][self.time_min_i:self.time_max_i+1, self.wln_min_i:self.wln_max_i+1],
                cmap=self['contour']['colormap']['map'], levels=self['contour']['contourlevel'],
                extend=extend)
            self.contour.set_xlim([self['ranges']['wln']['min'],  self['ranges']['wln']['max']])
            self.contour.set_ylim([self['ranges']['time']['min'], self['ranges']['time']['max']])
            self.contour.set_xticks(self['contour']['contourticks'])
            
            if self['layout']['type'] is PlotType.Lin:
                self.contour.set_yscale('linear')
            else:
                self.contour.set_yscale('log')
            self.contour.set_yticklabels([])
            
            self.contour.set_xlabel(self['axeslabel']['wlncontour'])
        
    def draw_traces (self):
        """
        Draw time traces into lin and/or log traces plots.
        
        Draws traces of specified list of times as well as wavelength lines
        through contour plots. If enabled, labels and/or a baseline will be 
        drawn, too. All corresponding user settings are applied.
        
        Notes
        -----
        Algorithm explanation for the calculation of proper time axis label:
            
        The x-coordinate doesn't need to be fixed, but the y-coordinate has to 
        be shifted downwards due to the addition of the linear part to the 
        overall traces plot height.
        
        Assume the log and lin plot parts to have heights a und b, respectively.
        The center position of the log part (and position of its label) will 
        be y = (a/2) / a, since the coords are given between 0 and 1.
        A proper position for the combination of log and lin plots is therefore 
        y' = ((a+b)/2) / a, or in other words the average height of both 
        normalized to the height of the log plot, in which coordinates the
        label will be given and drawn. The above equation is equivalent to 
        y' = ( (a/2) / a ) + ( (b/2) / a ) = y + ( (b/2) / a ).
        """
        
        def draw_wavelength_label (wln):
            """
            Place label for given wavelength in either time traces plot and/or
            contour plot.
            
            If enabled, labels for `wln` are drawn within linear parts of 
            traces and/or contour plot. If `WavelengthTrace['label']` is set to 
            automatic labeling, text is created from the wavelength value and 
            current unit setting.
            User settings regarding size, rotation and color are applied.
            
            Parameters
            ----------
            wln : Plot.WavelengthTrace
                wavelength trace which label should be drawn
            """
            if wln['label'] == PLOT_AUTO:
                tmp = str(round(wln['wln'])) if (wln['wln']).is_integer() else str(wln['wln'])
                text = tmp + ' ' + self.translator._(self['units']['wln'].value)
                if wln['factor'] != 1:
                    f = wln['factor']
                    text += f' x {f:g}'
            else:
                text = wln['label']
            linlog = self['layout']['type'] is PlotType.LinLog
            if wln['label_traces']:
                ax = self.traces_lin if linlog else self.traces
                label = ax.text(wln['trace_x'], wln['trace_y'],
                    text, color=wln['color'], rotation=wln['trace_rot'],
                    weight='normal', size=self['traces']['labelsize'])
                label.set_va('bottom') # do not move this outside if
            if wln['label_contour']:
                ax = self.contour_lin if linlog else self.contour
                label = ax.text(wln['contour_x'], wln['contour_y'],
                    text, color=wln['color'], rotation=wln['contour_rot'],
                    weight='normal', size=self['traces']['labelsize'])
                label.set_va('bottom') # do not move this outside if
        
        if self['layout']['type'] is PlotType.LinLog:
        
            for trace in self['traces']['list']:
                if type(trace) is Plot.ExtWavelengthTrace:
                    self.traces_log.plot(trace['data'][trace['index']+1:,1], trace['data'][trace['index']+1:,0], color=trace['color'])
                    self.traces_lin.plot(trace['data'][:trace['index']+1,1], trace['data'][:trace['index']+1,0], color=trace['color'])
                else:
                    self.traces_log.plot(self['data'][self.time_cut_i:self.time_max_i, trace['index']] * trace['factor'],
                                         self['timeaxis'][self.time_cut_i:self.time_max_i], color=trace['color'])
                    self.traces_lin.plot(self['data'][self.time_min_i:self.time_cut_i+1, trace['index']] * trace['factor'],
                                         self['timeaxis'][self.time_min_i:self.time_cut_i+1], color=trace['color'])
                    self.spectra.add_artist(self.create_wln_line(trace))
                draw_wavelength_label(trace)
            self.traces_log.set_yscale('log')
            self.traces_lin.set_yscale('linear')
            
            self.traces_log.set_xlim(self['traces']['xlim'])
            self.traces_log.set_ylim(self.contour_log.get_ylim())
            self.traces_lin.set_ylim(self.contour_lin.get_ylim())
            self.traces_lin.set_xticks(self['traces']['xticks'])
            self.traces_log.set_yticks(self['misc']['timetickslog'])
            self.traces_lin.set_yticks(self['misc']['timetickslin'])
            
            self.traces_log.tick_params(bottom=False, labelbottom=False)
            self.traces_lin.tick_params(top=False, labeltop=False)
            if self['axeslabel']['timedec']:
                self.traces_log.get_yaxis().set_major_formatter(mplScalarFormatter())
            else:
                self.traces_lin.get_yaxis().set_major_formatter(mplFuncFormatter(pc.sci_format_func))
            self.traces_log.spines['bottom'].set_visible(False)
            self.traces_lin.spines['top'].set_visible(False)
            self.traces_log.set_ylabel(self['axeslabel']['time'])
            self.traces_lin.set_xlabel(self['axeslabel']['abstraces'])
            # calculate center position of log and lin traces plots (cf. docstring)
            x, y = self.traces_log.yaxis.get_label().get_position()
            labelpos = (x, (1- (y + self['layout']['heights'][3]*0.5/self['layout']['heights'][2])))
            self.traces_log.yaxis.get_label().set_position(labelpos)
            
        else: # lin or log only
            
            for trace in self['traces']['list']:
                if type(trace) is Plot.ExtWavelengthTrace:
                    self.traces.plot(trace['data'][:,1], trace['data'][:,0], color=trace['color'])
                else:
                    self.traces.plot(self['data'][:,trace['index']] * trace['factor'], self['timeaxis'][:], color=trace['color'])
                    self.spectra.add_artist(self.create_wln_line(trace))
                draw_wavelength_label(trace)
            self.traces.set_xlim(self['traces']['xlim'])
            self.traces.set_ylim(self.contour.get_ylim())
            self.traces.set_xticks(self['traces']['xticks'])
            
            if self['layout']['type'] is PlotType.Lin:
                self.traces.set_yscale('linear')
                self.traces.set_yticks(self['misc']['timetickslin'])
            else:
                self.traces.set_yscale('log')
                self.traces.set_yticks(self['misc']['timetickslog'])
            
            self.traces.set_ylabel(self['axeslabel']['time'])
            self.traces.set_xlabel(self['axeslabel']['abstraces'])
        
    def draw_spectra (self):
        """
        Draw transient spectra into spectra plots.
        
        Draws spectra of specified list of transient spectra as well as 
        time lines through contour plots. If enabled, labels and/or a 
        baseline will be drawn, too. 
        All corresponding user settings are applied.
        """
        
        def draw_time_label (time):
            """
            Place label for given time in contour plot.
            
            If enabled, labels for `time` are drawn within the contour plot. 
            If `TransientSpectrum['label']` is set to automatic labeling, text 
            is created from the time value and current unit setting.
            User settings regarding size, rotation and color are applied.
            
            Parameters
            ----------
            time : Plot.TransientSpectrum
                transient spectrum which label should be drawn
            """
            if time['label'] == PLOT_AUTO:
                tmp = str(round(time['time'])) if time['time'].is_integer() else str(time['time'])
                text = tmp + ' ' + self.translator._(self['units']['time'].value)
                if time['factor'] != 1:
                    f = time['factor']
                    text += f' x {f:g}'
            else:
                text = time['label']
            if self['layout']['type'] is not PlotType.LinLog:
                ax = self.contour
            elif time['time'] < self['ranges']['time']['cut']:
                ax = self.contour_lin
            else:
                ax = self.contour_log
            label = mplAnnotation(text, xy=(time['contour_x'], time['contour_y']),
                xycoords=ax.transData, clip_on=False, color=time['labelcolor'], 
                weight='normal', size=self['spectra']['labelsize'], ha='right')
            return label
        
        def draw_scalebar ():
            """
            Place the scalebar to transient plot according to user
            settings given in scalebar dict.
            """
            value = self['spectra']['scalebar']['value']
            if self['spectra']['scalebar']['label'] == PLOT_AUTO:
                tmp = str(round(value)) if value.is_integer() else str(value)
                text = tmp + ' ' + self.translator._(self['units']['abs'].value)
            else:
                text = self['spectra']['scalebar']['label']
            middle = self['spectra']['scalebar']['posy']
            bottom = middle - (value / 2)
            top    = middle + (value / 2)
            self.spectra.plot([self['spectra']['scalebar']['poswln'], self['spectra']['scalebar']['poswln']],
                [bottom, top], ls='-', marker='_',color=self['spectra']['scalebar']['color'])
            self.spectra.annotate(text, (self['spectra']['scalebar']['poswln'], middle),
                xytext=(5,-1), textcoords='offset pixels', ha='left', va='center',
                color=self['spectra']['scalebar']['color'])
        
        for spectrum in self['spectra']['list']:
            if type(spectrum) is Plot.ExtTransientSpectrum:
                self.spectra.plot(spectrum['data'][:,0], spectrum['data'][:,1], color=spectrum['color'])
            else:
                if self['layout']['design'] is not PlotLayout.LType:
                    temp = self['data'][spectrum['index'], :] + spectrum['offset']
                else:
                    temp = self['data'][spectrum['index'], :] * spectrum['factor']
                self.spectra.plot(self['wlnaxis'], temp, color=spectrum['color'])
                self.spectra.add_artist(self.create_time_line(spectrum))
            if spectrum['label_on']:
                self.spectra.add_artist(draw_time_label(spectrum))
        
        if self['layout']['type'] is PlotType.LinLog:
            self.spectra.set_xlim(self.contour_lin.get_xlim())
        else:
            self.spectra.set_xlim(self.contour.get_xlim())
        
        if self['layout']['design'] is not PlotLayout.LType:
            self.spectra.set_ylim(PLOT_SPECTRA_SCALING_MIN/self['spectra']['scaling'],
                                  PLOT_SPECTRA_SCALING_MAX/self['spectra']['scaling'])
            self.spectra.set_xticks(self['spectra']['xticks'])
            self.spectra.set_yticks([])
            self.spectra.tick_params(left=False, right=False)
            self.spectra.set_xlabel(self['axeslabel']['wlntransients'])
        else:
            self.spectra.set_ylim(self['spectra']['scaling'])
            self.spectra.set_xticks(self['contour']['contourticks'])
            self.spectra.set_yticks(self['spectra']['yticks'])
            self.spectra.tick_params(labelbottom=False, labeltop=True)
            self.spectra.set_ylabel(self['axeslabel']['absspectra'])
        
        if self['spectra']['scalebar']['on']:
            draw_scalebar()
        
    def draw_plot (self, figure, file, reload, observer_func):
        """
        Draw plot consisting of contour, traces and transient spectra plots.
        
        Parameters
        ----------
        figure : matplotlib.Figure
            the figure to draw in
        
        file : str
            path of the datafile
        
        reload : bool
            whether or not the file should be reloaded, probably because of a 
            change in absorption multiply factor
        
        observer_func : callable
            function to call if progress has changed
        
        Raises
        ------
        pcerror.InternalError
            if something unexpected happens
        """
        self.observer = observer_func
        try:
            
            self.update_progress(0, 'STATUS_PLOT_LOAD_DATA')
            if (file != self['datafile']) or reload:
                self.load_data(file)
            
            self.update_progress(15, 'STATUS_PLOT_INITIALIZE')
            self.grid = mplGridSpec(6, 3, width_ratios=self['layout']['widths'], height_ratios=self['layout']['heights'])
            self.figure = figure
            self.figure.subplots_adjust(wspace=0, hspace=0)
            self.initialize()
            
            self.update_progress(25, 'STATUS_PLOT_DRAW_CONTOUR')
            self.draw_contour()
            
            self.update_progress(50, 'STATUS_PLOT_DRAW_TRACES')
            self.draw_traces()
            
            self.update_progress(65, 'STATUS_PLOT_DRAW_SPECTRA')
            self.draw_spectra()
            
            self.update_progress(80, 'STATUS_PLOT_DRAW_COLORBAR')
            self.draw_colorbar()
            
            self.update_progress(90, 'STATUS_PLOT_DRAW_BASELINES')
            self.draw_baselines()
            
            self.update_progress(92.5, 'STATUS_PLOT_DRAW_ARTISTS')
            self.draw_artists()
            
            self.update_progress(95, 'STATUS_PLOT_FINALIZE')
            self.finalize()
            self.update_progress(100,'')
            
        except (pcerror.InputError, pcerror.RangeError):
            raise
        
        except:
            raise pcerror.InternalError('ERROR_PLOT_INTERNAL_ERROR')
        