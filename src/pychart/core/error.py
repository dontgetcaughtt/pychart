#-----------------------------------------------------------------
#   ERROR CLASSES
#-----------------------------------------------------------------

class PychartError (Exception):
    pass

class InternalError (PychartError):
    """
    Error class to be used when unknown errors occur or python's
    build-in exceptions are caught.
    
    Attributes
    ----------
    message : str
        error message
    """
    def __init__ (self, message='ERROR_UNKOWN_ERROR'):
        self.msg = message
        self.title = 'ERROR_INTERNAL'

class InputError (PychartError):
    """
    Error class to be used for errors related to read / load / import
    data files, settings etc.
    
    For invalid user inputs see `pcerror.RangeError`.
    
    Attributes
    ----------
    message : str
        error message
    
    input : str
        invalid user input
    """
    
    def __init__ (self, message='ERROR_UNKOWN_ERROR', input=''):
        self.msg = message
        self.title = 'ERROR_INPUT'
        self.input = input

class OutputError (PychartError):
    """
    Error class to be used for errors related to write / export
    data files, graphics, settings etc.
    
    Attributes
    ----------
    message : str
        error message
    
    output : str
        invalid user input, e.g. file name
    """
    
    def __init__ (self, message='ERROR_UNKOWN_ERROR', output=''):
        self.msg = message
        self.title = 'ERROR_OUTPUT'
        self.output = output

class RangeError (PychartError):
    """
    Error class to be used for errors related to invalid or inconsistent
    user inputs.
    
    Attributes
    ----------
    message : str
        error message
    
    input : str
        invalid user input
    
    ranges : str
        valid range values
    """
    def __init__ (self, message='ERROR_UNKOWN_ERROR', input='', ranges=''):
        self.msg = message
        self.title = 'ERROR_RANGE'
        self.input = input
        self.ranges = ranges
