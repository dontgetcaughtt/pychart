PYCHART_VERSION_NAME = 'v5.2'
PYCHART_VERSION = 5,2


def version_is_newer (than, v=PYCHART_VERSION):
    return than < v

def version_is_newer_eq (than, v=PYCHART_VERSION):
    return than <= v

def version_from_string (s):
    try:
        return tuple(map(int, s[1:].split('.')))
    except (IndexError, TypeError, ValueError):
        return 0,0


PYCHART_HISTORY = []
def _add_old_version (v):
    PYCHART_HISTORY.append(v)

_add_old_version((5,1))
_add_old_version((5,0))
_add_old_version((4,0))
_add_old_version((3,0))
_add_old_version((2,0))
_add_old_version((1,0))
