from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QLabel, QComboBox, QTreeWidget, QTreeWidgetItem
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtGui import QIcon

from ..core import error as pcerror
from .ErrorDialog import ErrorDialog
from . import common as pcgui

class SellmeierWidget (QWidget):
    
    def __init__(self, sellmeier_handle, translator_handle, *args, **kwargs):
        super(SellmeierWidget, self).__init__(*args, **kwargs)
        self.sellmeier = sellmeier_handle
        self.translator = translator_handle
        
        self.offset_edit = pcgui.qRLineEdit60('1.0')
        lay2 = QHBoxLayout()
        lay2.addWidget(pcgui.qDummyLabel(10))
        lay2.addWidget(QLabel(self.translator._('SELLMEIER_OFFSET')))
        lay2.addWidget(self.offset_edit)
        lay2.addWidget(QLabel(self.translator._('SELLMEIER_PS')))
        lay2.addStretch()
        
        self.materials_combo = QComboBox()
        self.materials_combo.addItems(self.sellmeier.materials)
        self.thickness_edit = pcgui.qRLineEdit60('0.0')
        self.add_btn = pcgui.qPushButton30()
        self.del_btn = pcgui.qPushButton30()
        self.icon_add = QIcon('../images/icons/add.svg')
        self.icon_check = QIcon('../images/icons/check.svg')
        self.icon_remove = QIcon('../images/icons/remove.svg')
        self.add_btn.setIcon(self.icon_add)
        self.del_btn.setIcon(self.icon_remove)
        self.add_btn.setToolTip(self.translator._('IMPORT_SELLMEIER_ADD'))
        self.del_btn.setToolTip(self.translator._('IMPORT_SELLMEIER_DEL'))
        lay1 = QHBoxLayout()
        lay1.addWidget(self.materials_combo)
        lay1.addWidget(self.thickness_edit)
        lay1.addWidget(QLabel(self.translator._('SELLMEIER_MM')))
        lay1.addStretch()
        lay1.addWidget(self.add_btn)
        lay1.addWidget(self.del_btn)
        
        self.materials_list = QTreeWidget()
        self.materials_list.setColumnCount(2)
        headers = ['SELLMEIER_MATERIAL_HEADER', 'SELLMEIER_THICKNESS_HEADER']
        self.materials_list.setHeaderLabels([self.translator._(header) for header in headers])
        self.materials_list.setRootIsDecorated(False)
        self.materials_list.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        
        lay = QVBoxLayout()
        lay.addLayout(lay1)
        lay.addWidget(self.materials_list)
        lay.addLayout(lay2)
        
        self.setLayout(lay)
        
        self.add_btn.clicked.connect(self._updateMaterialInList)
        self.del_btn.clicked.connect(self._delMaterialFromList)
        self.materials_list.itemClicked.connect(self._updateMaterial)
        self.materials_combo.currentTextChanged.connect(self._updateMaterialInputFields)
    
    def _materialIndexInList (self, material):
        for i in range(self.materials_list.topLevelItemCount()):
            if material == self.materials_list.topLevelItem(i).text(0):
                return i
        return None
    
    @pyqtSlot('QTreeWidgetItem*', int)
    def _updateMaterial (self, item, col):
        self.materials_combo.setCurrentText(item.text(0))
        self.thickness_edit.setText(item.text(1))
    
    @pyqtSlot('QString')
    def _updateMaterialInputFields (self, material):
        index = self._materialIndexInList(material)
        if index is not None:
            self.thickness_edit.setText(self.materials_list.topLevelItem(index).text(1))
            self.add_btn.setIcon(self.icon_check)
            self.add_btn.setToolTip(self.translator._('IMPORT_SELLMEIER_UPDATE'))
        else:
            self.add_btn.setIcon(self.icon_add)
            self.add_btn.setToolTip(self.translator._('IMPORT_SELLMEIER_ADD'))
    
    @pyqtSlot()
    def _updateMaterialInList (self):
        try:
            thickness = str(float(self.thickness_edit.text()))
        except ValueError:
            e = pcerror.RangeError('ERROR_IMPORT_INVALID_THICKNESS_ERROR', input=self.thickness_edit.text())
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
            return
        material = self.materials_combo.currentText()
        index = self._materialIndexInList(material)
        if index is None:
            item = QTreeWidgetItem([material, thickness])
            self.materials_list.addTopLevelItem(item)
            self.add_btn.setIcon(self.icon_check)
        else:
            self.materials_list.topLevelItem(index).setText(1, thickness)
    
    @pyqtSlot()
    def _delMaterialFromList (self):
        material = self.materials_combo.currentText()
        index = self._materialIndexInList(material)
        if index is not None:
            self.materials_list.takeTopLevelItem(index)
            self.add_btn.setIcon(self.icon_add)
    
    def materialsList (self):
        l = []
        for i in range(self.materials_list.topLevelItemCount()):
            item = self.materials_list.topLevelItem(i)
            l.append((item.text(0), float(item.text(1))))
        return l
    
    def offsetTime (self):
        try:
            return float(self.offset_edit.text())
        except:
            raise pcerror.RangeError('ERROR_IMPORT_INVALID_TIMEOFFSET_ERROR', input=self.offset_edit.text())
    