from PyQt5.QtCore import pyqtSlot, pyqtSignal, QTimer, QUrl
from PyQt5.QtWidgets import QAction, QActionGroup, QMainWindow, QLabel, QPushButton, QDialog, QTabWidget, QToolBar, QProgressBar
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout
from PyQt5.QtGui import QKeySequence, QPixmap, QIcon, QFont, QDesktopServices

from .. import version
from ..utils import i18n
from ..utils import settings as pcsettings
from . import PlotTab, HDFImportTab, FitTab, SettingsDialog
from . import NetCDFImportTab, NetCDFSingleImportDialog, NetCDFMergeFilesDialog
from .ErrorDialog import ErrorDialog
from . import common as pcgui
from ..core import sellmeier as pcsellmeier
from ..core import error as pcerror

# TODO: tidy this module up!

# -----------------------------------------------------------------
#   ADDITIONAL DIALOGS
# -----------------------------------------------------------------

class AboutDialog (QDialog):
    
    def __init__ (self, parent, *args, **kwargs):
        super(AboutDialog, self).__init__(parent, *args, **kwargs)
        
        pixmap = QPixmap('../images/logo/logo.png')
        logo = QLabel()
        logo.setPixmap(pixmap)
        
        ok_btn = QPushButton(self.parent().translator._('OK'))
        ok_btn.clicked.connect(self.close)
        
        lay_left, lay_right = QVBoxLayout(), QVBoxLayout()
        lay2, lay3 = QHBoxLayout(), QHBoxLayout()
        
        lay_left.addWidget(logo)
        
        lay_right.addWidget(pcgui.qDummyLabel(30))
        lay_right.addWidget(QLabel(self.parent().translator._('ABOUT_CAPTION').format(version=version.PYCHART_VERSION_NAME)))
        lay_right.addWidget(pcgui.qDummyLabel(30))
        lay_right.addWidget(QLabel(self.parent().translator._('ABOUT_TEXT')))
        lay_right.addStretch()
        
        lay2.addLayout(lay_left)
        lay2.addWidget(pcgui.qDummyLabel(20))
        lay2.addLayout(lay_right)
        
        lay3.addStretch()
        lay3.addWidget(ok_btn)
        
        lay_main = QVBoxLayout()
        lay_main.addLayout(lay2)
        lay_main.addLayout(lay3)
        
        self.setWindowTitle(self.parent().translator._('ABOUT_TITLE'))
        self.setLayout(lay_main)

class NewDialog (QDialog):
    
    new_plot_requested = pyqtSignal()
    new_netcdf_import_requested = pyqtSignal()
    new_hdf_import_requested = pyqtSignal()
    new_fit_requested = pyqtSignal()
    
    def __init__(self, parent, *args, **kwargs):
        super(NewDialog, self).__init__(parent, *args, **kwargs)
        self.setWindowTitle(self.parent().translator._('NEW_DLG_TITLE'))
        self.setModal(False)
        
        self.new_plot_btn = pcgui.qIconButton('../images/icons/new-plot.svg', size=256)
        self.new_import_btn = pcgui.qIconButton('../images/icons/new-import.svg', size=256)
        self.new_fit_btn = pcgui.qIconButton('../images/icons/new-fit.svg', size=256)
        self.new_netcdf_btn = pcgui.qIconButton('../images/brands/netcdf.png', size=256)
        self.new_hdf_btn = pcgui.qIconButton('../images/brands/hdf.svg', size=256)
        
        font = QFont('Arial', 10, QFont.Bold)
        self.new_plot_label = pcgui.qCLabel(self.parent().translator._('NEW_PLOT_TAB'), font=font)
        self.new_import_label = pcgui.qCLabel(self.parent().translator._('NEW_IMPORT'), font=font)
        self.new_fit_label = pcgui.qCLabel(self.parent().translator._('NEW_TDFIT_TAB'), font=font)
        self.new_netcdf_label = pcgui.qCLabel(self.parent().translator._('NEW_NETCDF_TAB'), font=font)
        self.new_hdf_label = pcgui.qCLabel(self.parent().translator._('NEW_HDF_TAB'), font=font)
        
        self.new_plot_btn.clicked.connect(self.on_new_plot)
        self.new_import_btn.clicked.connect(self.on_new_import)
        self.new_fit_btn.clicked.connect(self.on_new_fit)
        self.new_netcdf_btn.clicked.connect(self.on_new_netcdf)
        self.new_hdf_btn.clicked.connect(self.on_new_hdf)
        
        self.lay1 = QGridLayout()
        self.lay1.addWidget(self.new_plot_btn,     0, 0)
        self.lay1.addWidget(self.new_import_btn,   0, 1)
        self.lay1.addWidget(self.new_fit_btn,      0, 2)
        self.lay1.addWidget(self.new_plot_label,   1, 0)
        self.lay1.addWidget(self.new_import_label, 1, 1)
        self.lay1.addWidget(self.new_fit_label,    1, 2)
        
        self.lay2 = QGridLayout()
        self.lay2.addWidget(self.new_netcdf_btn,   0, 0)
        self.lay2.addWidget(self.new_hdf_btn,      0, 1)
        self.lay2.addWidget(self.new_netcdf_label, 1, 0)
        self.lay2.addWidget(self.new_hdf_label,    1, 1)
        
        self.main_lay = QHBoxLayout()
        self.main_lay.addLayout(self.lay1)
        self.setLayout(self.main_lay)
        self._show_widgets('top')
    
    def _show_widgets (self, level):
        visibility = (level == 'top')
        
        self.new_plot_btn.setVisible(visibility)
        self.new_import_btn.setVisible(visibility)
        self.new_fit_btn.setVisible(visibility)
        self.new_netcdf_btn.setVisible(not visibility)
        self.new_hdf_btn.setVisible(not visibility)
        
        self.new_plot_label.setVisible(visibility)
        self.new_import_label.setVisible(visibility)
        self.new_fit_label.setVisible(visibility)
        self.new_netcdf_label.setVisible(not visibility)
        self.new_hdf_label.setVisible(not visibility)
        
        if level == 'top':
            self.main_lay.removeItem(self.lay2)
            self.main_lay.addLayout(self.lay1)
        else:
            self.main_lay.removeItem(self.lay1)
            self.main_lay.addLayout(self.lay2)
    
    def display (self, level='top'):
        self._show_widgets(level)
        self.show()
    
    def on_new_plot (self):
        self.new_plot_requested.emit()
        self.close()
    
    def on_new_import (self):
        self._show_widgets('import')
    
    def on_new_netcdf (self):
        self.new_netcdf_import_requested.emit()
        self.close()
        
    def on_new_hdf (self):
        self.new_hdf_import_requested.emit()
        self.close()
    
    def on_new_fit (self):
        self.new_fit_requested.emit()
        self.close()

# -----------------------------------------------------------------
#   MAIN WINDOW
# -----------------------------------------------------------------

class MainWindow(QMainWindow):
    
    # -----------------------------------------------------------------
    #   MAIN USER INTERFACE
    # -----------------------------------------------------------------
    
    def __init__ (self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        
        self.tabs = QTabWidget()
        self.tabs.setDocumentMode(False)
        self.tabs.setTabsClosable(True)
        self.tabs.setMovable(True)
        
        self.tab_count = 0
        self.plot_count = 0
        self.fit_count = 0
        self.netcdf_count = 0
        self.hdf_count = 0
        
        self.translator = i18n.Translator()
        # TODO: re-think initialization strategy and catch InternalError when no lang loaded
        
        self.settings = pcsettings.PyChartSettings(self.translator)
        self.settings_dialog = SettingsDialog.GeneralSettingsDialog(self.settings, self)
        self.load_settings()
        if not self.settings.language == self.translator._('de'):
            try:
                self.translator.load_lang(self.settings.language_i18n_identifier())
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            del self.settings_dialog
            self.settings_dialog = SettingsDialog.GeneralSettingsDialog(self.settings, self)
        self.settings_dialog.accepted.connect(self.save_settings)
        
        self.sellmeierdata = pcsellmeier.SellmeierCoefficients()
        errors = self.sellmeierdata.load_coeffs_from_files('../data/')
        if errors:
            self.handle_sellmeier_error(errors)
        
        self.about_dlg = AboutDialog(self)
        self.new_dlg = NewDialog(self)
        self.new_dlg.new_plot_requested.connect(self.new_PlotTab)
        self.new_dlg.new_netcdf_import_requested.connect(self.new_NetCDFTab)
        self.new_dlg.new_hdf_import_requested.connect(self.new_HDFTab)
        self.new_dlg.new_fit_requested.connect(self.new_TDTab)
        
        self.setup_menu_bar()
        self.setup_tool_bar()
        self.setup_status_bar()
        
        self.setWindowTitle('pyChart')
        self.setCentralWidget(self.tabs)
        self.move(0,0)
        
        self.tabs.tabCloseRequested.connect(self.close_tab)
        self.tabs.currentChanged.connect(self.update_tool_bar)
        
        self.open_new_tab()
    
    def setup_menu_bar (self):
        self.menus, self.actions = {}, {}
        
        action_list = {
            'new': ['../images/icons/new.svg', 'MENU_NEW', lambda: self.new_dlg.display('top')],
            'newPlot': ['../images/icons/new-plot.svg', 'MENU_NEW_PLOT', self.new_PlotTab],
            'newImport': ['../images/icons/new-import.svg', 'MENU_NEW_IMPORT', lambda: self.new_dlg.display('import')],
            'importNetCDF': ['../images/brands/netcdf.png', 'MENU_IMPORT_NETCDF', self.new_NetCDFTab],
            'importHDF': ['../images/brands/hdf.svg', 'MENU_IMPORT_HDF', self.new_HDFTab],
            'importSingle': ['../images/icons/single-spectrum.svg', 'MENU_IMPORT_SINGLE', self.import_single],
            'mergeFiles': ['../images/icons/merge-files.svg', 'MENU_IMPORT_MERGE', self.merge_files],
            'newTDFit': ['../images/icons/new-fit.svg', 'MENU_NEW_TD', self.new_TDTab],
            'close': [None, 'MENU_CLOSE', lambda: self.close_tab(self.tabs.currentIndex())],
            'closeall': [None, 'MENU_CLOSE_ALL', self.close_all],
            'exit': [None, 'MENU_EXIT', self.close_app],
            'settings': ['../images/icons/settings.svg', 'MENU_SET', self.show_settings_dialog],
            'help': ['../images/icons/help.svg', 'MENU_HELP', self.open_help],
            'about': [None, 'MENU_ABOUT', self.about_dlg.show],
            
            'plotPlot': ['../images/icons/plot.svg', 'PLOT_MENU_PLOT', self.plot_plot],
            'loadPlot': ['../images/icons/import-json.svg', 'PLOT_MENU_LOAD', self.load_plot],
            'savePlot': ['../images/icons/export-json.svg', 'PLOT_MENU_SAVE', self.save_plot],
            'exportPlot': ['../images/icons/save.svg', 'PLOT_MENU_EXPORT', self.export_plot],
            'colorbarSetup': ['../images/icons/colorbar.svg', 'PLOT_MENU_COLORBAR', self.colorbar_setup],
            'scalebarSetup': ['../images/icons/scalebar.svg', 'PLOT_MENU_SCALEBAR', self.scalebar_setup],
            'autoscalePlot': ['../images/icons/autoscale.svg', 'PLOT_MENU_AUTOSCALE', self.autoscale_plot],
            'baselinePlot': ['../images/icons/baseline.svg', 'PLOT_MENU_BASELINE', self.baseline_plot],
            'axeslabelPlot': ['../images/icons/axislabel.svg', 'PLOT_MENU_AXESLABEL', self.axeslabel_plot],
            'artistsPlot': ['../images/icons/add-artists.svg', 'PLOT_MENU_ARTISTS', self.artists_plot],
            
            'linlogPlot': [None, 'PLOT_MENU_LINLOG', self.change_plot_type],
            'linPlot': [None, 'PLOT_MENU_LIN', self.change_plot_type],
            'logPlot': [None, 'PLOT_MENU_LOG', self.change_plot_type],
            
            'fsTAValues': [None, 'PLOT_MENU_FSTA', self.default_values],
            'KerrValues': [None, 'PLOT_MENU_KERR', self.default_values],
            'nsTAValues': [None, 'PLOT_MENU_NSTA', self.default_values],
            'nsIRValues': [None, 'PLOT_MENU_NSIR', self.default_values],
            'HPLCValues': [None, 'PLOT_MENU_HPLC', self.default_values],
            'fsTApump':   [None, 'PLOT_MENU_PUMP', self.default_values],
            'fsTAprobe':  [None, 'PLOT_MENU_PROBE', self.default_values],
            'fsTAbg':     [None, 'PLOT_MENU_BG',   self.default_values],
            
            'showImportData': ['../images/icons/calculate.svg', 'IMPORT_MENU_SHOW', self.show_import_data],
            'openPlot': ['../images/icons/open-as-plot.svg', 'IMPORT_MENU_PLOT', self.open_as_plot],
            'exportData': ['../images/icons/export-data.svg', 'IMPORT_MENU_EXPORT', self.export_imported_data],
            'exportDataZ20': [None, 'IMPORT_MENU_EXPORT_Z20', self.export_z20_data],
            'showPhotodiodes': ['../images/icons/photodiodes.svg', 'IMPORT_MENU_PLOT_PD', self.show_photodiode_values]
        }
        
        for name, params in action_list.items():
            a = QAction(self.translator._(params[1]))
            if params[0] is not None:
                a.setIcon(QIcon(params[0]))
            a.triggered.connect(params[2])
            self.actions[name] = a
        
        self.actions['close'].setShortcut(QKeySequence.Close)
        self.actions['help'].setShortcut(QKeySequence.HelpContents)
        
        typeActionGroup = QActionGroup(self)
        for type_ in ['linlogPlot', 'linPlot', 'logPlot']:
            typeActionGroup.addAction(self.actions[type_])
            self.actions[type_].setCheckable(True)
        
        self.menus['file'] = self.menuBar().addMenu(self.translator._('MENU_FILE'))
        self.menus['file'].addAction(self.actions['new'])
        self.menus['file'].addAction(self.actions['newPlot'])
        
        self.menus['fileimport'] = self.menus['file'].addMenu(self.translator._('MENU_NEW_IMPORT'))
        self.menus['fileimport'].addAction(self.actions['importNetCDF'])
        self.menus['fileimport'].addAction(self.actions['importHDF'])
        
        self.menus['file'].addAction(self.actions['newTDFit'])
        self.menus['file'].addSeparator()
        self.menus['file'].addAction(self.actions['close'])
        self.menus['file'].addAction(self.actions['closeall'])
        self.menus['file'].addSeparator()
        self.menus['file'].addAction(self.actions['exit'])
        
        self.menus['plot'] = self.menuBar().addMenu(self.translator._('MENU_PLOT'))
        self.menus['plot'].addAction(self.actions['newPlot'])
        self.menus['plot'].addSeparator()
        self.menus['plot'].addAction(self.actions['linlogPlot'])
        self.menus['plot'].addAction(self.actions['linPlot'])
        self.menus['plot'].addAction(self.actions['logPlot'])
        self.menus['plot'].addSeparator()
        self.menus['plot'].addAction(self.actions['plotPlot'])
        self.menus['plot'].addAction(self.actions['loadPlot'])
        self.menus['plot'].addAction(self.actions['savePlot'])
        
        self.menus['default'] = self.menus['plot'].addMenu(self.translator._('PLOT_MENU_DEFAULT'))
        self.menus['default'].addAction(self.actions['fsTAValues'])
        self.menus['default'].addAction(self.actions['KerrValues'])
        self.menus['default'].addAction(self.actions['nsTAValues'])
        self.menus['default'].addAction(self.actions['nsIRValues'])
        self.menus['defaultMore'] = self.menus['default'].addMenu(self.translator._('PLOT_MENU_DEFAULT_MORE'))
        self.menus['defaultMore'].addAction(self.actions['HPLCValues'])
        self.menus['defaultMore'].addAction(self.actions['fsTApump'])
        self.menus['defaultMore'].addAction(self.actions['fsTAprobe'])
        self.menus['defaultMore'].addAction(self.actions['fsTAbg'])
        
        self.menus['plot'].addAction(self.actions['exportPlot'])
        self.menus['plot'].addSeparator()
        self.menus['plot'].addAction(self.actions['colorbarSetup'])
        self.menus['plot'].addAction(self.actions['scalebarSetup'])
        self.menus['plot'].addAction(self.actions['autoscalePlot'])
        self.menus['plot'].addAction(self.actions['axeslabelPlot'])
        self.menus['plot'].addAction(self.actions['baselinePlot'])
        self.menus['plot'].addAction(self.actions['artistsPlot'])
        
        self.menus['import'] = self.menuBar().addMenu(self.translator._('MENU_IMPORT'))
        self.menus['import'].addAction(self.actions['importNetCDF'])
        self.menus['import'].addAction(self.actions['importSingle'])
        self.menus['import'].addAction(self.actions['mergeFiles'])
        self.menus['import'].addAction(self.actions['importHDF'])
        self.menus['import'].addSeparator()
        self.menus['import'].addAction(self.actions['showImportData'])
        self.menus['import'].addAction(self.actions['showPhotodiodes'])
        self.menus['import'].addAction(self.actions['openPlot'])
        self.menus['import'].addAction(self.actions['exportData'])
        self.menus['import'].addAction(self.actions['exportDataZ20'])
        
        self.menus['opt'] = self.menuBar().addMenu(self.translator._('MENU_OPT'))
        self.menus['opt'].addAction(self.actions['settings'])
        
        self.menus['help'] = self.menuBar().addMenu(self.translator._('MENU_HELP_M'))
        self.menus['help'].addAction(self.actions['help'])
        self.menus['help'].addAction(self.actions['about'])
    
    def setup_tool_bar (self):
        self.tool_bar_new = QToolBar()
        self.tool_bar_new.addAction(self.actions['new'])
        self.tool_bar_new.addAction(self.actions['newPlot'])
        self.tool_bar_new.addAction(self.actions['newImport'])
        self.tool_bar_new.addAction(self.actions['newTDFit'])
        
        self.tool_bar_plot = QToolBar()
        self.tool_bar_plot.addAction(self.actions['plotPlot'])
        self.tool_bar_plot.addAction(self.actions['loadPlot'])
        self.tool_bar_plot.addAction(self.actions['savePlot'])
        self.tool_bar_plot.addAction(self.actions['exportPlot'])
        self.tool_bar_plot.addSeparator()
        self.tool_bar_plot.addAction(self.actions['colorbarSetup'])
        self.tool_bar_plot.addAction(self.actions['scalebarSetup'])
        self.tool_bar_plot.addAction(self.actions['autoscalePlot'])
        self.tool_bar_plot.addAction(self.actions['axeslabelPlot'])
        self.tool_bar_plot.addAction(self.actions['baselinePlot'])
        self.tool_bar_plot.addAction(self.actions['artistsPlot'])
        
        self.tool_bar_import = QToolBar()
        self.tool_bar_import.addAction(self.actions['importNetCDF'])
        self.tool_bar_import.addAction(self.actions['importSingle'])
        self.tool_bar_import.addAction(self.actions['mergeFiles'])
        self.tool_bar_import.addAction(self.actions['importHDF'])
        self.tool_bar_import.addSeparator()
        self.tool_bar_import.addAction(self.actions['showImportData'])
        self.tool_bar_import.addAction(self.actions['showPhotodiodes'])
        self.tool_bar_import.addAction(self.actions['openPlot'])
        self.tool_bar_import.addAction(self.actions['exportData'])
        
        self.addToolBar(self.tool_bar_new)
        self.addToolBar(self.tool_bar_plot)
        self.addToolBar(self.tool_bar_import)
    
    def update_tool_bar (self):
        temp = (isinstance(self.tabs.currentWidget(), PlotTab.PlotTab))
        for a in self.menus['plot'].actions():
            a.setEnabled(temp)
        self.actions['newPlot'].setEnabled(True)
        if temp:
            self.update_plot_layout()
        
        temp = (isinstance(self.tabs.currentWidget(), NetCDFImportTab.NetCDFImportTab)) \
               or (isinstance(self.tabs.currentWidget(), HDFImportTab.HDFImportTab))
        for a in self.menus['import'].actions():
            a.setEnabled(temp)
        self.actions['importNetCDF'].setEnabled(True)
        self.actions['importHDF'].setEnabled(True)
        
        if isinstance(self.tabs.currentWidget(), HDFImportTab.HDFImportTab):
            self.update_hdf_layout()
        else:
            self.actions['showPhotodiodes'].setEnabled(False)
        self.actions['importSingle'].setEnabled(isinstance(self.tabs.currentWidget(), NetCDFImportTab.NetCDFImportTab))
        self.actions['mergeFiles'].setEnabled(isinstance(self.tabs.currentWidget(), NetCDFImportTab.NetCDFImportTab))
    
    def setup_status_bar (self):
        self.progress_bar = QProgressBar()
        self.progress_bar.setRange(0,100)
        self.progress_bar.setMaximumWidth(500)
        self.statusBar().addPermanentWidget(self.progress_bar)
    
    @pyqtSlot('QString', int)
    def update_status (self, status, time):
        self.statusBar().showMessage(self.translator._(status), time)
    
    @pyqtSlot(int)
    def update_progress (self, percent):
        self.progress_bar.setValue(percent)
        if percent == 100:
            QTimer.singleShot(1500, self.progress_bar.reset)
    
    def handle_sellmeier_error (self, errors):
        for error in errors:
            code, filepath = error
            if code == 'decode':
                msg = 'ERROR_PYCHART_SELLMEIER_JSON_ERROR'
            elif code == 'load':
                msg = 'ERROR_PYCHART_SELLMEIER_LOAD_ERROR'
            else:
                msg = 'ERROR_UNKOWN_ERROR'
            dlg = ErrorDialog(self, self.translator._(msg).format(input=filepath), self.translator._('ERROR_INPUT'))
            dlg.exec()
    
    @pyqtSlot('QString')
    def update_tab_name (self, text):
        self.tabs.setTabText(self.tabs.currentIndex(), text)
    
    def open_new_tab (self):
        if self.settings.startup_tab == self.translator._('SETTINGS_TAB_PLOT'):
            self.new_PlotTab()
        elif self.settings.startup_tab == self.translator._('SETTINGS_TAB_TDFIT'):
            self.new_TDTab()
        elif self.settings.startup_tab == self.translator._('SETTINGS_TAB_NETCDF'):
            self.new_NetCDFTab()
        elif self.settings.startup_tab == self.translator._('SETTINGS_TAB_HDF'):
            self.new_HDFTab()
        elif self.settings.startup_tab == self.translator._('SETTINGS_TAB_CHOOSE'):
            self.new_dlg.display('top')
        else:
            pass
    
    def close_tab (self, index):
        self.tabs.removeTab(index)
        self.tab_count -= 1
        if self.tab_count == 0:
            self.open_new_tab()
    
    def close_all (self):
        while self.tab_count > 0:
            self.tabs.removeTab(0)
            self.tab_count -= 1
        self.open_new_tab()
    
    def close_app (self):
        self.close()
    
    # -----------------------------------------------------------------
    #   COMMON PYCHART DIALOGS
    # -----------------------------------------------------------------
    
    def load_settings (self):
        try:
            self.settings.load_settings_from_file('../settings/pychart-settings.json')
        except pcerror.InputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
        else:
            self.settings_dialog.display_settings()
    
    def save_settings (self):
        try:
            self.settings.save_settings_to_file('../settings/pychart-settings.json')
        except pcerror.OutputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(output=e.output), self.translator._(e.title))
            dlg.exec()
    
    def show_settings_dialog (self):
        self.settings_dialog.display_settings()
        self.settings_dialog.exec()
    
    def open_help (self):
        a = 'https://gitlab.com/dontgetcaughtt/pychart/-/blob/master/docs/'
        if isinstance(self.tabs.currentWidget(), PlotTab.PlotTab):
            url = a + 'Plotting.md'
        elif isinstance(self.tabs.currentWidget(), NetCDFImportTab.NetCDFImportTab):
            url = a + 'NetCDFImport.md'
        elif isinstance(self.tabs.currentWidget(), HDFImportTab.HDFImportTab):
            url = a + 'HDFImport.md'
        else:
            url = 'https://gitlab.com/dontgetcaughtt/pychart/-/tree/master/docs'
        QDesktopServices.openUrl(QUrl(url))
    
    # -----------------------------------------------------------------
    #   PLOT TAB FUNCTIONS
    # -----------------------------------------------------------------
    
    def new_PlotTab (self):
        self.plot_count += 1
        name = self.translator._('PLOT_TAB_NAME') + ' ' + str(self.plot_count)
        tab = PlotTab.PlotTab(name, self.translator)
        tab.load_settings(self.settings.default_plot_settings)
        tab.new_status.connect(self.update_status)
        tab.plot_progress.connect(self.update_progress)
        tab.plot_name_line_edit.textChanged.connect(self.update_tab_name)
        tab.plot_type_changed.connect(self.update_plot_type)
        tab.plot_layout_changed.connect(self.update_plot_layout)
        tab.set_working_dir(self.settings.default_dir)
        self.settings_dialog.default_dir_changed.connect(tab.set_working_dir)
        index = self.tabs.addTab(tab, name)
        self.update_plot_type(tab.plot_type)
        self.tabs.setCurrentIndex(index)
        self.tab_count += 1
    
    def change_plot_type (self):
        sender = self.sender()
        if sender == self.actions['linlogPlot']:
            self.tabs.widget(self.tabs.currentIndex()).change_type('lin-log')
        elif sender == self.actions['linPlot']:
            self.tabs.widget(self.tabs.currentIndex()).change_type('lin')
        elif sender == self.actions['logPlot']:
            self.tabs.widget(self.tabs.currentIndex()).change_type('log')
        else:
            pass
    
    @pyqtSlot('QString')
    def update_plot_type (self, text):
        if text == 'lin-log':
            self.actions['linlogPlot'].setChecked(True)
        elif text == 'lin':
            self.actions['linPlot'].setChecked(True)
        elif text == 'log':
            self.actions['logPlot'].setChecked(True)
        else:
            pass
    
    @pyqtSlot()
    def update_plot_layout (self):
        layout = self.tabs.widget(self.tabs.currentIndex()).plot_layout
        if layout == 'PLOT_LAYOUT_LINEAR':
            self.actions['scalebarSetup'].setEnabled(True)
        elif layout == 'PLOT_LAYOUT_L_TYPE':
            self.actions['scalebarSetup'].setEnabled(False)
        else:
            pass
    
    def plot_plot (self):
        self.tabs.widget(self.tabs.currentIndex()).plot_data()
    
    def load_plot (self):
        self.tabs.widget(self.tabs.currentIndex()).load_settings_dialog()
    
    def save_plot (self):
        self.tabs.widget(self.tabs.currentIndex()).save_settings_dialog()
    
    def default_values (self):
        sender = self.sender()
        try:
            if sender == self.actions['fsTAValues']:
                self.tabs.widget(self.tabs.currentIndex()).load_settings('../settings/default_fsTA.json')
            elif sender == self.actions['KerrValues']:
                self.tabs.widget(self.tabs.currentIndex()).load_settings('../settings/default_Kerr.json')
            elif sender == self.actions['nsTAValues']:
                self.tabs.widget(self.tabs.currentIndex()).load_settings('../settings/default_nsTA.json')
            elif sender == self.actions['nsIRValues']:
                self.tabs.widget(self.tabs.currentIndex()).load_settings('../settings/default_nsIR.json')
            elif sender == self.actions['HPLCValues']:
                self.tabs.widget(self.tabs.currentIndex()).load_settings('../settings/default_HPLC.json')
            elif sender == self.actions['fsTApump']:
                self.tabs.widget(self.tabs.currentIndex()).load_settings('../settings/default_fsTA_pump.json')
            elif sender == self.actions['fsTAprobe']:
                self.tabs.widget(self.tabs.currentIndex()).load_settings('../settings/default_fsTA_probe.json')
            elif sender == self.actions['fsTAbg']:
                self.tabs.widget(self.tabs.currentIndex()).load_settings('../settings/default_fsTA_bg.json')
            else:
                pass
            setup = list(self.actions.keys())[list(self.actions.values()).index(sender)][:-6]
            self.update_status(self.translator._('STATUS_PLOT_DEFAULT_SETTINGS_LOADED').format(setup=setup), 10000)
        except pcerror.InputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
    
    def export_plot (self):
        try:
            self.tabs.widget(self.tabs.currentIndex()).save_plot(self.settings.export_resolution)
        except pcerror.OutputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.output), self.translator._(e.title))
            dlg.exec()
    
    def autoscale_plot (self):
        self.tabs.widget(self.tabs.currentIndex()).auto_ranges()
    
    def baseline_plot (self):
        self.tabs.widget(self.tabs.currentIndex()).baseline_dialog()
    
    def axeslabel_plot (self):
        self.tabs.widget(self.tabs.currentIndex()).axes_label_dialog()
    
    def artists_plot (self):
        self.tabs.widget(self.tabs.currentIndex()).artists_dialog()
    
    def colorbar_setup (self):
        self.tabs.widget(self.tabs.currentIndex()).colorbar_dialog()
    
    def scalebar_setup (self):
        self.tabs.widget(self.tabs.currentIndex()).scalebar_dialog()
    
    # -----------------------------------------------------------------
    #   FIT TAB FUNCTIONS
    # -----------------------------------------------------------------
    
    def new_TDTab (self):
        self.fit_count += 1
        name = 'TDFit ' + str(self.fit_count)
        tab = FitTab.TDFitTab(name, self.translator)
        index = self.tabs.addTab(tab, name)
        tab.fit_name_line_edit.textChanged.connect(self.update_tab_name)
        self.tabs.setCurrentIndex(index)
        self.tab_count += 1
    
    # -----------------------------------------------------------------
    #   IMPORT FUNCTIONS
    # -----------------------------------------------------------------
    
    def new_NetCDFTab (self):
        self.netcdf_count += 1
        name = self.translator._('IMPORT_NETCDF_TAB_NAME') + ' ' + str(self.netcdf_count)
        tab = NetCDFImportTab.NetCDFImportTab(name, self.translator, self.sellmeierdata)
        tab.new_status.connect(self.update_status)
        tab.calculation_progress.connect(self.update_progress)
        tab.set_working_dir(self.settings.default_dir)
        tab.set_gate_time_range(*self.settings.gate_time_range) # unpack tuple
        tab.set_tolerances(self.settings.netcdf_wln_tolerance, self.settings.netcdf_time_tolerance)
        self.settings_dialog.default_dir_changed.connect(tab.set_working_dir)
        self.settings_dialog.gate_time_range_changed.connect(tab.set_gate_time_range)
        self.settings_dialog.netcdf_axes_tolerances_changed.connect(tab.set_tolerances)
        index = self.tabs.addTab(tab, name)
        self.tabs.setCurrentIndex(index)
        self.tab_count += 1
    
    def new_HDFTab (self):
        self.hdf_count += 1
        name = self.translator._('IMPORT_HDF_TAB_NAME') + ' ' + str(self.hdf_count)
        tab = HDFImportTab.HDFImportTab(name, self.translator, self.sellmeierdata)
        tab.new_status.connect(self.update_status)
        tab.calculation_progress.connect(self.update_progress)
        tab.pd_data_loaded.connect(self.update_hdf_layout)
        tab.set_working_dir(self.settings.default_dir)
        tab.set_tolerances(self.settings.hdf_wln_tolerance, self.settings.hdf_wln_tolerance)
        self.settings_dialog.default_dir_changed.connect(tab.set_working_dir)
        self.settings_dialog.hdf_axes_tolerances_changed.connect(tab.set_tolerances)
        index = self.tabs.addTab(tab, name)
        self.tabs.setCurrentIndex(index)
        self.tab_count += 1
    
    def import_single (self):
        dlg = NetCDFSingleImportDialog.NetCDFSingleImportDialog(self.translator, self.settings.default_dir,
                                                                self.settings.netcdf_to_pychart_factor, self.settings.netcdf_wln_tolerance)
        dlg.exec()
    
    def merge_files (self):
        dlg = NetCDFMergeFilesDialog.NetCDFMergeFilesDialog(self.translator, self.settings.default_dir,
                                                            self.settings.netcdf_wln_tolerance, self.settings.netcdf_time_tolerance)
        dlg.exec()
    
    def show_photodiode_values (self):
        self.tabs.widget(self.tabs.currentIndex()).show_photodiode_values(self.settings.pd_plot_layout)
    
    @pyqtSlot()
    def update_hdf_layout (self):
        self.actions['showPhotodiodes'].setEnabled(self.tabs.currentWidget().pd_available())
    
    def show_import_data (self):
        try:
            self.tabs.widget(self.tabs.currentIndex()).generate_data()
        except pcerror.InputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input, ranges=e.ranges), self.translator._(e.title))
            dlg.exec()
    
    def open_as_plot (self):
        try:
            filepath = self.tabs.widget(self.tabs.currentIndex()).open_as_plot()
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input, ranges=e.ranges), self.translator._(e.title))
            dlg.exec()
        except pcerror.OutputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(output=e.output), self.translator._(e.title))
            dlg.exec()
        else:
            if filepath is not None:
                self.new_PlotTab()
                self.tabs.widget(self.tabs.currentIndex()).set_datafile(filepath)
    
    def export_imported_data (self):
        if isinstance(self.tabs.currentWidget(), NetCDFImportTab.NetCDFImportTab):
            factor = self.settings.netcdf_to_pychart_factor
        elif isinstance(self.tabs.currentWidget(), HDFImportTab.HDFImportTab):
            factor = self.settings.hdf_to_pychart_factor
        else:
            factor = None
        self.tabs.widget(self.tabs.currentIndex()).export_data(factor)
    
    def export_z20_data (self):
        if isinstance(self.tabs.currentWidget(), NetCDFImportTab.NetCDFImportTab):
            factor = self.settings.netcdf_to_z20_factor
        elif isinstance(self.tabs.currentWidget(), HDFImportTab.HDFImportTab):
            factor = self.settings.hdf_to_z20_factor
        else:
            factor = None
        self.tabs.widget(self.tabs.currentIndex()).export_data(factor, z20_mode=True)
