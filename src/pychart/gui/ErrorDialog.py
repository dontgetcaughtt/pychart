from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize

# -----------------------------------------------------------------
#   ERROR HANDLING DIALOG
# -----------------------------------------------------------------

class ErrorDialog(QMessageBox):
    """ Dialog class for displaying error messages. """
    
    def __init__ (self, parent, error, caption='', *args, **kwargs):
        super(ErrorDialog, self).__init__(parent, *args, **kwargs)
        self.setWindowTitle(self.parent().translator._('ERROR_ERROR'))
        self.setText(f"{caption}")
        self.setInformativeText(f"{error}")
        self.setStandardButtons(QMessageBox.Ok)
        self.setIconPixmap(QIcon('../images/icons/warning.svg').pixmap(QSize(48,48)))
        self.setMinimumWidth(250)
