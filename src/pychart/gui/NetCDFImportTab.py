import re

import numpy as np

from matplotlib.gridspec import GridSpec as mplGrid
from matplotlib.figure import Figure as mplFigure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as mplCanvas

from PyQt5.QtCore import Qt, QDir
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QApplication, QWidget, QDialog, QFileDialog, QWhatsThis
from PyQt5.QtWidgets import QLabel, QLineEdit, QPushButton, QCheckBox, QRadioButton, QComboBox, QSpinBox, QTableWidget, QTableWidgetItem
from PyQt5.QtWidgets import QGroupBox, QButtonGroup, QVBoxLayout, QHBoxLayout, QGridLayout, QFormLayout, QSplitter, QSizePolicy
from PyQt5.QtGui import QIcon

from . import common as pcgui
from .DataTableDialogs import ReplaceInfDialog, ReplaceNaNDialog, InsertDataDialog
from .GateCorrectionSetupDialog import GateCorrectionSetupDialog
from .SellmeierWidget import SellmeierWidget
from .ErrorDialog import ErrorDialog
from ..core import netcdfimport as pcnetcdf
from ..core import pychart as pc
from ..core import error as pcerror

class NetCDFImportTab (QWidget):
    """
    Tab widget providing the user interface for importing NetCDF data from fsTA.
    
    Widget provides all necessary input / settings fields to generate data to the
    desired level of correction. The user settings are only read, checked and
    interpreted when correction functions are called.
    
    Attributes
    ----------
    tabname : str
        name of tab and default plot title
    
    working_dir : str
        working directory to open file dialogs at
    
    translator : i18n.Translator
        reference to the applications translator instance
    
    sellmeier_handle : pcsellmeier.SellmeierCoefficients
        reference to Sellmeier correction class
    
    netcdfimport : pcnetcdf.NetCDFImport
        import class that is responsible for handling / calculating data
    
    data : numpy.ndarray
        corrected data values
    
    wlns : numpy.ndarray
        wavelength axis values
    
    times : numpy.ndarray
        time axis values
    
    main_loaded : bool
        flag that indicates whether a main data set was loaded
    
    solv_loaded : bool
        flag that indicates whether a solvent measurement was loaded
    
    gate_loaded : bool
        flag that indicates whether a gate measurement was loaded
    
    gate_start_time : float
        start of time range for gate correction
    
    gate_stop_time : float
        stop of time range for gate correction
    
    wln_tolerance : float
        wavelength axis tolerance
    
    time_tolerance : float
        time axis tolerance
    
    wln_i : int
        index of first wavelength to plot its time trace
    
    table_editable : bool
        flag that indicates whether the data table is currently editable by the user
    
    Emits
    -----
    new_status : str, int
        emits new status messages and timeout in milliseconds
    
    calculation_progress : int
        current estimated progress of calculation and correction
    """
    
    new_status = pyqtSignal('QString', int)
    calculation_progress = pyqtSignal(int)
    
    def __init__ (self, name, translatorhandle, sellmeierhandle, *args, **kwargs):
        super(NetCDFImportTab, self).__init__(*args, **kwargs)
        
        self.tabname = name
        self.working_dir = QDir.currentPath()
        self.sellmeier_handle = sellmeierhandle
        
        self.data, self.wlns, self.times = None, None, None
        self.main_loaded, self.solv_loaded, self.gate_loaded = False, False, False
        self.gate_start_time, self.gate_stop_time = 0, 0
        self.wln_tolerance, self.time_tolerance = 0, 0
        
        self.table_editable = False
        self.wln_i = 0
        
        # user interface
        self.translator = translatorhandle
        self.setup_gui()
        
        self.setup_gate_dlg = GateCorrectionSetupDialog(self)
        
        # connections
        self.data_file_dlg_button.clicked.connect(self.update_data_file)
        self.data_bg_file_dlg_button.clicked.connect(self.update_bg_file)
        self.data_solv_file_dlg_button.clicked.connect(self.update_solv_file)
        self.data_solv_bg_file_dlg_button.clicked.connect(self.update_solv_bg_file)
        self.data_gate_file_dlg_button.clicked.connect(self.update_gate_file)
        self.data_gate_bg_file_dlg_button.clicked.connect(self.update_gate_bg_file)
        self.data_bg_check.stateChanged.connect(self.change_main_bg_layout)
        self.data_solv_bg_check.stateChanged.connect(self.change_solv_bg_layout)
        self.data_gate_bg_check.stateChanged.connect(self.change_gate_bg_layout)
        
        self.pump_radio.clicked.connect(self.change_layout)
        self.probe_radio.clicked.connect(self.change_layout)
        self.pumpprobe_radio.clicked.connect(self.change_layout)
        self.background_radio.clicked.connect(self.change_layout)
        self.mode4_radio.clicked.connect(self.change_layout)
        self.mode2_radio.clicked.connect(self.change_layout)
        self.mode4_solv_radio.clicked.connect(self.change_solv_bg_layout)
        self.mode2_solv_radio.clicked.connect(self.change_solv_bg_layout)
        self.mode4_gate_radio.clicked.connect(self.change_gate_bg_layout)
        self.mode2_gate_radio.clicked.connect(self.change_gate_bg_layout)
        self.diffabs_gate_radio.clicked.connect(self.change_gate_bg_layout)
        self.sigdiff_gate_radio.clicked.connect(self.change_gate_bg_layout)
        
        self.scans_all_radio.clicked.connect(self.change_main_scan_layout)
        self.scans_select_radio.clicked.connect(self.change_main_scan_layout)
        self.scans_solv_all_radio.clicked.connect(self.change_solv_scan_layout)
        self.scans_solv_select_radio.clicked.connect(self.change_solv_scan_layout)
        self.scans_gate_all_radio.clicked.connect(self.change_gate_scan_layout)
        self.scans_gate_select_radio.clicked.connect(self.change_gate_scan_layout)
        
        self.group_solv.toggled.connect(self.change_solvent_correction)
        self.solv_factor_combo.currentTextChanged.connect(self.change_solv_factor_label)
        self.solv_factor_info_button.clicked.connect(
            lambda: QWhatsThis.showText(self.solv_factor_info_button.pos(), self.translator._('IMPORT_INFOTEXT_SOLV'),
                                        self.solv_factor_info_button))
        self.group_gate.toggled.connect(self.change_timezero_correction)
        self.gate_dlg_button.clicked.connect(self.setup_gate_dialog)
        self.setup_gate_dlg.time_zeros_requested.connect(self.generate_time_zeros)
        self.group_sellmeier.toggled.connect(self.change_timezero_correction)
        
        self.add_table_column_button.clicked.connect(self.add_wavelength_dialog)
        self.delete_table_column_button.clicked.connect(self.delete_wavelength_from_table)
        self.add_table_row_button.clicked.connect(self.add_time_dialog)
        self.delete_table_row_button.clicked.connect(self.delete_time_from_table)
        self.replace_nan_button.clicked.connect(self.replace_nan_dialog)
        self.replace_inf_button.clicked.connect(self.replace_inf_dialog)
        self.table_edit_switch_button.clicked.connect(self.switch_data_table_edit_behaviour)
        
        self.traces_update_button.clicked.connect(self.update_time_traces)
        self.traces_time_min_edit.editingFinished.connect(self.finalize_time_traces)
        self.traces_time_max_edit.editingFinished.connect(self.finalize_time_traces)
        self.traces_amp_min_edit.editingFinished.connect(self.finalize_time_traces)
        self.traces_amp_max_edit.editingFinished.connect(self.finalize_time_traces)
        self.traces_time_axis_combo.currentTextChanged.connect(self.finalize_time_traces)
        self.traces_first_button.clicked.connect(self.goto_first_wln)
        self.traces_back_button.clicked.connect(self.goto_back_wln)
        self.traces_prev_button.clicked.connect(self.goto_prev_wln)
        self.traces_goto_button.clicked.connect(lambda: self.goto_wln(self.traces_goto_spinbox.value()))
        self.traces_next_button.clicked.connect(self.goto_next_wln)
        self.traces_forward_button.clicked.connect(self.goto_forward_wln)
        self.traces_last_button.clicked.connect(self.goto_last_wln)
        
        # initialization
        self.netcdfimport = pcnetcdf.NetCDFImport()
        self.change_main_bg_layout()
        self.change_main_scan_layout()
        self.change_solv_bg_layout()
        self.change_solv_factor_label()
        self.change_solv_scan_layout()
        self.change_gate_bg_layout()
        self.change_gate_scan_layout()
    
    @pyqtSlot('QString')
    def set_working_dir (self, directory):
        self.working_dir = directory
    
    @pyqtSlot(float, float)
    def set_gate_time_range (self, start, stop):
        self.gate_start_time = start
        self.gate_stop_time = stop
    
    @pyqtSlot(float, float)
    def set_tolerances (self, wln, time):
        self.wln_tolerance = wln
        self.time_tolerance = time
    
    @pyqtSlot(int, 'QString')
    def monitor_calculation_progress (self, progress, message):
        """ Emit signals notifying about current `progress` with `message`. """
        self.new_status.emit(message, 0)
        self.calculation_progress.emit(progress)
    
    # -----------------------------------------------------------------
    #   USER INTERFACE
    # -----------------------------------------------------------------
    
    def setup_gui (self):
        """ Create graphical user interface as import tab. """
        
        # data group
        self.data_line_edit = QLineEdit()
        self.data_line_edit.setPalette(pcgui.qPalette(pcgui.GREEN))
        self.data_file_dlg_button = pcgui.qPushButton30('...')
        lay0 = QHBoxLayout()
        lay0.addWidget(QLabel(self.translator._('IMPORT_RAWDATA')))
        lay0.addWidget(self.data_line_edit)
        lay0.addWidget(self.data_file_dlg_button)
        
        self.data_bg_check = QCheckBox(self.translator._('IMPORT_BGDATA'))
        self.data_bg_line_edit = QLineEdit()
        self.data_bg_file_dlg_button = pcgui.qPushButton30('...')
        lay1 = QHBoxLayout()
        lay1.addWidget(self.data_bg_check)
        lay1.addWidget(self.data_bg_line_edit)
        lay1.addWidget(self.data_bg_file_dlg_button)
        
        self.reference_corr_rb1 = QRadioButton(self.translator._('IMPORT_CORR_ON'))
        self.reference_corr_rb2 = QRadioButton(self.translator._('IMPORT_CORR_OFF'))
        self.reference_corr_btn_group = QButtonGroup()
        self.reference_corr_btn_group.addButton(self.reference_corr_rb1)
        self.reference_corr_btn_group.addButton(self.reference_corr_rb2)
        self.reference_corr_rb1.setChecked(True)
        lay2 = QHBoxLayout()
        lay2.addWidget(QLabel(self.translator._('IMPORT_REF_CORR')))
        lay2.addWidget(self.reference_corr_rb1)
        lay2.addWidget(self.reference_corr_rb2)
        lay2.addStretch()
        
        self.wln_count_label, self.time_count_label = QLabel(), QLabel()
        self.shot_count_label, self.scan_count_label = QLabel(), QLabel()
        self.mode_count_label = QLabel()
        lay4 = QFormLayout()
        lay4.addRow(QLabel('     ' + self.translator._('IMPORT_WLN_COUNT')), self.wln_count_label)
        lay4.addRow(QLabel('     ' + self.translator._('IMPORT_TIME_COUNT')), self.time_count_label)
        lay4.addRow(QLabel('     ' + self.translator._('IMPORT_SHOT_COUNT')), self.shot_count_label)
        lay4.addRow(QLabel('     ' + self.translator._('IMPORT_SCAN_COUNT')), self.scan_count_label)
        lay4.addRow(QLabel('     ' + self.translator._('IMPORT_MODE_COUNT')), self.mode_count_label)
        
        self.pump_radio = QRadioButton(self.translator._('IMPORT_PUMP'))
        self.probe_radio = QRadioButton(self.translator._('IMPORT_PROBE'))
        self.pumpprobe_radio = QRadioButton(self.translator._('IMPORT_PUMPPROBE'))
        self.background_radio = QRadioButton(self.translator._('IMPORT_BACKGROUND'))
        self.mode4_radio = QRadioButton(self.translator._('IMPORT_MODE4'))
        self.mode2_radio = QRadioButton(self.translator._('IMPORT_MODE2'))
        self.mode_btn_group = QButtonGroup()
        self.mode_btn_group.addButton(self.pump_radio)
        self.mode_btn_group.addButton(self.probe_radio)
        self.mode_btn_group.addButton(self.pumpprobe_radio)
        self.mode_btn_group.addButton(self.background_radio)
        self.mode_btn_group.addButton(self.mode4_radio)
        self.mode_btn_group.addButton(self.mode2_radio)
        self.mode4_radio.setChecked(True)
        
        self.scans_all_radio = QRadioButton(self.translator._('IMPORT_SCANS_ALL'))
        self.scans_select_radio = QRadioButton(self.translator._('IMPORT_SCANS_SELECT'))
        self.scans_btn_group = QButtonGroup()
        self.scans_btn_group.addButton(self.scans_all_radio)
        self.scans_btn_group.addButton(self.scans_select_radio)
        self.scans_all_radio.setChecked(True)
        self.scans = pcgui.qScanComboBox()
        self.update_scan_list(0, self.scans)
        self.scans.setMinimumContentsLength(len(self.translator._('IMPORT_SCANS_LABEL').format(scan=0)))
        self.scans.setSizeAdjustPolicy(QComboBox.AdjustToMinimumContentsLengthWithIcon)
        self.scans.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        lay3 = QHBoxLayout()
        lay3.addWidget(QLabel(self.translator._('IMPORT_SCANS')))
        lay3.addWidget(self.scans_all_radio)
        lay3.addWidget(self.scans_select_radio)
        lay3.addWidget(self.scans)
        
        lay = QGridLayout()
        lay.addLayout(lay0, 0, 0, 1, 4)
        lay.addLayout(lay1, 1, 0, 1, 4)
        lay.addLayout(lay2, 2, 0, 1, 4)
        lay.addLayout(lay4, 3, 0, 5, 4)
        lay.addWidget(pcgui.qSeparatorLine(), 8, 0, 1, 4)
        lay.addWidget(self.pump_radio, 9, 1)
        lay.addWidget(self.probe_radio, 10, 1)
        lay.addWidget(self.pumpprobe_radio, 9, 2)
        lay.addWidget(self.background_radio, 10, 2)
        lay.addWidget(self.mode4_radio, 9, 0)
        lay.addWidget(self.mode2_radio, 10, 0)
        lay.addWidget(pcgui.qSeparatorLine(), 11, 0, 1, 4)
        lay.addLayout(lay3, 12, 0, 1, 4)
        self.group_main = QGroupBox(self.translator._('IMPORT_DATA_GROUP'))
        self.group_main.setLayout(lay)
        
        # solvent group
        self.data_solv_line_edit = QLineEdit()
        self.data_solv_line_edit.setPalette(pcgui.qPalette(pcgui.BLUE))
        self.data_solv_file_dlg_button = pcgui.qPushButton30('...')
        lay6 = QHBoxLayout()
        lay6.addWidget(QLabel(self.translator._('IMPORT_RAWDATA')))
        lay6.addWidget(self.data_solv_line_edit)
        lay6.addWidget(self.data_solv_file_dlg_button)
        
        self.data_solv_bg_check = QCheckBox(self.translator._('IMPORT_BGDATA'))
        self.data_solv_bg_line_edit = QLineEdit()
        self.data_solv_bg_file_dlg_button = pcgui.qPushButton30('...')
        lay7 = QHBoxLayout()
        lay7.addWidget(self.data_solv_bg_check)
        lay7.addWidget(self.data_solv_bg_line_edit)
        lay7.addWidget(self.data_solv_bg_file_dlg_button)
        
        self.reference_corr_solv_rb1 = QRadioButton(self.translator._('IMPORT_CORR_ON'))
        self.reference_corr_solv_rb2 = QRadioButton(self.translator._('IMPORT_CORR_OFF'))
        self.reference_corr_solv_btn_group = QButtonGroup()
        self.reference_corr_solv_btn_group.addButton(self.reference_corr_solv_rb1)
        self.reference_corr_solv_btn_group.addButton(self.reference_corr_solv_rb2)
        self.reference_corr_solv_rb1.setChecked(True)
        lay8 = QHBoxLayout()
        lay8.addWidget(QLabel(self.translator._('IMPORT_REF_CORR')))
        lay8.addWidget(self.reference_corr_solv_rb1)
        lay8.addWidget(self.reference_corr_solv_rb2)
        lay8.addStretch()
        
        self.solv_factor_combo = QComboBox()
        self.solv_factor_combo.addItem(self.translator._('IMPORT_SOLV_ABSORPTION'))
        self.solv_factor_combo.addItem(self.translator._('IMPORT_SOLV_TRANSMISSION'))
        self.solv_factor_combo.addItem(self.translator._('IMPORT_SOLV_FACTOR'))
        self.solv_factor_combo.addItem(self.translator._('IMPORT_SOLV_FREE'))
        self.solv_factor_combo.setEditable(False)
        self.solv_factor_combo.setCurrentText(self.translator._('IMPORT_SOLV_FACTOR'))
        self.solv_factor_info_button = pcgui.qInfoButton()
        lay8b = QHBoxLayout()
        lay8b.addWidget(QLabel(self.translator._('IMPORT_SOLV_SUBSTRACT')))
        lay8b.addWidget(self.solv_factor_combo)
        lay8b.addStretch()
        lay8b.addWidget(self.solv_factor_info_button)
        
        self.solv_factor_edit = pcgui.qRLineEdit35('1')
        self.solv_factor_label_1, self.solv_factor_label_2 = QLabel(), QLabel()
        lay8a = QHBoxLayout()
        lay8a.addWidget(pcgui.qDummyLabel(10))
        lay8a.addWidget(self.solv_factor_label_1)
        lay8a.addWidget(self.solv_factor_edit)
        lay8a.addWidget(self.solv_factor_label_2)
        lay8a.addStretch()
        
        self.solv_shift_edit = pcgui.qRLineEdit35('-0.0')
        lay8c = QHBoxLayout()
        lay8c.addWidget(QLabel(self.translator._('IMPORT_SOLV_SHIFT_LABEL')))
        lay8c.addWidget(self.solv_shift_edit)
        lay8c.addWidget(QLabel(self.translator._('IMPORT_SOLV_SHIFT_UNIT')))
        lay8c.addStretch()
        
        self.mode4_solv_radio = QRadioButton(self.translator._('IMPORT_MODE4'))
        self.mode2_solv_radio = QRadioButton(self.translator._('IMPORT_MODE2'))
        self.mode_solv_btn_group = QButtonGroup()
        self.mode_solv_btn_group.addButton(self.mode4_solv_radio)
        self.mode_solv_btn_group.addButton(self.mode2_solv_radio)
        self.mode4_solv_radio.setChecked(True)
        
        self.scans_solv_all_radio = QRadioButton(self.translator._('IMPORT_SCANS_ALL'))
        self.scans_solv_select_radio = QRadioButton(self.translator._('IMPORT_SCANS_SELECT'))
        self.scans_solv_btn_group = QButtonGroup()
        self.scans_solv_btn_group.addButton(self.scans_solv_all_radio)
        self.scans_solv_btn_group.addButton(self.scans_solv_select_radio)
        self.scans_solv_all_radio.setChecked(True)
        self.scans_solv = pcgui.qScanComboBox()
        self.update_scan_list(0, self.scans_solv)
        self.scans_solv.setMinimumContentsLength(len(self.translator._('IMPORT_SCANS_LABEL').format(scan=0)))
        self.scans_solv.setSizeAdjustPolicy(QComboBox.AdjustToMinimumContentsLengthWithIcon)
        self.scans_solv.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        lay9 = QHBoxLayout()
        lay9.addWidget(QLabel(self.translator._('IMPORT_SCANS')))
        lay9.addWidget(self.scans_solv_all_radio)
        lay9.addWidget(self.scans_solv_select_radio)
        lay9.addWidget(self.scans_solv)
        
        lay10 = QGridLayout()
        lay10.addLayout(lay6,  0, 0, 1, 4)
        lay10.addLayout(lay7,  1, 0, 1, 4)
        lay10.addLayout(lay8,  2, 0, 1, 4)
        lay10.addLayout(lay8b, 3, 0, 1, 4)
        lay10.addLayout(lay8a, 4, 0, 1, 4)
        lay10.addLayout(lay8c, 5, 0, 1, 4)
        lay10.addWidget(pcgui.qSeparatorLine(), 6, 0, 1, 4)
        lay10.addWidget(self.mode4_solv_radio,  7, 0)
        lay10.addWidget(self.mode2_solv_radio,  8, 0)
        lay10.addWidget(pcgui.qSeparatorLine(), 9, 0, 1, 4)
        lay10.addLayout(lay9, 10, 0, 1, 4)
        self.group_solv = QGroupBox(self.translator._('IMPORT_SOLV_GROUP'))
        self.group_solv.setLayout(lay10)
        self.group_solv.setCheckable(True)
        self.group_solv.setChecked(False)
        
        # gate group
        self.data_gate_line_edit = QLineEdit()
        self.data_gate_line_edit.setPalette(pcgui.qPalette(pcgui.ORANGE))
        self.data_gate_file_dlg_button = pcgui.qPushButton30('...')
        lay12 = QHBoxLayout()
        lay12.addWidget(QLabel(self.translator._('IMPORT_RAWDATA')))
        lay12.addWidget(self.data_gate_line_edit)
        lay12.addWidget(self.data_gate_file_dlg_button)
        
        self.data_gate_bg_check = QCheckBox(self.translator._('IMPORT_BGDATA'))
        self.data_gate_bg_line_edit = QLineEdit()
        self.data_gate_bg_file_dlg_button = pcgui.qPushButton30('...')
        lay13 = QHBoxLayout()
        lay13.addWidget(self.data_gate_bg_check)
        lay13.addWidget(self.data_gate_bg_line_edit)
        lay13.addWidget(self.data_gate_bg_file_dlg_button)
        
        self.gate_dlg_button = QPushButton(self.translator._('IMPORT_GATE_DLG'))
        self.gate_dlg_button.setEnabled(False)
        lay14 = QHBoxLayout()
        lay14.addStretch()
        lay14.addWidget(self.gate_dlg_button)
        lay14.addStretch()
        
        self.gate_shift_edit = pcgui.qRLineEdit35('-0.0')
        lay14c = QHBoxLayout()
        lay14c.addWidget(QLabel(self.translator._('IMPORT_GATE_SHIFT_LABEL')))
        lay14c.addWidget(self.gate_shift_edit)
        lay14c.addWidget(QLabel(self.translator._('IMPORT_GATE_SHIFT_UNIT')))
        lay14c.addStretch()
        
        self.mode4_gate_radio = QRadioButton(self.translator._('IMPORT_MODE4_GATE'))
        self.mode2_gate_radio = QRadioButton(self.translator._('IMPORT_MODE2_GATE'))
        self.mode_gate_btn_group = QButtonGroup()
        self.mode_gate_btn_group.addButton(self.mode4_gate_radio)
        self.mode_gate_btn_group.addButton(self.mode2_gate_radio)
        self.mode4_gate_radio.setChecked(True)
        
        self.diffabs_gate_radio = QRadioButton(self.translator._('IMPORT_DIFFABS'))
        self.sigdiff_gate_radio = QRadioButton(self.translator._('IMPORT_SIGDIFF'))
        self.signal_gate_btn_group = QButtonGroup()
        self.signal_gate_btn_group.addButton(self.diffabs_gate_radio)
        self.signal_gate_btn_group.addButton(self.sigdiff_gate_radio)
        self.sigdiff_gate_radio.setChecked(True)
        
        self.scans_gate_all_radio = QRadioButton(self.translator._('IMPORT_SCANS_ALL'))
        self.scans_gate_select_radio = QRadioButton(self.translator._('IMPORT_SCANS_SELECT'))
        self.scans_gate_btn_group = QButtonGroup()
        self.scans_gate_btn_group.addButton(self.scans_gate_all_radio)
        self.scans_gate_btn_group.addButton(self.scans_gate_select_radio)
        self.scans_gate_all_radio.setChecked(True)
        self.scans_gate = pcgui.qScanComboBox()
        self.update_scan_list(0, self.scans_gate)
        self.scans_gate.setMinimumContentsLength(len(self.translator._('IMPORT_SCANS_LABEL').format(scan=0)))
        self.scans_gate.setSizeAdjustPolicy(QComboBox.AdjustToMinimumContentsLengthWithIcon)
        self.scans_gate.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        lay15 = QHBoxLayout()
        lay15.addWidget(QLabel(self.translator._('IMPORT_SCANS')))
        lay15.addWidget(self.scans_gate_all_radio)
        lay15.addWidget(self.scans_gate_select_radio)
        lay15.addWidget(self.scans_gate)
        
        lay16 = QGridLayout()
        lay16.addLayout(lay12,  0, 0, 1, 4)
        lay16.addLayout(lay13,  1, 0, 1, 4)
        lay16.addLayout(lay14,  2, 0, 5, 4)
        lay16.addLayout(lay14c, 7, 0, 1, 4)
        lay16.addWidget(pcgui.qSeparatorLine(), 8, 0, 1, 4)
        lay16.addWidget(self.mode4_gate_radio, 9, 0)
        lay16.addWidget(self.mode2_gate_radio, 10, 0)
        lay16.addWidget(self.diffabs_gate_radio, 9, 1)
        lay16.addWidget(self.sigdiff_gate_radio, 10, 1)
        lay16.addWidget(pcgui.qSeparatorLine(), 11, 0, 1, 4)
        lay16.addLayout(lay15, 12, 0, 1, 4)
        self.group_gate = QGroupBox(self.translator._('IMPORT_GATE_GROUP'))
        self.group_gate.setLayout(lay16)
        self.group_gate.setCheckable(True)
        self.group_gate.setChecked(False)
        
        # sellmeier group
        self.sellmeier_widget = SellmeierWidget(self.sellmeier_handle, self.translator)
        lay18 = QHBoxLayout()
        lay18.addWidget(self.sellmeier_widget)
        
        self.group_sellmeier = QGroupBox(self.translator._('IMPORT_SELLMEIER_GROUP'))
        self.group_sellmeier.setCheckable(True)
        self.group_sellmeier.setChecked(False)
        self.group_sellmeier.setLayout(lay18)
        
        # table group
        self.table = QTableWidget()
        lay11 = QVBoxLayout()
        lay11.addWidget(self.table)
        
        self.add_table_column_button = pcgui.qPushButton30()
        self.delete_table_column_button = pcgui.qPushButton30()
        self.add_table_row_button = pcgui.qPushButton30()
        self.delete_table_row_button = pcgui.qPushButton30()
        self.replace_nan_button = pcgui.qPushButton30()
        self.replace_inf_button = pcgui.qPushButton30()
        self.table_edit_switch_button = pcgui.qPushButton30()
        
        self.add_table_column_button.setIcon(QIcon('../images/icons/add-col.svg'))
        self.delete_table_column_button.setIcon(QIcon('../images/icons/remove-col.svg'))
        self.add_table_row_button.setIcon(QIcon('../images/icons/add-row.svg'))
        self.delete_table_row_button.setIcon(QIcon('../images/icons/remove-row.svg'))
        self.replace_nan_button.setIcon(QIcon('../images/icons/replace-nan.svg'))
        self.replace_inf_button.setIcon(QIcon('../images/icons/replace-inf.svg'))
        self.table_edit_switch_button.setIcon(QIcon('../images/icons/edit.svg'))
        
        self.add_table_column_button.setToolTip(self.translator._('IMPORT_TABLE_ADD_COL'))
        self.delete_table_column_button.setToolTip(self.translator._('IMPORT_TABLE_DEL_COL'))
        self.add_table_row_button.setToolTip(self.translator._('IMPORT_TABLE_ADD_ROW'))
        self.delete_table_row_button.setToolTip(self.translator._('IMPORT_TABLE_DEL_ROW'))
        self.replace_nan_button.setToolTip(self.translator._('IMPORT_TABLE_REPLACE_NAN'))
        self.replace_inf_button.setToolTip(self.translator._('IMPORT_TABLE_REPLACE_INF'))
        self.table_edit_switch_button.setToolTip(self.translator._('IMPORT_TABLE_ENABLE_EDIT'))
        
        self.table_edit_switch_button.setFlat(True)
        self.table_edit_switch_button.setCheckable(True)
        
        lay17 = QVBoxLayout()
        lay17.addWidget(self.add_table_column_button)
        lay17.addWidget(self.delete_table_column_button)
        lay17.addWidget(self.add_table_row_button)
        lay17.addWidget(self.delete_table_row_button)
        lay17.addWidget(self.replace_nan_button)
        lay17.addWidget(self.replace_inf_button)
        lay17.addWidget(self.table_edit_switch_button)
        
        lay19 = QHBoxLayout()
        lay19.addLayout(lay11)
        lay19.addLayout(lay17)
        group_11 = QGroupBox(self.translator._('IMPORT_TABLE_GROUP'))
        group_11.setLayout(lay19)
        
        # main layout
        lay_00 = QGridLayout()
        lay_00.addWidget(self.group_main, 0, 0)
        lay_00.addWidget(self.group_solv, 0, 1)
        lay_00.addWidget(self.group_gate, 0, 2)
        lay_00.addWidget(self.group_sellmeier, 0, 3)
        
        # time traces group
        self.traces_rows_spin = QSpinBox()
        self.traces_cols_spin = QSpinBox()
        self.traces_rows_spin.setRange(1, 5)
        self.traces_cols_spin.setRange(1, 2)
        self.traces_rows_spin.setValue(5)
        self.traces_cols_spin.setValue(2)
        self.traces_time_min_edit = pcgui.qRLineEdit60('-1')
        self.traces_time_max_edit = pcgui.qRLineEdit60('4000')
        self.traces_time_axis_combo = QComboBox()
        self.traces_time_axis_combo.addItems(['linear', 'log', 'symlog'])
        self.traces_time_axis_combo.setCurrentText('symlog')
        self.traces_time_auto_scale = QCheckBox(self.translator._('IMPORT_PLOT_AUTO'))
        self.traces_time_auto_scale.setChecked(Qt.Unchecked)
        self.traces_amp_min_edit = pcgui.qRLineEdit60('-5')
        self.traces_amp_max_edit = pcgui.qRLineEdit60('35')
        self.traces_amp_auto_scale = QCheckBox(self.translator._('IMPORT_PLOT_AUTO'))
        self.traces_amp_multiply = QCheckBox(self.translator._('IMPORT_PLOT_MULTIPLY'))
        self.traces_amp_auto_scale.setChecked(Qt.Checked)
        self.traces_amp_multiply.setChecked(Qt.Checked)
        
        lay21 = QGridLayout()
        lay21.addWidget(QLabel(self.translator._('IMPORT_PLOT_ROWS')), 1, 0)
        lay21.addWidget(QLabel(self.translator._('IMPORT_PLOT_COLS')), 2, 0)
        lay21.addWidget(self.traces_rows_spin, 1, 1)
        lay21.addWidget(self.traces_cols_spin, 2, 1)
        lay21.addWidget(pcgui.qDummyLabel(20), 0, 2)
        lay21.addWidget(pcgui.qCLabel(self.translator._('IMPORT_PLOT_MIN')), 0, 4)
        lay21.addWidget(pcgui.qCLabel(self.translator._('IMPORT_PLOT_CUT')), 0, 5)
        lay21.addWidget(pcgui.qCLabel(self.translator._('IMPORT_PLOT_MAX')), 0, 6)
        lay21.addWidget(QLabel(self.translator._('IMPORT_PLOT_AMP')), 1, 3)
        lay21.addWidget(QLabel(self.translator._('IMPORT_PLOT_TIME')), 2, 3)
        lay21.addWidget(self.traces_time_min_edit, 2, 4)
        lay21.addWidget(self.traces_time_axis_combo, 2, 5)
        lay21.addWidget(self.traces_time_max_edit, 2, 6)
        lay21.addWidget(self.traces_time_auto_scale, 2, 7)
        lay21.addWidget(self.traces_amp_min_edit, 1, 4)
        lay21.addWidget(self.traces_amp_max_edit, 1, 6)
        lay21.addWidget(self.traces_amp_auto_scale, 1, 7)
        
        self.traces_corr_button = pcgui.qColoredButton(self.translator._('IMPORT_PLOT_CORR'), (0, 0, 0), 100)
        self.traces_main_button = pcgui.qColoredButton(self.translator._('IMPORT_PLOT_MAIN'), pcgui.GREEN, 100)
        self.traces_solv_button = pcgui.qColoredButton(self.translator._('IMPORT_PLOT_SOLV'), pcgui.BLUE, 100)
        self.traces_gate_button = pcgui.qColoredButton(self.translator._('IMPORT_PLOT_GATE'), pcgui.ORANGE, 100)
        self.traces_corr_button.setChecked(True)
        self.traces_corr_button.setEnabled(False)
        self.traces_main_button.setEnabled(False)
        self.traces_solv_button.setEnabled(False)
        self.traces_gate_button.setEnabled(False)
        
        lay22 = QHBoxLayout()
        lay22.addWidget(self.traces_corr_button)
        lay22.addWidget(self.traces_main_button)
        lay22.addWidget(self.traces_solv_button)
        lay22.addWidget(self.traces_gate_button)
        
        self.traces_first_button, self.traces_last_button = pcgui.qPushButton30(), pcgui.qPushButton30()
        self.traces_prev_button, self.traces_next_button = pcgui.qPushButton30(), pcgui.qPushButton30()
        self.traces_back_button, self.traces_forward_button = pcgui.qPushButton30(), pcgui.qPushButton30()
        
        self.traces_first_button.setToolTip(self.translator._('IMPORT_PLOT_GOTO_FIRST'))
        self.traces_last_button.setToolTip(self.translator._('IMPORT_PLOT_GOTO_LAST'))
        self.traces_prev_button.setToolTip(self.translator._('IMPORT_PLOT_GOTO_PREV'))
        self.traces_next_button.setToolTip(self.translator._('IMPORT_PLOT_GOTO_NEXT'))
        self.traces_back_button.setToolTip(self.translator._('IMPORT_PLOT_GOTO_BACK'))
        self.traces_forward_button.setToolTip(self.translator._('IMPORT_PLOT_GOTO_FRWD'))
        
        self.traces_first_button.setIcon(QIcon('../images/icons/go-first.svg'))
        self.traces_last_button.setIcon(QIcon('../images/icons/go-last.svg'))
        self.traces_prev_button.setIcon(QIcon('../images/icons/go-previous.svg'))
        self.traces_next_button.setIcon(QIcon('../images/icons/go-next.svg'))
        self.traces_back_button.setIcon(QIcon('../images/icons/go-back.svg'))
        self.traces_forward_button.setIcon(QIcon('../images/icons/go-forward.svg'))
        
        self.traces_goto_button = pcgui.qPushButton30()
        self.traces_goto_button.setToolTip(self.translator._('IMPORT_PLOT_GOTO_GOTO'))
        self.traces_goto_button.setIcon(QIcon('../images/icons/go-to.svg'))
        self.traces_goto_spinbox = pcgui.qIntSpinBox(400,200,800,1)
        
        self.traces_update_button = QPushButton(self.translator._('IMPORT_PLOT_UPDATE'))
        
        self.traces_first_button.setEnabled(False)
        self.traces_back_button.setEnabled(False)
        self.traces_prev_button.setEnabled(False)
        self.traces_next_button.setEnabled(False)
        self.traces_forward_button.setEnabled(False)
        self.traces_last_button.setEnabled(False)
        self.traces_update_button.setEnabled(False)
        self.traces_goto_spinbox.setEnabled(False)
        self.traces_goto_button.setEnabled(False)
        
        lay23 = QHBoxLayout()
        lay23.addWidget(self.traces_update_button)
        lay23.addStretch()
        lay23.addWidget(self.traces_amp_multiply)
        lay23.addStretch()
        lay23.addWidget(self.traces_first_button)
        lay23.addWidget(self.traces_back_button)
        lay23.addWidget(self.traces_prev_button)
        lay23.addWidget(self.traces_next_button)
        lay23.addWidget(self.traces_forward_button)
        lay23.addWidget(self.traces_last_button)
        lay23.addStretch()
        lay23.addWidget(self.traces_goto_spinbox)
        lay23.addWidget(self.traces_goto_button)
        
        self.figure = mplFigure()
        self.figure.set_tight_layout('True')
        self.canvas = mplCanvas(self.figure)
        lay20 = QVBoxLayout()
        lay20.addLayout(lay21)
        lay20.addWidget(pcgui.qSeparatorLine())
        lay20.addLayout(lay22)
        lay20.addWidget(pcgui.qSeparatorLine())
        lay20.addLayout(lay23)
        lay20.addStretch()
        lay20.addWidget(self.canvas)
        
        # call these before handing the canvas over to the splitter
        self.setup_time_traces()
        self.finalize_time_traces()
        
        # splitter
        lay_top = QWidget()
        lay_top.setLayout(lay_00)
        vsplitter = QSplitter(Qt.Vertical)
        vsplitter.addWidget(lay_top)
        vsplitter.addWidget(group_11)
        vsplitter.setSizes([lay_top.minimumSizeHint().height(), 10000])
        vsplitter.setChildrenCollapsible(False)
        
        lay_right = QWidget()
        lay_right.setLayout(lay20)
        hsplitter = QSplitter(Qt.Horizontal)
        hsplitter.addWidget(vsplitter)
        hsplitter.addWidget(lay_right)
        hsplitter.setSizes([10000, lay_right.minimumSizeHint().width()])
        hsplitter.setChildrenCollapsible(False)
        
        lay_main = QVBoxLayout()
        lay_main.addWidget(hsplitter)
        self.setLayout(lay_main)
    
    def change_layout (self):
        """ Enable widgets according to user selection of extraction mode. """
        state = (self.mode4_radio.isChecked() or self.mode2_radio.isChecked())
        self.reference_corr_rb1.setEnabled(state)
        self.reference_corr_rb2.setEnabled(state)
        self.group_solv.setEnabled(state)
        self.group_gate.setEnabled(state)
        self.group_sellmeier.setEnabled(state)
        self.traces_amp_multiply.setEnabled(state)
        self.traces_main_button.setEnabled(state and self.main_loaded)
        self.traces_solv_button.setEnabled(state and self.group_solv.isChecked() and self.solv_loaded)
        self.traces_gate_button.setEnabled(state and self.group_gate.isChecked() and self.gate_loaded)
        self.change_main_bg_layout()
    
    def change_solv_factor_label (self):
        """ Change solvent factor label texts according to current method of calculation.  """
        if self.solv_factor_combo.currentText() == self.translator._('IMPORT_SOLV_FREE'):
            self.solv_factor_label_1.setText(self.translator._('IMPORT_SOLV_FREE_LABEL'))
            self.solv_factor_label_2.setText('')
        else:
            self.solv_factor_label_1.setText(self.translator._('IMPORT_SOLV_ABS_LABEL'))
            self.solv_factor_label_2.setText(self.translator._('IMPORT_SOLV_PATHLENGTH'))
    
    def change_solvent_correction (self):
        """ Enable solvent plot button according to availability of solvent data. """
        self.traces_solv_button.setEnabled(self.group_solv.isChecked() and self.solv_loaded)
    
    def change_timezero_correction (self):
        """ Switch type of time zero correction based on user's choice. """
        self.traces_gate_button.setEnabled(self.group_gate.isChecked() and self.gate_loaded)
        if not self.sender().isChecked():
            return  # important to avoid infinite recursion
        sender_is_gate = (self.sender() == self.group_gate)
        self.group_gate.setChecked(sender_is_gate)
        self.group_sellmeier.setChecked(not sender_is_gate)
    
    def setup_gate_dialog (self):
        """ Show setup gate dialog with current file settings. """
        scan_selected = lambda i: self.scans_gate_all_radio.isChecked() or self.scans_gate.isChecked(i)
        scans = [i+1 for i in range(self.scans_gate.count()) if scan_selected(i)]
        
        if self.data_gate_bg_check.isEnabled() and self.data_gate_bg_check.isChecked():
            bgsrc = self.data_gate_bg_line_edit.text()
        else:
            bgsrc = ''
        
        t_start_i = pc.lookup_index(self.gate_start_time, self.netcdfimport['gatedata']['times'])
        t_stop_i = pc.lookup_index(self.gate_stop_time, self.netcdfimport['gatedata']['times'])
        
        self.setup_gate_dlg.set_file_settings(self.data_gate_line_edit.text(), bgsrc,
            (self.netcdfimport['gatedata']['wlns'][0],self.netcdfimport['gatedata']['wlns'][-1],self.netcdfimport['gatedata']['wlns'].size),
            (self.netcdfimport['gatedata']['times'][t_start_i],self.netcdfimport['gatedata']['times'][t_stop_i],self.netcdfimport['gatedata']['times'][t_start_i:t_stop_i].size),
            self.translator._('IMPORT_MODE4_GATE') if self.mode4_gate_radio.isChecked() else self.translator._('IMPORT_MODE2_GATE'),
            self.translator._('IMPORT_DIFFABS') if self.diffabs_gate_radio.isChecked() else self.translator._('IMPORT_SIGDIFF'),
            ', '.join(map(str, scans)))
        
        cache = self.setup_gate_dlg.get_processing_settings()
        if self.setup_gate_dlg.exec() == QDialog.Rejected:
            self.setup_gate_dlg.set_processing_settings(cache)
    
    def update_scan_list (self, nscans, scan_combo):
        """ Make list for `nscans` in given `scan_combo` box. """
        scan_combo.clear()
        if nscans >= 0:
            for i in range(nscans):
                scan_combo.addItem(self.translator._('IMPORT_SCANS_LABEL').format(scan=i+1))
                scan_combo.setScanChecked(i, True)
        scan_combo.updateText()
    
    def change_main_layout (self, choppermode, nscans):
        """ Enable main widgets according to available chopper positions and number of scans. """
        self.mode4_radio.setEnabled(choppermode is pcnetcdf.ChopperMode.TwoChopper)
        self.pump_radio.setEnabled(choppermode is pcnetcdf.ChopperMode.TwoChopper)
        self.background_radio.setEnabled(choppermode is pcnetcdf.ChopperMode.TwoChopper)
        self.update_scan_list(nscans, self.scans)
    
    def change_solv_layout (self, choppermode, nscans):
        """ Enable solvent widgets according to available chopper positions and number of scans. """
        self.mode4_solv_radio.setEnabled(choppermode is pcnetcdf.ChopperMode.TwoChopper)
        self.update_scan_list(nscans, self.scans_solv)
    
    def change_gate_layout (self, choppermode, nscans):
        """ Enable gate widgets according to available chopper positions and number of scans. """
        self.mode4_gate_radio.setEnabled(choppermode is pcnetcdf.ChopperMode.TwoChopper)
        self.update_scan_list(nscans, self.scans_gate)
    
    def change_main_bg_layout (self):
        """ Enable background widgets according to available chopper mode and current background selection. """
        mode2 = self.mode2_radio.isChecked()
        bg = self.data_bg_check.checkState()
        self.data_bg_check.setEnabled(mode2)
        self.data_bg_line_edit.setEnabled(mode2 and bg)
        self.data_bg_file_dlg_button.setEnabled(mode2 and bg)
    
    def change_solv_bg_layout (self):
        """ Enable solvent background widgets according to available chopper mode and current background selection. """
        mode2 = self.mode2_solv_radio.isChecked()
        bg = self.data_solv_bg_check.checkState()
        self.data_solv_bg_check.setEnabled(mode2)
        self.data_solv_bg_line_edit.setEnabled(mode2 and bg)
        self.data_solv_bg_file_dlg_button.setEnabled(mode2 and bg)
    
    def change_gate_bg_layout (self):
        """ Enable gate background widgets according to available chopper mode and current user settings. """
        mode2 = self.mode2_gate_radio.isChecked()
        diffabs = self.diffabs_gate_radio.isChecked()
        bg = self.data_gate_bg_check.checkState()
        self.data_gate_bg_check.setEnabled(mode2 and diffabs)
        self.data_gate_bg_line_edit.setEnabled(mode2 and diffabs and bg)
        self.data_gate_bg_file_dlg_button.setEnabled(mode2 and diffabs and bg)
    
    def change_main_scan_layout (self):
        """ Enable widgets according to user selection regarding main data scans. """
        self.scans.setEnabled(self.scans_select_radio.isChecked())
    
    def change_solv_scan_layout (self):
        """ Enable widgets according to user selection regarding solvent data scans. """
        self.scans_solv.setEnabled(self.scans_solv_select_radio.isChecked())
    
    def change_gate_scan_layout (self):
        """ Enable widgets according to user selection regarding gate data scans. """
        self.scans_gate.setEnabled(self.scans_gate_select_radio.isChecked())
    
    # -----------------------------------------------------------------
    #   HANDLING DATA FILES
    # -----------------------------------------------------------------
    
    def update_data_file (self):
        """ Get data file path from open dialog and display meta data. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'), self.working_dir, pc.FILE_TYPE_NCDF)
        if file_name != '':
            try:
                mode, scans = self.netcdfimport.load_main_data(file_name)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            except pcerror.RangeError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input, ranges=e.ranges), self.translator._(e.title))
                dlg.exec()
            else:
                self.data_line_edit.setText(file_name)
                self.main_loaded = True
                self.change_main_layout(mode, scans)
                self.wln_count_label.setText(str(len(self.netcdfimport['maindata']['wlns'])))
                self.time_count_label.setText(str(len(self.netcdfimport['maindata']['times'])))
                self.shot_count_label.setText(str(self.netcdfimport['maindata']['nshots']))
                self.scan_count_label.setText(str(self.netcdfimport['maindata']['nscans']))
                self.mode_count_label.setText(str(self.netcdfimport['maindata']['mode'].value))
                self.traces_corr_button.setEnabled(True)
                self.traces_main_button.setEnabled(True)
    
    def update_bg_file (self):
        """ Get background data file path from open dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'), self.working_dir, pc.FILE_TYPE_NCDF)
        if file_name != '':
            try:
                self.netcdfimport.load_bg_data(file_name)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            else:
                self.data_bg_line_edit.setText(file_name)
    
    def update_solv_file (self):
        """ Get solvent data file path and meta data from open dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'), self.working_dir, pc.FILE_TYPE_NCDF)
        if file_name != '':
            try:
                mode, scans = self.netcdfimport.load_solv_data(file_name)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            else:
                self.data_solv_line_edit.setText(file_name)
                self.solv_loaded = True
                self.change_solv_layout(mode, scans)
                self.change_solvent_correction()
    
    def update_solv_bg_file (self):
        """ Get solvent background data file path from open dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'), self.working_dir, pc.FILE_TYPE_NCDF)
        if file_name != '':
            try:
                self.netcdfimport.load_solv_bg_data(file_name)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            else:
                self.data_solv_bg_line_edit.setText(file_name)
    
    def update_gate_file (self):
        """ Get gate data file path and meta data from open dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'), self.working_dir, pc.FILE_TYPE_NCDF)
        if file_name != '':
            try:
                mode, scans = self.netcdfimport.load_gate_data(file_name)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            else:
                self.data_gate_line_edit.setText(file_name)
                self.gate_loaded = True
                self.gate_dlg_button.setEnabled(True)
                self.change_gate_layout(mode, scans)
                self.change_timezero_correction()
    
    def update_gate_bg_file (self):
        """ Get gate background data file path from open dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'), self.working_dir, pc.FILE_TYPE_NCDF)
        if file_name != '':
            try:
                self.netcdfimport.load_gate_bg_data(file_name)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            else:
                self.data_gate_bg_line_edit.setText(file_name)
    
    # -----------------------------------------------------------------
    #   DATA MANIPULATION
    # -----------------------------------------------------------------
    
    @pyqtSlot('QString', 'QString', dict)
    def generate_time_zeros (self, gatemethod, gateprocessing, gatesettings):
        """
        Calculate time zeros from gate measurements.
        
        Determines time zeros according to user settings and hands
        them over to the setup dialog for plotting.
        
        Parameters
        ----------
        gatemethod : str
            method from setup dialog
        
        gateprocessing : ['off', 'smooth', 'fit']
            method of post-processing
        
        gatesettings : dict
            settings as dict from setup dialog
        """
        if self.scans_gate_all_radio.isChecked():
            gatescans = list(range(self.scans_gate.count()))
        else:
            gatescans = [i for i in range(self.scans_gate.count()) if self.scans_gate.isChecked(i)]
        gatedict = {'bgcorr': self.data_gate_bg_check.isChecked(),
                    'mode':   pcnetcdf.ChopperMode.TwoChopper if self.mode4_gate_radio.isChecked() else pcnetcdf.ChopperMode.OneChopper,
                    'type':   'sigdiff' if self.sigdiff_gate_radio.isChecked() else 'diffabs',
                    'scans':  gatescans, 'time_range': (self.gate_start_time, self.gate_stop_time),
                    'method': gatemethod, 'processing': gateprocessing, 'settings': gatesettings}
        try:
            wlns, peaks = self.netcdfimport.extract_gate_peaks(gatedict)
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
        else:
            self.setup_gate_dlg.show_preview(wlns, peaks)
    
    def generate_data (self):
        """
        Generate corrected data.
        
        Reads user settings and generates (corrected) data, which is displayed
        in the data value table.
        
        Raises
        ------
        pcerror.RangeError
            if an invalid absorption is entered
            if an invalid solvent time shift is entered
            if an invalid material thickness is entered
        """
        activate = self.data is None  # enable stuff when data is loaded for the first time
        
        if not self.main_loaded:
            raise pcerror.InputError('ERROR_IMPORT_MISSING_MAIN_ERROR')
        
        # read main data settings
        if self.mode4_radio.isChecked():
            mainmode = pcnetcdf.ChopperMode.TwoChopper
        elif self.mode2_radio.isChecked():
            mainmode = pcnetcdf.ChopperMode.OneChopper
        elif self.pump_radio.isChecked():
            mainmode = 'pump'
        elif self.probe_radio.isChecked():
            mainmode = 'probe'
        elif self.pumpprobe_radio.isChecked():
            mainmode = 'pumpprobe'
        else:
            mainmode = 'bg'
        if self.scans_all_radio.isChecked():
            mainscans = list(range(self.scans.count()))
        else:
            mainscans = [i for i in range(self.scans.count()) if self.scans.isChecked(i)]
        maindict = {'bgcorr': self.data_bg_check.isChecked(), 'refcorr': self.reference_corr_rb1.isChecked(),
                    'mode':   mainmode, 'scans': mainscans}
        
        # read solvent settings
        if self.group_solv.isChecked():
            if not self.solv_loaded:
                raise pcerror.InputError('ERROR_IMPORT_MISSING_SOLV_ERROR')
            
            if self.scans_solv_all_radio.isChecked():
                solvscans = list(range(self.scans_solv.count()))
            else:
                solvscans = [i for i in range(self.scans_solv.count()) if self.scans_solv.isChecked(i)]
            solvmethod = self.solv_factor_combo.currentText()
            if solvmethod == self.translator._('IMPORT_SOLV_FACTOR'):
                solvmethod = 'factor'
            elif solvmethod == self.translator._('IMPORT_SOLV_TRANSMISSION'):
                solvmethod = 'transmission'
            elif solvmethod == self.translator._('IMPORT_SOLV_ABSORPTION'):
                solvmethod = 'absorption'
            else:
                solvmethod = 'free'
            try:
                tmp1 = float(self.solv_factor_edit.text())
            except:
                raise pcerror.RangeError('ERROR_IMPORT_INVALID_ABSORPTION_ERROR', input=self.solv_factor_edit.text())
            try:
                tmp2 = float(self.solv_shift_edit.text())
            except:
                raise pcerror.RangeError('ERROR_IMPORT_INVALID_TIME_SHIFT_ERROR', input=self.solv_shift_edit.text())
            
            solvdict = {'bgcorr':  self.data_solv_bg_check.isChecked(),
                        'refcorr': self.reference_corr_solv_rb1.isChecked(),
                        'mode':    pcnetcdf.ChopperMode.TwoChopper if self.mode4_solv_radio.isChecked() else pcnetcdf.ChopperMode.OneChopper,
                        'scans':   solvscans, 'absorption': tmp1, 'shift': tmp2, 'method': solvmethod}
        else:
            solvdict = None
        
        # read gate settings
        if self.group_gate.isChecked():
            if not self.gate_loaded:
                raise pcerror.InputError('ERROR_IMPORT_MISSING_GATE_ERROR')
            
            try:
                tmp = float(self.gate_shift_edit.text())
            except:
                raise pcerror.RangeError('ERROR_IMPORT_INVALID_TIME_SHIFT_ERROR', input=self.gate_shift_edit.text())
            if self.scans_gate_all_radio.isChecked():
                gatescans = list(range(self.scans_gate.count()))
            else:
                gatescans = [i for i in range(self.scans_gate.count()) if self.scans_gate.isChecked(i)]
            gatemethod, gateprocessing, gatesettings = self.setup_gate_dlg.get_processing_settings()
            gatedict = {'bgcorr':   self.data_gate_bg_check.isChecked(),
                        'mode':     pcnetcdf.ChopperMode.TwoChopper if self.mode4_gate_radio.isChecked() else pcnetcdf.ChopperMode.OneChopper,
                        'type':     'sigdiff' if self.sigdiff_gate_radio.isChecked() else 'diffabs',
                        'scans':    gatescans, 'time_range': (self.gate_start_time, self.gate_stop_time),
                        'method':   gatemethod, 'processing': gateprocessing,
                        'settings': gatesettings, 'shift': tmp}
        else:
            gatedict = None
        
        # read sellmeier thicknesses
        if self.group_sellmeier.isChecked():
            sellmeierdict = {
                'list': self.sellmeier_widget.materialsList(),
                'offset': self.sellmeier_widget.offsetTime(),
                'handle': self.sellmeier_handle
            }
        else:
            sellmeierdict = None
        
        # generate data
        try:
            if type(mainmode) is pcnetcdf.ChopperMode:
                self.data, self.wlns, self.times = self.netcdfimport.correct_main_data(maindict, self.monitor_calculation_progress,
                                                        solvdict=solvdict, gatedict=gatedict, sellmeierdict=sellmeierdict,
                                                        wln_tolerance=self.wln_tolerance, time_tolerance=self.time_tolerance)
            else:
                self.data, self.wlns, self.times = self.netcdfimport.extract_main_data(maindict)
        
        except pcerror.InputError as e:
            self.monitor_calculation_progress(100, 'STATUS_IMPORT_FAILED')
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
            return
        
        except pcerror.RangeError as e:
            self.monitor_calculation_progress(100, 'STATUS_IMPORT_FAILED')
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input, ranges=e.ranges), self.translator._(e.title))
            dlg.exec()
            return
        
        except pcerror.InternalError as e:
            self.monitor_calculation_progress(100, 'STATUS_IMPORT_FAILED')
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
            return
        
        # show result
        if activate and self.data is not None:
            self.traces_update_button.setEnabled(True)
            self.traces_first_button.setEnabled(True)
            self.traces_back_button.setEnabled(True)
            self.traces_prev_button.setEnabled(True)
            self.traces_goto_button.setEnabled(True)
            self.traces_next_button.setEnabled(True)
            self.traces_forward_button.setEnabled(True)
            self.traces_last_button.setEnabled(True)
            self.traces_goto_spinbox.setEnabled(True)
        self.traces_goto_spinbox.setRange(int(self.wlns[0]), int(self.wlns[-1]))
        
        self.display_data()
        if self.group_gate.isChecked():
            self.setup_gate_dlg.show_recent_time_zeros(self.wlns, self.netcdfimport['timezeros'])
        else:
            self.setup_gate_dlg.clear_preview()
    
    def display_data (self):
        """ Display corrected data in table. """
        
        # Items default to editable. Switch that flag off, if necessary, remain otherwise.
        flag = Qt.ItemIsEditable if not self.table_editable else 0x00
        
        ntimes = len(self.times)
        nwlns = len(self.wlns)
        self.table.clear()
        self.table.setRowCount(ntimes)
        self.table.setColumnCount(nwlns)
        for time in range(ntimes):
            for wln in range(nwlns):
                item = QTableWidgetItem(f"{self.data[time, wln]:.6f}")
                item.setFlags(item.flags() ^ flag)
                item.setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
                self.table.setItem(time, wln, item)
        self.table.setHorizontalHeaderLabels([f"{i:.1f} nm" for i in self.wlns])
        self.table.setVerticalHeaderLabels([f"{i:.3f} ps" for i in self.times])
        for row in range(ntimes):
            self.table.verticalHeaderItem(row).setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
    
    def update_data_from_table (self):
        """ Retrieve data from table. """
        ncols = self.table.columnCount()
        nrows = self.table.rowCount()
        try:
            self.data = np.asarray([[self.table.item(r, c).text() for c in range(ncols)] for r in range(nrows)], float)
        except ValueError as error:
            userinput = re.search("'.*'", error.args[0])
            e = pcerror.RangeError('ERROR_IMPORT_INVALID_NUMBER', input=userinput.group())
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
            return False
        else:
            return True
    
    def update_axes_from_table (self):
        """ Retrieve wavelength and time axis values from table header. """
        try:
            self.wlns = np.asarray(
                [self.table.model().headerData(i, Qt.Horizontal)[:-3] for i in range(self.table.columnCount())], float
            )
            self.times = np.asarray(
                [self.table.model().headerData(i, Qt.Vertical)[:-3] for i in range(self.table.rowCount())], float
            )
        except ValueError as error:
            userinput = re.search("'.*'", error.args[0])
            e = pcerror.RangeError('ERROR_IMPORT_INVALID_NUMBER', input=userinput.group())
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
            return False
        else:
            return True
    
    def add_wavelength_dialog (self):
        """ Receive user inputs for adding a wavelength column from dialog. """
        if self.table.currentColumn() == -1:
            return
        dlg = InsertDataDialog(self)
        dlg.setup('column', self.translator._('IMPORT_INSERT_WAVELENGTH'), self.table.horizontalHeaderItem(self.table.currentColumn()).text()[:-3], 'nm')
        dlg.insert_requested.connect(self.add_wavelength_to_table)
        dlg.exec()
    
    @pyqtSlot(float, float)
    def add_wavelength_to_table (self, wavelength, value):
        """ Add one wavelength column to table and initialize with `value`. """
        self.table.insertColumn(self.table.currentColumn())
        col = self.table.currentColumn() - 1
        
        # Items default to editable. Switch that flag off, if necessary, remain otherwise.
        flag = Qt.ItemIsEditable if not self.table_editable else 0x00
        
        for row in range(self.table.rowCount()):
            item = QTableWidgetItem(f"{value:.6f}")
            item.setFlags(item.flags() ^ flag)
            item.setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
            self.table.setItem(row, col, item)
        self.table.setHorizontalHeaderItem(col, QTableWidgetItem(f"{wavelength:.1f} nm"))
    
    def delete_wavelength_from_table (self):
        """ Remove current wavelength information from data table. """
        if self.table.currentColumn() == -1:
            return
        col = self.table.currentColumn()
        self.table.removeColumn(col)
        if col <= self.wln_i:
            self.goto_prev_wln()
        if (col >= self.wln_i) and (col < self.wln_i + self.traces_rows_spin.value() * self.traces_cols_spin.value()):
            self.update_time_traces()
    
    def add_time_dialog (self):
        """ Receive user inputs for adding a time row from dialog. """
        if self.table.currentRow() == -1:
            return
        dlg = InsertDataDialog(self)
        dlg.setup('row', self.translator._('IMPORT_INSERT_TIME'), self.table.verticalHeaderItem(self.table.currentRow()).text()[:-3], 'ps')
        dlg.insert_requested.connect(self.add_time_to_table)
        dlg.exec()
    
    @pyqtSlot(float, float)
    def add_time_to_table (self, time, value):
        """ Add one time row to table and initialize with `value`. """
        self.table.insertRow(self.table.currentRow())
        row = self.table.currentRow() - 1
        
        # Items default to editable. Switch that flag off, if necessary, remain otherwise.
        flag = Qt.ItemIsEditable if not self.table_editable else 0x00
        
        for col in range(self.table.columnCount()):
            item = QTableWidgetItem(f"{value:.6f}")
            item.setFlags(item.flags() ^ flag)
            item.setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
            self.table.setItem(row, col, item)
        self.table.setVerticalHeaderItem(row, QTableWidgetItem(f"{time:.3f} ps"))
        self.table.verticalHeaderItem(row).setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
    
    def delete_time_from_table (self):
        """ Remove current time information from data table. """
        self.table.removeRow(self.table.currentRow())
    
    def switch_data_table_edit_behaviour (self):
        """ Switch editability of data table items. """
        
        # retrieve changes made to table, if necessary and possible
        if self.table_editable:
            if not self.update_data_from_table():
                self.table_edit_switch_button.setChecked(self.table_editable)
                return
        
        # toggle flags on table items
        ncols = self.table.columnCount()
        nrows = self.table.rowCount()
        for row in range(nrows):
            for col in range(ncols):
                item = self.table.item(row, col)
                item.setFlags(item.flags() ^ Qt.ItemIsEditable)
        
        # toggle property
        self.table_editable = not self.table_editable
    
    def replace_nan_dialog (self):
        """ Receive user inputs for nan replacement from dialog. """
        dlg = ReplaceNaNDialog(self)
        dlg.nan_replacement_requested.connect(self.replace_nan)
        dlg.exec()
    
    @pyqtSlot(float)
    def replace_nan (self, value):
        """
        Replace all NaNs in data table by `value`.
        
        Parameters
        ----------
        value : float
            value to replace NaNs with
        """
        if self.data is None:
            return 0
        count = pc.replace_nan(self.data, value)
        self.display_data()
        self.new_status.emit(self.translator._('STATUS_IMPORT_NAN_REPLACED').format(number=count), 5000)
    
    def replace_inf_dialog (self):
        """ Receive user inputs for inf replacement from dialog. """
        dlg = ReplaceInfDialog(self)
        dlg.inf_replacement_requested.connect(self.replace_inf)
        dlg.exec()
    
    @pyqtSlot(float, bool, bool)
    def replace_inf (self, value, pos=True, neg=True):
        """
        Replace all positive and/or negative infinite numbers in data table by `value`.
        
        Parameters
        ----------
        value : float
            value to replace infs with
        
        pos : bool
            whether to replace positive inf
        
        neg : bool
            whether to replace negative inf
        """
        if self.data is None:
            return 0
        count = pc.replace_inf(self.data, value, pos, neg)
        self.display_data()
        self.new_status.emit(self.translator._('STATUS_IMPORT_INF_REPLACED').format(number=count), 5000)
    
    def export_data (self, factor, z20_mode=False):
        """
        Export data as ASCII file.
        
        Exports data in a file guaranteed to be compatible either with pychart
        or with Z20, if `z20_mode` is set to True.
        
        Parameters
        ----------
        factor : float
            factor to multiply data with prior to export
        
        z20_mode : bool
            whether to use Z20 compatibility mode, defaults to False
        """
        if not self.update_data_from_table() or not self.update_axes_from_table():
            return
        
        file_name, file_filter = QFileDialog.getSaveFileName(self, self.translator._('FILE_EXPORT'),
                                    self.working_dir + '/' + self.tabname, pc.FILE_TYPE_FILTER_DATA)
        if file_name == '':
            return
        
        if factor is None:
            factor = 1
        
        if z20_mode:
            export_func = pc.export_z20_file
            status = 'STATUS_IMPORT_DATA_EXPORTED_Z20'
        else:
            export_func = pc.export_pychart_file
            status = 'STATUS_IMPORT_DATA_EXPORTED'
        
        try:
            export_func(self.data * factor, self.wlns, self.times, file_name)
            self.new_status.emit(self.translator._(status).format(file=file_name), 5000)
        except pcerror.OutputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(output=e.output), self.translator._(e.title))
            dlg.exec()
    
    def open_as_plot (self):
        """
        Save data in file to open it as `PlotTab`.
        
        Returns
        -------
        str
            file path of exported data
        """
        if not self.update_data_from_table() or not self.update_axes_from_table():
            return None
        else:
            return pc.export_pychart_file(self.data, self.wlns, self.times, QDir.currentPath() + '/../temp/plot-NetCDF-temp')
    
    # -----------------------------------------------------------------
    #   TIME TRACES PLOTS
    # -----------------------------------------------------------------
    
    def setup_time_traces (self):
        """ Create set of empty time traces according to current user settings. """
        nrows = self.traces_rows_spin.value()
        ncolumns = self.traces_cols_spin.value()
        self.figure.clear()
        self.grid = mplGrid(nrows, ncolumns)
        self.traces = [[None for col in range(ncolumns)] for row in range(nrows)]
        for row in range(nrows):
            for col in range(ncolumns):
                self.traces[row][col] = self.figure.add_subplot(self.grid[row, col])
        self.figure.subplots_adjust(wspace=0.3, hspace=0.5)
    
    def finalize_time_traces (self):
        """ Apply ranges and finalize time traces plots. """
        
        def wln_label (i, row, col):
            """ Generate wavelength label from index. """
            if self.wlns is not None:
                return f"{self.wlns[i + row + nrows * col]:.1f} nm"
            else:
                return ''
        
        try:
            if not self.traces_time_auto_scale.isChecked():
                minx = float(self.traces_time_min_edit.text())
                maxx = float(self.traces_time_max_edit.text())
            if not self.traces_amp_auto_scale.isChecked():
                miny = float(self.traces_amp_min_edit.text())
                maxy = float(self.traces_amp_max_edit.text())
        except ValueError:
            e = pcerror.RangeError('ERROR_IMPORT_INVALID_PLOT_RANGE_ERROR')
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
        else:
            nrows = self.traces_rows_spin.value()
            ncols = self.traces_cols_spin.value()
            scale = self.traces_time_axis_combo.currentText()
            for row in range(nrows):
                for col in range(ncols):
                    self.traces[row][col].set_xscale(scale)
                    if not self.traces_time_auto_scale.isChecked():
                        self.traces[row][col].set_xlim(minx, maxx)
                    else:
                        self.figure.axes[0].autoscale(axis='x', tight=True)
                    if not self.traces_amp_auto_scale.isChecked():
                        self.traces[row][col].set_ylim(miny, maxy)
                    else:
                        self.figure.axes[0].autoscale(axis='y', tight=True)
                    self.traces[row][col].set_title(wln_label(self.wln_i, row, col), fontdict={'fontsize': 9})
                    self.traces[row][col].tick_params(which='both', direction='in', top=True,
                                                      bottom=True, left=True, right=True, labelsize=9)
                    self.traces[row][col].grid(which='major', axis='x', color='gray', ls='--', lw=0.75)
                    self.traces[row][col].grid(which='minor', axis='x', color='gray', ls=':', lw=0.75)
        finally:
            # For some reason Qt does not resize the canvas to fill the available space.
            # As a workaround the figure size is set to a value likely to be larger than
            # the available widget space. To force Qt to scale the figure down to fit into
            # that space the canvas is resized to a very large size and back again to its
            # proper size. A single resize event, which not actually changes the size,
            # is ignored and therefore does not work.
            screensize = QApplication.desktop().screenGeometry(self).size() / self.figure.dpi
            self.figure.set_size_inches(screensize.width() * 0.5, screensize.height())
            size = self.canvas.size()
            self.canvas.resize(self.canvas.minumumSizeHint())
            self.canvas.resize(size)
            
            self.canvas.draw()
    
    def update_time_traces (self):
        """ Draw new set of time traces. """
        if not self.update_data_from_table() or not self.update_axes_from_table():
            return
        
        self.setup_time_traces()
        self.check_wln_index()
        nrows = self.traces_rows_spin.value()
        ncols = self.traces_cols_spin.value()
        f = 1000 if (self.traces_amp_multiply.isChecked() and self.traces_amp_multiply.isEnabled()) else 1
        
        if self.traces_corr_button.is_active() and (self.data is not None):
            for row in range(nrows):
                for col in range(ncols):
                    self.traces[row][col].plot(self.times, self.data[:, self.wln_i + row + nrows * col] * f,
                            '-', color=(0, 0, 0))
        
        if self.traces_main_button.is_active() and (self.netcdfimport['maindata']['diffabs'] is not None):
            for row in range(nrows):
                for col in range(ncols):
                    self.traces[row][col].plot(self.netcdfimport['maindata']['times'],
                            self.netcdfimport['maindata']['diffabs'].values[:,self.wln_i + row + nrows * col] * f,
                            '-', color=pc.mpl_color(pcgui.GREEN))
        
        if self.traces_solv_button.is_active() and (self.netcdfimport['solvdata']['diffabs'] is not None) \
                and not np.isscalar(self.netcdfimport['solvdata']['diffabs']): # could be set to 0 in plot.py ln 675
            for row in range(nrows):
                for col in range(ncols):
                    self.traces[row][col].plot(self.netcdfimport['solvdata']['times'] + float(self.solv_shift_edit.text()),
                            self.netcdfimport['solvdata']['diffabs'].values[:,self.wln_i + row + nrows * col] * f,
                            '-', color=pc.mpl_color(pcgui.BLUE))
        
        if self.traces_gate_button.is_active() and (self.netcdfimport['gatedata']['diffabs'] is not None):
            for row in range(nrows):
                for col in range(ncols):
                    self.traces[row][col].plot(self.netcdfimport['gatedata']['times'],
                            self.netcdfimport['gatedata']['diffabs'].values[:,self.wln_i + row + nrows * col],
                            '-', color=pc.mpl_color(pcgui.ORANGE))
        
        self.finalize_time_traces()
    
    def goto_first_wln (self):
        """ Go to first wavelength trace. """
        self.wln_i = 0
        self.update_time_traces()
    
    def goto_back_wln (self):
        """ Go back by number of wavelength traces visible w/o checking for index ranges. """
        self.wln_i -= self.traces_rows_spin.value() * self.traces_cols_spin.value()
        self.update_time_traces()
    
    def goto_prev_wln (self):
        """ Go to previous wavelength trace w/o checking for index ranges. """
        self.wln_i -= 1
        self.update_time_traces()
    
    def goto_wln (self, wln):
        """ Go to wavelength closest to value of `wln`. """
        self.wln_i = pc.lookup_index(wln, self.wlns)
        self.update_time_traces()
    
    def goto_next_wln (self):
        """ Go to next wavelength trace w/o checking for index ranges. """
        self.wln_i += 1
        self.update_time_traces()
    
    def goto_forward_wln (self):
        """ Go forward by number of wavelength traces visible w/o checking for index ranges. """
        self.wln_i += self.traces_rows_spin.value() * self.traces_cols_spin.value()
        self.update_time_traces()
    
    def goto_last_wln (self):
        """ Go to last wavelength trace. """
        self.wln_i = len(self.wlns) - self.traces_rows_spin.value() * self.traces_cols_spin.value()
        self.update_time_traces()
    
    def check_wln_index (self):
        """
        Check current wavelength index.
        
        Checks if current wavelength index allows to plot nrow*ncols traces.
        If not, it tries to set the index to a suitable value. In the rare
        case that less wavelengths are part of the data than nrow*ncols traces,
        which should be drawn, the index will be out of range anyway.
        """
        if self.wln_i < 0:
            self.goto_first_wln()
        elif (self.wln_i + self.traces_rows_spin.value() * self.traces_cols_spin.value()) >= len(self.wlns):
            self.wln_i = len(self.wlns) - self.traces_rows_spin.value() * self.traces_cols_spin.value()
        else:
            pass  # <- correct
    