from PyQt5.QtCore import Qt, QSize
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QColor, QBrush, QPalette, QIcon
from PyQt5.QtWidgets import QLabel, QLineEdit, QPushButton, QComboBox, QSpinBox, QDoubleSpinBox, QFrame, QTableWidgetItem, QStyledItemDelegate
from PyQt5.QtWidgets import QHBoxLayout

from ..core.pychart import MPL_LINESTYLES

"""
Helper functions which return a Qt Widget with predefined properties to shorten
creation of user interface while trying to avoid sub-classing.

Functions / customized widgets:
-------------------------------

qCLabel : QLabel
    Label with centered alignment and text `t` as well as optional QFont `font`.
    
qDummyLabel : QLabel
    Label with minimum width `w` for use as spacer.
    
qRLineEdit : QLineEdit
    Right-aligned edit widget with text `t`. Defaults to no text.
    
qRLineEditW : QLineEdit
    Right-aligned edit widget with text `t` and width `w`.
    
qRLineEdit30 : QLineEdit
    Right-aligned edit widget with text `t` of width 30. Defaults to no text.
    
qRLineEdit35 : QLineEdit
    Right-aligned edit widget with text `t` of width 35. Defaults to no text.
    
qRLineEdit60 : QLineEdit
    Right-aligned edit widget with text `t` of width 60. Defaults to no text.
    
qRLineEdit80 : QLineEdit
    Right-aligned edit widget with text `t` of width 80. Defaults to no text.
    
qPushButton30 : QPushButton
    Push button with text `t` and width `w`. Defaults to no text and width 30.
    
qIntSpinBox : QDoubleSpinBox
    Spin box for integer values covering the range [`minimum`,`maximum`]
    with a step size of `step`. Defaults to `value` on creation.
    
qFloatSpinBox : QDoubleSpinBox
    Spin box for floating point values covering the range [`minimum`,`maximum`]
    with a step size of `step`. Defaults to `value` on creation, while
    displaying `decimals` numbers of decimals.
    
qSeparatorLine : QFrame
    Horizontal line for use as separator.
    
qLabeledWidget : QHBoxLayout
    Horizontal layout consisting of a widget `w` and a QLabel with text `t`.



qInfoButton : QPushButton
    Push button of fixed width featuring an i-icon.
    
qColoredButton: inherits QPushButton
    Checkable push button in flat style with text `t`, color `c` and width `w`.
    If unchecked, its text is displayed in `c`; while checked background color
    switches to `c` and text color to white. Defaults to no text and black.
    
    Provides a function `ìs_active()`, which returns True if qColoredButton is
    enabled and checked, otherwise False.
    
qIconButton: inherits QPushButton
    Push button that displays an icon found at `filepath` and resembles the
    appearance of an label rather than an button. The shape is quadratic with
    given side length `size` (defaults to 64).
    
qlsComboBox : QComboBox
    ComboBox consisting of all supported matplotlib line styles.
    
qYellowTableItem : QTableWidgetItem
    TableWidgetItem with text `t` and yellow dot pattern. Defaults to no text.
    
qScanComboBox : inherits QComboBox
    ComboBox with checkable items.



qPalette : QPalette
    Palette with `color`as RGB tuple.
"""

#-----------------------------------------------------------------
#   SHORTCUT QT WIDGETS
#-----------------------------------------------------------------

def qCLabel (t, font=None):
    qlabel = QLabel(t)
    qlabel.setAlignment(Qt.AlignCenter)
    if font is not None:
        qlabel.setFont(font)
    return qlabel
    
def qDummyLabel (w):
    qlabel = QLabel()
    qlabel.setMinimumWidth(w)
    return qlabel
    
def qRLineEdit (t=''):
    qlineedit = QLineEdit(t)
    qlineedit.setAlignment(Qt.AlignRight)
    return qlineedit
    
def qRLineEditW (t, w):
    qlineedit = qRLineEdit(t)
    qlineedit.setMinimumWidth(w)
    qlineedit.setMaximumWidth(w)
    return qlineedit
    
def qRLineEdit30 (t=''):
    return qRLineEditW(t, 30)
    
def qRLineEdit35 (t=''):
    return qRLineEditW(t, 35)
    
def qRLineEdit60 (t=''):
    return qRLineEditW(t, 60)
    
def qRLineEdit80 (t=''):
    return qRLineEditW(t, 80)
    
def qPushButton30 (t='', w=30):
    qpushbutton = QPushButton(t)
    qpushbutton.setMinimumWidth(w)
    qpushbutton.setMaximumWidth(w)
    return qpushbutton
    
def qIntSpinBox (value, minimum, maximum, step, suffix=None):
    qspinbox = QSpinBox()
    qspinbox.setRange(minimum, maximum)
    qspinbox.setSingleStep(step)
    qspinbox.setValue(value)
    if suffix is not None:
        qspinbox.setSuffix(suffix)
    qspinbox.setAlignment(Qt.AlignRight)
    return qspinbox
    
def qFloatSpinBox (value, minimum, maximum, step, decimals=2, suffix=None):
    qspinbox = QDoubleSpinBox()
    qspinbox.setRange(minimum, maximum)
    qspinbox.setSingleStep(step)
    qspinbox.setDecimals(decimals)
    qspinbox.setValue(value)
    if suffix is not None:
        qspinbox.setSuffix(suffix)
    qspinbox.setAlignment(Qt.AlignRight)
    return qspinbox
    
def qSeparatorLine ():
    line = QFrame()
    line.setFrameShape(QFrame.HLine)
    return line
    
def qLabeledWidget (w, t):
    qlayout = QHBoxLayout()
    qlayout.addWidget(w)
    qlayout.addWidget(QLabel(t))
    qlayout.addStretch()
    return qlayout

#-----------------------------------------------------------------
#   CUSTOMIZED QT WIDGETS
#-----------------------------------------------------------------

def qInfoButton ():
    qinfobutton = qPushButton30()
    qinfobutton.setIcon(QIcon('../images/icons/information.svg'))
    return qinfobutton
    
class qColoredButton (QPushButton):
    
    def __init__ (self, t='', c=(0,0,0), w=60):
        super(QPushButton, self).__init__(t)
        self.setMinimumWidth(w)
        self.setMaximumWidth(w)
        self.setCheckable(True)
        self.setFlat(True)
        color = "rgb" + str(c)
        self.setStyleSheet(
            "QPushButton {color:" + color + ";"
            + "border: 1px solid" + color + ";"
            + "padding: 5px}"
            + "QPushButton:checked {color: 'white';"
            + "background-color:" + color + ";"
            + "border: none}"
            + "QPushButton:disabled {color: rgb(128,128,128);"
            + "background-color: transparent}")
    
    def is_active (self):
        return self.isChecked() and self.isEnabled()

class qIconButton (QPushButton):
    
    def __init__ (self, filepath, size=64):
        super(qIconButton, self).__init__()
        self.setIcon(QIcon(filepath))
        self.setIconSize(QSize(size,size))
        self.setFlat(True)
        self.setStyleSheet("QPushButton:hover {border: none; background-color: 'white'}")

def qlsComboBox ():
    qcombobox = QComboBox()
    for style in MPL_LINESTYLES:
        qcombobox.addItem(style)
    qcombobox.setEditable(False)
    return qcombobox

def qSubplotComboBox (parent=None):
    # TODO: For including this into table widgets, it might be better to make use of
    #   delegates. But my first attempt on that was not successful...
    combobox = QComboBox(parent=parent)
    combobox.addItems(['contour', 'traces', 'spectra'])
    return combobox

def qYellowTableItem (t=''):
    item = QTableWidgetItem(t)
    item.setBackground(QBrush(QColor.fromRgb(241,196,15), Qt.Dense7Pattern))
    return item

class qScanComboBox (QComboBox):
    
    def __init__(self, *args, **kwargs):
        super(qScanComboBox, self).__init__(*args, **kwargs)
        self.setEditable(True)
        self.lineEdit().setReadOnly(True)
        self._user_active = False
        self.view().pressed.connect(self.toggleItem)
        self.activated.connect(self.updateText)
    
    def hidePopup (self):
        if not self._user_active:
            super().hidePopup()
        self._user_active = False
    
    @pyqtSlot()
    def updateText (self):
        checked_items = []
        for i in range(self.model().rowCount()):
            if self.model().item(i, 0).checkState() == Qt.Checked:
                checked_items.append(str(i+1))
        self.lineEdit().setText(','.join(checked_items))
    
    def setScanChecked (self, index, checked):
        item = self.model().item(index, 0)
        item.setCheckState(Qt.Checked if checked else Qt.Unchecked)
    
    def isChecked (self, index):
        item = self.model().item(index, self.modelColumn())
        return item.checkState() == Qt.Checked
    
    @pyqtSlot('QModelIndex')
    def toggleItem (self, index):
        item = self.model().itemFromIndex(index)
        item.setCheckState(Qt.Unchecked if item.checkState() == Qt.Checked else Qt.Checked)
        self._user_active = True
        self.updateText()

#-----------------------------------------------------------------
#   COLORS
#-----------------------------------------------------------------

GREEN  = (55,173,107)
BLUE   = (26,111,223)
ORANGE = (255,150,14)
RED    = (255, 51,51)

def qPalette (color):
    r, g, b = color
    qpalette = QPalette()
    qpalette.setColor(QPalette.Text, QColor.fromRgb(r,g,b))
    return qpalette
