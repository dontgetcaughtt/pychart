import re

import numpy as np

from matplotlib.figure import Figure as mplFigure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as mplCanvas

from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog, QWhatsThis, QFileDialog
from PyQt5.QtWidgets import QLabel, QCheckBox, QRadioButton, QLineEdit, QComboBox, QTableWidget, QTableWidgetItem
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout, QFormLayout, QGroupBox, QButtonGroup
from PyQt5.QtGui import QIcon

from . import common as pcgui
from .DataTableDialogs import ReplaceInfDialog, ReplaceNaNDialog, InsertDataDialog
from .ErrorDialog import ErrorDialog
from ..core import pychart as pc
from ..core import netcdfimport as pcnetcdf
from ..core import error as pcerror

class NetCDFSingleImportDialog (QDialog):
    """
    Dialog providing the user interface for importing and exporting a single
    fsTA spectrum from NetCDF background files.
    
    Widget provides all necessary input / settings fields to generate data to
    the desired level of correction. The user settings are only read, checked
    and interpreted when correction functions are called.
    
    Attributes
    ----------
    working_dir : str
        working directory to open file dialogs at
    
    translator : i18n.Translator
        reference to the applications translator instance
    
    export_factor : float
        export factor for netcdf to pychart exports as set in GeneralSettingsDialog
    
    wln_tol : float
        absolute tolerance of wavelength axes values
    
    netcdfimport : pcnetcdf.NetCDFSingleImport
        import class that is responsible for handling / calculating data
    
    data : numpy.ndarray
        corrected data values
    
    wlns : numpy.ndarray
        wavelength axis values
    
    main_loaded : bool
        flag that indicates whether a main data set was loaded
    
    corr_loaded : bool
        flag that indicates whether a correction measurement was loaded
    
    table_editable : bool
        flag that indicates whether the data table is currently editable by the user
    """
    
    def __init__(self, translatorhandle, working_dir, export_factor, wln_tolerance, *args, **kwargs):
        super(NetCDFSingleImportDialog, self).__init__(*args, **kwargs)
        
        self.translator = translatorhandle
        self.working_dir = working_dir
        self.export_factor = export_factor if export_factor is not None else 1
        self.wln_tol = wln_tolerance
        self.main_loaded, self.corr_loaded = False, False
        self.table_editable = False
        
        # user interface
        self.setup_gui()
        self.plot = None
        
        # connections
        self.pump_radio.clicked.connect(self.change_layout)
        self.probe_radio.clicked.connect(self.change_layout)
        self.pumpprobe_radio.clicked.connect(self.change_layout)
        self.background_radio.clicked.connect(self.change_layout)
        self.mode4_radio.clicked.connect(self.change_layout)
        self.mode2_radio.clicked.connect(self.change_layout)
        
        self.group_corr.toggled.connect(self.change_solvent_correction)
        self.correction_rb1.clicked.connect(self.change_correction)
        self.correction_rb2.clicked.connect(self.change_correction)
        self.solv_factor_combo.currentTextChanged.connect(self.change_solv_factor_label)
        self.solv_factor_info_button.clicked.connect(
            lambda: QWhatsThis.showText(self.solv_factor_info_button.pos(), self.translator._('IMPORT_INFOTEXT_SOLV'),
                                        self.solv_factor_info_button))
        
        self.spectrum_update_btn.clicked.connect(self.update_preview)
        self.spectrum_wln_min_edit.editingFinished.connect(self.finalize_preview)
        self.spectrum_wln_max_edit.editingFinished.connect(self.finalize_preview)
        self.spectrum_amp_min_edit.editingFinished.connect(self.finalize_preview)
        self.spectrum_amp_max_edit.editingFinished.connect(self.finalize_preview)
        self.spectrum_wln_auto_scale.clicked.connect(self.finalize_preview)
        self.spectrum_amp_auto_scale.clicked.connect(self.finalize_preview)
        
        self.add_table_row_button.clicked.connect(self.add_wln_dialog)
        self.delete_table_row_button.clicked.connect(self.delete_wln_from_table)
        self.replace_nan_button.clicked.connect(self.replace_nan_dialog)
        self.replace_inf_button.clicked.connect(self.replace_inf_dialog)
        self.table_edit_switch_button.clicked.connect(self.switch_data_table_edit_behaviour)
        
        self.data_file_dlg_button.clicked.connect(self.update_data_file)
        self.data_corr_file_dlg_button.clicked.connect(self.update_corr_file)
        self.calculate_btn.clicked.connect(self.generate_data)
        self.export_btn.clicked.connect(self.export_data)
        
        # initialization
        self.netcdfimport = pcnetcdf.NetCDFSingleImport()
        self.change_layout()
        self.change_correction()
        self.change_solv_factor_label()
        self.calculate_btn.setDefault(True)
        
        self.setWindowTitle(self.translator._('IMPORT_SINGLE_DLG_TITLE'))
        self.setModal(True)
    
    # -----------------------------------------------------------------
    #   USER INTERFACE
    # -----------------------------------------------------------------
    
    def setup_gui (self):
        """ Setup graphical user interface. """
        
        # data group
        self.data_line_edit = QLineEdit()
        self.data_line_edit.setPalette(pcgui.qPalette(pcgui.GREEN))
        self.data_file_dlg_button = pcgui.qPushButton30('...')
        lay0 = QHBoxLayout()
        lay0.addWidget(QLabel(self.translator._('IMPORT_RAWDATA')))
        lay0.addWidget(self.data_line_edit)
        lay0.addWidget(self.data_file_dlg_button)
        
        self.wln_count_label, self.shot_count_label, self.mode_count_label = QLabel(), QLabel(), QLabel()
        lay4 = QFormLayout()
        lay4.addRow(QLabel('     ' + self.translator._('IMPORT_WLN_COUNT')), self.wln_count_label)
        lay4.addRow(QLabel('     ' + self.translator._('IMPORT_SHOT_COUNT')), self.shot_count_label)
        lay4.addRow(QLabel('     ' + self.translator._('IMPORT_MODE_COUNT')), self.mode_count_label)
        
        self.pump_radio = QRadioButton(self.translator._('IMPORT_PUMP'))
        self.probe_radio = QRadioButton(self.translator._('IMPORT_PROBE'))
        self.pumpprobe_radio = QRadioButton(self.translator._('IMPORT_PUMPPROBE'))
        self.background_radio = QRadioButton(self.translator._('IMPORT_BACKGROUND'))
        self.mode4_radio = QRadioButton(self.translator._('IMPORT_MODE4'))
        self.mode2_radio = QRadioButton(self.translator._('IMPORT_MODE2'))
        self.mode_btn_group = QButtonGroup()
        self.mode_btn_group.addButton(self.pump_radio)
        self.mode_btn_group.addButton(self.probe_radio)
        self.mode_btn_group.addButton(self.pumpprobe_radio)
        self.mode_btn_group.addButton(self.background_radio)
        self.mode_btn_group.addButton(self.mode4_radio)
        self.mode_btn_group.addButton(self.mode2_radio)
        self.mode4_radio.setChecked(True)
        
        lay = QGridLayout()
        lay.addLayout(lay0, 0, 0, 1, 4)
        lay.addLayout(lay4, 1, 0, 3, 4)
        lay.addWidget(pcgui.qSeparatorLine(), 4, 0, 1, 4)
        lay.addWidget(self.pump_radio, 5, 1)
        lay.addWidget(self.probe_radio, 6, 1)
        lay.addWidget(self.pumpprobe_radio, 5, 2)
        lay.addWidget(self.background_radio, 6, 2)
        lay.addWidget(self.mode4_radio, 5, 0)
        lay.addWidget(self.mode2_radio, 6, 0)
        self.group_main = QGroupBox(self.translator._('IMPORT_DATA_GROUP'))
        self.group_main.setLayout(lay)
        
        # correction group
        self.data_corr_line_edit = QLineEdit()
        self.data_corr_line_edit.setPalette(pcgui.qPalette(pcgui.BLUE))
        self.data_corr_file_dlg_button = pcgui.qPushButton30('...')
        lay6 = QHBoxLayout()
        lay6.addWidget(QLabel(self.translator._('IMPORT_RAWDATA')))
        lay6.addWidget(self.data_corr_line_edit)
        lay6.addWidget(self.data_corr_file_dlg_button)
        
        self.correction_rb1 = QRadioButton(self.translator._('IMPORT_SINGLE_CORR_REF'))
        self.correction_rb2 = QRadioButton(self.translator._('IMPORT_SINGLE_CORR_SOLV'))
        self.correction_btn_group = QButtonGroup()
        self.correction_btn_group.addButton(self.correction_rb1)
        self.correction_btn_group.addButton(self.correction_rb2)
        self.correction_rb1.setChecked(True)
        lay8 = QVBoxLayout()
        lay8.addWidget(self.correction_rb1)
        lay8.addWidget(self.correction_rb2)
        lay8.addStretch()
        
        self.solv_factor_combo = QComboBox()
        self.solv_factor_combo.addItem(self.translator._('IMPORT_SOLV_ABSORPTION'))
        self.solv_factor_combo.addItem(self.translator._('IMPORT_SOLV_TRANSMISSION'))
        self.solv_factor_combo.addItem(self.translator._('IMPORT_SOLV_FACTOR'))
        self.solv_factor_combo.addItem(self.translator._('IMPORT_SOLV_FREE'))
        self.solv_factor_combo.setEditable(False)
        self.solv_factor_combo.setCurrentText(self.translator._('IMPORT_SOLV_FACTOR'))
        self.solv_factor_label_0 = QLabel(self.translator._('IMPORT_SOLV_SUBSTRACT'))
        self.solv_factor_info_button = pcgui.qInfoButton()
        lay8b = QHBoxLayout()
        lay8b.addWidget(pcgui.qDummyLabel(20))
        lay8b.addWidget(self.solv_factor_label_0)
        lay8b.addWidget(self.solv_factor_combo)
        lay8b.addStretch()
        lay8b.addWidget(self.solv_factor_info_button)
        
        self.solv_factor_edit = pcgui.qRLineEdit35('1')
        self.solv_factor_label_1, self.solv_factor_label_2 = QLabel(), QLabel()
        lay8a = QHBoxLayout()
        lay8a.addWidget(pcgui.qDummyLabel(35))
        lay8a.addWidget(self.solv_factor_label_1)
        lay8a.addWidget(self.solv_factor_edit)
        lay8a.addWidget(self.solv_factor_label_2)
        lay8a.addStretch()
        
        self.mode4_corr_radio = QRadioButton(self.translator._('IMPORT_MODE4'))
        self.mode2_corr_radio = QRadioButton(self.translator._('IMPORT_MODE2'))
        self.mode_corr_btn_group = QButtonGroup()
        self.mode_corr_btn_group.addButton(self.mode4_corr_radio)
        self.mode_corr_btn_group.addButton(self.mode2_corr_radio)
        self.mode4_corr_radio.setChecked(True)
        
        lay10 = QGridLayout()
        lay10.addLayout(lay6, 0, 0, 1, 4)
        lay10.addLayout(lay8, 1, 0, 1, 4)
        lay10.addLayout(lay8b, 3, 0, 1, 4)
        lay10.addLayout(lay8a, 4, 0, 1, 4)
        lay10.addWidget(pcgui.qSeparatorLine(), 5, 0, 1, 4)
        lay10.addWidget(self.mode4_corr_radio, 6, 0)
        lay10.addWidget(self.mode2_corr_radio, 7, 0)
        self.group_corr = QGroupBox(self.translator._('IMPORT_SINGLE_CORR_GROUP'))
        self.group_corr.setLayout(lay10)
        self.group_corr.setCheckable(True)
        self.group_corr.setChecked(False)
        
        # preview group
        self.spectrum_wln_min_edit = pcgui.qRLineEdit60('320')
        self.spectrum_wln_max_edit = pcgui.qRLineEdit60('780')
        self.spectrum_amp_min_edit = pcgui.qRLineEdit60('-5')
        self.spectrum_amp_max_edit = pcgui.qRLineEdit60('35')
        self.spectrum_amp_auto_scale = QCheckBox(self.translator._('IMPORT_SINGLE_PLOT_AUTO'))
        self.spectrum_wln_auto_scale = QCheckBox(self.translator._('IMPORT_SINGLE_PLOT_AUTO'))
        self.spectrum_amp_auto_scale.setChecked(Qt.Checked)
        self.spectrum_wln_auto_scale.setChecked(Qt.Checked)
        self.spectrum_update_btn = pcgui.qPushButton30()
        self.spectrum_update_btn.setIcon(QIcon('../images/icons/update.svg'))
        self.spectrum_update_btn.setToolTip(self.translator._('IMPORT_SINGLE_PLOT_UPDATE'))
        self.spectrum_amp_multiply = QCheckBox(self.translator._('IMPORT_SINGLE_PLOT_MULTIPLY'))
        self.spectrum_amp_multiply.setChecked(Qt.Checked)
        
        lay21 = QGridLayout()
        lay21.addWidget(pcgui.qCLabel(self.translator._('IMPORT_SINGLE_PLOT_MIN')), 0, 1)
        lay21.addWidget(pcgui.qCLabel(self.translator._('IMPORT_SINGLE_PLOT_MAX')), 0, 2)
        lay21.addWidget(pcgui.qCLabel(self.translator._('IMPORT_SINGLE_PLOT_MIN')), 0, 7)
        lay21.addWidget(pcgui.qCLabel(self.translator._('IMPORT_SINGLE_PLOT_MAX')), 0, 8)
        lay21.addWidget(QLabel(self.translator._('IMPORT_SINGLE_PLOT_AMP')), 1, 0)
        lay21.addWidget(QLabel(self.translator._('IMPORT_SINGLE_PLOT_WLN')), 1, 6)
        lay21.addWidget(self.spectrum_amp_min_edit,   1, 1)
        lay21.addWidget(self.spectrum_amp_max_edit,   1, 2)
        lay21.addWidget(self.spectrum_amp_auto_scale, 1, 3)
        lay21.addWidget(self.spectrum_update_btn,     1, 4, 1, 2, Qt.AlignCenter)
        lay21.addWidget(self.spectrum_amp_multiply,   0, 4, 1, 2, Qt.AlignCenter)
        lay21.addWidget(self.spectrum_wln_min_edit,   1, 7)
        lay21.addWidget(self.spectrum_wln_max_edit,   1, 8)
        lay21.addWidget(self.spectrum_wln_auto_scale, 1, 9)
        
        self.spectrum_data_button = pcgui.qColoredButton(self.translator._('IMPORT_SINGLE_PLOT_DATA'), (0, 0, 0), 100)
        self.spectrum_main_button = pcgui.qColoredButton(self.translator._('IMPORT_SINGLE_PLOT_MAIN'), pcgui.GREEN, 100)
        self.spectrum_corr_button = pcgui.qColoredButton(self.translator._('IMPORT_SINGLE_PLOT_CORR'), pcgui.BLUE, 100)
        self.spectrum_data_button.setChecked(True)
        self.spectrum_data_button.setEnabled(False)
        self.spectrum_main_button.setEnabled(False)
        self.spectrum_corr_button.setEnabled(False)
        
        lay22 = QHBoxLayout()
        lay22.addWidget(self.spectrum_data_button)
        lay22.addWidget(self.spectrum_main_button)
        lay22.addWidget(self.spectrum_corr_button)
        
        self.preview_fig = mplFigure()
        self.preview_canvas = mplCanvas(self.preview_fig)
        self.preview_fig.set_size_inches(4, 2, forward=True)
        self.preview_canvas.setMinimumHeight(400)
        lay2 = QVBoxLayout()
        lay2.addLayout(lay21)
        lay2.addWidget(pcgui.qSeparatorLine())
        lay2.addLayout(lay22)
        lay2.addWidget(pcgui.qSeparatorLine())
        lay2.addWidget(self.preview_canvas)
        self.group_result = QGroupBox(self.translator._('IMPORT_SINGLE_PREVIEW_GROUP'))
        self.group_result.setLayout(lay2)
        
        # table group
        self.table = QTableWidget()
        lay11 = QVBoxLayout()
        lay11.addWidget(self.table)
        
        self.add_table_row_button = pcgui.qPushButton30()
        self.delete_table_row_button = pcgui.qPushButton30()
        self.replace_nan_button = pcgui.qPushButton30()
        self.replace_inf_button = pcgui.qPushButton30()
        self.table_edit_switch_button = pcgui.qPushButton30()
        
        self.add_table_row_button.setIcon(QIcon('../images/icons/add-row.svg'))
        self.delete_table_row_button.setIcon(QIcon('../images/icons/remove-row.svg'))
        self.replace_nan_button.setIcon(QIcon('../images/icons/replace-nan.svg'))
        self.replace_inf_button.setIcon(QIcon('../images/icons/replace-inf.svg'))
        self.table_edit_switch_button.setIcon(QIcon('../images/icons/edit.svg'))
        
        self.add_table_row_button.setToolTip(self.translator._('IMPORT_SINGLE_TABLE_ADD_ROW'))
        self.delete_table_row_button.setToolTip(self.translator._('IMPORT_SINGLE_TABLE_DEL_ROW'))
        self.replace_nan_button.setToolTip(self.translator._('IMPORT_SINGLE_TABLE_REPLACE_NAN'))
        self.replace_inf_button.setToolTip(self.translator._('IMPORT_SINGLE_TABLE_REPLACE_INF'))
        self.table_edit_switch_button.setToolTip(self.translator._('IMPORT_SINGLE_TABLE_ENABLE_EDIT'))
        
        self.table_edit_switch_button.setFlat(True)
        self.table_edit_switch_button.setCheckable(True)
        
        lay12 = QHBoxLayout()
        lay12.addWidget(self.add_table_row_button)
        lay12.addWidget(self.delete_table_row_button)
        lay12.addWidget(self.replace_nan_button)
        lay12.addWidget(self.replace_inf_button)
        lay12.addWidget(self.table_edit_switch_button)
        
        lay13 = QVBoxLayout()
        lay13.addLayout(lay11)
        lay13.addLayout(lay12)
        self.group_table = QGroupBox(self.translator._('IMPORT_TABLE_GROUP'))
        self.group_table.setLayout(lay13)
        
        # buttons and main layout
        self.calculate_btn = pcgui.qIconButton('../images/icons/calculate.svg')
        self.export_btn = pcgui.qIconButton('../images/icons/export-data.svg')
        self.calculate_btn.setToolTip(self.translator._('IMPORT_SINGLE_CALCULATE'))
        self.export_btn.setToolTip(self.translator._('IMPORT_SINGLE_EXPORT'))
        
        lay5 = QHBoxLayout()
        lay5.addStretch()
        lay5.addWidget(self.calculate_btn)
        lay5.addWidget(self.export_btn)
        lay5.addStretch()
        
        lay = QGridLayout()
        lay.addWidget(self.group_main,   0, 0, 2, 1)
        lay.addWidget(self.group_corr,   0, 1, 2, 1)
        lay.addWidget(self.group_result, 2, 0, 1, 2)
        lay.addWidget(self.group_table,  1, 2, 2, 1)
        lay.addLayout(lay5,              0, 2, 1, 1)
        
        self.setLayout(lay)
        
    def change_layout (self):
        """ Enable widgets according to user selection of extraction mode. """
        state = (self.mode4_radio.isChecked() or self.mode2_radio.isChecked())
        self.group_corr.setEnabled(state)
        self.spectrum_amp_multiply.setEnabled(state)
        self.spectrum_main_button.setEnabled(state and self.main_loaded)
        self.spectrum_corr_button.setEnabled(state and self.group_corr.isChecked() and self.corr_loaded)
    
    def change_correction (self):
        """ Enable solvent widgets according to user selection of correction mode. """
        state = self.correction_rb2.isChecked()
        self.solv_factor_combo.setEnabled(state)
        self.solv_factor_edit.setEnabled(state)
        self.solv_factor_info_button.setEnabled(state)
        self.solv_factor_label_0.setEnabled(state)
        self.solv_factor_label_1.setEnabled(state)
        self.solv_factor_label_2.setEnabled(state)
    
    def change_solvent_correction (self):
        """ Enable solvent plot button according to availability of solvent data. """
        self.spectrum_corr_button.setEnabled(self.group_corr.isChecked() and self.corr_loaded)
    
    def change_solv_factor_label (self):
        """ Change solvent factor label texts according to current method of calculation.  """
        if self.solv_factor_combo.currentText() == self.translator._('IMPORT_SOLV_FREE'):
            self.solv_factor_label_1.setText(self.translator._('IMPORT_SOLV_FREE_LABEL'))
            self.solv_factor_label_2.setText('')
        else:
            self.solv_factor_label_1.setText(self.translator._('IMPORT_SOLV_ABS_LABEL'))
            self.solv_factor_label_2.setText(self.translator._('IMPORT_SOLV_PATHLENGTH'))
    
    # -----------------------------------------------------------------
    #   HANDLING DATA FILES
    # -----------------------------------------------------------------
    
    def update_data_file (self):
        """ Get data file path from open dialog and display meta data. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'), self.working_dir, pc.FILE_TYPE_NCDF)
        if file_name != '':
            try:
                mode, _ = self.netcdfimport.load_main_data(file_name)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            else:
                self.data_line_edit.setText(file_name)
                self.main_loaded = True
                self.mode4_radio.setEnabled(mode is pcnetcdf.ChopperMode.TwoChopper)
                self.pump_radio.setEnabled(mode is pcnetcdf.ChopperMode.TwoChopper)
                self.background_radio.setEnabled(mode is pcnetcdf.ChopperMode.TwoChopper)
                self.wln_count_label.setText(str(len(self.netcdfimport['maindata']['wlns'])))
                self.shot_count_label.setText(str(self.netcdfimport['maindata']['nshots']))
                self.mode_count_label.setText(str(self.netcdfimport['maindata']['mode'].value))
                self.spectrum_data_button.setEnabled(True)
                self.spectrum_main_button.setEnabled(True)
    
    def update_corr_file (self):
        """ Get correction data file path and meta data from open dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'), self.working_dir, pc.FILE_TYPE_NCDF)
        if file_name != '':
            try:
                mode, _ = self.netcdfimport.load_solv_data(file_name)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
            else:
                self.data_corr_line_edit.setText(file_name)
                self.corr_loaded = True
                self.mode4_corr_radio.setEnabled(mode is pcnetcdf.ChopperMode.TwoChopper)
                self.spectrum_corr_button.setEnabled(True)
    
    # -----------------------------------------------------------------
    #   DATA MANIPULATION
    # -----------------------------------------------------------------
    
    def generate_data (self):
        """
        Generate corrected data.
        
        Reads user settings and generates (corrected) data, which is displayed
        in the data value table and as a plot.
        """
        if not self.main_loaded:
            e = pcerror.InputError('ERROR_IMPORT_MISSING_MAIN_ERROR')
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
            return
        if self.group_corr.isChecked() and not self.corr_loaded:
            e = pcerror.RangeError('ERROR_IMPORT_MISSING_CORR_ERROR')
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
            return
        
        # read main data settings
        if self.mode4_radio.isChecked():
            mainmode = pcnetcdf.ChopperMode.TwoChopper
        elif self.mode2_radio.isChecked():
            mainmode = pcnetcdf.ChopperMode.OneChopper
        elif self.pump_radio.isChecked():
            mainmode = 'pump'
        elif self.probe_radio.isChecked():
            mainmode = 'probe'
        elif self.pumpprobe_radio.isChecked():
            mainmode = 'pumpprobe'
        else:
            mainmode = 'bg'
        maindict = {'mode': mainmode}
        
        # read correction settings
        if self.group_corr.isChecked():
            
            corrmode = pcnetcdf.ChopperMode.TwoChopper if self.mode4_corr_radio.isChecked() else pcnetcdf.ChopperMode.OneChopper
            refcorr = self.correction_rb1.isChecked()
            solvmethod = absorption = ''
            
            if self.correction_rb2.isChecked():
                solvmethod = self.solv_factor_combo.currentText()
                if solvmethod == self.translator._('IMPORT_SOLV_FACTOR'):
                    solvmethod = 'factor'
                elif solvmethod == self.translator._('IMPORT_SOLV_TRANSMISSION'):
                    solvmethod = 'transmission'
                elif solvmethod == self.translator._('IMPORT_SOLV_ABSORPTION'):
                    solvmethod = 'absorption'
                else:
                    solvmethod = 'free'
                try:
                    absorption = float(self.solv_factor_edit.text())
                except:
                    e = pcerror.RangeError('ERROR_IMPORT_INVALID_ABSORPTION_ERROR', input=self.solv_factor_edit.text())
                    dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                    dlg.exec()
                    return
            
            solvdict = {'mode': corrmode, 'absorption': absorption, 'method': solvmethod}
            
        else:
            solvdict = None
            refcorr = None
        
        # generate data
        try:
            if type(mainmode) is pcnetcdf.ChopperMode:
                self.data, self.wlns = self.netcdfimport.correct_main_data(maindict, refcorr=refcorr, solvdict=solvdict,
                                                                           wln_tolerance=self.wln_tol)
            else:
                self.data, self.wlns = self.netcdfimport.extract_main_data(maindict)
        except pcerror.InputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
            return
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input, ranges=e.ranges), self.translator._(e.title))
            dlg.exec()
            return
        except pcerror.InternalError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
            return
        
        # show result
        self.show_preview()
        self.display_data()
    
    def show_preview (self):
        """ Plot (corrected) spectra. """
        f = self.get_preview_amp_factor()
        
        self.preview_fig.clear()
        self.preview_fig.subplots_adjust(left=0.12, right=0.98, top=0.92, bottom=0.15)
        self.plot = self.preview_fig.add_subplot()
        
        if self.spectrum_data_button.is_active() and (self.data is not None):
            self.plot.plot(self.wlns, self.data * f, '-', color=(0, 0, 0))
        if self.spectrum_main_button.is_active() and (self.netcdfimport['maindata']['diffabs'] is not None):
            self.plot.plot(self.netcdfimport['maindata']['wlns'], self.netcdfimport['maindata']['diffabs'][0,:].values * f,
                           '-', color=pc.mpl_color(pcgui.GREEN))
        if self.spectrum_corr_button.is_active() and (self.netcdfimport['solvdata']['diffabs'] is not None):
            self.plot.plot(self.netcdfimport['solvdata']['wlns'], self.netcdfimport['solvdata']['diffabs'][0,:].values * f,
                           '-', color=pc.mpl_color(pcgui.BLUE))
        
        self.finalize_preview()
    
    def finalize_preview (self):
        """ Finalize preview with some nice settings. """
        if self.plot is None:
            return
        
        try:
            if not self.spectrum_wln_auto_scale.isChecked():
                minx = float(self.spectrum_wln_min_edit.text())
                maxx = float(self.spectrum_wln_max_edit.text())
            if not self.spectrum_amp_auto_scale.isChecked():
                miny = float(self.spectrum_amp_min_edit.text())
                maxy = float(self.spectrum_amp_max_edit.text())
        
        except ValueError:
            e = pcerror.RangeError('ERROR_IMPORT_INVALID_PLOT_RANGE_ERROR')
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
        
        else:
            if not self.spectrum_wln_auto_scale.isChecked():
                self.plot.set_xlim(minx, maxx)
            else:
                self.preview_fig.axes[0].autoscale(axis='x', tight=True)
            if not self.spectrum_amp_auto_scale.isChecked():
                self.plot.set_ylim(miny, maxy)
            else:
                self.preview_fig.axes[0].autoscale(axis='y', tight=True)
        
        finally:
            self.plot.tick_params(which='both', direction='in', top=True, bottom=True, left=True, right=True, labelsize=10)
            self.plot.grid(which='both', axis='both', color='gray', ls='--', lw=0.75)
            self.plot.set_xlabel(self.translator._('IMPORT_SINGLE_PLOT_WLN_LABEL'), fontdict={'fontsize': 10})
            self.plot.set_ylabel(self.translator._(self.get_preview_amp_label()), fontdict={'fontsize': 10})
        
        self.preview_canvas.draw()
    
    def get_preview_amp_factor (self):
        """ Return amplitude multiplication factor for preview based on user input and availability. """
        if self.spectrum_amp_multiply.isChecked() and self.spectrum_amp_multiply.isEnabled():
            return 1000
        else:
            return 1
    
    def get_preview_amp_label (self):
        """ Return amplitude axis label for preview based on user input. """
        if self.spectrum_amp_multiply.isEnabled():
            if self.spectrum_amp_multiply.isChecked():
                return 'IMPORT_SINGLE_PLOT_AMP_LABEL_MULTIPLY'
            else:
                 return 'IMPORT_SINGLE_PLOT_AMP_LABEL'
        else:
            return 'IMPORT_SINGLE_PLOT_AMP_LABEL_RAW'
    
    def update_preview (self):
        """ Get manipulated data from table and plot preview. """
        if not self.update_data_from_table() or not self.update_axes_from_table():
            return
        else:
            self.show_preview()
    
    def display_data (self):
        """ Display corrected data in table. """
        
        # Items default to editable. Switch that flag off, if necessary, remain otherwise.
        flag = Qt.ItemIsEditable if not self.table_editable else 0x00
        
        nwlns = len(self.wlns)
        self.table.clear()
        self.table.setRowCount(nwlns)
        self.table.setColumnCount(1)
        for wln in range(nwlns):
            item = QTableWidgetItem(f"{self.data[wln]:.6f}")
            item.setFlags(item.flags() ^ flag)
            item.setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
            self.table.setItem(wln, 0, item)
        self.table.setVerticalHeaderLabels([f"{i:.1f} nm" for i in self.wlns])
        for row in range(nwlns):
            self.table.verticalHeaderItem(row).setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
        self.table.horizontalHeader().hide()
    
    def update_data_from_table (self):
        """ Retrieve data from table. """
        try:
            self.data = np.asarray(
                [self.table.item(row, 0).text() for row in range(self.table.rowCount())], float
            )
        except ValueError as error:
            userinput = re.search("'.*'", error.args[0])
            e = pcerror.RangeError('ERROR_IMPORT_INVALID_NUMBER', input=userinput.group())
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
            return False
        else:
            return True
    
    def update_axes_from_table (self):
        """ Retrieve wavelength axis values from table header. """
        try:
            self.wlns = np.asarray(
                [self.table.model().headerData(i, Qt.Vertical)[:-3] for i in range(self.table.rowCount())], float
            )
        except ValueError as error:
            userinput = re.search("'.*'", error.args[0])
            e = pcerror.RangeError('ERROR_IMPORT_INVALID_NUMBER', input=userinput.group())
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
            return False
        else:
            return True
    
    def switch_data_table_edit_behaviour (self):
        """ Switch editability of data table items. """
        
        # retrieve changes made to table, if necessary and possible
        if self.table_editable:
            if not self.update_data_from_table() or not self.update_axes_from_table():
                self.table_edit_switch_button.setChecked(self.table_editable)
                return
            else:
                self.show_preview()
        
        # toggle flags on table items
        nrows = self.table.rowCount()
        for row in range(nrows):
            item = self.table.item(row, 0)
            item.setFlags(item.flags() ^ Qt.ItemIsEditable)
        
        # toggle property
        self.table_editable = not self.table_editable
    
    def add_wln_dialog (self):
        """ Receive user inputs for adding a wavelength row from dialog. """
        if self.table.currentRow() == -1:
            return
        dlg = InsertDataDialog(self)
        dlg.setup('row', self.translator._('IMPORT_INSERT_WAVELENGTH'),
                  self.table.verticalHeaderItem(self.table.currentRow()).text()[:-3], 'nm')
        dlg.insert_requested.connect(self.add_wln_to_table)
        dlg.exec()
    
    @pyqtSlot(float, float)
    def add_wln_to_table (self, wln, value):
        """ Add one wavelength row to table and initialize with `value`. """
        self.table.insertRow(self.table.currentRow())
        row = self.table.currentRow() - 1
        
        # Items default to editable. Switch that flag off, if necessary, remain otherwise.
        flag = Qt.ItemIsEditable if not self.table_editable else 0x00
        
        for col in range(self.table.columnCount()):
            item = QTableWidgetItem(f"{value:.6f}")
            item.setFlags(item.flags() ^ flag)
            item.setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
            self.table.setItem(row, col, item)
        self.table.setVerticalHeaderItem(row, QTableWidgetItem(f"{wln:.1f} nm"))
        self.table.verticalHeaderItem(row).setTextAlignment(Qt.AlignRight ^ Qt.AlignVCenter)
    
    def delete_wln_from_table (self):
        """ Remove current wavelength from data table. """
        self.table.removeRow(self.table.currentRow())
    
    def replace_nan_dialog (self):
        """ Receive user inputs for nan replacement from dialog. """
        dlg = ReplaceNaNDialog(self)
        dlg.nan_replacement_requested.connect(self.replace_nan)
        dlg.exec()
    
    @pyqtSlot(float)
    def replace_nan (self, value):
        """
        Replace all NaNs in data table by `value`.
        
        Parameters
        ----------
        value : float
            value to replace NaNs with
        """
        if self.data is None:
            return 0
        pc.replace_nan(self.data, value)
        self.display_data()
    
    def replace_inf_dialog (self):
        """ Receive user inputs for inf replacement from dialog. """
        dlg = ReplaceInfDialog(self)
        dlg.inf_replacement_requested.connect(self.replace_inf)
        dlg.exec()
    
    @pyqtSlot(float, bool, bool)
    def replace_inf (self, value, pos=True, neg=True):
        """
        Replace all positive and/or negative infinite numbers in data table by `value`.
        
        Parameters
        ----------
        value : float
            value to replace infs with
        
        pos : bool
            whether to replace positive inf
        
        neg : bool
            whether to replace negative inf
        """
        if self.data is None:
            return 0
        pc.replace_inf(self.data, value, pos, neg)
        self.display_data()
    
    def export_data (self):
        """
        Export data as ASCII file.
        
        Exports data in a file guaranteed to be compatible with pychart's
        plotting feature for loading it as external spectrum.
        
        Notes
        -----
        Data is exported as shown in the data table widget with two columns,
        one for the wavelength axis and one for the difference absorption data.
        The number of rows is defined by the number of wavelengths. No header
        information is included in the exported file.
        """
        if not self.update_data_from_table() or not self.update_axes_from_table():
            return
        
        file_name, file_filter = QFileDialog.getSaveFileName(self, self.translator._('FILE_EXPORT'),
                                    self.working_dir + '/', pc.FILE_TYPE_FILTER_DATA)
        if file_name == '':
            return
        
        try:
            pc.export_external_pychart_file(self.data * self.export_factor, self.wlns, file_name)
        except pcerror.OutputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(output=e.output), self.translator._(e.title))
            dlg.exec()
    