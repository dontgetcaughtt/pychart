import json

import matplotlib as mpl
from matplotlib.figure import Figure as mplFigure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as mplCanvas

from PyQt5.QtCore import Qt, QDir
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QWidget, QFileDialog, QWhatsThis
from PyQt5.QtWidgets import QLabel, QLineEdit, QPushButton, QCheckBox, QComboBox, QRadioButton, QTableWidget, QTableWidgetItem
from PyQt5.QtWidgets import QGroupBox, QButtonGroup, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtGui import QIcon

from . import common as pcgui
from . import PlotTabDialogs
from .ErrorDialog import ErrorDialog
from ..core import pychart as pc
from ..core import plot as pcplot
from ..core import error as pcerror

""" Lists of indices for QTableWidgetItems in traces table """
TRACES_TEXT_WIDGET_INDICES  = [0, 1, 2, 4, 5, 8, 9, 11, 12, 13]
TRACES_SPIN_WIDGET_INDICES  = [6, 10]
TRACES_CHECK_WIDGET_INDICES = [3, 7]

""" List of indices for QTableWidgetItems in spectra table """
SPECTRA_TEXT_WIDGET_INDICES  = [0, 1, 2, 4, 5, 6, 7, 8, 9]

#-----------------------------------------------------------------
#   MAJOR PLOT WIDGET
#-----------------------------------------------------------------

class PlotTab (QWidget):
    """
    Tab widget providing the user interface for plotting.
    
    Widget provides - in combination with additional settings dialogs - all 
    necessary input / settings fields to generate custom plot. The user 
    settings are only read, checked and interpreted when plot function
    is called.
    
    Attributes
    ----------
    tabname : str
        name of tab and default plot title
    
    cache : dict
        dictionary containing all inputs from additional dialogs, to be passed
        and processed not earlier than on plot command
    
    reload : bool
        flag indicating that the check box for multiplying absorption values 
        was activated / deactivated or the value was changed
    
    plot_type = ['lin-log', 'lin', 'log']
        current plot type
        
    working_dir : str
        working directory to open file dialogs at
    
    translator : i18n.Translator
        reference to the applications' translator instance
    
    axes_label_dlg : PlotTabDialogs.AxesLabelDialog
        dialog for axes label user input
    
    colorbar_dlg :  PlotTabDialogs.ColorbarDialog
        dialog for colorbar settings
    
    baseline_dlg : PlotTabDialogs.BaselineDialog
        dialog for baseline user settings
        
    scalebar_dlg : PlotTabDialogs.ScaleBarDialog
        dialog for scalebar user settings
    
    artists_dlg : PlotTabDialogs.ArtistsDialog
        dialog for additional artists
    
    plot : pcplot.Plot()
        plot instance for plotting data
    
    Emits
    -----
    plot_type_changed : str
        indicates a change of current plot type (linlog or lin or log)
    
    plot_layout_changed
        indicated a change of current plot layout (linear, L-type, ...)
    
    new_status : str, int
        emits new status messages and timeout in milliseconds
    
    plot_progress : int
        current estimated plot progress
    """
    
    plot_type_changed = pyqtSignal('QString')
    plot_layout_changed = pyqtSignal()
    new_status = pyqtSignal('QString', int)
    plot_progress = pyqtSignal(int)
    
    def __init__ (self, name, translatorhandle, *args, **kwargs):
        super(PlotTab, self).__init__(*args, **kwargs)

        self.tabname = name
        self.cache = {}
        self.reload = True
        self.plot_type = ''
        self.working_dir = QDir.currentPath()
        
        # user interface
        self._translator = translatorhandle
        self.setup_gui()
        try:
            self.import_colormaps()
        except pcerror.InputError as error:
            dlg = ErrorDialog(error.msg, error.title)
            dlg.exec()
        
        self.axes_label_dlg = PlotTabDialogs.AxesLabelDialog(self)
        self.colorbar_dlg   = PlotTabDialogs.ColorbarDialog(self)
        self.baseline_dlg   = PlotTabDialogs.BaselineDialog(self)
        self.scalebar_dlg   = PlotTabDialogs.ScaleBarDialog(self)
        self.artists_dlg    = PlotTabDialogs.ArtistsDialog(self)
        
        # connections
        self.data_file_dlg_button.clicked.connect(self.update_data_file)
        self.data_file_info_button.clicked.connect(lambda: QWhatsThis.showText(self.data_file_info_button.pos(), self.translator._('PLOT_INFOTEXT_FILE'), self.data_file_info_button))
        self.wln_unit_combo_box.currentTextChanged.connect(self.update_wln_unit)
        self.time_unit_combo_box.currentTextChanged.connect(self.update_time_unit)
        self.abs_unit_combo_box.currentTextChanged.connect(self.update_abs_unit)
        self.abs_multiply_check.stateChanged.connect(self.set_reload_flag)
        self.abs_multiply_line_edit.textChanged.connect(self.set_reload_flag)
        self.linlog_button.clicked.connect(lambda: self.change_type('lin-log'))
        self.lin_button.clicked.connect(lambda: self.change_type('lin'))
        self.log_button.clicked.connect(lambda: self.change_type('log'))
        
        self.plot_name_line_edit.textChanged.connect(self.update_tab_name)
        self.layout_type_combo.currentTextChanged.connect(self.change_layout)
        self.colorbar_button.clicked.connect(self.colorbar_dialog)
        self.scalebar_button.clicked.connect(self.scalebar_dialog)
        
        self.traces_table_add_button.clicked.connect(self.add_traces_row)
        self.traces_table_add_ext_button.clicked.connect(self.add_traces_external_row)
        self.traces_table_remove_button.clicked.connect(self.remove_traces_row)
        self.traces_table_down_button.clicked.connect(self.move_traces_row_down)
        self.traces_table_up_button.clicked.connect(self.move_traces_row_up)
        self.traces_table_info_button.clicked.connect(lambda: QWhatsThis.showText(self.traces_table_info_button.pos(), self.translator._('PLOT_INFOTEXT_TRACES'), self.traces_table_info_button))
        self.spectra_table_add_button.clicked.connect(self.add_spectra_row)
        self.spectra_table_add_ext_button.clicked.connect(self.add_spectra_external_row)
        self.spectra_table_remove_button.clicked.connect(self.remove_spectra_row)
        self.spectra_table_down_button.clicked.connect(self.move_spectra_row_down)
        self.spectra_table_up_button.clicked.connect(self.move_spectra_row_up)
        self.spectra_table_info_button.clicked.connect(lambda: QWhatsThis.showText(self.spectra_table_info_button.pos(), self.translator._('PLOT_INFOTEXT_SPECTRA'), self.spectra_table_info_button))
        
        self.axes_label_dlg.apply_btn.clicked.connect(self.update_axes_labels)
        self.axes_label_dlg.ok_btn.clicked.connect(self.close_and_apply_axes_dlg)
        self.colorbar_dlg.apply_btn.clicked.connect(self.update_colorbar)
        self.colorbar_dlg.ok_btn.clicked.connect(self.close_and_apply_colorbar_dlg)
        self.baseline_dlg.apply_btn.clicked.connect(self.update_baseline)
        self.baseline_dlg.ok_btn.clicked.connect(self.close_and_apply_baseline_dlg)
        self.scalebar_dlg.apply_btn.clicked.connect(self.update_scalebar)
        self.scalebar_dlg.ok_btn.clicked.connect(self.close_and_apply_scalebar_dlg)
        self.artists_dlg.apply_btn.clicked.connect(self.update_artists)
        self.artists_dlg.ok_btn.clicked.connect(self.close_and_apply_artists_dlg)
        
        # initialization
        self.plot = pcplot.Plot(self.translator)
    
    @property
    def translator (self):
        return self._translator
    
    @property
    def plot_layout (self):
        layout = self.layout_type_combo.currentText()
        if layout == self.translator._('PLOT_LAYOUT_LINEAR'):
            return 'PLOT_LAYOUT_LINEAR'
        elif layout == self.translator._('PLOT_LAYOUT_L_TYPE'):
            return 'PLOT_LAYOUT_L_TYPE'
        else:
            dlg = ErrorDialog(self, 'Internal error encountered at PlotTab.py -> Plot.layout()')
            return dlg.exec()
    
    def set_reload_flag (self):
        """ Set flag for recalculating absorption values before plotting. """
        self.reload = True
    
    @pyqtSlot('QString')
    def set_working_dir (self, dir):
        self.working_dir = dir
    
    @pyqtSlot(int,'QString')
    def monitor_plot_progress (self, progress, message):
        self.new_status.emit(message,0)
        self.plot_progress.emit(progress)
    
    #-----------------------------------------------------------------
    #   USER INTERFACE
    #-----------------------------------------------------------------
    
    def setup_gui (self):
        """ Create graphical user interface as plotting tab. """
        
        # input group
        self.data_line_edit = QLineEdit()
        self.data_file_dlg_button = pcgui.qPushButton30('...')
        self.data_file_info_button = pcgui.qInfoButton()
        self.wln_unit_combo_box = QComboBox()
        for unit in pc.WavelengthUnit:
            self.wln_unit_combo_box.addItem(self.translator._(unit.value))
        self.wln_unit_combo_box.setEditable(False)
        self.time_unit_combo_box = QComboBox()
        for unit in pc.TimeUnit:
            self.time_unit_combo_box.addItem(self.translator._(unit.value))
        self.time_unit_combo_box.setEditable(False)
        self.abs_unit_combo_box = QComboBox()
        for unit in pc.AbsorptionUnit:
            self.abs_unit_combo_box.addItem(self.translator._(unit.value))
        self.abs_unit_combo_box.setEditable(False)
        self.abs_multiply_check = QCheckBox()
        self.abs_multiply_label = pcgui.qCLabel(self.translator._('PLOT_ABS_FACTOR'))
        self.abs_multiply_line_edit = pcgui.qRLineEditW('1000', 45)
        self.linlog_button = pcgui.qColoredButton(self.translator._('PLOT_LINLOG'), pcgui.GREEN, 80)
        self.lin_button = pcgui.qColoredButton(self.translator._('PLOT_LIN'), pcgui.BLUE, 80)
        self.log_button = pcgui.qColoredButton(self.translator._('PLOT_LOG'), pcgui.ORANGE, 80)
        self.type_btn_group = QButtonGroup()
        self.type_btn_group.addButton(self.linlog_button)
        self.type_btn_group.addButton(self.lin_button)
        self.type_btn_group.addButton(self.log_button)
        
        lay0 = QHBoxLayout()
        lay0.addWidget(QLabel(self.translator._('PLOT_DATAFILE')))
        lay0.addWidget(self.data_line_edit)
        lay0.addWidget(self.data_file_dlg_button)
        lay0.addWidget(self.data_file_info_button)
        lay0.addWidget(pcgui.qDummyLabel(10))
        lay0.addWidget(QLabel(self.translator._('PLOT_UNITS')))
        lay0.addWidget(self.wln_unit_combo_box)
        lay0.addWidget(self.time_unit_combo_box)
        lay0.addWidget(self.abs_unit_combo_box)
        lay0.addWidget(self.abs_multiply_check)
        lay0.addWidget(self.abs_multiply_label)
        lay0.addWidget(self.abs_multiply_line_edit)
        lay0.addWidget(pcgui.qDummyLabel(10))
        lay0.addWidget(self.linlog_button)
        lay0.addWidget(self.lin_button)
        lay0.addWidget(self.log_button)
        
        # general group
        self.plot_name_line_edit = QLineEdit(self.tabname)
        self.show_title_check = QCheckBox(self.translator._('PLOT_SHOW_TITLE'))
        self.show_title_check.setTristate(False)
        lay1a = QHBoxLayout()
        lay1a.addWidget(QLabel(self.translator._('PLOT_NAME')))
        lay1a.addWidget(self.plot_name_line_edit)
        lay1a.addWidget(self.show_title_check)
        
        self.layout_type_combo = QComboBox()
        self.layout_type_combo.addItem(self.translator._('PLOT_LAYOUT_LINEAR'))
        self.layout_type_combo.addItem(self.translator._('PLOT_LAYOUT_L_TYPE'))
        self.layout_type_combo.setEditable(False)
        self.layout_colorbar_radio_1 = QRadioButton() # top or right
        self.layout_colorbar_radio_2 = QRadioButton() # bottom
        self.layout_colorbar_radio_2.setText(self.translator._('PLOT_LAYOUT_COLORBAR_BOTTOM'))
        lay1c = QHBoxLayout()
        lay1c.addWidget(QLabel(self.translator._('PLOT_LAYOUT')))
        lay1c.addWidget(self.layout_type_combo)
        lay1c.addStretch()
        lay1c.addWidget(QLabel(self.translator._('PLOT_COLORBAR')))
        lay1c.addWidget(self.layout_colorbar_radio_1)
        lay1c.addWidget(self.layout_colorbar_radio_2)
        
        self.plt_widths = [pcgui.qRLineEdit30(),
            pcgui.qRLineEdit30(), pcgui.qRLineEdit30()]
        self.plt_heights = [
            pcgui.qRLineEdit30(), pcgui.qRLineEdit30(), pcgui.qRLineEdit30(),
            pcgui.qRLineEdit30(), pcgui.qRLineEdit30(), pcgui.qRLineEdit30()]
        lay1d = QHBoxLayout()
        lay1d.addWidget(QLabel(self.translator._('PLOT_LAYOUT_WIDTHS')))
        for i in range(3):
            lay1d.addWidget(self.plt_widths[i])
        lay1d.addStretch()
        lay1d.addWidget(QLabel(self.translator._('PLOT_LAYOUT_HEIGHTS')))
        for i in range(6):
            lay1d.addWidget(self.plt_heights[i])
        
        lay1 = QVBoxLayout()
        lay1.addLayout(lay1a)
        lay1.addLayout(lay1c)
        lay1.addLayout(lay1d)
        group1 = QGroupBox(self.translator._('PLOT_GROUP_GENERAL'))
        group1.setLayout(lay1)
        
        # axes group
        self.wln_start_edit  = pcgui.qRLineEdit60()
        self.wln_end_edit    = pcgui.qRLineEdit60()
        self.time_start_edit = pcgui.qRLineEdit60()
        self.time_cut_edit   = pcgui.qRLineEdit60()
        self.time_end_edit   = pcgui.qRLineEdit60()
        self.abs_start_edit  = pcgui.qRLineEdit60()
        self.abs_step_edit   = pcgui.qRLineEdit60()
        self.abs_end_edit    = pcgui.qRLineEdit60()
        self.unit_label_wln  = pcgui.qDummyLabel(40)
        self.unit_label_time = pcgui.qDummyLabel(40)
        self.unit_label_abs  = pcgui.qDummyLabel(40)
        self.contour_ticks_edit  = pcgui.qRLineEdit()
        self.time_lin_ticks_edit = pcgui.qRLineEdit()
        self.time_log_ticks_edit = pcgui.qRLineEdit()
        self.colorbar_combo = QComboBox()
        self.colorbar_combo.setEditable(False)
        self.colorbar_tick_step_edit = pcgui.qRLineEdit60()
        self.colorbar_button = pcgui.qPushButton30()
        self.colorbar_button.setIcon(QIcon('../images/icons/colorbar.svg'))
        lay8a = QHBoxLayout()
        lay8a.addWidget(self.time_lin_ticks_edit)
        lay8a.addWidget(QLabel('-//-'))
        lay8a.addWidget(self.time_log_ticks_edit)
        
        lay8 = QGridLayout()
        lay8.addWidget(QLabel(self.translator._('PLOT_AXES_WLN')), 1, 0)
        lay8.addWidget(QLabel(self.translator._('PLOT_AXES_TIME')), 2, 0)
        lay8.addWidget(QLabel(self.translator._('PLOT_AXES_ABS')), 4, 0)
        lay8.addWidget(pcgui.qCLabel(self.translator._('PLOT_AXES_START')), 0, 1)
        lay8.addWidget(pcgui.qCLabel(self.translator._('PLOT_AXES_CUT')), 0, 2)
        lay8.addWidget(pcgui.qCLabel(self.translator._('PLOT_AXES_STOP')), 0, 3)
        lay8.addWidget(pcgui.qCLabel(self.translator._('PLOT_AXES_STEP')), 3, 2)
        lay8.addWidget(self.wln_start_edit,                 1, 1)
        lay8.addWidget(self.wln_end_edit,                   1, 3)
        lay8.addWidget(self.time_start_edit,                2, 1)
        lay8.addWidget(self.time_cut_edit,                  2, 2)
        lay8.addWidget(self.time_end_edit,                  2, 3)
        lay8.addWidget(self.abs_start_edit,                 4, 1)
        lay8.addWidget(self.abs_step_edit,                  4, 2)
        lay8.addWidget(self.abs_end_edit,                   4, 3)
        lay8.addWidget(self.unit_label_wln,                 1, 4)
        lay8.addWidget(self.unit_label_time,                2, 4)
        lay8.addWidget(self.unit_label_abs,                 4, 4)
        lay8.addWidget(pcgui.qCLabel(self.translator._('PLOT_AXES_TICKS')), 0, 5, 1, 3)
        lay8.addWidget(self.contour_ticks_edit,             1, 5, 1, 3)
        lay8.addLayout(lay8a,                               2, 5, 1, 3)
        lay8.addWidget(pcgui.qCLabel(self.translator._('PLOT_COLORBAR')), 3, 5, 1, 2)
        lay8.addWidget(pcgui.qCLabel(self.translator._('PLOT_AXES_TICKS')), 3, 7)
        lay8.addWidget(self.colorbar_combo,                 4, 5)
        lay8.addWidget(self.colorbar_button,                4, 6)
        lay8.addWidget(self.colorbar_tick_step_edit,        4, 7)
        
        group8 = QGroupBox(self.translator._('PLOT_GROUP_AXES'))
        group8.setLayout(lay8)
        
        # traces group
        self.traces_table = QTableWidget(3,14)
        self.traces_table.setHorizontalHeaderItem(0, QTableWidgetItem(self.translator._('PLOT_WAVELENGTH')))
        self.traces_table.setHorizontalHeaderItem(1, QTableWidgetItem(self.translator._('PLOT_COLOR')))
        self.traces_table.setHorizontalHeaderItem(2, QTableWidgetItem(self.translator._('PLOT_LABEL')))
        self.traces_table.setHorizontalHeaderItem( 3, QTableWidgetItem())
        self.traces_table.setHorizontalHeaderItem(4, QTableWidgetItem(self.translator._('PLOT_POS_X1')))
        self.traces_table.setHorizontalHeaderItem(5, QTableWidgetItem(self.translator._('PLOT_POS_Y1')))
        self.traces_table.setHorizontalHeaderItem(6, QTableWidgetItem(self.translator._('PLOT_ROTATION')))
        self.traces_table.setHorizontalHeaderItem( 7, QTableWidgetItem())
        self.traces_table.setHorizontalHeaderItem(8, QTableWidgetItem(self.translator._('PLOT_POS_X2')))
        self.traces_table.setHorizontalHeaderItem(9, QTableWidgetItem(self.translator._('PLOT_POS_Y2')))
        self.traces_table.setHorizontalHeaderItem(10, QTableWidgetItem(self.translator._('PLOT_ROTATION')))
        self.traces_table.setHorizontalHeaderItem(11, QTableWidgetItem(self.translator._('PLOT_INT_EXT')))
        self.traces_table.setHorizontalHeaderItem(12, QTableWidgetItem(self.translator._('PLOT_LIM_A_MAX')))
        self.traces_table.setHorizontalHeaderItem(13, QTableWidgetItem(self.translator._('PLOT_LIM_A_MIN')))
        self.traces_table.verticalHeader().setVisible(True)
        
        self.traces_table_add_button     = pcgui.qPushButton30()
        self.traces_table_add_ext_button = pcgui.qPushButton30()
        self.traces_table_remove_button  = pcgui.qPushButton30()
        self.traces_table_down_button    = pcgui.qPushButton30()
        self.traces_table_up_button      = pcgui.qPushButton30()
        self.traces_table_info_button    = pcgui.qInfoButton()
        
        self.traces_table_add_button.setToolTip(self.translator._('PLOT_ADD_INT_TRACE'))
        self.traces_table_add_ext_button.setToolTip(self.translator._('PLOT_ADD_EXT_TRACES'))
        self.traces_table_remove_button.setToolTip(self.translator._('PLOT_DEL_TRACE'))
        self.traces_table_down_button.setToolTip(self.translator._('PLOT_MOVE_DOWN'))
        self.traces_table_up_button.setToolTip(self.translator._('PLOT_MOVE_UP'))
        
        self.traces_table_add_button.setIcon(QIcon('../images/icons/add.svg'))
        self.traces_table_add_ext_button.setIcon(QIcon('../images/icons/add-ext.svg'))
        self.traces_table_remove_button.setIcon(QIcon('../images/icons/remove.svg'))
        self.traces_table_down_button.setIcon(QIcon('../images/icons/down.svg'))
        self.traces_table_up_button.setIcon(QIcon('../images/icons/up.svg'))

        lay9b = QVBoxLayout()
        lay9b.addWidget(self.traces_table_add_button)
        lay9b.addWidget(self.traces_table_add_ext_button)
        lay9b.addWidget(self.traces_table_remove_button)
        lay9b.addWidget(self.traces_table_up_button)
        lay9b.addWidget(self.traces_table_down_button)
        lay9b.addWidget(self.traces_table_info_button)
        
        self.traces_xlim_edit      = pcgui.qRLineEdit()
        self.traces_ticks_edit     = pcgui.qRLineEdit()
        self.traces_labelsize_spin = pcgui.qIntSpinBox(11, 0, 100, 1)
        self.traces_lw_spin        = pcgui.qFloatSpinBox(1.5, 0.0, 100, 0.1, 1)
        self.traces_ls_combo       = pcgui.qlsComboBox()
        self.expand_wln_lines = QCheckBox(self.translator._('PLOT_EXPAND_WAVELENGTH_LINES'))
        self.expand_wln_lines.setTristate(False)
        lay9a = QHBoxLayout()
        lay9a.addWidget(QLabel(self.translator._('PLOT_SCALING')))
        lay9a.addWidget(self.traces_xlim_edit)
        lay9a.addWidget(QLabel(self.translator._('PLOT_TICKS_ΔA')))
        lay9a.addWidget(self.traces_ticks_edit)
        lay9a.addWidget(QLabel(self.translator._('PLOT_LABELSIZE')))
        lay9a.addWidget(self.traces_labelsize_spin)
        lay9a.addWidget(QLabel(self.translator._('PLOT_LINEWIDTH')))
        lay9a.addWidget(self.traces_lw_spin)
        lay9a.addWidget(QLabel(self.translator._('PLOT_LINESTYLE')))
        lay9a.addWidget(self.traces_ls_combo)
        lay9a.addWidget(self.expand_wln_lines)
        
        lay9 = QGridLayout()
        lay9.addWidget(self.traces_table, 0, 0, 1, 1)
        lay9.addLayout(lay9b,             0, 1, 1, 1)
        lay9.addLayout(lay9a,             1, 0, 1, 2)
        group9 = QGroupBox(self.translator._('PLOT_GROUP_TRACES'))
        group9.setLayout(lay9)
        
        # spectra group
        self.spectra_table = QTableWidget(0,10)
        self.spectra_table.setHorizontalHeaderItem(0, QTableWidgetItem(self.translator._('PLOT_TIME')))
        self.spectra_table.setHorizontalHeaderItem(1, QTableWidgetItem(self.translator._('PLOT_COLOR')))
        self.spectra_table.setHorizontalHeaderItem(2, QTableWidgetItem(self.translator._('PLOT_LABEL')))
        self.spectra_table.setHorizontalHeaderItem(3, QTableWidgetItem())
        self.spectra_table.setHorizontalHeaderItem(4, QTableWidgetItem(self.translator._('PLOT_POS_X')))
        self.spectra_table.setHorizontalHeaderItem(5, QTableWidgetItem(self.translator._('PLOT_POS_Y')))
        self.spectra_table.setHorizontalHeaderItem(6, QTableWidgetItem(self.translator._('PLOT_COLOR')))
        self.spectra_table.setHorizontalHeaderItem(7, QTableWidgetItem(self.translator._('PLOT_INT_EXT')))
        self.spectra_table.setHorizontalHeaderItem(8, QTableWidgetItem(self.translator._('PLOT_LIM_A_MIN')))
        self.spectra_table.setHorizontalHeaderItem(9, QTableWidgetItem(self.translator._('PLOT_LIM_A_MAX')))
        self.spectra_table.verticalHeader().setVisible(True)
        
        self.spectra_table_add_button     = pcgui.qPushButton30()
        self.spectra_table_add_ext_button = pcgui.qPushButton30()
        self.spectra_table_remove_button  = pcgui.qPushButton30()
        self.spectra_table_down_button    = pcgui.qPushButton30()
        self.spectra_table_up_button      = pcgui.qPushButton30()
        self.spectra_table_info_button    = pcgui.qInfoButton()
        
        self.spectra_table_add_button.setToolTip(self.translator._('PLOT_ADD_INT_SPECTRUM'))
        self.spectra_table_add_ext_button.setToolTip(self.translator._('PLOT_ADD_EXT_SPECTRUM'))
        self.spectra_table_remove_button.setToolTip(self.translator._('PLOT_DEL_SPECTRUM'))
        self.spectra_table_down_button.setToolTip(self.translator._('PLOT_MOVE_DOWN'))
        self.spectra_table_up_button.setToolTip(self.translator._('PLOT_MOVE_UP'))
        
        self.spectra_table_add_button.setIcon(QIcon('../images/icons/add.svg'))
        self.spectra_table_add_ext_button.setIcon(QIcon('../images/icons/add-ext.svg'))
        self.spectra_table_remove_button.setIcon(QIcon('../images/icons/remove.svg'))
        self.spectra_table_down_button.setIcon(QIcon('../images/icons/down.svg'))
        self.spectra_table_up_button.setIcon(QIcon('../images/icons/up.svg'))
        
        lay7a = QVBoxLayout()
        lay7a.addWidget(self.spectra_table_add_button)
        lay7a.addWidget(self.spectra_table_add_ext_button)
        lay7a.addWidget(self.spectra_table_remove_button)
        lay7a.addWidget(self.spectra_table_up_button)
        lay7a.addWidget(self.spectra_table_down_button)
        lay7a.addWidget(self.spectra_table_info_button)
        
        self.expand_spectra_lines = QCheckBox(self.translator._('PLOT_EXPAND_SPECTRA_LINES'))
        self.expand_spectra_lines.setTristate(False)
        self.spectra_labelsize_spin = pcgui.qIntSpinBox(11, 0, 100, 1)
        self.spectra_lw_spin = pcgui.qFloatSpinBox(1.5, 0.0, 100, 0.1, 1)
        self.spectra_ls_combo = pcgui.qlsComboBox()
        self.spectra_scaling_edit = pcgui.qRLineEdit60()
        self.spectra_limits_edit = pcgui.qRLineEdit()
        self.scalebar_button = QPushButton(self.translator._('PLOT_SCALEBAR_DLG'))
        # lay7c and lay7g will be used later in change_layout()
        # depending on the layout some widgets will be added / removed
        self.lay7c, self.lay7g = QHBoxLayout(), QHBoxLayout()
        self.spectra_ticks_label = QLabel(self.translator._('PLOT_TICKS_λ'))
        self.spectra_xticks_edit  = pcgui.qRLineEdit()
        self.spectra_yticks_edit = pcgui.qRLineEdit()
        lay7b = QHBoxLayout()
        lay7b.addWidget(QLabel(self.translator._('PLOT_SCALING')))
        lay7b.addLayout(self.lay7c)
        lay7b.addWidget(self.spectra_ticks_label)
        lay7b.addLayout(self.lay7g)
        lay7b.addWidget(QLabel(self.translator._('PLOT_LABELSIZE')))
        lay7b.addWidget(self.spectra_labelsize_spin)
        lay7b.addWidget(QLabel(self.translator._('PLOT_LINEWIDTH')))
        lay7b.addWidget(self.spectra_lw_spin)
        lay7b.addWidget(QLabel(self.translator._('PLOT_LINESTYLE')))
        lay7b.addWidget(self.spectra_ls_combo)
        lay7b.addWidget(self.expand_spectra_lines)
        
        lay7 = QGridLayout()
        lay7.addWidget(self.spectra_table, 0, 0, 1, 1)
        lay7.addLayout(lay7a,              0, 1, 1, 1)
        lay7.addLayout(lay7b,              1, 0, 1, 2)
        group7 = QGroupBox(self.translator._('PLOT_GROUP_SPECTRA'))
        group7.setLayout(lay7)
        
        # plot preview
        self.figure = mplFigure()
        self.canvas = mplCanvas(self.figure)
        lay5 = QVBoxLayout()
        lay5.addWidget(self.canvas)
        group5 = QGroupBox(self.translator._('PLOT_GROUP_PREVIEW'))
        group5.setLayout(lay5)
        
        # main layout
        lay_main = QGridLayout()
        lay_main.addLayout(lay0,   0, 0, 1, 2)
        lay_main.addWidget(group1, 1, 0)
        lay_main.addWidget(group8, 1, 1)
        lay_main.addWidget(group9, 2, 0)
        lay_main.addWidget(group7, 3, 0)
        lay_main.addWidget(group5, 2, 1, 2, 1)
        self.setLayout(lay_main)
    
    def remove_traces_row (self):
        """ Remove current row from traces table. """
        self.traces_table.removeRow(self.traces_table.currentRow())
    
    def add_traces_row (self):
        """ Add new row to traces table. """
        row = self.traces_table.currentRow() + 1
        self.traces_table.insertRow(row)
        for col in range(self.traces_table.columnCount()):
            self.traces_table.setItem(row, col, QTableWidgetItem())
        for col in TRACES_CHECK_WIDGET_INDICES:
            self.traces_table.item(row, col).setCheckState(Qt.Unchecked)
        self.traces_table.item(row, 11).setText(self.translator._('PLOT_INTERNAL'))
        for i in range(11,14):
            item = self.traces_table.item(row, i)
            item.setFlags(item.flags() & (not Qt.ItemIsEditable))
    
    def add_traces_external_row (self):
        """ Add new row for external files to traces table. """
        row = self.traces_table.currentRow() + 1
        self.traces_table.insertRow(row)
        for col in range(self.traces_table.columnCount()):
            self.traces_table.setItem(row, col, pcgui.qYellowTableItem())
        for col in TRACES_CHECK_WIDGET_INDICES:
            self.traces_table.item(row, col).setCheckState(Qt.Unchecked)
        externalitem = self.traces_table.item(row, 11)
        externalitem.setText(self.translator._('PLOT_EXTERNAL'))
        externalitem.setFlags(externalitem.flags() & (not Qt.ItemIsEditable))
    
    def move_traces_row(self, i):
        """ Move current traces row by increment `i`. """
        row = self.traces_table.currentRow()
        ncols = self.traces_table.columnCount()
        temp = [self.traces_table.takeItem(row, col) for col in range(ncols)]
        for col in range(ncols):
            self.traces_table.setItem(row, col, self.traces_table.takeItem(row+i, col))
            self.traces_table.setItem(row+i, col, temp[col])
        self.traces_table.setCurrentCell(row+i,self.traces_table.currentColumn())
    
    def move_traces_row_up(self):
        """ Move traces row upwards, if it's not the last one. """
        if self.traces_table.currentRow() != self.traces_table.rowCount() - 1:
            self.move_traces_row(1)

    def move_traces_row_down(self):
        """ Move traces row downwards, if it's not the first one. """
        if self.traces_table.currentRow() != 0:
            self.move_traces_row(-1)
    
    def remove_spectra_row (self):
        """ Remove current row from spectra table. """
        self.spectra_table.removeRow(self.spectra_table.currentRow())
    
    def add_spectra_row (self):
        """ Add new row to spectra table. """
        row = self.spectra_table.currentRow() + 1
        self.spectra_table.insertRow(row)
        for col in range(self.spectra_table.columnCount()):
            self.spectra_table.setItem(row, col, QTableWidgetItem())
        self.spectra_table.item(row, 3).setCheckState(Qt.Unchecked)
        self.spectra_table.item(row, 7).setText(self.translator._('PLOT_INTERNAL'))
        for i in range(7,10):
            item = self.spectra_table.item(row, i)
            item.setFlags(item.flags() & (not Qt.ItemIsEditable))
    
    def add_spectra_external_row (self):
        """ Add new row for external files to spectra table. """
        row = self.spectra_table.currentRow() + 1
        self.spectra_table.insertRow(row)
        for col in range(self.spectra_table.columnCount()):
            self.spectra_table.setItem(row, col, pcgui.qYellowTableItem())
        self.spectra_table.item(row, 3).setCheckState(Qt.Unchecked)
        externalitem = self.spectra_table.item(row, 7)
        externalitem.setText(self.translator._('PLOT_EXTERNAL'))
        externalitem.setFlags(externalitem.flags() & (not Qt.ItemIsEditable))
    
    def move_spectra_row (self, i):
        """ Move current spectra row by increment `i`. """
        row = self.spectra_table.currentRow()
        ncols = self.spectra_table.columnCount()
        temp = [self.spectra_table.takeItem(row,col) for col in range(ncols)]
        for col in range(ncols):
            self.spectra_table.setItem(row, col, self.spectra_table.takeItem(row+i,col))
            self.spectra_table.setItem(row+i, col, temp[col])
        self.spectra_table.setCurrentCell(row+i, self.spectra_table.currentColumn())
    
    def move_spectra_row_up (self):
        """ Move spectra row upwards, if it's not the last one. """
        if self.spectra_table.currentRow() != self.spectra_table.rowCount()-1:
            self.move_spectra_row(1)
    
    def move_spectra_row_down (self):
        """ Move spectra row downwards, if it's not the first one. """
        if self.spectra_table.currentRow() != 0:
            self.move_spectra_row(-1)
    
    def change_layout (self):
        """
        Change widgets according to user defined layout.
        
        Responds to user input regarding the overall plot layout by enabling / 
        disabling certain widgets.
        """
        layout = self.plot_layout
        if  layout == 'PLOT_LAYOUT_LINEAR':
            self.layout_colorbar_radio_1.setText(self.translator._('PLOT_LAYOUT_COLORBAR_TOP'))
            
            self.expand_wln_lines.setEnabled(False)
            
            self.lay7c.removeWidget(self.spectra_limits_edit)
            self.spectra_limits_edit.setVisible(False)
            self.lay7c.addWidget(self.spectra_scaling_edit)
            self.lay7c.addWidget(self.scalebar_button)
            self.spectra_scaling_edit.setVisible(True)
            self.scalebar_button.setVisible(True)
            
            self.spectra_ticks_label.setText(self.translator._('PLOT_TICKS_λ'))
            self.lay7g.removeWidget(self.spectra_yticks_edit)
            self.spectra_yticks_edit.setVisible(False)
            self.lay7g.addWidget(self.spectra_xticks_edit)
            self.spectra_xticks_edit.setVisible(True)
            
            self.baseline_dlg.activate_widgets(False)
            
        elif layout == 'PLOT_LAYOUT_L_TYPE':
            self.layout_colorbar_radio_1.setText(self.translator._('PLOT_LAYOUT_COLORBAR_RIGHT'))
            
            self.expand_wln_lines.setEnabled(True)
            
            self.lay7c.removeWidget(self.spectra_scaling_edit)
            self.lay7c.removeWidget(self.scalebar_button)
            self.spectra_scaling_edit.setVisible(False)
            self.scalebar_button.setVisible(False)
            self.lay7c.addWidget(self.spectra_limits_edit)
            self.spectra_limits_edit.setVisible(True)
            
            self.spectra_ticks_label.setText(self.translator._('PLOT_TICKS_ΔA'))
            self.lay7g.removeWidget(self.spectra_xticks_edit)
            self.spectra_xticks_edit.setVisible(False)
            self.lay7g.addWidget(self.spectra_yticks_edit)
            self.spectra_yticks_edit.setVisible(True)
            
            self.baseline_dlg.activate_widgets(True)
            self.scalebar_dlg.close()
        
        else:
            dlg = ErrorDialog(self, 'Internal error encountered at PlotTab.py -> Plot.change_layout()')
            return dlg.exec()
        
        self.axes_label_dlg.set_available_spectra_label(layout)
        self.plot_layout_changed.emit()
    
    def change_type (self, t):
        """
        Change widgets according to user defined plot type `t`.
        
        Responds to user input regarding the plot type by enabling / disabling 
        certain widgets.
        """
        if t == 'lin-log':
            self.data_line_edit.setPalette(pcgui.qPalette(pcgui.GREEN))
            self.axes_label_dlg.set_time_dec_notation('both')
            self.linlog_button.setChecked(True)
        elif t == 'lin':
            self.data_line_edit.setPalette(pcgui.qPalette(pcgui.BLUE))
            self.axes_label_dlg.set_time_dec_notation('dec')
            self.lin_button.setChecked(True)
        elif t == 'log':
            self.data_line_edit.setPalette(pcgui.qPalette(pcgui.ORANGE))
            self.axes_label_dlg.set_time_dec_notation('log')
            self.log_button.setChecked(True)
        else:
            dlg = ErrorDialog(self, 'Internal error encountered at PlotTab.py -> Plot.change_type()')
            return dlg.exec()
        self.time_lin_ticks_edit.setEnabled('lin' in t)
        self.time_log_ticks_edit.setEnabled('log' in t)
        self.time_cut_edit.setEnabled(t == 'lin-log')
        self.plot_type = t
        self.plot_type_changed.emit(t)
    
    def set_datafile (self, file):
        self.data_line_edit.setText(file)
    
    #-----------------------------------------------------------------
    #   SETTINGS
    #-----------------------------------------------------------------
    
    def load_settings_dialog (self):
        """ Get settings file path from open dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'),
                                                             self.working_dir, pc.FILE_TYPE_JSON)
        if file_name != '':
            try:
                self.load_settings(file_name)
                self.new_status.emit(self.translator._('STATUS_PLOT_SETTINGS_LOADED').format(file=file_name), 5000)
            except pcerror.InputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                dlg.exec()
    
    def save_settings_dialog (self):
        """ Get settings file path from save dialog. """
        file_name, file_filter = QFileDialog.getSaveFileName(self, self.translator._('FILE_SAVE'),
                                                             self.working_dir + '/' + self.tabname, pc.FILE_TYPE_JSON)
        if file_name != '':
            try:
                self.save_settings(file_name)
                self.new_status.emit(self.translator._('STATUS_PLOT_SETTINGS_SAVED').format(file=file_name), 5000)
            except pcerror.OutputError as e:
                dlg = ErrorDialog(self, self.translator._(e.msg).format(output=e.output), self.translator._(e.title))
                dlg.exec()
    
    def load_settings (self, path):
        """
        Load settings from file.
        
        Reads all settings defined in `path` and writes them into the user
        interface.
        
        File has to be in JSON format, defining a dict which is basically 
        identical to the `Plot` class. Since the settings are copied to the 
        user input widgets most values are required as string rather than their 
        real type. Refer to existing default settings files, e.g. 
        `default_fsTA.json`, for a valid specimen.
        
        Parameters
        ----------
        path :  str
            filepath to be loaded
        
        Raises
        ------
        pcerror.InputError
            if an JSON decoding error is raised
        
        pcerror.InputError
            if a type conversion or set function fails
        """
        try:
            with open(path, 'r', encoding='utf-8') as file:
                default = json.load(file)
            
            self.wln_unit_combo_box.setCurrentText(self.translator._(default['units']['wln']))
            self.time_unit_combo_box.setCurrentText(self.translator._(default['units']['time']))
            self.abs_unit_combo_box.setCurrentText(self.translator._(default['units']['abs']))
            self.show_title_check.setCheckState(Qt.Checked if default['axeslabel']['titleon'] else Qt.Unchecked)
            if default['units']['multiply'] != 'None':
                self.abs_multiply_check.setCheckState(Qt.Checked)
                self.abs_multiply_line_edit.setText(default['units']['multiply'])
            else:
                self.abs_multiply_check.setCheckState(Qt.Unchecked)
                self.abs_multiply_line_edit.setText('1000')
            self.change_type(default['layout']['type'])
            if default['layout']['design'] == 'Linear':
                self.layout_type_combo.setCurrentText(self.translator._('PLOT_LAYOUT_LINEAR'))
            elif default['layout']['design'] == 'LType':
                self.layout_type_combo.setCurrentText(self.translator._('PLOT_LAYOUT_L_TYPE'))
            else:
                pass
            self.change_layout()
            self.layout_colorbar_radio_1.setChecked(default['layout']['colorbarpos'] != 'bottom')
            self.layout_colorbar_radio_2.setChecked(default['layout']['colorbarpos'] == 'bottom')
            for i in range(3):
                self.plt_widths[i].setText(default['layout']['widths'][i])
            for i in range(6):
                self.plt_heights[i].setText(default['layout']['heights'][i])
            self.wln_start_edit.setText(default['ranges']['wln']['min'])
            self.wln_end_edit.setText(default['ranges']['wln']['max'])
            self.time_start_edit.setText(default['ranges']['time']['min'])
            self.time_cut_edit.setText(default['ranges']['time']['cut'])
            self.time_end_edit.setText(default['ranges']['time']['max'])
            self.abs_start_edit.setText(default['ranges']['abs']['min'])
            self.abs_step_edit.setText(default['ranges']['abs']['step'])
            self.abs_end_edit.setText(default['ranges']['abs']['max'])
            self.contour_ticks_edit.setText(default['contour']['contourticks'])
            self.time_lin_ticks_edit.setText(default['misc']['timetickslin'])
            self.time_log_ticks_edit.setText(default['misc']['timetickslog'])
            self.colorbar_combo.setCurrentText(default['contour']['map'])
            self.colorbar_tick_step_edit.setText(default['contour']['tickstep'])
            self.cache['cmapflip']    = default['contour']['flipmap']
            self.cache['cmapover']    = default['contour']['overvalues']
            self.cache['cmapunder']   = default['contour']['undervalues']
            self.cache['cmapnan']     = default['contour']['nanvalues']
            self.cache['cmapoveron']  = default['contour']['overon']
            self.cache['cmapunderon'] = default['contour']['underon']
            self.cache['cmapstyle']   = default['contour']['colorbarstyle']
            self.cache['title'] = self.tabname
            self.cache['wlncontourtitle'] = default['axeslabel']['wlncontour']
            self.cache['wlntransientstitle'] = default['axeslabel']['wlntransients']
            self.cache['timetitle'] = default['axeslabel']['time']
            self.cache['timedec'] = default['axeslabel']['timedec']
            self.cache['abstracestitle'] = default['axeslabel']['abstraces']
            self.cache['abscolorbartitle'] = default['axeslabel']['abscolorbar']
            self.cache['absspectratitle'] = default['axeslabel'].get('absspectra', self.cache['abstracestitle'])
            self.traces_table.setRowCount(0)
            self.traces_table.setRowCount(len(default['traces']['list']))
            for i in range(len(default['traces']['list'])):
                internal = default['traces']['list'][i][11] == self.translator._('PLOT_INTERNAL')
                if internal:
                    twi = lambda x: QTableWidgetItem(x)
                else:
                    twi = lambda x: pcgui.qYellowTableItem(x)
                for j in TRACES_TEXT_WIDGET_INDICES:
                    self.traces_table.setItem(i, j, twi(default['traces']['list'][i][j]))
                for j in TRACES_SPIN_WIDGET_INDICES:
                    self.traces_table.setItem(i, j, twi(default['traces']['list'][i][j]))
                for j in TRACES_CHECK_WIDGET_INDICES:
                    temp = twi('')
                    temp.setCheckState(Qt.Checked if default['traces']['list'][i][j] else Qt.Unchecked)
                    self.traces_table.setItem(i, j, temp)
                stop = 14 if internal else 12
                for k in range(11,stop):
                    item = self.traces_table.item(i, k)
                    item.setFlags(item.flags() & (not Qt.ItemIsEditable))
            self.traces_table.resizeColumnsToContents()
            self.traces_xlim_edit.setText(default['traces']['xlim'])
            self.traces_ticks_edit.setText(default['traces']['xticks'])
            self.traces_labelsize_spin.setValue(int(default['traces']['labelsize']))
            self.traces_lw_spin.setValue(float(default['traces']['lw']))
            self.traces_ls_combo.setCurrentText(default['traces']['ls'])
            self.expand_wln_lines.setCheckState(Qt.Checked if default['traces']['longwlnline'] else Qt.Unchecked)
            self.cache['baselinetraceson']     = default['traces']['baseline']['on']
            self.cache['baselinetracesvalue']  = default['traces']['baseline']['value']
            self.cache['baselinetracescolor']  = default['traces']['baseline']['color']
            self.cache['baselinetracesls']     = default['traces']['baseline']['ls']
            self.cache['baselinetraceslw']     = default['traces']['baseline']['lw']
            self.cache['baselinetraceszorder'] = default['traces']['baseline']['zorder']
            self.cache['baselinespectraon']     = default['spectra']['baseline']['on']
            self.cache['baselinespectravalue']  = default['spectra']['baseline']['value']
            self.cache['baselinespectracolor']  = default['spectra']['baseline']['color']
            self.cache['baselinespectrals']     = default['spectra']['baseline']['ls']
            self.cache['baselinespectralw']     = default['spectra']['baseline']['lw']
            self.cache['baselinespectrazorder'] = default['spectra']['baseline']['zorder']
            self.spectra_xticks_edit.setText(default['spectra']['xticks'])
            self.spectra_yticks_edit.setText(default['spectra']['yticks'])
            self.spectra_scaling_edit.setText(default['spectra']['scaling'])
            self.spectra_limits_edit.setText(default['spectra']['limits'])
            self.spectra_table.setRowCount(0)
            self.spectra_table.setRowCount(len(default['spectra']['list']))
            for i in range(len(default['spectra']['list'])):
                internal = default['spectra']['list'][i][7] == self.translator._('PLOT_INTERNAL')
                if internal:
                    twi = lambda x: QTableWidgetItem(x)
                else:
                    twi = lambda x: pcgui.qYellowTableItem(x)
                for j in SPECTRA_TEXT_WIDGET_INDICES:
                    self.spectra_table.setItem(i, j, twi(default['spectra']['list'][i][j]))
                temp = twi('')
                temp.setCheckState(Qt.Checked if default['spectra']['list'][i][3] else Qt.Unchecked)
                self.spectra_table.setItem(i, 3, temp)
                stop = 10 if internal else 8
                for k in range(7,stop):
                    item = self.spectra_table.item(i, k)
                    item.setFlags(item.flags() & (not Qt.ItemIsEditable))
            self.spectra_table.resizeColumnsToContents()
            self.spectra_labelsize_spin.setValue(int(default['spectra']['labelsize']))
            self.spectra_lw_spin.setValue(float(default['spectra']['lw']))
            self.spectra_ls_combo.setCurrentText(default['spectra']['ls'])
            self.expand_spectra_lines.setCheckState(Qt.Checked if default['spectra']['longtimeline'] else Qt.Unchecked)
            self.cache['scalebaron']     = default['spectra']['scalebar']['on']
            self.cache['scalebarvalue']  = float(default['spectra']['scalebar']['value'])
            self.cache['scalebarlabel']  = default['spectra']['scalebar']['label']
            self.cache['scalebarcolor']  = default['spectra']['scalebar']['color']
            self.cache['scalebarposwln'] = default['spectra']['scalebar']['poswln']
            self.cache['scalebarposy']   = default['spectra']['scalebar']['posy']
            self.cache['artists'] = default.setdefault('artists', [])
            for artist in self.cache['artists']:
                if len(artist) == 10: # rectangle fill color added at index 9 with version 5.2
                    artist.insert(9, None)
            
            # call this at the end for proper update of additional dialogs
            self.update_units()
            if self.axes_label_dlg.isVisible():
                self.axes_label_dlg.set_current_labels(
                    self.cache['wlncontourtitle'],
                    self.cache['wlntransientstitle'],
                    self.cache['timetitle'],
                    self.cache['timedec'],
                    self.cache['abstracestitle'],
                    self.cache['abscolorbartitle'],
                    self.cache['absspectratitle'])
        
        except json.JSONDecodeError:
            raise pcerror.InputError('ERROR_PLOT_JSON_SETTINGS_ERROR', input=path)
        
        except:
            raise pcerror.InputError('ERROR_PLOT_IMPORT_SETTINGS_ERROR', input=path)
    
    def import_colormaps (self):
        """
        Import colormaps from files.
        
        A selection of matplotlib's predefined colormaps is loaded from 
        'cmaps/predefined.txt'. All colormaps, which are not commented out, are 
        included into the colormaps combo box. Furthermore all .cmap files in 
        this directory are loaded.
        
        Raises
        ------
        pcerror.InputError
            if user defined colormap file can not be registered
        
        pcerror.InputError
            if no colormaps were loaded at all
        
        Notes
        -----
        'cmap/predefined.txt' : list of matplotlib's colormaps
            One colormap per line; those with leading '#' will not be included.
        
        'cmap/*.cmap' : user-defined colormap file
            Colors listed in these files define a linear segmented colormap. 
            Each line has to specify one color as RGB tuple, e.g.
            '(255,255,255)'. Everything after '#' will be ignored until EOL.
        
        TODO: global handling of cmaps
        """
        count = 0
        
        cmap_dir = QDir()
        cmap_dir.cdUp()
        if cmap_dir.cd('cmaps'):
            cmap_path = cmap_dir.path() + '/'
            
            # predefined cmaps
            try:
                file = open(cmap_path + 'predefined.txt', 'r')
                content = file.read().splitlines()
                for line in content:
                    if line.startswith('#'):
                        continue
                    else:
                        self.colorbar_combo.addItem(line)
                        count += 1
            except IOError:
                pass
            
            # user-defined cmaps
            cmap_dir.setFilter(QDir.Files or QDir.Readable)
            cmap_dir.setNameFilters(['*.cmap'])
            cmap_list = cmap_dir.entryList()
            if cmap_list:
                self.colorbar_combo.insertSeparator(self.colorbar_combo.count())
            for cmap in cmap_list:
                try:
                    mpl.pyplot.register_cmap(cmap[:-5], pc.cmap_from_file(cmap_path + cmap, cmap))
                    self.colorbar_combo.addItem(cmap[:-5])
                    count += 1
                except pcerror.InputError as e:
                    dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
                    dlg.exec()
        
        if count == 0:
            raise pcerror.InputError('ERROR_PLOT_NO_CMAPS_LOADED_ERROR')
    
    def save_settings (self, path):
        """
        Save settings to file.
        
        Stores all settings currently visible on the the user interface to 
        a JSON file.
        
        Parameters
        ----------
        path :  str
            filepath to which the data is stored
        
        Raises
        ------
        pcerror.OutputError
            if an JSON decoding error is raised
        
        pcerror.OutputError
            if a type conversion or set function fails
        """
        try:
            # prepare dictionary
            default = {}
            default['units'] = {}
            default['ranges'], default['ranges']['wln'] = {}, {}
            default['ranges']['time'], default['ranges']['abs'] = {}, {}
            default['contour'] = {}
            default['traces'] = {}
            default['traces']['list'], default['traces']['baseline'] = [], {}
            default['spectra'] = {}
            default['spectra']['list'], default['spectra']['baseline'] = [], {}
            default['spectra']['scalebar'] = {}
            default['misc'] = {}
            default['axeslabel'], default['layout'] = {}, {}
            
            # read current settings from user interface
            default['units']['wln'] = self.translator.retranslate(self.wln_unit_combo_box.currentText())
            default['units']['time'] = self.translator.retranslate(self.time_unit_combo_box.currentText())
            default['units']['abs'] = self.translator.retranslate(self.abs_unit_combo_box.currentText())
            if self.abs_multiply_check.isChecked():
                default['units']['multiply'] = self.abs_multiply_line_edit.text()
            else:
                default['units']['multiply'] = 'None'
            default['layout']['widths'] = [w.text() for w in self.plt_widths]
            default['layout']['heights'] = [h.text() for h in self.plt_heights]
            if self.linlog_button.isChecked():
                default['layout']['type'] = 'lin-log'
            elif self.lin_button.isChecked():
                default['layout']['type'] = 'lin'
            else:
                default['layout']['type'] = 'log'
            if self.layout_type_combo.currentText() == self.translator._('PLOT_LAYOUT_LINEAR'):
                default['layout']['design'] = 'Linear'
                default['layout']['colorbarpos'] = 'bottom' if self.layout_colorbar_radio_2.isChecked() else 'top'
            elif self.layout_type_combo.currentText() == self.translator._('PLOT_LAYOUT_L_TYPE'):
                default['layout']['design'] = 'LType'
                default['layout']['colorbarpos'] = 'bottom' if self.layout_colorbar_radio_2.isChecked() else 'right'
            else:
                pass
            default['ranges']['wln']['min'] = self.wln_start_edit.text()
            default['ranges']['wln']['max'] = self.wln_end_edit.text()
            default['ranges']['time']['min'] = self.time_start_edit.text()
            default['ranges']['time']['cut'] = self.time_cut_edit.text()
            default['ranges']['time']['max'] = self.time_end_edit.text()
            default['ranges']['abs']['min'] = self.abs_start_edit.text()
            default['ranges']['abs']['step'] = self.abs_step_edit.text()
            default['ranges']['abs']['max'] = self.abs_end_edit.text()
            default['misc']['timetickslin'] = self.time_lin_ticks_edit.text()
            default['misc']['timetickslog'] = self.time_log_ticks_edit.text()
            default['contour']['contourticks'] = self.contour_ticks_edit.text()
            default['contour']['map'] = self.colorbar_combo.currentText()
            default['contour']['flipmap'] = self.cache['cmapflip']
            default['contour']['overvalues'] = self.cache['cmapover']
            default['contour']['undervalues'] = self.cache['cmapunder']
            default['contour']['nanvalues'] = self.cache['cmapnan']
            default['contour']['overon'] = self.cache['cmapoveron']
            default['contour']['underon'] = self.cache['cmapunderon']
            default['contour']['colorbarstyle'] = self.cache['cmapstyle']
            default['contour']['tickstep'] = self.colorbar_tick_step_edit.text()
            default['axeslabel']['titleon'] = self.show_title_check.isChecked()
            default['axeslabel']['wlncontour'] = self.cache['wlncontourtitle']
            default['axeslabel']['wlntransients'] = self.cache['wlntransientstitle']
            default['axeslabel']['time'] = self.cache['timetitle']
            default['axeslabel']['timedec'] = self.cache['timedec']
            default['axeslabel']['abstraces'] = self.cache['abstracestitle']
            default['axeslabel']['abscolorbar'] = self.cache['abscolorbartitle']
            default['axeslabel']['absspectra'] = self.cache['absspectratitle']
            for row in range(self.traces_table.rowCount()):
                default['traces']['list'].append([self.traces_table.item(row, col).text() for col in range(self.traces_table.columnCount())])
                default['traces']['list'][row][3] = True if self.traces_table.item(row, 3).checkState() else False
                default['traces']['list'][row][7] = True if self.traces_table.item(row, 7).checkState() else False
            default['traces']['xlim'] = self.traces_xlim_edit.text()
            default['traces']['xticks'] = self.traces_ticks_edit.text()
            default['traces']['labelsize'] = self.traces_labelsize_spin.value()
            default['traces']['lw'] = self.traces_lw_spin.value()
            default['traces']['ls'] = self.traces_ls_combo.currentText()
            default['traces']['longwlnline'] = self.expand_wln_lines.isChecked()
            default['traces']['baseline']['on'] = self.cache['baselinetraceson']
            default['traces']['baseline']['value'] = self.cache['baselinetracesvalue']
            default['traces']['baseline']['color'] = self.cache['baselinetracescolor']
            default['traces']['baseline']['ls'] = self.cache['baselinetracesls']
            default['traces']['baseline']['lw'] = self.cache['baselinetraceslw']
            default['traces']['baseline']['zorder'] = self.cache['baselinetraceszorder']
            for row in range(self.spectra_table.rowCount()):
                default['spectra']['list'].append([self.spectra_table.item(row, col).text() for col in range(self.spectra_table.columnCount())])
                default['spectra']['list'][row][3] = True if self.spectra_table.item(row, 3).checkState() else False
            default['spectra']['scaling'] = self.spectra_scaling_edit.text()
            default['spectra']['limits'] = self.spectra_limits_edit.text()
            default['spectra']['xticks'] = self.spectra_xticks_edit.text()
            default['spectra']['yticks'] = self.spectra_yticks_edit.text()
            default['spectra']['labelsize'] = self.spectra_labelsize_spin.value()
            default['spectra']['lw'] = self.spectra_lw_spin.value()
            default['spectra']['ls'] = self.spectra_ls_combo.currentText()
            default['spectra']['longtimeline'] = self.expand_spectra_lines.isChecked()
            default['spectra']['scalebar']['value'] = self.cache['scalebarvalue']
            default['spectra']['scalebar']['label'] = self.cache['scalebarlabel']
            default['spectra']['scalebar']['color'] = self.cache['scalebarcolor']
            default['spectra']['scalebar']['poswln'] = self.cache['scalebarposwln']
            default['spectra']['scalebar']['posy'] = self.cache['scalebarposy']
            default['spectra']['scalebar']['on'] = self.cache['scalebaron']
            default['spectra']['baseline']['on'] = self.cache['baselinespectraon']
            default['spectra']['baseline']['value'] = self.cache['baselinespectravalue']
            default['spectra']['baseline']['color'] = self.cache['baselinespectracolor']
            default['spectra']['baseline']['ls'] = self.cache['baselinespectrals']
            default['spectra']['baseline']['lw'] = self.cache['baselinespectralw']
            default['spectra']['baseline']['zorder'] = self.cache['baselinespectrazorder']
            default['artists'] = self.cache['artists']
            
            # write settings to file
            with open(path, 'w', encoding='utf-8') as file:
                json.dump(default, file, indent=4, ensure_ascii=False)
        
        except json.JSONDecodeError:
            raise pcerror.OutputError('ERROR_PLOT_JSON_SETTINGS_ERROR', output=path)
    
        except:
            raise pcerror.OutputError('ERROR_PLOT_EXPORT_SETTINGS_ERROR', output=path)
    
    #-----------------------------------------------------------------
    #   PLOTTING DATA
    #-----------------------------------------------------------------
    
    def update_data_file (self):
        """ Get data file path from open dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self, self.translator._('FILE_OPEN'),
            self.working_dir, pc.FILE_TYPE_FILTER_DATA)
        if file_name != '':
            self.data_line_edit.setText(file_name)
    
    def auto_ranges (self):
        """ Get auto ranges from data and insert to corresponding input fields. """
        try:
            self.wln_start_edit.setText(str(self.plot.get_lowest_wln()))
            self.wln_end_edit.setText(str(self.plot.get_largest_wln()))
            self.time_start_edit.setText(str(self.plot.get_lowest_time()))
            self.time_cut_edit.setText(str(self.plot.get_cut_time()))
            self.time_end_edit.setText(str(self.plot.get_largest_time()))
            self.abs_start_edit.setText(str(self.plot.get_lowest_abs()))
            self.abs_step_edit.setText(str(self.plot.get_abs_step_width()))
            self.abs_end_edit.setText(str(self.plot.get_largest_abs()))
        except pcerror.InputError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
        
    def plot_data (self):
        """
        Make plot from user settings and display it.
        
        Reads all user data from plot tab and additional dialogs to generate 
        plot from it. If no errors occur, plot is shown in the preview area.
        
        All exceptions raised during read-in of user input or plotting are 
        caught and handled via `pcerror.ErrorDialog`.
        """
        try:
            
            # units and title 
            self.read_units()
            if self.abs_multiply_check.checkState():
                self.plot['datafactor'] = float(self.abs_multiply_line_edit.text())
            else:
                self.plot['datafactor'] = 1
            self.plot['title'] = self.plot_name_line_edit.text()
            self.plot['axeslabel']['titleon'] = self.show_title_check.checkState()
            
            # layout
            if self.linlog_button.isChecked():
                self.plot['layout']['type'] = pcplot.PlotType.LinLog
            elif self.lin_button.isChecked():
                self.plot['layout']['type'] = pcplot.PlotType.Lin
            else:
                self.plot['layout']['type'] = pcplot.PlotType.Log
            if self.layout_type_combo.currentText() == self.translator._('PLOT_LAYOUT_LINEAR'):
                self.plot['layout']['design'] = pcplot.PlotLayout.Linear
            elif self.layout_type_combo.currentText() == self.translator._('PLOT_LAYOUT_L_TYPE'):
                self.plot['layout']['design'] = pcplot.PlotLayout.LType
            else:
                pass
            if self.layout_colorbar_radio_1.isChecked():
                if self.layout_colorbar_radio_1.text() == self.translator._('PLOT_LAYOUT_COLORBAR_TOP'):
                    self.plot['layout']['colorbarpos'] = pcplot.ColorbarPosition.Top
                else:
                    self.plot['layout']['colorbarpos'] = pcplot.ColorbarPosition.Right
            else:
                self.plot['layout']['colorbarpos'] = pcplot.ColorbarPosition.Bottom
            self.plot['layout']['widths'] = [int(self.plt_widths[i].text()) for i in range(3)]
            self.plot['layout']['heights'] = [int(self.plt_heights[i].text()) for i in range(6)]
            
            # axes ranges
            self.plot['ranges']['wln']['min']  = float(self.wln_start_edit.text())
            self.plot['ranges']['wln']['max']  = float(self.wln_end_edit.text())
            self.plot['ranges']['time']['min'] = float(self.time_start_edit.text())
            self.plot['ranges']['time']['cut'] = float(self.time_cut_edit.text())
            self.plot['ranges']['time']['max'] = float(self.time_end_edit.text())
            self.plot['ranges']['abs']['min']  = float(self.abs_start_edit.text())
            self.plot['ranges']['abs']['step'] = float(self.abs_step_edit.text())
            self.plot['ranges']['abs']['max']  = float(self.abs_end_edit.text())
            self.plot['misc']['timetickslin'] = list(map(float, self.time_lin_ticks_edit.text().split(',')))
            self.plot['misc']['timetickslog'] = list(map(int, self.time_log_ticks_edit.text().split(',')))
            self.plot['contour']['contourticks'] = list(map(float, self.contour_ticks_edit.text().split(',')))
            self.plot['contour']['colormap']['name'] = self.colorbar_combo.currentText()
            self.plot['contour']['tickstep'] = float(self.colorbar_tick_step_edit.text())

            # colormap from cache
            self.plot['contour']['colormap']['flip'] = self.cache['cmapflip']
            self.plot['contour']['colormap']['overon'] = self.cache['cmapoveron']
            self.plot['contour']['colormap']['underon'] = self.cache['cmapunderon']
            if self.cache['cmapstyle'] == 'none':
                self.plot['contour']['colorbarstyle'] = pcplot.ColorbarStyle.NoneStyle
            elif self.cache['cmapstyle'] == 'tri':
                self.plot['contour']['colorbarstyle'] = pcplot.ColorbarStyle.Triangular
            else:
                self.plot['contour']['colorbarstyle'] = pcplot.ColorbarStyle.Rectangular
            tmp = self.cache['cmapover']
            if tmp.strip() and self.plot['contour']['colormap']['overon']:
                self.plot['contour']['colormap']['over'] = pc.mpl_color(tmp)
            else:
                self.plot['contour']['colormap']['over'] = False
            tmp = self.cache['cmapunder']
            if tmp.strip() and self.plot['contour']['colormap']['underon']:
                self.plot['contour']['colormap']['under'] = pc.mpl_color(tmp)
            else:
                self.plot['contour']['colormap']['under'] = False
            self.plot['contour']['colormap']['nan']  = pc.mpl_color(self.cache['cmapnan'])
            
            # traces
            self.plot['traces']['list'] = []
            for i in range(self.traces_table.rowCount()):
                if self.traces_table.item(i, 11).text() == self.translator._('PLOT_EXTERNAL'):
                    trace = pcplot.Plot.ExtWavelengthTrace()
                    trace['path'] = self.traces_table.item(i, 0).text()
                    trace['lim_amax'] = float(self.traces_table.item(i, 12).text())
                    trace['lim_amin'] = float(self.traces_table.item(i, 13).text())
                else:
                    trace = pcplot.Plot.WavelengthTrace()
                    temp = self.traces_table.item(i, 0).text() + ' '
                    x = temp.find('*') # doing this instead of `.split('*')` raises ValueError for more than one '*'
                    trace['wln'] = float(temp[:x])
                    trace['factor'] = float(temp[x+1:]) if x != -1 else 1
                trace['color'] = pc.mpl_color(self.traces_table.item(i, 1).text())
                trace['label'] = self.traces_table.item(i, 2).text()
                if self.traces_table.item(i, 3).checkState():
                    trace['label_traces'] = True
                    trace['trace_x']   = float(self.traces_table.item(i, 4).text())
                    trace['trace_y']   = float(self.traces_table.item(i, 5).text())
                    trace['trace_rot'] = int(self.traces_table.item(i, 6).text())
                else:
                    trace['label_traces'] = False
                if self.traces_table.item(i, 7).checkState():
                    trace['label_contour'] = True
                    trace['contour_x']   = float(self.traces_table.item(i, 8).text())
                    trace['contour_y']   = float(self.traces_table.item(i, 9).text())
                    trace['contour_rot'] = int(self.traces_table.item(i,10).text())
                else:
                    trace['label_contour'] = False
                self.plot['traces']['list'].append(trace)
            self.plot['traces']['xlim'] = tuple(map(float, self.traces_xlim_edit.text().split(',')))
            self.plot['traces']['xticks'] = list(map(float, self.traces_ticks_edit.text().split(',')))
            self.plot['traces']['labelsize'] = self.traces_labelsize_spin.value()
            self.plot['traces']['lw'] = self.traces_lw_spin.value()
            self.plot['traces']['ls'] = self.traces_ls_combo.currentText()
            self.plot['traces']['longwlnline'] = self.expand_wln_lines.checkState()
            
            # spectra
            self.plot['spectra']['list'] = []
            for i in range(self.spectra_table.rowCount()):
                if self.spectra_table.item(i, 7).text() == self.translator._('PLOT_EXTERNAL'):
                    spectrum = pcplot.Plot.ExtTransientSpectrum()
                    spectrum['path'] = self.spectra_table.item(i, 0).text()
                    spectrum['lim_amin'] = float(self.spectra_table.item(i, 8).text())
                    spectrum['lim_amax'] = float(self.spectra_table.item(i, 9).text())
                else:
                    spectrum = pcplot.Plot.TransientSpectrum()
                    temp = self.spectra_table.item(i, 0).text() + ' '
                    x = temp.find('*') # doing this instead of `.split('*')` raises ValueError for more than one '*'
                    spectrum['time'] = float(temp[:x]) if temp[:x] != pcplot.PLOT_OFFSET else pcplot.PLOT_OFFSET
                    spectrum['factor'] = float(temp[x+1:]) if x != -1 else 1
                spectrum['color'] = pc.mpl_color(self.spectra_table.item(i, 1).text())
                if self.spectra_table.item(i, 3).checkState():
                    spectrum['label_on'] = True
                    temp = self.spectra_table.item(i, 6).text()
                    if temp == '':
                        spectrum['labelcolor'] = spectrum['color']
                    else:
                        spectrum['labelcolor'] = pc.mpl_color(temp)
                    spectrum['label'] = self.spectra_table.item(i, 2).text()
                    spectrum['contour_x'] = float(self.spectra_table.item(i, 4).text())
                    spectrum['contour_y'] = float(self.spectra_table.item(i, 5).text())
                else:
                    spectrum['label_on'] = False
                self.plot['spectra']['list'].append(spectrum)
            if self.plot['layout']['design'] is pcplot.PlotLayout.Linear:
                self.plot['spectra']['scaling'] = float(self.spectra_scaling_edit.text())
                self.plot['spectra']['xticks'] = list(map(float, self.spectra_xticks_edit.text().split(',')))
            elif self.plot['layout']['design'] is pcplot.PlotLayout.LType:
                self.plot['spectra']['scaling'] = list(map(float, self.spectra_limits_edit.text().split(',')))
                self.plot['spectra']['yticks'] = list(map(float, self.spectra_yticks_edit.text().split(',')))
            else:
                pass
            self.plot['spectra']['labelsize'] = self.spectra_labelsize_spin.value()
            self.plot['spectra']['lw'] = self.spectra_lw_spin.value()
            self.plot['spectra']['ls'] = self.spectra_ls_combo.currentText()
            self.plot['spectra']['longtimeline'] = self.expand_spectra_lines.checkState()
            
            # axes label from cache
            self.plot['axeslabel']['wlncontour'] = self.cache['wlncontourtitle'].replace('$UNIT$', self.translator._(self.plot['units']['wln'].value))
            self.plot['axeslabel']['wlntransients'] = self.cache['wlntransientstitle'].replace('$UNIT$', self.translator._(self.plot['units']['wln'].value))
            self.plot['axeslabel']['time'] = self.cache['timetitle'].replace('$UNIT$', self.translator._(self.plot['units']['time'].value))
            self.plot['axeslabel']['timedec'] = self.cache['timedec']
            self.plot['axeslabel']['abstraces'] = self.cache['abstracestitle'].replace('$UNIT$', self.translator._(self.plot['units']['abs'].value))
            self.plot['axeslabel']['abscolorbar'] = self.cache['abscolorbartitle'].replace('$UNIT$', self.translator._(self.plot['units']['abs'].value))
            self.plot['axeslabel']['absspectra'] = self.cache['absspectratitle'].replace('$UNIT$', self.translator._(self.plot['units']['abs'].value))
            
            # baselines from cache
            self.plot['traces']['baseline']['on'] = self.cache['baselinetraceson']
            self.plot['traces']['baseline']['value'] = self.cache['baselinetracesvalue']
            self.plot['traces']['baseline']['color'] = pc.mpl_color(self.cache['baselinetracescolor'])
            self.plot['traces']['baseline']['ls'] = self.cache['baselinetracesls']
            self.plot['traces']['baseline']['lw'] = self.cache['baselinetraceslw']
            self.plot['traces']['baseline']['zorder'] = self.cache['baselinetraceszorder']
            
            self.plot['spectra']['baseline']['on'] = self.cache['baselinespectraon']
            self.plot['spectra']['baseline']['value'] = self.cache['baselinespectravalue']
            self.plot['spectra']['baseline']['color'] = pc.mpl_color(self.cache['baselinespectracolor'])
            self.plot['spectra']['baseline']['ls'] = self.cache['baselinespectrals']
            self.plot['spectra']['baseline']['lw'] = self.cache['baselinespectralw']
            self.plot['spectra']['baseline']['zorder'] = self.cache['baselinespectrazorder']
            
            # scalebar from cache
            if self.plot['layout']['design'] is pcplot.PlotLayout.LType:
                self.plot['spectra']['scalebar']['on'] = False
            elif self.plot['layout']['design'] is pcplot.PlotLayout.Linear:
                self.plot['spectra']['scalebar']['on']    = self.cache['scalebaron']
                self.plot['spectra']['scalebar']['value'] = self.cache['scalebarvalue']
                self.plot['spectra']['scalebar']['label'] = self.cache['scalebarlabel']
                self.plot['spectra']['scalebar']['color'] = pc.mpl_color(self.cache['scalebarcolor'])
                self.plot['spectra']['scalebar']['poswln'] = self.cache['scalebarposwln']
                self.plot['spectra']['scalebar']['posy']   = self.cache['scalebarposy']
            else:
                pass
            
            # artists from cache
            self.plot['artists'] = []
            for i in range(len(self.cache['artists'])):
                temp = list(self.cache['artists'][i])
                for j in PlotTabDialogs.ARTISTS_FLOAT_WIDGET_INDICES:
                    temp[j] = float(self.cache['artists'][i][j])
                temp[6] = pc.mpl_color(temp[6])
                temp[9] = pc.mpl_color(temp[9]) if temp[9] is not None else None
                temp[0], temp[3] = self.cache['artists'][i][0], self.cache['artists'][i][3]
                temp[10] = int(temp[10])
                self.plot['artists'].append(temp)
            
            # plot
            self.figure.clear()
            self.plot.draw_plot(self.figure, self.data_line_edit.text(), self.reload, self.monitor_plot_progress)
            self.canvas.draw()
            self.reload = False
        
        except pcerror.InputError as e:
            self.monitor_plot_progress(100, 'STATUS_PLOT_FAILED')
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
        
        except pcerror.OutputError as e:
            self.monitor_plot_progress(100, 'STATUS_PLOT_FAILED')
            dlg = ErrorDialog(self, self.translator._(e.msg).format(output=e.output), self.translator._(e.title))
            dlg.exec()
        
        except pcerror.RangeError as e:
            self.monitor_plot_progress(100, 'STATUS_PLOT_FAILED')
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input, ranges=e.ranges), self.translator._(e.title))
            dlg.exec()
        
        except pcerror.InternalError as e:
            self.monitor_plot_progress(100, 'STATUS_PLOT_FAILED')
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
        
        except ValueError:
            self.monitor_plot_progress(100, 'STATUS_PLOT_FAILED')
            e = pcerror.RangeError('ERROR_PLOT_INVALID_INPUT_ERROR')
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
        
    def save_plot (self, res):
        """
        Export plot to file.
        
        Stores plot as image or vector graphic. Supported file formats are
        png, jpg and tiff as well as pdf, svg and eps.
        
        Parameters
        ----------
        res : int
            resolution in dpi
        
        Raises
        ------
        pcerror.OutputError
            if an unsupported file format is chosen
        
        pcerror.OutputError
            if export fails
        """
        file_name, file_filter = QFileDialog.getSaveFileName(self, self.translator._('FILE_EXPORT'),
            self.working_dir + '/' + self.tabname, pc.FILE_TYPE_FILTER_PLOT)
        if file_name != '':
            match file_filter:
                case pc.FILE_TYPE_PNG: f = 'png'
                case pc.FILE_TYPE_PDF: f = 'pdf'
                case pc.FILE_TYPE_SVG: f = 'svg'
                case pc.FILE_TYPE_EPS: f = 'eps'
                case pc.FILE_TYPE_JPG: f = 'jpg'
                case pc.FILE_TYPE_TIF: f = 'tiff'
                case _:
                    raise pcerror.OutputError('ERROR_PLOT_EXPORT_FILE_FORMAT_ERROR', output=file_filter)
            try:
                self.figure.savefig(file_name, format=f, dpi=res, bbox_inches='tight')
                self.new_status.emit(self.translator._('STATUS_PLOT_IMAGE_SAVED').format(file=file_name), 5000)
            except:
                raise pcerror.OutputError('ERROR_PLOT_EXPORT_ERROR')
    
    def update_tab_name (self):
        """ Store tab name for use as new plot title. """
        self.tabname = self.plot_name_line_edit.text()
    
    #-----------------------------------------------------------------
    #   UNIT HANDLING
    #-----------------------------------------------------------------
    
    def update_wln_unit (self):
        """ Update wavelength unit labels. """
        self.read_wln_unit()
        if self.scalebar_dlg.isVisible():
            self.scalebar_dlg.set_current_labels(
                self.cache['scalebaron'],
                self.cache['scalebarvalue'],
                self.cache['scalebarlabel'],
                self.cache['scalebarcolor'],
                self.cache['scalebarposwln'],
                self.cache['scalebarposy'],
                self.translator._(self.plot['units']['abs'].value),
                self.translator._(self.plot['units']['wln'].value))
        wln_unit = self.plot['units']['wln']
        self.unit_label_wln.setText(self.translator._(wln_unit.value))
        if wln_unit is pc.WavelengthUnit.NanoMeter:
            self.traces_table.setHorizontalHeaderItem(0, QTableWidgetItem(self.translator._('PLOT_WAVELENGTH')))
        elif wln_unit is pc.WavelengthUnit.PerCentiMeter:
            self.traces_table.setHorizontalHeaderItem(0, QTableWidgetItem(self.translator._('PLOT_WAVENUMBER')))
        elif wln_unit is pc.WavelengthUnit.ElectronVolt:
            self.traces_table.setHorizontalHeaderItem(0, QTableWidgetItem(self.translator._('PLOT_ENERGY')))
        else:
            self.traces_table.setHorizontalHeaderItem(0, QTableWidgetItem(self.translator._('PLOT_WAVELENGTH')))
    
    def update_time_unit (self):
        """ Update time unit labels. """
        self.read_time_unit()
        self.unit_label_time.setText(self.translator._(self.plot['units']['time'].value))
    
    def update_abs_unit (self):
        """ Update absorption unit labels. """
        self.read_abs_unit()
        if self.scalebar_dlg.isVisible():
            self.scalebar_dlg.set_current_labels(
                self.cache['scalebaron'],
                self.cache['scalebarvalue'],
                self.cache['scalebarlabel'],
                self.cache['scalebarcolor'],
                self.cache['scalebarposwln'],
                self.cache['scalebarposy'],
                self.translator._(self.plot['units']['abs'].value),
                self.translator._(self.plot['units']['wln'].value))
        if self.baseline_dlg.isVisible():
            self.baseline_dlg.set_current_labels(
                self.cache['baselinetraceson'],
                self.cache['baselinespectraon'],
                self.cache['baselinetracesvalue'],
                self.cache['baselinespectravalue'],
                self.cache['baselinetracescolor'],
                self.cache['baselinespectracolor'],
                self.cache['baselinetracesls'],
                self.cache['baselinespectrals'],
                self.cache['baselinetraceslw'],
                self.cache['baselinespectralw'],
                self.cache['baselinetraceszorder'],
                self.cache['baselinespectrazorder'],
                self.translator._(self.plot['units']['abs'].value))
        self.unit_label_abs.setText(self.translator._(self.plot['units']['abs'].value))
        
    def update_units (self):
        """ Update unit labels. """
        self.update_wln_unit()
        self.update_time_unit()
        self.update_abs_unit()
    
    def read_wln_unit (self):
        """ Read wavelength unit and store it. """
        self.plot['units']['wln'] = pc.WavelengthUnit(self.translator.retranslate(self.wln_unit_combo_box.currentText()))
    
    def read_time_unit (self):
        """ Read time unit and store it. """
        self.plot['units']['time'] = pc.TimeUnit(self.translator.retranslate(self.time_unit_combo_box.currentText()))
    
    def read_abs_unit (self):
        """ Read absorption unit and store it. """
        self.plot['units']['abs'] = pc.AbsorptionUnit(self.translator.retranslate(self.abs_unit_combo_box.currentText()))
    
    def read_units (self):
        """ Read units. """
        self.read_wln_unit()
        self.read_time_unit()
        self.read_abs_unit()
    
    #-----------------------------------------------------------------
    #   HANDLING OF ADDITIONAL DIALOGS
    #-----------------------------------------------------------------
    
    def axes_label_dialog (self):
        """ Show axes label dialog. """
        self.axes_label_dlg.set_current_labels(
            self.cache['wlncontourtitle'],
            self.cache['wlntransientstitle'],
            self.cache['timetitle'],
            self.cache['timedec'],
            self.cache['abstracestitle'],
            self.cache['abscolorbartitle'],
            self.cache['absspectratitle']
        )
        self.axes_label_dlg.show()
        
    def update_axes_labels (self):
        """ Cache settings from axes label dialog. """
        self.cache['wlncontourtitle']    = self.axes_label_dlg.get_wln_contour_label()
        self.cache['wlntransientstitle'] = self.axes_label_dlg.get_wln_transient_label()
        self.cache['timetitle']          = self.axes_label_dlg.get_time_label()
        self.cache['timedec']            = self.axes_label_dlg.get_time_dec_notation()
        self.cache['abstracestitle']     = self.axes_label_dlg.get_amp_traces_label()
        self.cache['abscolorbartitle']   = self.axes_label_dlg.get_colorbar_label()
        self.cache['absspectratitle']    = self.axes_label_dlg.get_amp_spectra_label()
        
    def close_and_apply_axes_dlg (self):
        """ Cache settings from axes label dialog and close it. """
        self.update_axes_labels()
        self.axes_label_dlg.close()
    
    def colorbar_dialog (self):
        """ Show colorbar dialog. """
        self.colorbar_dlg.set_current_labels(
            self.colorbar_combo.currentText(),
            self.cache['cmapflip'],
            self.abs_start_edit.text(),
            self.abs_end_edit.text(),
            self.abs_step_edit.text(),
            self.cache['cmapover'],
            self.cache['cmapunder'],
            self.cache['cmapoveron'],
            self.cache['cmapunderon'],
            self.cache['cmapstyle'],
            self.cache['cmapnan'])
        self.colorbar_dlg.show()
    
    def update_colorbar (self):
        """ Cache settings from colorbar dialog. """
        self.cache['cmapflip']    = self.colorbar_dlg.get_flip_policy()
        self.cache['cmapoveron']  = self.colorbar_dlg.get_over_policy()
        self.cache['cmapunderon'] = self.colorbar_dlg.get_under_policy()
        self.cache['cmapstyle']   = self.colorbar_dlg.get_style()
        self.cache['cmapover']    = self.colorbar_dlg.get_over()
        self.cache['cmapunder']   = self.colorbar_dlg.get_under()
        self.cache['cmapnan']     = self.colorbar_dlg.get_NaN()
    
    def close_and_apply_colorbar_dlg (self):
        """ Cache settings from colorbar dialog and close it. """
        self.update_colorbar()
        self.colorbar_dlg.close()
    
    def baseline_dialog (self):
        """ Show baseline dialog. """
        self.baseline_dlg.set_current_labels(
            self.cache['baselinetraceson'],
            self.cache['baselinespectraon'],
            self.cache['baselinetracesvalue'],
            self.cache['baselinespectravalue'],
            self.cache['baselinetracescolor'],
            self.cache['baselinespectracolor'],
            self.cache['baselinetracesls'],
            self.cache['baselinespectrals'],
            self.cache['baselinetraceslw'],
            self.cache['baselinespectralw'],
            self.cache['baselinetraceszorder'],
            self.cache['baselinespectrazorder'],
            self.translator._(self.plot['units']['abs'].value))
        self.baseline_dlg.show()
    
    def update_baseline (self):
        """ Cache settings from baseline dialog. """
        self.cache['baselinetraceson'], self.cache['baselinespectraon'] = self.baseline_dlg.get_show_policy()
        self.cache['baselinetracesvalue'], self.cache['baselinespectravalue'] = self.baseline_dlg.get_value()
        self.cache['baselinetracescolor'], self.cache['baselinespectracolor'] = self.baseline_dlg.get_color()
        self.cache['baselinetracesls'], self.cache['baselinespectrals'] = self.baseline_dlg.get_ls()
        self.cache['baselinetraceslw'], self.cache['baselinespectralw'] = self.baseline_dlg.get_lw()
        self.cache['baselinetraceszorder'], self.cache['baselinespectrazorder'] = self.baseline_dlg.get_zorder()
    
    def close_and_apply_baseline_dlg (self):
        """ Cache settings from baseline dialog and close it. """
        self.update_baseline()
        self.baseline_dlg.close()
    
    def scalebar_dialog (self):
        """ Show scalebar dialog. """
        self.scalebar_dlg.set_current_labels(
            self.cache['scalebaron'],
            self.cache['scalebarvalue'],
            self.cache['scalebarlabel'],
            self.cache['scalebarcolor'],
            self.cache['scalebarposwln'],
            self.cache['scalebarposy'],
            self.translator._(self.plot['units']['abs'].value),
            self.translator._(self.plot['units']['wln'].value))
        self.scalebar_dlg.show()
    
    def update_scalebar (self):
        """ Cache settings from scalebar dialog. """
        self.cache['scalebaron']     = self.scalebar_dlg.get_show_policy()
        self.cache['scalebarvalue']  = self.scalebar_dlg.get_value()
        self.cache['scalebarlabel']  = self.scalebar_dlg.get_label()
        self.cache['scalebarcolor']  = self.scalebar_dlg.get_color()
        self.cache['scalebarposwln'] = self.scalebar_dlg.get_pos_wln()
        self.cache['scalebarposy']   = self.scalebar_dlg.get_pos_y()
    
    def close_and_apply_scalebar_dlg (self):
        """ Cache settings from scalebar dialog and close it. """
        self.update_scalebar()
        self.scalebar_dlg.close()
    
    def artists_dialog (self):
        """ Show artists dialog. """
        self.artists_dlg.set_current_artists(self.cache['artists'])
        self.artists_dlg.show()
    
    def update_artists (self):
        """ Cache settings from artists dialog. """
        self.cache['artists'] = self.artists_dlg.get_artists()
    
    def close_and_apply_artists_dlg (self):
        """ Cache settings from artists dialog and close it. """
        self.update_artists()
        self.artists_dlg.close()
    