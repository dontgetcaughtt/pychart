from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QLabel, QPushButton, QCheckBox
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QFormLayout

from . import common as pcgui

# -----------------------------------------------------------------
#   INF REPLACEMENT DIALOG
# -----------------------------------------------------------------

class ReplaceInfDialog(QDialog):
    """
    Input dialog for Inf replacement.
    
    Dialog asks user for a float value to replace inf values with.
    Furthermore, it is possible to replace +inf and -inf independently.
    
    Attributes
    ----------
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    
    Emits
    -----
    inf_replacement_requested : float, bool, bool
        emits signal that indicates the replacement value and
        activation status for positive and negative inf
    """
    
    inf_replacement_requested = pyqtSignal(float, bool, bool)
    
    def __init__ (self, parent, *args, **kwargs):
        super(ReplaceInfDialog, self).__init__(parent, *args, **kwargs)
        
        self.pos_check = QCheckBox('+inf')
        self.neg_check = QCheckBox('-inf')
        self.pos_check.setCheckState(Qt.Checked)
        self.neg_check.setCheckState(Qt.Checked)
        lay1 = QHBoxLayout()
        lay1.addWidget(self.pos_check)
        lay1.addWidget(self.neg_check)
        lay1.addStretch()
        
        self.edit = pcgui.qRLineEditW('0.0', 100)
        lay2 = QFormLayout()
        lay2.addRow(QLabel(self.parent().translator._('IMPORT_REPLACE_INF_VALUE') + '   '), self.edit)
        lay2.addRow(QLabel(self.parent().translator._('IMPORT_REPLACE_INF_APPLY') + '   '), lay1)
        
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        lay3 = QHBoxLayout()
        lay3.addWidget(self.ok_btn)
        lay3.addWidget(self.cancel_btn)
        
        lay0 = QVBoxLayout()
        lay0.addWidget(QLabel(self.parent().translator._('IMPORT_REPLACE_INF_TEXT')))
        lay0.addWidget(pcgui.qDummyLabel(10))
        lay0.addLayout(lay2)
        lay0.addWidget(pcgui.qDummyLabel(10))
        lay0.addLayout(lay3)
        
        self.setLayout(lay0)
        self.ok_btn.clicked.connect(self.on_ok_clicked)
        self.cancel_btn.clicked.connect(self.reject)
        self.setWindowTitle(self.parent().translator._('IMPORT_REPLACE_INF_TITLE'))
        self.setModal(False)
    
    def on_ok_clicked (self):
        try:
            temp = float(self.edit.text())
        except ValueError:
            return
        self.inf_replacement_requested.emit(temp, self.pos_check.isChecked(), self.neg_check.isChecked())
        self.close()

# -----------------------------------------------------------------
#   NAN REPLACEMENT DIALOG
# -----------------------------------------------------------------

class ReplaceNaNDialog(QDialog):
    """
    Input dialog for NaN replacement.
    
    Dialog asks user for a float value to replace nan values with.
    
    Attributes
    ----------
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    
    Emits
    -----
    nan_replacement_requested : float
        emits signal that indicates the replacement value
    """
    
    nan_replacement_requested = pyqtSignal(float)
    
    def __init__ (self, parent, *args, **kwargs):
        super(ReplaceNaNDialog, self).__init__(parent, *args, **kwargs)
        
        self.edit = pcgui.qRLineEditW('0.0', 100)
        lay1 = QFormLayout()
        lay1.addRow(QLabel(self.parent().translator._('IMPORT_REPLACE_NAN_VALUE') + '   '), self.edit)
        
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        lay2 = QHBoxLayout()
        lay2.addWidget(self.ok_btn)
        lay2.addWidget(self.cancel_btn)
        
        lay0 = QVBoxLayout()
        lay0.addWidget(QLabel(self.parent().translator._('IMPORT_REPLACE_NAN_TEXT')))
        lay0.addWidget(pcgui.qDummyLabel(10))
        lay0.addLayout(lay1)
        lay0.addWidget(pcgui.qDummyLabel(10))
        lay0.addLayout(lay2)
        
        self.setLayout(lay0)
        self.ok_btn.clicked.connect(self.on_ok_clicked)
        self.cancel_btn.clicked.connect(self.reject)
        self.setWindowTitle(self.parent().translator._('IMPORT_REPLACE_NAN_TITLE'))
        self.setModal(False)
    
    def on_ok_clicked (self):
        try:
            temp = float(self.edit.text())
        except ValueError:
            return
        self.nan_replacement_requested.emit(temp)
        self.close()

# -----------------------------------------------------------------
#   INSERT DATA DIALOG
# -----------------------------------------------------------------

class InsertDataDialog(QDialog):
    """
    Input dialog for insertion of a new column.
    
    Dialog asks user for a header value as well as a float value to initialize with.
    
    Attributes
    ----------
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    
    Emits
    -----
    insert_requested : float, float
        emits signal that indicates the header and initialization values
    """
    
    insert_requested = pyqtSignal(float, float)
    
    def __init__ (self, parent, *args, **kwargs):
        super(InsertDataDialog, self).__init__(parent, *args, **kwargs)
        
        self.msg_label = QLabel()
        
        self.header_edit = pcgui.qRLineEdit60('0.0')
        self.header_label, self.unit_label = QLabel(), QLabel()
        lay1 = QHBoxLayout()
        lay1.addWidget(self.header_label)
        lay1.addWidget(self.header_edit)
        lay1.addWidget(self.unit_label)
        lay1.addStretch()
        
        self.ini_value_edit = pcgui.qRLineEdit60('nan')
        lay2 = QHBoxLayout()
        lay2.addWidget(QLabel(self.parent().translator._('IMPORT_INSERT_INI_VALUE')))
        lay2.addWidget(self.ini_value_edit)
        lay2.addStretch()
        
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        lay3 = QHBoxLayout()
        lay3.addWidget(self.ok_btn)
        lay3.addWidget(self.cancel_btn)
        
        lay0 = QVBoxLayout()
        lay0.addWidget(self.msg_label)
        lay0.addWidget(pcgui.qDummyLabel(10))
        lay0.addLayout(lay1)
        lay0.addLayout(lay2)
        lay0.addWidget(pcgui.qDummyLabel(10))
        lay0.addLayout(lay3)
        
        self.setLayout(lay0)
        self.ok_btn.clicked.connect(self.on_ok_clicked)
        self.cancel_btn.clicked.connect(self.reject)
        self.setWindowTitle(self.parent().translator._('IMPORT_INSERT_DLG_TITLE'))
        self.setModal(False)
    
    def setup (self, orientation, quantity, default, unit):
        if orientation == 'column':
            msg = 'IMPORT_INSERT_COLUMN_TEXT'
        elif orientation == 'row':
            msg = 'IMPORT_INSERT_ROW_TEXT'
        else:
            return
        self.msg_label.setText(self.parent().translator._(msg))
        self.header_label.setText(quantity)
        self.header_edit.setText(default)
        self.unit_label.setText(unit)
    
    def on_ok_clicked (self):
        try:
            wln = float(self.header_edit.text())
            value = float(self.ini_value_edit.text())
        except ValueError:
            return
        self.insert_requested.emit(wln,value)
        self.close()
