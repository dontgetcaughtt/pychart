# TODO: EXPERIMENTAL: WIP - CODE REVIEW needed
# code is very ugly and constantly changed on-the-fly

import numpy as np

import matplotlib as plt
from matplotlib.figure import Figure as pltFigure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as pltCanvas

from PyQt5.QtCore import Qt, QDir
from PyQt5.QtWidgets import QWidget, QFileDialog, QDialog
from PyQt5.QtWidgets import QLabel, QLineEdit, QPushButton, QCheckBox, QComboBox, QSpinBox, QRadioButton
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QFormLayout, QSplitter

from . import common as pcgui
from ..core import pychart as pc
from ..core import fit as pcfit

plt.rcParams['axes.linewidth']    = 1.25

class SaveFitDialog (QDialog):
    
    def __init__ (self, *args, **kwargs):
        super(SaveFitDialog, self).__init__(*args, **kwargs)


class TDFitTab (QWidget):
    
    def __init__ (self, name, translator, *args, **kwargs):
        super(TDFitTab, self).__init__(*args, **kwargs)

        self.fit = pcfit.TDFit()
        self.tabname = name
        self._translator = translator
        
        self.params = [
            {'param': self.translator._('TDFIT_PARAMS_TSHIFT'),  'start': None, 'result': None, 'fix': None},
            {'param': self.translator._('TDFIT_PARAMS_BG'),      'start': None, 'result': None, 'fix': None},
            {'param': self.translator._('TDFIT_PARAMS_BGIRF'),   'start': None, 'result': None, 'fix': None},
            {'param': self.translator._('TDFIT_PARAMS_CUTTOFF'), 'start': None, 'result': None, 'fix': None},
            {'param': self.translator._('TDFIT_PARAMS_SCATTER'), 'start': None, 'result': None, 'fix': None}
        ]
        
        self.times = [
            [None, None, None, None, None, None], [None, None, None, None, None, None],
            [None, None, None, None, None, None], [None, None, None, None, None, None],
            [None, None, None, None, None, None], [None, None, None, None, None, None],
            [None, None, None, None, None, None], [None, None, None, None, None, None],
        ]
        
        self.setup_gui()
        
        # CONNECTIONS
        self.data_file_dlg_button.clicked.connect(lambda: self.update_data_file())
        self.irf_file_dlg_button.clicked.connect(lambda: self.update_irf_file())
        self.irf_file_radio.clicked.connect(self.enable_convolution_widgets)
        self.irf_gauss_radio.clicked.connect(self.enable_convolution_widgets)
        self.irf_none_radio.clicked.connect(self.enable_convolution_widgets)
        self.times_spin_box.valueChanged.connect(self.enable_times_widgets)
        self.save_button.clicked.connect(lambda: self.save_fit())
        self.run_button.clicked.connect(self.start_fit)
        
        self.xaxis_combobox.currentTextChanged.connect(self.finalize_plot)
        self.yaxis_combobox.currentTextChanged.connect(self.finalize_plot)
        self.xaxis_min_edit.editingFinished.connect(self.finalize_plot)
        self.xaxis_max_edit.editingFinished.connect(self.finalize_plot)
        self.yaxis_min_edit.editingFinished.connect(self.finalize_plot)
        self.yaxis_max_edit.editingFinished.connect(self.finalize_plot)
        
        # INITIALIZE
        self.enable_convolution_widgets()
        self.enable_times_widgets()
        self.rescale_plot = False
        self.finalize_plot()
    
    @property
    def translator (self):
        return self._translator
    
    def setup_gui (self):
        
        # DATA FILE
        self.data_line_edit = QLineEdit()
        self.data_line_edit.setPalette(pcgui.qPalette(pcgui.BLUE))
        self.data_file_dlg_button = QPushButton('...')
        self.data_file_dlg_button.setMaximumWidth(30)
        self.unit_combo_box = QComboBox()
        for unit in pc.TimeUnit:
            self.unit_combo_box.addItem(self.translator._(unit.value))
        #self.unit_combo_box.setCurrentText(pc.TimeUnit[2])
        self.unit_combo_box.setEditable(False)
        lay4 = QHBoxLayout()
        lay4.addWidget(QLabel(self.translator._('TDFIT_FILES_DATA')))
        lay4.addWidget(self.data_line_edit)
        lay4.addWidget(self.data_file_dlg_button)
        lay4.addWidget(pcgui.qDummyLabel(10))
        lay4.addWidget(QLabel(self.translator._('TDFIT_GENERAL_UNITS')))
        lay4.addWidget(self.unit_combo_box)
        
        # GENERAL GROUP BOX
        self.fit_name_line_edit = QLineEdit(self.tabname)
        lay2  = QFormLayout()
        lay2.addRow(QLabel(self.translator._('TDFIT_GENERAL_FIT')), QLabel(self.translator._('TDFIT_GENERAL_FIT_t')))
        lay2.addRow(QLabel(self.translator._('TDFIT_GENERAL_NAME')), self.fit_name_line_edit)
        group1 = QGroupBox(self.translator._('TDFIT_GENERAL_GROUP'))
        group1.setLayout(lay2)
        
        # CONVOLUTION GROUP BOX
        self.irf_file_radio  = QRadioButton(self.translator._('TDFIT_CONV_IRF'))
        self.irf_gauss_radio = QRadioButton(self.translator._('TDFIT_CONV_GAUSS'))
        self.irf_none_radio  = QRadioButton(self.translator._('TDFIT_CONV_NONE'))
        self.irf_file_radio.setChecked(True)
        self.irf_line_edit = QLineEdit()
        self.irf_line_edit.setPalette(pcgui.qPalette(pcgui.GREEN))
        self.irf_file_dlg_button = QPushButton('...')
        self.irf_file_dlg_button.setMaximumWidth(30)
        lay8 = QHBoxLayout()
        lay8.addWidget(self.irf_line_edit)
        lay8.addWidget(self.irf_file_dlg_button)
        self.fwhm_line_edit = pcgui.qRLineEdit()
        self.fwhm_line_edit.setMaximumWidth(50)
        lay9 = QHBoxLayout()
        lay9.addWidget(QLabel(self.translator._('TDFIT_CONV_FWHM')))
        lay9.addWidget(self.fwhm_line_edit)
        lay9.addStretch()
        lay3 = QGridLayout()
        lay3.addWidget(self.irf_file_radio,  0, 0)
        lay3.addWidget(self.irf_gauss_radio, 1, 0)
        lay3.addWidget(self.irf_none_radio,  2, 0)
        lay3.addLayout(lay8, 0, 2)
        lay3.addLayout(lay9, 1, 2)
        lay3.setColumnMinimumWidth(1,10)
        group2 = QGroupBox(self.translator._('TDFIT_CONV_GROUP'))
        group2.setLayout(lay3)
        
        # PARAMETERS GROUP BOX
        self.start_line_edit = pcgui.qRLineEdit('1')
        self.end_line_edit   = pcgui.qRLineEdit('50')
        self.step_line_edit  = pcgui.qRLineEdit('1')
        self.very_start_button = QPushButton('⇥') #⇥ ⟼
        self.very_end_button   = QPushButton('>|') #⇤ ⟻
        self.very_start_button.setMaximumWidth(30)
        self.very_end_button.setMaximumWidth(30)
        lay5 = QGridLayout()
        lay5.addWidget(QLabel(self.translator._('TDFIT_PARAMS_BEGIN')), 0, 0)
        lay5.addWidget(QLabel(self.translator._('TDFIT_PARAMS_END')),   1, 0)
        lay5.addWidget(QLabel(self.translator._('TDFIT_PARAMS_STEP')),  2, 0)
        lay5.addWidget(self.start_line_edit,    0, 1)
        lay5.addWidget(self.end_line_edit,      1, 1)
        lay5.addWidget(self.step_line_edit,     2, 1)
        lay5.addWidget(self.very_start_button,  0, 2)
        lay5.addWidget(self.very_end_button,    1, 2)
        lay11 = QGridLayout()
        lay11.addWidget(pcgui.qCLabel(self.translator._('TDFIT_PARAMS_START')), 0, 1)
        lay11.addWidget(pcgui.qCLabel(self.translator._('TDFIT_PARAMS_RESULT')), 0, 2)
        row = 1
        for param in self.params:
            param['start']  = pcgui.qRLineEdit()
            param['result'] = pcgui.qRLineEdit()
            param['fix']    = QCheckBox()
            param['result'].setReadOnly(True)
            param['result'].setPalette(pcgui.qPalette(pcgui.ORANGE))
            param['fix'].setText(self.translator._('TDFIT_PARAMS_FIX'))
            lay11.addWidget(QLabel(param['param']), row, 0)
            lay11.addWidget(param['start'], row, 1)
            lay11.addWidget(param['result'], row, 2)
            lay11.addWidget(param['fix'], row, 3)
            param['start'].setText('0')
            row += 1
        lay12 = QHBoxLayout()
        lay12.addLayout(lay5)
        lay12.addStretch()
        lay12.addLayout(lay11)
        group4 = QGroupBox(self.translator._('TDFIT_PARAMS_GROUP'))
        group4.setLayout(lay12)
        
        # TIME CONSTANTS GROUP BOX
        self.times_spin_box = QSpinBox()
        self.times_spin_box.setRange(1,5)
        self.times_spin_box.setValue(1)
        lay7 = QHBoxLayout()
        lay7.addWidget(pcgui.qDummyLabel(30))
        lay7.addWidget(QLabel(self.translator._('TDFIT_TIMES_TIMES')))
        lay7.addWidget(self.times_spin_box)
        lay7.addStretch()
        lay6 = QGridLayout()
        lay6.addWidget(pcgui.qCLabel(self.translator._('TDFIT_TIMES_MINIMUM')),   0, 1)
        lay6.addWidget(pcgui.qCLabel(self.translator._('TDFIT_TIMES_START')),     0, 3)
        lay6.addWidget(pcgui.qCLabel(self.translator._('TDFIT_TIMES_MAXIMUM')),   0, 5)
        lay6.addWidget(pcgui.qCLabel(self.translator._('TDFIT_TIMES_AMPLITUDE')), 0, 6)
        lay6.addWidget(pcgui.qCLabel(self.translator._('TDFIT_TIMES_FIX')),       0, 7)
        lay6.addWidget(pcgui.qCLabel(self.translator._('TDFIT_TIMES_RESULT')),    0, 9)
        row = 1
        for i in [0,1,2,3,4]:
            self.times[i][0] = pcgui.qRLineEdit() # 0: min
            self.times[i][1] = pcgui.qRLineEdit() # 1: start
            self.times[i][2] = pcgui.qRLineEdit() # 2: max
            self.times[i][3] = pcgui.qRLineEdit() # 3: amp
            self.times[i][4] = QCheckBox()          # 4: fix
            self.times[i][5] = pcgui.qRLineEdit() # 5: fit
            self.times[i][0].setMaximumWidth(round(self.times[i][1].width() * 2/3))
            self.times[i][5].setReadOnly(True)
            self.times[i][5].setPalette(pcgui.qPalette(pcgui.ORANGE))
            temp_a, temp_b = pcgui.qCLabel('≤'), pcgui.qCLabel('≤')
            temp_a.setMaximumWidth(20)
            temp_b.setMaximumWidth(20)
            lay6.addWidget(QLabel('τ<sub>'+str(i+1)+'</sub>:'), row, 0)
            lay6.addWidget(self.times[i][0], row, 1)
            lay6.addWidget(temp_a,           row, 2)
            lay6.addWidget(self.times[i][1], row, 3)
            lay6.addWidget(temp_b,           row, 4)
            lay6.addWidget(self.times[i][2], row, 5)
            lay6.addWidget(self.times[i][3], row, 6)
            lay6.addWidget(self.times[i][4], row, 7)
            lay6.setColumnMinimumWidth(8,15)
            lay6.addWidget(self.times[i][5], row, 9)
            row += 1
        lay13 = QVBoxLayout()
        lay13.addLayout(lay7)
        lay13.addStretch()
        lay13.addLayout(lay6)
        group5 = QGroupBox(self.translator._('TDFIT_TIMES_GROUP'))
        group5.setLayout(lay13)
        
        # FIT BUTTONS
        self.ini_button  = QPushButton(self.translator._('TDFIT_FIT_INI'))
        self.run_button  = QPushButton(self.translator._('TDFIT_FIT_RUN'))
        self.save_button = QPushButton(self.translator._('TDFIT_FIT_SAVE'))
        lay10 = QHBoxLayout()
        lay10.addWidget(self.ini_button)
        lay10.addWidget(self.run_button)
        lay10.addStretch()
        lay10.addWidget(self.save_button)
        
        # FIT RESULT
        self.xaxis_combobox = QComboBox()
        self.xaxis_combobox.addItem(self.translator._('TDFIT_PLOT_LIN'))
        self.xaxis_combobox.addItem(self.translator._('TDFIT_PLOT_LOG'))
        self.xaxis_combobox.setEditable(False)
        self.xaxis_min_edit = pcgui.qRLineEdit('0')
        self.xaxis_max_edit = pcgui.qRLineEdit('100')
        self.yaxis_combobox = QComboBox()
        self.yaxis_combobox.addItem(self.translator._('TDFIT_PLOT_LOG'))
        self.yaxis_combobox.addItem(self.translator._('TDFIT_PLOT_LIN'))
        self.yaxis_combobox.setEditable(False)
        self.yaxis_min_edit = pcgui.qRLineEdit('1')
        self.yaxis_max_edit = pcgui.qRLineEdit('100')
        lay14 = QGridLayout()
        lay14.addWidget(QLabel(self.translator._('TDFIT_PLOT_XAXIS')), 0, 0)
        lay14.addWidget(self.xaxis_combobox,   0, 1)
        lay14.addWidget(self.xaxis_min_edit,   0, 2)
        lay14.addWidget(self.xaxis_max_edit,   0, 3)
        lay14.addWidget(QLabel(self.translator._('TDFIT_PLOT_YAXIS')), 1, 0)
        lay14.addWidget(self.yaxis_combobox,   1, 1)
        lay14.addWidget(self.yaxis_min_edit,   1, 2)
        lay14.addWidget(self.yaxis_max_edit,   1, 3)
        
        self.figure  = pltFigure()
        self.canvas  = pltCanvas(self.figure)
        grid = plt.gridspec.GridSpec(2, 1, height_ratios=[5,1])
        self.data_plot = self.figure.add_subplot(grid[0,0])
        self.residuals_plot = self.figure.add_subplot(grid[1,0])
        self.figure.subplots_adjust(left=0.15, right=0.95, top=0.95, bottom=0.075)
        lay1 = QVBoxLayout()
        lay1.addLayout(lay14)
        lay1.addWidget(self.canvas)
        group6 = QGroupBox(self.translator._('TDFIT_FIT_GROUP'))
        group6.setLayout(lay1)
        lay16 = QVBoxLayout()
        lay16.addWidget(group6)
        
        # MAIN LAYOUT
        lay15 = QGridLayout()
        lay15.addLayout(lay4,   0, 0, 1, 2)
        lay15.setRowMinimumHeight(1,10)
        lay15.addWidget(group1, 2, 0)
        lay15.addWidget(group2, 2, 1)
        lay15.addWidget(group4, 3, 0, 1, 2)
        lay15.addWidget(group5, 4, 0, 1, 2)
        lay15.addLayout(lay10,  5, 0, 1, 2)
        lay_left, lay_right = QWidget(), QWidget()
        lay_left.setLayout(lay15)
        lay_right.setLayout(lay16)
        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(lay_left)
        splitter.addWidget(lay_right)
        lay_main = QVBoxLayout()
        lay_main.addWidget(splitter)
        self.setLayout(lay_main)
    
    def update_irf_file (self):
        file_name, file_filter = QFileDialog.getOpenFileName(self,
            self.translator._('FILE_OPEN'), QDir.currentPath(), pc.FILE_TYPE_FILTER_DATA)
        if not file_name == '':
            try:
                self.fit.load_irf_data(file_name)
                self.irf_line_edit.setText(file_name)
                self.rescale_plot = True
                self.update_plot()
            except:
                pass
    
    def update_data_file (self):
        file_name, file_filter = QFileDialog.getOpenFileName(self,
            self.translator._('FILE_OPEN'), QDir.currentPath(), pc.FILE_TYPE_FILTER_DATA)
        if not file_name == '':
            try:
                self.fit.load_data(file_name)
                self.data_line_edit.setText(file_name)
                self.rescale_plot = True
                self.update_plot()
            except:
                pass
    
    def enable_times_widgets (self):
        num = self.times_spin_box.value()
        for i in [0,1,2,3,4]:
            state = i < num
            self.times[i][0].setEnabled(state)
            self.times[i][1].setEnabled(state)
            self.times[i][2].setEnabled(state)
            self.times[i][3].setEnabled(state)
            self.times[i][4].setEnabled(state)
            self.times[i][5].setEnabled(state)
    
    def enable_convolution_widgets (self):
        irf_state = self.irf_file_radio.isChecked()
        self.irf_line_edit.setEnabled(irf_state)
        self.irf_file_dlg_button.setEnabled(irf_state)
        self.fwhm_line_edit.setEnabled(self.irf_gauss_radio.isChecked())
        for param in self.params:
            if param['param'] == self.translator._('TDFIT_PARAMS_TSHIFT'):
                state = not self.irf_none_radio.isChecked()
            elif param['param'] == (self.translator._('TDFIT_PARAMS_BGIRF') or self.translator._('TDFIT_PARAMS_CUTOFF')):
                state = irf_state
            else:
                continue
            param['start'].setEnabled(state)
            param['result'].setEnabled(state)
            param['fix'].setEnabled(state)
    
    def save_fit (self):
        return 0
    
    def finalize_plot (self):
        typex = self.xaxis_combobox.currentText()
        typey = self.yaxis_combobox.currentText()
        self.data_plot.set_xscale('linear' if typex == self.translator._('TDFIT_PLOT_LIN') else 'log')
        self.data_plot.set_yscale('linear' if typey == self.translator._('TDFIT_PLOT_LIN') else 'log')
        if not self.rescale_plot: # stick to user's settings
            try:
                minx = float(self.xaxis_min_edit.text())
                maxx = float(self.xaxis_max_edit.text())
                miny = float(self.yaxis_min_edit.text())
                maxy = float(self.yaxis_max_edit.text())
                self.data_plot.set_xlim(minx, maxx)
                self.data_plot.set_ylim(miny, maxy)
            except ValueError:
                pass
        else: # keep autoscaling from update_plot()
            self.rescale_plot = False
            minx, maxx = self.data_plot.get_xlim()
            miny, maxy = self.data_plot.get_ylim()
            self.xaxis_min_edit.setText(f"{minx:.5f}")
            self.xaxis_max_edit.setText(f"{maxx:.5f}")
            self.yaxis_min_edit.setText(f"{miny:.5f}")
            self.yaxis_max_edit.setText(f"{maxy:.5f}")
        self.data_plot.set_xlabel(self.translator._('TDFIT_PLOT_XLABEL') + self.unit_combo_box.currentText(), fontsize=12)
        self.data_plot.set_ylabel(self.translator._('TDFIT_PLOT_YLABEL'), fontsize=12)
        self.data_plot.tick_params(which='both', direction='in', top=True,
            bottom=True, left=True, right=True, labelsize=12)
        self.data_plot.grid(which='major', axis='both', color='gray', ls='-', lw=0.75)
        self.data_plot.grid(which='minor', axis='both', color='gray', ls=':', lw=0.75)
        try:
            a = float(self.start_line_edit.text())
            b = float(self.end_line_edit.text())
            t = float(self.params[0]['start'].text())
            self.data_plot.axvspan(a,b, fill=True, lw=2, color=tuple(map(lambda x: float(x) / 255, pcgui.ORANGE)))
            self.data_plot.plot([t,t],[miny,maxy], lw=2, color='c')
        
        except ValueError:
            pass # in case that data files are loaded, but no ranges given yet
        self.residuals_plot.set_xscale('linear' if typex == self.translator._('TDFIT_PLOT_LIN') else 'log')
        self.residuals_plot.set_xlim(self.data_plot.get_xlim())
        self.residuals_plot.set_xlabel(self.translator._('TDFIT_PLOT_XLABEL') + self.unit_combo_box.currentText())
        self.residuals_plot.tick_params(which='both', direction='in')
        self.residuals_plot.grid(which='major', axis='both', color='gray', ls='-', lw=0.75)
        self.canvas.draw()
    
    def update_plot (self):
        self.data_plot.clear()
        self.residuals_plot.clear()
        if self.fit['data'] is not None:
            self.data_plot.plot(self.fit['data'][:,0], self.fit['data'][:,1],
                                '.', color=tuple(map(lambda x: float(x) / 255, pcgui.BLUE)))
        if self.fit['irfdata'] is not None:
            self.data_plot.plot(self.fit['irfdata'][:,0], self.fit['irfdata'][:,1],
                                'x', color=tuple(map(lambda x: float(x) / 255, pcgui.GREEN)))
        if self.fit['fit'] is not None:
            self.data_plot.plot(self.fit['axis'], self.fit['fit'].best_fit,
                                '-', lw=3, color='r')
            self.residuals_plot.plot(self.fit['axis'], self.fit['fit'].residual, 'k-')
            #pc.export_pychart_file(self.fit['fit'].best_fit,[0],self.fit['axis'],'C:\\Users\\Kristoffer\\Documents\\Uni\\Macrocycle\\export.txt')
        self.finalize_plot()
    
    def start_fit (self):
        
        def five_digit_string (number):
            return f"{number:.5f}"
        
        try:
            self.fit['name'] = self.fit_name_line_edit.text()
        
            # read convolution 
            if self.irf_file_radio.isChecked():
                self.fit['convolution'] = pcfit.TDFit.Conv.IRF
            elif self.irf_gauss_radio.isChecked():
                self.fit['convolution'] = pcfit.TDFit.Conv.Gauss
                try:
                    self.fit['fwhm'] = float(self.fwhm_line_edit.text())
                except ValueError:
                    print('Value Error in FWHM')
            else:
                self.fit['convolution'] = pcfit.TDFit.Conv.NoConv
            
            # read time unit
            self.fit['unit'] = pc.TimeUnit(self.translator.retranslate(self.unit_combo_box.currentText()))
            
            # read fit ranges
            try:
                self.fit['ranges']['start'] = float(self.start_line_edit.text())
                self.fit['ranges']['end']   = float(self.end_line_edit.text())
                self.fit['ranges']['step']  = float(self.step_line_edit.text())
            except ValueError:
                print('Value Error for "ranges"')
            
            # read fit paramaters
            for param, i in (('shift', 0), ('bg', 1), ('bgirf', 2), ('cuttoff', 3), ('scatter', 4)):
                try:
                    self.fit['params'][param]['start'] = float(self.params[i]['start'].text())
                except ValueError:
                    if ((param == 'shift') and (self.fit['convolution'] is pcfit.TDFit.Conv.NoConv)) or ((param == ('bgirf' or 'cuttoff')) and (self.fit['convolution'] is not pcfit.TDFit.Conv.IRF)):
                        pass # invalid params do not need to be given
                    else:
                        print('ValueError in "params"')
                self.fit['params'][param]['fix'] = self.params[i]['fix'].isChecked()
            
            # read times
            self.fit['timescount'] = self.times_spin_box.value()
            for i in range(self.times_spin_box.value()):
                try:
                    temp = self.times[i][0].text()
                    self.fit['times'][i]['range']['min'] = -np.inf if temp == '' else float(temp)
                    self.fit['times'][i]['start']['time'] = float(self.times[i][1].text())
                    temp = self.times[i][2].text()
                    self.fit['times'][i]['range']['max'] =  np.inf if temp == '' else float(temp)
                    self.fit['times'][i]['start']['amp']  = float(self.times[i][3].text())
                except ValueError:
                    print('ValueError in "times"')
                self.fit['times'][i]['fix'] = self.times[i][4].isChecked()
            
            #timer = QElapsedTimer()
            #timer.start()
            self.fit.run_fit()
            self.update_plot()
        
        except ValueError as error:
            print(error)
        
        finally:
            if self.fit['success']:
                for i in range(self.times_spin_box.value()):
                    self.times[i][5].setText(five_digit_string(self.fit['times'][i]['fit']['time']) + ';' + five_digit_string(self.fit['times'][i]['fit']['amp']))
                self.params[1]['result'].setText(five_digit_string(self.fit['params']['bg']['fit']))
                self.params[2]['result'].setText(five_digit_string(self.fit['params']['bgirf']['fit']))
                #self.params[0]['result'].setText(five_digit_string(self.fit['params']['shift']['fit']))
            else:
                for i in range(5):
                    self.times[i][5].setText('')
                for param in self.params:
                    param['result'].setText('')

    def enable_save (self):
        self.save_button.setEnabled(self.fit['success'])
    
    def close (self):
        return 0
        