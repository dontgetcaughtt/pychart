import lmfit

from matplotlib.figure import Figure as mplFigure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as mplCanvas
from matplotlib.lines import Line2D as mplLine

from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QLabel, QPushButton, QCheckBox, QRadioButton
from PyQt5.QtWidgets import QHBoxLayout, QGridLayout, QGroupBox
from PyQt5.QtGui import QPixmap, QIcon

from . import common as pcgui
from .ErrorDialog import ErrorDialog
from ..core import pychart as pc
from ..core import error as pcerror
from ..core.sellmeier import sellmeier_func

class GateCorrectionSetupDialog (QDialog):
    """
    Setup dialog for gate measurement processing.
    
    Dialog lets the user choose between two methods: Gaussfit and
    Minima. For the Gaussfit option start values for fitting
    parameters can be set.
    
    For post-processing the time zero curve can be smoothed or
    fitted according to Sellmeier's equation.
    
    Results are previewed, but main data correction based on
    the current settings is only conducted from the NetCDFImportTab.
    
    Attributes
    ----------
    plot : mpl.subplot
        subplot to draw the preview in
    
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    
    Emits
    -----
    time_zeros_requested : ('QString', QString', dict)
        emits signal along with processing settings that indicates
        that time zeros should be determined
    """
    
    time_zeros_requested = pyqtSignal('QString', 'QString', dict)
    
    def __init__ (self, parent, *args, **kwargs):
        super(GateCorrectionSetupDialog, self).__init__(parent, *args, **kwargs)
        
        self.plot = None
        
        # data file settings
        self.data_file_label, self.bg_file_label = QLabel(), QLabel()
        self.choppermode_label, self.signaltype_label = QLabel(), QLabel()
        self.scans_label, self.wlns_label, self.times_label = QLabel(), QLabel(), QLabel()
        lay5 = QGridLayout()
        lay5.addWidget(QLabel(self.parent().translator._('IMPORT_RAWDATA')),              0, 0)
        lay5.addWidget(QLabel(self.parent().translator._('IMPORT_BGDATA')),               1, 0)
        lay5.addWidget(QLabel(self.parent().translator._('IMPORT_GATE_DLG_WAVELENGTHS')), 2, 0)
        lay5.addWidget(QLabel(self.parent().translator._('IMPORT_GATE_DLG_TIMES')),       3, 0)
        lay5.addWidget(QLabel(self.parent().translator._('IMPORT_GATE_DLG_CHOPPERMODE')), 2, 3)
        lay5.addWidget(QLabel(self.parent().translator._('IMPORT_GATE_DLG_SIGNALTYPE')),  3, 3)
        lay5.addWidget(QLabel(self.parent().translator._('IMPORT_GATE_DLG_SCANS')),       4, 0)
        lay5.addWidget(self.data_file_label,            0, 1, 1, 10)
        lay5.addWidget(self.bg_file_label,              1, 1, 1, 10)
        lay5.addWidget(self.wlns_label,                 2, 1)
        lay5.addWidget(self.times_label,                3, 1)
        lay5.addWidget(self.choppermode_label,          2, 4)
        lay5.addWidget(self.signaltype_label,           3, 4)
        lay5.addWidget(self.scans_label,                4, 1)
        group5 = QGroupBox(self.parent().translator._('IMPORT_GATE_DLG_FILE_GROUP'))
        group5.setLayout(lay5)
        
        # processing settings
        self.gate_method_rb1, self.gate_method_rb2 = QRadioButton(), QRadioButton()
        self.gate_method_rb1.setChecked(True)
        lay4a = pcgui.qLabeledWidget(self.gate_method_rb1, self.parent().translator._('IMPORT_GATE_DLG_GAUSS'))
        lay4b = pcgui.qLabeledWidget(self.gate_method_rb2, self.parent().translator._('IMPORT_GATE_DLG_MAXIMA'))
        
        eq_gauss = QPixmap('../images/equations/gauss.jpg')
        eq_gauss_label = QLabel()
        eq_gauss_label.setPixmap(eq_gauss)
        eq_gauss_label.setMask(eq_gauss.mask())
        lay4f = QHBoxLayout()
        lay4f.addStretch()
        lay4f.addWidget(eq_gauss_label)
        lay4f.addStretch()
        
        self.amp_start_edit = pcgui.qFloatSpinBox(1, -100, 100, 1, 1)
        self.center_start_edit = pcgui.qFloatSpinBox(0.5, -2, 2, 0.1, 1)
        self.center_min_limit_edit = pcgui.qFloatSpinBox(-1, -2, 2, 0.1, 1)
        self.center_max_limit_edit = pcgui.qFloatSpinBox(1.2, -2, 2, 0.1, 1)
        self.width_start_edit = pcgui.qFloatSpinBox(0.1, -2, 2, 0.05, 2)
        self.offset_start_edit = pcgui.qFloatSpinBox(0.15, -2, 2, 0.01, 2)
        lay4c = QHBoxLayout()
        lay4c.addWidget(self.center_min_limit_edit)
        lay4c.addWidget(QLabel(self.parent().translator._('IMPORT_GATE_DLG_CENTER_RANGE')))
        lay4c.addWidget(self.center_max_limit_edit)
        
        self.amp_start_auto_check, self.center_start_auto_check = QCheckBox(), QCheckBox()
        self.center_start_auto_check.setCheckState(Qt.Checked)
        lay4d = pcgui.qLabeledWidget(self.amp_start_auto_check, self.parent().translator._('IMPORT_GATE_DLG_AMP_AUTO'))
        lay4e = pcgui.qLabeledWidget(self.center_start_auto_check, self.parent().translator._('IMPORT_GATE_DLG_CENTER_AUTO'))
        
        lay4 = QGridLayout()
        lay4.addLayout(lay4a,                                     0, 0, 1, 5)
        lay4.addLayout(lay4f,                                     1, 0, 1, 5)
        lay4.addWidget(QLabel('     ' + self.parent().translator._('IMPORT_GATE_DLG_AMP')),    2, 0)
        lay4.addWidget(QLabel('     ' + self.parent().translator._('IMPORT_GATE_DLG_CENTER')), 3, 0)
        lay4.addWidget(QLabel('     ' + self.parent().translator._('IMPORT_GATE_DLG_WIDTH')),  4, 0)
        lay4.addWidget(QLabel('     ' + self.parent().translator._('IMPORT_GATE_DLG_OFFSET')), 5, 0)
        lay4.addWidget(self.amp_start_edit,                       2, 1)
        lay4.addLayout(lay4d,                                     2, 2)
        lay4.addWidget(self.center_start_edit,                    3, 1)
        lay4.addLayout(lay4e,                                     3, 2)
        lay4.addWidget(pcgui.qDummyLabel(10),                     3, 3)
        lay4.addLayout(lay4c,                                     3, 4)
        lay4.addWidget(self.width_start_edit,                     4, 1)
        lay4.addWidget(self.offset_start_edit,                    5, 1)
        lay4.addLayout(lay4b,                                     6, 0, 1, 5)
        group4 = QGroupBox(self.parent().translator._('IMPORT_GATE_DLG_PROCESSING_GROUP'))
        group4.setLayout(lay4)
        
        # post-processing settings
        self.post_processing_rb1 = QRadioButton(self.parent().translator._('IMPORT_GATE_DLG_NO_SMOOTH'))
        self.post_processing_rb2 = QRadioButton(self.parent().translator._('IMPORT_GATE_DLG_SMOOTH'))
        self.post_processing_rb3 = QRadioButton(self.parent().translator._('IMPORT_GATE_DLG_SELLMEIER'))
        self.post_processing_rb1.setChecked(True)
        
        self.smooth_spin = pcgui.qIntSpinBox(5, 2, 20, 1)
        
        eq_sellmeier = QPixmap('../images/equations/sellmeier-fix.jpg')
        eq_sellmeier_label = QLabel()
        eq_sellmeier_label.setPixmap(eq_sellmeier)
        eq_sellmeier_label.setMask(eq_sellmeier.mask())
        lay3a = QHBoxLayout()
        lay3a.addStretch()
        lay3a.addWidget(eq_sellmeier_label)
        lay3a.addStretch()
        
        self.fit_wln_start_spin = pcgui.qIntSpinBox(330, 100, 1000, 1, ' nm')
        self.fit_wln_end_spin = pcgui.qIntSpinBox(720, 100, 1000, 1, ' nm')
        self.fit_thickness_spin = pcgui.qFloatSpinBox(2, 0.1, 100, 0.1, 3, ' mm')
        self.fit_offset_spin = pcgui.qFloatSpinBox(10, -200, 200, 0.1, 3, ' ps')
        
        self.fit_thickness_label = pcgui.qRLineEdit80()
        self.fit_offset_label = pcgui.qRLineEdit80()
        self.fit_thickness_label.setEnabled(False)
        self.fit_offset_label.setEnabled(False)
        
        self.fit_thickness_check = QCheckBox(self.parent().translator._('IMPORT_GATE_DLG_FIX'))
        self.fit_offset_check = QCheckBox(self.parent().translator._('IMPORT_GATE_DLG_FIX'))
        
        self.fit_thickness_convey_btn = pcgui.qPushButton30()
        self.fit_offset_convey_btn = pcgui.qPushButton30()
        self.fit_thickness_convey_btn.setIcon(QIcon('../images/icons/convey.svg'))
        self.fit_offset_convey_btn.setIcon(QIcon('../images/icons/convey.svg'))
        self.fit_thickness_convey_btn.setToolTip(self.parent().translator._('IMPORT_GATE_DLG_CONVEY'))
        self.fit_offset_convey_btn.setToolTip(self.parent().translator._('IMPORT_GATE_DLG_CONVEY'))
        
        lay3 = QGridLayout()
        lay3.addWidget(self.post_processing_rb1,                     0, 0, 1, 6)
        lay3.addWidget(self.post_processing_rb2,                     1, 0)
        lay3.addWidget(self.post_processing_rb3,                     2, 0, 1, 6)
        lay3.addWidget(self.smooth_spin,                             1, 2)
        lay3.addLayout(lay3a,                                        3, 0, 1, 6)
        lay3.addWidget(QLabel('     ' + self.parent().translator._('IMPORT_GATE_DLG_WLN')),       4, 0)
        lay3.addWidget(QLabel('     ' + self.parent().translator._('IMPORT_GATE_DLG_THICKNESS')), 5, 0)
        lay3.addWidget(QLabel('     ' + self.parent().translator._('IMPORT_GATE_DLG_OFFSET')),    6, 0)
        lay3.addWidget(self.fit_wln_start_spin,                      4, 1)
        lay3.addWidget(pcgui.qCLabel(self.parent().translator._('IMPORT_GATE_DLG_WLN_RANGE')),    4, 2, 1, 2)
        lay3.addWidget(self.fit_wln_end_spin,                        4, 4)
        lay3.addWidget(self.fit_thickness_spin,                      5, 1)
        lay3.addWidget(self.fit_thickness_check,                     5, 2, alignment=Qt.AlignRight)
        lay3.addWidget(self.fit_thickness_label,                     5, 4)
        lay3.addWidget(self.fit_thickness_convey_btn,                5, 3)
        lay3.addWidget(self.fit_offset_spin,                         6, 1)
        lay3.addWidget(self.fit_offset_check,                        6, 2, alignment=Qt.AlignRight)
        lay3.addWidget(self.fit_offset_label,                        6, 4)
        lay3.addWidget(self.fit_offset_convey_btn,                   6, 3)
        group3 = QGroupBox(self.parent().translator._('IMPORT_GATE_DLG_POSTPROCESSING_GROUP'))
        group3.setLayout(lay3)
        
        # result
        self.preview_fig = mplFigure()
        self.preview_canvas = mplCanvas(self.preview_fig)
        self.preview_fig.set_size_inches(4, 2, forward=True)
        self.preview_canvas.setMinimumHeight(250)
        lay2 = QHBoxLayout()
        lay2.addWidget(self.preview_canvas)
        group2 = QGroupBox(self.parent().translator._('IMPORT_GATE_DLG_RESULT_GROUP'))
        group2.setLayout(lay2)
        
        # buttons and main layout
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.apply_btn = QPushButton(self.parent().translator._('IMPORT_GATE_DLG_PREVIEW'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        
        lay1 = QHBoxLayout()
        lay1.addStretch()
        lay1.addWidget(self.ok_btn)
        lay1.addWidget(self.apply_btn)
        lay1.addWidget(self.cancel_btn)
        lay1.addStretch()
        
        lay = QGridLayout()
        lay.addWidget(group5, 0, 0, 1, 2)
        lay.addWidget(group4, 1, 0)
        lay.addWidget(group3, 1, 1)
        lay.addWidget(group2, 2, 0, 1, 2)
        lay.addLayout(lay1,   4, 0, 1, 2)
        
        # connections
        self.gate_method_rb1.clicked.connect(self.update_layout)
        self.gate_method_rb2.clicked.connect(self.update_layout)
        self.amp_start_auto_check.stateChanged.connect(self.update_layout)
        self.center_start_auto_check.stateChanged.connect(self.update_layout)
        self.post_processing_rb1.clicked.connect(self.update_layout)
        self.post_processing_rb2.clicked.connect(self.update_layout)
        self.post_processing_rb3.clicked.connect(self.update_layout)
        self.fit_thickness_convey_btn.clicked.connect(self.convey_fit_thickness)
        self.fit_offset_convey_btn.clicked.connect(self.convey_fit_offset)
        self.apply_btn.clicked.connect(self.request_preview)
        
        # finalisation
        self.update_layout()
        self.setLayout(lay)
        self.ok_btn.setDefault(True)
        self.ok_btn.clicked.connect(self.accept)
        self.cancel_btn.clicked.connect(self.reject)
        self.setWindowTitle(self.parent().translator._('IMPORT_GATE_DLG_TITLE'))
        self.setModal(True)
    
    def update_layout (self):
        """ Enable / disable widgets. """
        state = self.gate_method_rb1.isChecked()
        self.amp_start_edit.setEnabled(state and not self.amp_start_auto_check.isChecked())
        self.center_start_edit.setEnabled(state and not self.center_start_auto_check.isChecked())
        self.center_min_limit_edit.setEnabled(state)
        self.center_max_limit_edit.setEnabled(state)
        self.width_start_edit.setEnabled(state)
        self.offset_start_edit.setEnabled(state)
        self.amp_start_auto_check.setEnabled(state)
        self.center_start_auto_check.setEnabled(state)
        
        self.smooth_spin.setEnabled(self.post_processing_rb2.isChecked())
        self.fit_wln_start_spin.setEnabled(self.post_processing_rb3.isChecked())
        self.fit_wln_end_spin.setEnabled(self.post_processing_rb3.isChecked())
        self.fit_thickness_spin.setEnabled(self.post_processing_rb3.isChecked())
        self.fit_offset_spin.setEnabled(self.post_processing_rb3.isChecked())
        self.fit_thickness_check.setEnabled(self.post_processing_rb3.isChecked())
        self.fit_offset_check.setEnabled(self.post_processing_rb3.isChecked())
        self.fit_thickness_convey_btn.setEnabled(self.post_processing_rb3.isChecked())
        self.fit_offset_convey_btn.setEnabled(self.post_processing_rb3.isChecked())
    
    def set_file_settings (self, datafile, bgfile, wlns, times, mode, signaltype, scans):
        """ Set file settings in data file group. """
        self.data_file_label.setText(datafile)
        self.bg_file_label.setText(bgfile)
        wlna, wlnb, nwlns = wlns
        self.wlns_label.setText(f"[{int(round(wlna))},{int(round(wlnb))}] (n={nwlns})")
        self.fit_wln_start_spin.setRange(int(round(wlna)), int(round(wlnb)) - 1)
        self.fit_wln_end_spin.setRange(int(round(wlna)) + 1, int(round(wlnb)))
        timea, timeb, ntimes = times
        self.times_label.setText(f"[{timea:.1f},{timeb:.1f}] (n={ntimes})")
        self.choppermode_label.setText(mode)
        self.signaltype_label.setText(signaltype)
        self.scans_label.setText(scans)
    
    def get_processing_settings (self):
        """
        Returns user-defined processing settings for gate correction.
        
        Returns
        -------
        (str, str, dict)
            method for peak determination, processing method, fitting settings
        """
        method = 'gauss' if self.gate_method_rb1.isChecked() else 'maxima'
        
        if self.post_processing_rb2.isChecked():
            processing = 'smooth'
        elif self.post_processing_rb3.isChecked():
            processing = 'fit'
        else:
            processing = 'off'
        
        settings = {
            'amp':               self.amp_start_edit.value(),
            'amp_auto':          self.amp_start_auto_check.isChecked(),
            'center':            self.center_start_edit.value(),
            'center_auto':       self.center_start_auto_check.isChecked(),
            'center_min':        self.center_min_limit_edit.value(),
            'center_max':        self.center_max_limit_edit.value(),
            'width':             self.width_start_edit.value(),
            'offset':            self.offset_start_edit.value(),
            'smooth':            self.smooth_spin.value(),
            'fit_start':         self.fit_wln_start_spin.value(),
            'fit_end':           self.fit_wln_end_spin.value(),
            'fit_thickness':     self.fit_thickness_spin.value(),
            'fit_thickness_fix': bool(self.fit_thickness_check.checkState()),
            'fit_offset':        self.fit_offset_spin.value(),
            'fit_offset_fix':    bool(self.fit_offset_check.checkState())
        }
        return method, processing, settings
    
    def set_processing_settings (self, old):
        """ Restore old settings in case the cancel button was used the last time. """
        method, processing, settings = old
        self.gate_method_rb1.setChecked(method == 'gauss')
        self.gate_method_rb2.setChecked(method != 'gauss')
        
        self.amp_start_edit.setValue(settings['amp'])
        self.amp_start_auto_check.setChecked(settings['amp_auto'])
        self.center_start_edit.setValue(settings['center'])
        self.center_start_auto_check.setChecked(settings['center_auto'])
        self.center_min_limit_edit.setValue(settings['center_min'])
        self.center_max_limit_edit.setValue(settings['center_max'])
        self.width_start_edit.setValue(settings['width'])
        self.offset_start_edit.setValue(settings['offset'])
        
        self.post_processing_rb1.setChecked(processing == 'off')
        self.post_processing_rb2.setChecked(processing == 'smooth')
        self.post_processing_rb3.setChecked(processing == 'fit')
        
        self.smooth_spin.setValue(settings['smooth'])
        self.fit_wln_start_spin.setValue(settings['fit_start'])
        self.fit_wln_end_spin.setValue(settings['fit_end'])
        self.fit_thickness_spin.setValue(settings['fit_thickness'])
        self.fit_thickness_check.setCheckState(settings['fit_thickness_fix'])
        self.fit_offset_spin.setValue(settings['fit_offset'])
        self.fit_offset_check.setCheckState(settings['fit_offset_fix'])
    
    def initialize_preview (self):
        """ Clear last preview and generate new one. """
        self.preview_fig.clear()
        self.preview_fig.subplots_adjust(left=0.08, right=0.98, top=0.85, bottom=0.2)
        self.plot = self.preview_fig.add_subplot()
    
    def finalize_preview (self):
        """ Apply some standard layout to the figure. """
        self.plot.tick_params(which='both', direction='in', top=True, bottom=True, left=True, right=True, labelsize=10)
        self.plot.grid(which='both', axis='both', color='gray', ls='--', lw=0.75)
        self.plot.set_xlabel(self.parent().translator._('IMPORT_GATE_DLG_WLN_LABEL'), fontdict={'fontsize': 10})
        self.plot.set_ylabel(self.parent().translator._('IMPORT_GATE_DLG_TIME_LABEL'), fontdict={'fontsize': 10})
        
        legend_handles = [mplLine([0], [0], marker='.', lw=0, color=pc.mpl_color(pcgui.ORANGE)),
                          mplLine([0], [0], color=pc.mpl_color(pcgui.BLUE)),
                          mplLine([0], [0], color=(1, 0, 0)),
                          mplLine([0], [0], marker='x', lw=0, color=(0, 0, 0))]
        legend_labels = [self.parent().translator._('IMPORT_GATE_DLG_LEGEND_GATE'),
                         self.parent().translator._('IMPORT_GATE_DLG_LEGEND_SMOOTH'),
                         self.parent().translator._('IMPORT_GATE_DLG_LEGEND_FIT'),
                         self.parent().translator._('IMPORT_GATE_DLG_LEGEND_RECENT')]
        self.plot.legend(legend_handles, legend_labels, bbox_to_anchor=(0, 1.02, 1, .1), loc='lower left',
                         mode='expand', borderaxespad=0, ncol=4, fontsize=10, frameon=False, numpoints=3)
        
        self.preview_canvas.draw()
    
    def request_preview (self):
        """ Request the NetCDFImportTab to determine time zeros. """
        method, processing, settings = self.get_processing_settings()
        self.time_zeros_requested.emit(method, 'off', settings)
    
    def show_preview (self, wlns, peaks):
        """ Plot time zeros determined from gate measurement and preview smooth results. """
        self.initialize_preview()
        
        # wlns and peaks must be of the same dtype for the fit to work properly
        # usually peaks.dtype == np.float64, while peaks.dtype == np.float32
        x = wlns.astype(dtype=peaks.dtype)
        
        self.plot.plot(x, peaks, '.', color=pc.mpl_color(pcgui.ORANGE))
        
        if self.post_processing_rb2.isChecked():
            self.plot.plot(x, pc.smooth(peaks, self.smooth_spin.value()), '-', color=pc.mpl_color(pcgui.BLUE))
        
        if self.post_processing_rb3.isChecked():
            a = self.fit_wln_start_spin.value()
            b = self.fit_wln_end_spin.value()
            a_i = pc.lookup_index(a, x)
            b_i = pc.lookup_index(b, x)
            try:
                if not a_i < b_i:
                    raise pcerror.RangeError('ERROR_IMPORT_INVALID_GATE_WLN_RANGE_ERROR')
                if (b_i - a_i) < 2:
                    raise pcerror.RangeError('ERROR_IMPORT_SMALL_GATE_WLN_RANGE_ERROR')
            except pcerror.RangeError as e:
                dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg), self.parent().translator._(e.title))
                dlg.exec()
                return
            
            # Reminder: When changing the post processing procedure (e.g. the nan policy or the
            # fitting method) apply the same changes to the actual processing routine implemented
            # in `pychart.determine_time_zeros`, too.
            model = lmfit.Model(sellmeier_func, independent_vars=['λ'], nan_policy='omit')
            params = model.make_params(d=self.fit_thickness_spin.value(), offset=self.fit_offset_spin.value())
            params['d'].set(vary=not self.fit_thickness_check.checkState())
            params['offset'].set(vary=not self.fit_offset_check.checkState())
            result = model.fit(peaks[a_i:b_i], λ=x[a_i:b_i], params=params, method='leastsq')
            
            self.fit_thickness_label.setText(f"{result.best_values['d']:.3f} mm")
            self.fit_offset_label.setText(f"{result.best_values['offset']:.3f} ps")
            
            time_zeros = sellmeier_func(wlns, result.best_values['d'], result.best_values['offset'])
            self.plot.plot(wlns, time_zeros, '-', color=(1, 0, 0))
            self.plot.axvspan(a, b, fill=True, lw=0, color=(1, 0, 0, 0.1))
        
        self.finalize_preview()
    
    def clear_preview (self):
        """ Clear preview when gate correction was disabled. """
        self.preview_fig.clear()
    
    def show_recent_time_zeros (self, wlns, time_zeros):
        """ Plot recently used time zeros. """
        self.initialize_preview()
        self.plot.plot(wlns, time_zeros, 'x', color=(0, 0, 0))
        self.finalize_preview()
    
    def convey_fit_thickness (self):
        """ Convey Sellmeier fit result for thickness into corresponding input field. """
        try:
            self.fit_thickness_spin.setValue(float(self.fit_thickness_label.text()[:-3]))
        except ValueError:
            pass
    
    def convey_fit_offset (self):
        """ Convey Sellmeier fit result for the offset into corresponding input field. """
        try:
            self.fit_offset_spin.setValue(float(self.fit_offset_label.text()[:-3]))
        except ValueError:
            pass
