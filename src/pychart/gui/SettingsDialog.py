from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QFileDialog, QWidget, QTabWidget, QLabel, QPushButton, QLineEdit, QComboBox, QCheckBox, QRadioButton
from PyQt5.QtWidgets import QGridLayout, QHBoxLayout, QVBoxLayout, QFormLayout, QGroupBox, QButtonGroup

from . import common as pcgui
from .ErrorDialog import ErrorDialog
from ..utils import i18n
from ..core import pychart as pc
from ..core import error as pcerror

""" Lists of indices for rcParams edit widgets.  """
RCPARAMS_INDICES_1  = [0, 1, 10, 2, 4, 5, 8]
RCPARAMS_INDICES_2  = [11, 3, 6, 7, 9]
SEPARATOR_INDICES_1 = [3, 1]
SEPARATOR_INDICES_2 = [1]

class GeneralSettingsDialog (QDialog):
    """
    Input dialog for general user settings.
    
    Dialog lets the user define several pychart-wide default values
    or rather specific settings.
    
    Attributes
    ----------
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    
    Emits
    -----
    default_dir_changed : str
        indicates a change of the default directory
    
    gate_time_range_changed : float, float
        indicates changes about the gate time ranges
    
    netcdf_axes_tolerances_changed : float, float
        indicates changes about the axes tolerances for NetCDF imports
    
    hdf_axes_tolerances_changed : float, float
        indicates changes about the axes tolerances for HDF imports
    """
    
    default_dir_changed = pyqtSignal('QString')
    gate_time_range_changed = pyqtSignal(float,float)
    netcdf_axes_tolerances_changed = pyqtSignal(float,float)
    hdf_axes_tolerances_changed = pyqtSignal(float,float)
    
    def __init__ (self, settings, parent, *args, **kwargs):
        
        super(GeneralSettingsDialog, self).__init__(parent, *args, **kwargs)
        
        self.settings = settings
        
        self.tabs = QTabWidget()
        self.tabs.addTab(self.setup_general_tab(), self.parent().translator._('SETTINGS_GENERAL'))
        self.tabs.addTab(self.setup_plot_tab(), self.parent().translator._('SETTINGS_PLOT'))
        self.tabs.addTab(self.setup_netcdf_tab(), self.parent().translator._('SETTINGS_NETCDF'))
        self.tabs.addTab(self.setup_hdf_tab(), self.parent().translator._('SETTINGS_HDF'))
        
        self.default_btn = QPushButton(self.parent().translator._('SETTINGS_DLG_RESTORE'))
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        lay_btn = QHBoxLayout()
        lay_btn.addWidget(self.default_btn)
        lay_btn.addStretch()
        lay_btn.addWidget(self.ok_btn)
        lay_btn.addWidget(self.cancel_btn)
        
        lay = QVBoxLayout()
        lay.addWidget(self.tabs)
        lay.addLayout(lay_btn)
        
        self.setLayout(lay)
        self.default_btn.clicked.connect(self.restore_default_settings)
        self.ok_btn.clicked.connect(self.on_ok)
        self.cancel_btn.clicked.connect(self.reject)
        self.setWindowTitle(self.parent().translator._('SETTINGS_DLG_TITLE'))
    
    # -----------------------------------------------------------------
    #   USER INTERFACE
    # -----------------------------------------------------------------
    
    def setup_general_tab (self):
        """ Generate general settings user interface. """
        self.default_dir_edit = QLineEdit()
        self.default_dir_button = pcgui.qPushButton30('...')
        self.default_dir_button.clicked.connect(self.set_default_directory)
        lay_dir = QHBoxLayout()
        lay_dir.addWidget(self.default_dir_edit)
        lay_dir.addWidget(self.default_dir_button)
        
        self.language_combo = QComboBox()
        for lang in i18n.LanguageCode:
            self.language_combo.addItem(self.parent().translator._(lang.value))
        
        self.default_tab_combo = QComboBox()
        self.default_tab_combo.addItem(self.parent().translator._('SETTINGS_TAB_CHOOSE'))
        self.default_tab_combo.addItem(self.parent().translator._('SETTINGS_TAB_PLOT'))
        self.default_tab_combo.addItem(self.parent().translator._('SETTINGS_TAB_TDFIT'))
        self.default_tab_combo.addItem(self.parent().translator._('SETTINGS_TAB_NETCDF'))
        self.default_tab_combo.addItem(self.parent().translator._('SETTINGS_TAB_HDF'))
        
        lay = QFormLayout()
        lay.addRow(QLabel(self.parent().translator._('SETTINGS_LANGUAGE')), self.language_combo)
        lay.addRow(QLabel(self.parent().translator._('SETTINGS_DEFAULT_DIR')), lay_dir)
        lay.addRow(QLabel(self.parent().translator._('SETTINGS_DEFAULT_TAB')), self.default_tab_combo)
        
        w = QWidget()
        w.setLayout(lay)
        return w
    
    def setup_plot_tab (self):
        """ Generate PlotTab settings user interface. """
        self.default_settings_edit = QLineEdit()
        self.default_settings_button = pcgui.qPushButton30('...')
        self.default_settings_button.clicked.connect(self.set_plot_settings_file)
        lay_dir = QHBoxLayout()
        lay_dir.addWidget(QLabel(self.parent().translator._('SETTINGS_DEFAULT_PLOT_SETTINGS')))
        lay_dir.addWidget(self.default_settings_edit)
        lay_dir.addWidget(self.default_settings_button)
        
        self.export_dpi_spin = pcgui.qIntSpinBox(300, 60, 600, 10)
        lay_dpi = QHBoxLayout()
        lay_dpi.addWidget(QLabel(self.parent().translator._('SETTINGS_EXPORT_RESOLUTION')))
        lay_dpi.addWidget(self.export_dpi_spin)
        lay_dpi.addWidget(QLabel(self.parent().translator._('SETTINGS_EXPORT_DPI')))
        lay_dpi.addStretch()
        
        widgets_list = [
            pcgui.qIntSpinBox(    15,   0, 100,    1   ), pcgui.qIntSpinBox(    12,   0, 100,    1   ),
            pcgui.qIntSpinBox(    11,   0, 100,    1   ), pcgui.qIntSpinBox(    11,   0, 100,    1   ),
            pcgui.qFloatSpinBox( 5.0, 0.0, 100,  0.1, 1), pcgui.qFloatSpinBox( 5.0, 0.0, 100,  0.1, 1),
            pcgui.qFloatSpinBox( 3.0, 0.0, 100,  0.1, 1), pcgui.qFloatSpinBox( 3.0, 0.0, 100,  0.1, 1),
            pcgui.qFloatSpinBox( 1.0, 0.0, 100,  0.1, 1), pcgui.qFloatSpinBox( 1.0, 0.0, 100,  0.1, 1),
            pcgui.qFloatSpinBox(1.25, 0.0, 100, 0.05, 2), pcgui.qFloatSpinBox(1.25, 0.0, 100, 0.05, 2)]
        self.mpl_rcparam_spins = dict(zip(pc.MPL_RCPARAMS, widgets_list))
        
        lay1 = QFormLayout()
        for i in RCPARAMS_INDICES_1:
            key = pc.MPL_RCPARAMS[i]
            lay1.addRow(QLabel(key), self.mpl_rcparam_spins[key])
        for i in SEPARATOR_INDICES_1:
            lay1.insertRow(i, pcgui.qSeparatorLine())
        
        lay2 = QFormLayout()
        for i in RCPARAMS_INDICES_2:
            key = pc.MPL_RCPARAMS[i]
            lay2.addRow(QLabel(key), self.mpl_rcparam_spins[key])
        for i in SEPARATOR_INDICES_2:
            lay2.insertRow(i, pcgui.qSeparatorLine())
        
        self.mpl_font_family_combo = QComboBox()
        self.mpl_font_family_combo.addItems(pc.MPL_FONT_FAMILIES)
        lay3 = QHBoxLayout()
        lay3.addWidget(QLabel('font.family'))
        lay3.addWidget(self.mpl_font_family_combo)
        
        lay4 = QGridLayout()
        lay4.addWidget(QLabel(self.parent().translator._('SETTINGS_RCPARAMS_WARNING')), 0, 0, 1, 4)
        lay4.addWidget(QLabel(), 1, 0)
        lay4.addLayout(lay1,     2, 0, 2, 1)
        lay4.addLayout(lay2,     3, 1, 1, 1)
        lay4.addWidget(QLabel(), 4, 0)
        lay4.addLayout(lay3,     5, 0)
        
        group1 = QGroupBox(self.parent().translator._('SETTINGS_RCPARAMS_GROUP'))
        group1.setLayout(lay4)
        
        lay = QVBoxLayout()
        lay.addLayout(lay_dir)
        lay.addLayout(lay_dpi)
        lay.addWidget(QLabel())
        lay.addWidget(group1)
        
        w = QWidget()
        w.setLayout(lay)
        return w
    
    def setup_netcdf_tab (self):
        """ Generate NetCDF settings user interface. """
        
        # export factor group
        self.netcdf_export_pc_factor_check = QCheckBox(self.parent().translator._('SETTINGS_PC_FACTOR'))
        self.netcdf_export_pc_factor_edit = pcgui.qRLineEdit60('1000')
        self.netcdf_export_pc_factor_check.setCheckState(Qt.Unchecked)
        self.netcdf_export_pc_factor_edit.setEnabled(False)
        self.netcdf_export_pc_factor_check.stateChanged.connect(
            lambda: self.netcdf_export_pc_factor_edit.setEnabled(self.netcdf_export_pc_factor_check.isChecked()))
        lay_pc = QHBoxLayout()
        lay_pc.addWidget(self.netcdf_export_pc_factor_check)
        lay_pc.addWidget(self.netcdf_export_pc_factor_edit)
        lay_pc.addStretch()
        
        self.netcdf_export_z20_factor_check = QCheckBox(self.parent().translator._('SETTINGS_Z20_FACTOR'))
        self.netcdf_export_z20_factor_edit = pcgui.qRLineEdit60('1000')
        self.netcdf_export_z20_factor_check.setCheckState(Qt.Unchecked)
        self.netcdf_export_z20_factor_edit.setEnabled(False)
        self.netcdf_export_z20_factor_check.stateChanged.connect(
            lambda: self.netcdf_export_z20_factor_edit.setEnabled(self.netcdf_export_z20_factor_check.isChecked()))
        lay_z20 = QHBoxLayout()
        lay_z20.addWidget(self.netcdf_export_z20_factor_check)
        lay_z20.addWidget(self.netcdf_export_z20_factor_edit)
        lay_z20.addStretch()
        
        lay1 = QVBoxLayout()
        lay1.addWidget(QLabel(self.parent().translator._('SETTINGS_FACTOR')))
        lay1.addLayout(lay_pc)
        lay1.addLayout(lay_z20)
        
        group1 = QGroupBox(self.parent().translator._('SETTINGS_EXPORT_GROUP'))
        group1.setLayout(lay1)
        
        # gate correction time range group
        self.netcdf_gate_spin1 = pcgui.qFloatSpinBox(-1.2,-5,-0.1,0.1,1,' ps')
        self.netcdf_gate_spin2 = pcgui.qFloatSpinBox(1.2,0.1,10,0.1,1,' ps')
        
        lay2 = QHBoxLayout()
        lay2.addWidget(QLabel(self.parent().translator._('SETTINGS_NETCDF_GATE_CONFINE')))
        lay2.addWidget(self.netcdf_gate_spin1)
        lay2.addWidget(QLabel(self.parent().translator._('SETTINGS_NETCDF_GATE_RANGE')))
        lay2.addWidget(self.netcdf_gate_spin2)
        lay2.addStretch()
        
        group2 = QGroupBox(self.parent().translator._('SETTINGS_NETCDF_GATE_GROUP'))
        group2.setLayout(lay2)
        
        # axes tolerances group
        self.netcdf_wln_tol_edit  = pcgui.qRLineEdit60()
        self.netcdf_time_tol_edit = pcgui.qRLineEdit60()
        
        lay3 = QFormLayout()
        lay3.addRow(QLabel(self.parent().translator._('SETTINGS_WLN_TOLERANCE')), self.netcdf_wln_tol_edit)
        lay3.addRow(QLabel(self.parent().translator._('SETTINGS_TIME_TOLERANCE')), self.netcdf_time_tol_edit)
        
        group3 = QGroupBox(self.parent().translator._('SETTINGS_TOLERANCE_GROUP'))
        group3.setLayout(lay3)
        
        # main tab layout
        lay = QVBoxLayout()
        lay.addWidget(group1)
        lay.addWidget(group2)
        lay.addWidget(group3)
        lay.addStretch()
        
        w = QWidget()
        w.setLayout(lay)
        return w
    
    def setup_hdf_tab (self):
        """ Generate HDF settings user interface. """
        
        # export factor group
        self.hdf_export_pc_factor_check = QCheckBox(self.parent().translator._('SETTINGS_PC_FACTOR'))
        self.hdf_export_pc_factor_edit = pcgui.qRLineEdit60('1000')
        self.hdf_export_pc_factor_check.setCheckState(Qt.Unchecked)
        self.hdf_export_pc_factor_edit.setEnabled(False)
        self.hdf_export_pc_factor_check.stateChanged.connect(
            lambda: self.hdf_export_pc_factor_edit.setEnabled(self.hdf_export_pc_factor_check.isChecked()))
        lay_pc = QHBoxLayout()
        lay_pc.addWidget(self.hdf_export_pc_factor_check)
        lay_pc.addWidget(self.hdf_export_pc_factor_edit)
        lay_pc.addStretch()
        
        self.hdf_export_z20_factor_check = QCheckBox(self.parent().translator._('SETTINGS_Z20_FACTOR'))
        self.hdf_export_z20_factor_edit = pcgui.qRLineEdit60('1000')
        self.hdf_export_z20_factor_check.setCheckState(Qt.Unchecked)
        self.hdf_export_z20_factor_edit.setEnabled(False)
        self.hdf_export_z20_factor_check.stateChanged.connect(
            lambda: self.hdf_export_z20_factor_edit.setEnabled(self.hdf_export_z20_factor_check.isChecked()))
        lay_z20 = QHBoxLayout()
        lay_z20.addWidget(self.hdf_export_z20_factor_check)
        lay_z20.addWidget(self.hdf_export_z20_factor_edit)
        lay_z20.addStretch()
        
        lay1 = QVBoxLayout()
        lay1.addWidget(QLabel(self.parent().translator._('SETTINGS_FACTOR')))
        lay1.addLayout(lay_pc)
        lay1.addLayout(lay_z20)
        
        group1 = QGroupBox(self.parent().translator._('SETTINGS_EXPORT_GROUP'))
        group1.setLayout(lay1)
        
        # axes tolerances group
        self.hdf_wln_tol_edit = pcgui.qRLineEdit60()
        self.hdf_time_tol_edit = pcgui.qRLineEdit60()
        
        lay2 = QFormLayout()
        lay2.addRow(QLabel(self.parent().translator._('SETTINGS_WLN_TOLERANCE')), self.hdf_wln_tol_edit)
        lay2.addRow(QLabel(self.parent().translator._('SETTINGS_TIME_TOLERANCE')), self.hdf_time_tol_edit)
        
        group2 = QGroupBox(self.parent().translator._('SETTINGS_TOLERANCE_GROUP'))
        group2.setLayout(lay2)
        
        # photodiode plot group
        self.hdf_pd_separated_radio = QRadioButton(self.parent().translator._('SETTINGS_HDF_PLOT_PD_SEPARATED'))
        self.hdf_pd_combined_radio = QRadioButton(self.parent().translator._('SETTINGS_HDF_PLOT_PD_COMBINED'))
        pd_btn_group = QButtonGroup()
        pd_btn_group.addButton(self.hdf_pd_separated_radio)
        pd_btn_group.addButton(self.hdf_pd_combined_radio)
        
        lay3 = QVBoxLayout()
        lay3.addWidget(QLabel(self.parent().translator._('SETTINGS_HDF_PLOT_PD')))
        lay3.addWidget(self.hdf_pd_separated_radio)
        lay3.addWidget(self.hdf_pd_combined_radio)
        
        group3 = QGroupBox(self.parent().translator._('SETTINGS_HDF_PLOT_PD_GROUP'))
        group3.setLayout(lay3)
        
        # main tab layout
        lay = QVBoxLayout()
        lay.addWidget(group1)
        lay.addWidget(group2)
        lay.addWidget(group3)
        lay.addStretch()
        
        w = QWidget()
        w.setLayout(lay)
        return w
    
    # -----------------------------------------------------------------
    #   SLOTS
    # -----------------------------------------------------------------
    
    def restore_default_settings (self):
        """ Restore default settings and display them. """
        self.settings.default_settings()
        self.display_settings()
    
    def set_default_directory (self):
        """ Choose default directory from file dialog. """
        dir_name = QFileDialog.getExistingDirectory(self, self.parent().translator._('DIRECTORY_OPEN'), self.settings.default_dir)
        if dir_name != '':
            self.default_dir_edit.setText(dir_name)
    
    def set_plot_settings_file (self):
        """ Choose default plot settings from file dialog. """
        file_name, file_filter = QFileDialog.getOpenFileName(self,
            self.parent().translator._('FILE_OPEN'), self.settings.default_dir, pc.FILE_TYPE_JSON)
        if file_name != '':
            self.default_settings_edit.setText(file_name)
    
    def on_ok (self):
        self.write_settings()
        self.accept()
    
    # -----------------------------------------------------------------
    #   HANDLE SETTINGS
    # -----------------------------------------------------------------
    
    def display_settings (self):
        """ Display current settings. """
        
        # general
        self.language_combo.setCurrentText(self.settings.language)
        self.default_tab_combo.setCurrentText(self.settings.startup_tab)
        self.default_dir_edit.setText(self.settings.default_dir)
        
        # plot
        self.default_settings_edit.setText(self.settings.default_plot_settings)
        self.export_dpi_spin.setValue(self.settings.export_resolution)
        for key in self.settings.rcparams.keys():
            try:
                self.mpl_rcparam_spins[key].setValue(self.settings.rcparams[key])
            except KeyError:
                pass
        self.mpl_font_family_combo.setCurrentText(self.settings.rcparams['font.family'])
        
        # netcdf
        if self.settings.netcdf_to_pychart_factor is not None:
            self.netcdf_export_pc_factor_check.setCheckState(Qt.Checked)
            self.netcdf_export_pc_factor_edit.setText(str(self.settings.netcdf_to_pychart_factor))
        else:
            self.netcdf_export_pc_factor_check.setCheckState(Qt.Unchecked)
            self.netcdf_export_pc_factor_edit.setText('1000')
        if self.settings.netcdf_to_z20_factor is not None:
            self.netcdf_export_z20_factor_check.setCheckState(Qt.Checked)
            self.netcdf_export_z20_factor_edit.setText(str(self.settings.netcdf_to_z20_factor))
        else:
            self.netcdf_export_z20_factor_check.setCheckState(Qt.Unchecked)
            self.netcdf_export_z20_factor_edit.setText('1000')
        
        self.netcdf_gate_spin1.setValue(self.settings.gate_time_range[0])
        self.netcdf_gate_spin2.setValue(self.settings.gate_time_range[1])
        
        self.netcdf_wln_tol_edit.setText(str(self.settings.netcdf_wln_tolerance))
        self.netcdf_time_tol_edit.setText(str(self.settings.netcdf_time_tolerance))
        
        # hdf
        if self.settings.hdf_to_pychart_factor is not None:
            self.hdf_export_pc_factor_check.setCheckState(Qt.Checked)
            self.hdf_export_pc_factor_edit.setText(str(self.settings.hdf_to_pychart_factor))
        else:
            self.hdf_export_pc_factor_check.setCheckState(Qt.Unchecked)
            self.hdf_export_pc_factor_edit.setText('1000')
        if self.settings.hdf_to_z20_factor is not None:
            self.hdf_export_z20_factor_check.setCheckState(Qt.Checked)
            self.hdf_export_z20_factor_edit.setText(str(self.settings.hdf_to_z20_factor))
        else:
            self.hdf_export_z20_factor_check.setCheckState(Qt.Unchecked)
            self.hdf_export_z20_factor_edit.setText('1000')
        
        self.hdf_wln_tol_edit.setText(str(self.settings.hdf_wln_tolerance))
        self.hdf_time_tol_edit.setText(str(self.settings.hdf_time_tolerance))
        
        self.hdf_pd_separated_radio.setChecked(self.settings.pd_plot_layout == 'separated')
        self.hdf_pd_combined_radio.setChecked(self.settings.pd_plot_layout == 'combined')
    
    def write_settings (self):
        """ Write user inputs to settings class. """
        
        # general
        self.settings.language = self.language_combo.currentText()
        self.settings.default_dir = self.default_dir_edit.text()
        self.default_dir_changed.emit(self.settings.default_dir)
        self.gate_time_range_changed.emit(self.netcdf_gate_spin1.value(),self.netcdf_gate_spin2.value())
        self.settings.startup_tab = self.default_tab_combo.currentText()
        
        # plot
        self.settings.default_plot_settings = self.default_settings_edit.text()
        self.settings.export_resolution = self.export_dpi_spin.value()
        params = {}
        for key in self.mpl_rcparam_spins.keys():
            params[key] = self.mpl_rcparam_spins[key].value()
        params['font.family'] = self.mpl_font_family_combo.currentText()
        self.settings.rcparams = params
        
        # netcdf
        try:
            if self.netcdf_export_pc_factor_check.isChecked():
                self.settings.netcdf_to_pychart_factor = self.netcdf_export_pc_factor_edit.text()
            else:
                self.settings.netcdf_to_pychart_factor = 0
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg).format(input=self.netcdf_export_pc_factor_edit.text()), self.parent().translator._(e.title))
            dlg.exec()
        try:
            if self.netcdf_export_z20_factor_check.isChecked():
                self.settings.netcdf_to_z20_factor = self.netcdf_export_z20_factor_edit.text()
            else:
                self.settings.netcdf_to_z20_factor = 0
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg).format(input=self.netcdf_export_z20_factor_edit.text()), self.parent().translator._(e.title))
            dlg.exec()
        
        self.settings.gate_time_range = (self.netcdf_gate_spin1.value(), self.netcdf_gate_spin2.value())
        
        try:
            self.settings.netcdf_wln_tolerance = self.netcdf_wln_tol_edit.text()
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg).format(input=self.netcdf_wln_tol_edit.text()), self.parent().translator._(e.title))
            dlg.exec()
        try:
            self.settings.netcdf_time_tolerance = self.netcdf_time_tol_edit.text()
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg).format(input=self.netcdf_time_tol_edit.text()), self.parent().translator._(e.title))
            dlg.exec()
        self.netcdf_axes_tolerances_changed.emit(self.settings.netcdf_wln_tolerance, self.settings.netcdf_time_tolerance)
        
        # hdf
        try:
            if self.hdf_export_pc_factor_check.isChecked():
                self.settings.hdf_to_pychart_factor = self.hdf_export_pc_factor_edit.text()
            else:
                self.settings.hdf_to_pychart_factor = 0
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg).format(
                input=self.hdf_export_pc_factor_edit.text()), self.parent().translator._(e.title))
            dlg.exec()
        try:
            if self.hdf_export_z20_factor_check.isChecked():
                self.settings.hdf_to_z20_factor = self.hdf_export_z20_factor_edit.text()
            else:
                self.settings.hdf_to_z20_factor = 0
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg).format(
                input=self.hdf_export_z20_factor_edit.text()), self.parent().translator._(e.title))
            dlg.exec()
        
        try:
            self.settings.hdf_wln_tolerance = self.hdf_wln_tol_edit.text()
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg).format(input=self.hdf_wln_tol_edit.text()), self.parent().translator._(e.title))
            dlg.exec()
        try:
            self.settings.hdf_time_tolerance = self.hdf_time_tol_edit.text()
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg).format(input=self.hdf_time_tol_edit.text()), self.parent().translator._(e.title))
            dlg.exec()
        self.hdf_axes_tolerances_changed.emit(self.settings.hdf_wln_tolerance, self.settings.hdf_time_tolerance)
        
        if self.hdf_pd_separated_radio.isChecked():
            self.settings.pd_plot_layout = 'separated'
        else:
            self.settings.pd_plot_layout = 'combined'
        