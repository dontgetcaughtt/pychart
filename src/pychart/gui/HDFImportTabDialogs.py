import numpy as np

from matplotlib.figure import Figure as mplFigure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as mplCanvas
from matplotlib.ticker  import MultipleLocator as mplMultipleLocator
from matplotlib.ticker  import FixedLocator    as mplFixedLocator
from matplotlib.ticker  import NullLocator     as mplNullLocator
from matplotlib.ticker  import FixedFormatter  as mplFixedFormatter
from matplotlib.ticker  import NullFormatter   as mplNullFormatter
from matplotlib.lines import Line2D as mplLine
import matplotlib.cm as mplColormaps

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QAction, QFileDialog
from PyQt5.QtWidgets import QVBoxLayout, QScrollArea

from ..core import pychart as pc
from ..core import error as pcerror
from ..gui.ErrorDialog import ErrorDialog
from ..gui import common as pcgui

# -----------------------------------------------------------------
#   PHOTODIODE VALUES DIALOG
# -----------------------------------------------------------------

class HDFPhotodiodeDialog (QDialog):
    """
    Dialog that displays photodiode values for pump and gate beams.
    
    Attributes
    ----------
    pump_data, gate_data : numpy.ndarray
        photodiode data arrays
    
    times : numpy.ndarray
        delay times
    
    nscans, ntimes : int
        number of scans, number of delay times
    
    i_zero : int
        index of time closed to zero in `times`
    
    plot : matplotlib.subplot
        subplot used for combined layout
    
    plot_pump, plot_gate : matplotlib.subplot
        subplots used for separated layout
    
    translator : i18n.Translator
        reference to the applications' translator instance
    
    working_dir : str
        working directory to start file dialogs at
    
    parent_tabname : str
        name of tab and default export title
    """
    
    def __init__ (self, parent, translatorhandle, working_dir, parent_tabname, *args, **kwargs):
        super(HDFPhotodiodeDialog, self).__init__(parent, *args, **kwargs)
        self.translator = translatorhandle
        self.working_dir = working_dir
        self.parent_tabname = parent_tabname
        
        self.pump_data, self.gate_data, self.times = None, None, None
        self.nscans, self.ntimes, self.i_zero = 0, 0, 0
        
        self.figure = mplFigure()
        self.canvas = mplCanvas(self.figure)
        
        self.export_action = QAction(QIcon('../images/icons/save.svg'), self.translator._('IMPORT_PD_PLOT_EXPORT_ACTION'))
        self.export_action.triggered.connect(self.export_plot)
        self.canvas.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.canvas.addAction(self.export_action)
        
        self.scroll_area = QScrollArea()
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll_area.setAlignment(Qt.AlignCenter)
        self.scroll_area.setWidget(self.canvas)
        self.scroll_area.setWidgetResizable(True)
        
        lay = QVBoxLayout()
        lay.addWidget(self.scroll_area)
        self.setLayout(lay)
        
        self.setWindowTitle(self.translator._('IMPORT_PD_PLOT_DLG_TITLE'))
    
    def plot_separated (self, scans):
        """ Plot both photodiodes independently and per scan. """
        # initialize
        self.plot_pump = self.figure.add_subplot(211)
        self.plot_gate = self.figure.add_subplot(212, sharex=self.plot_pump)
        self.figure.subplots_adjust(wspace=0, hspace=0)
        
        # plot data
        color_cycle = mplColormaps.viridis(np.linspace(0, 1, len(scans)))
        self.plot_pump.set_prop_cycle(color=color_cycle)
        self.plot_gate.set_prop_cycle(color=color_cycle)
        for scan in scans:
            self.plot_pump.plot(np.arange(self.ntimes), self.pump_data[scan, :, 0], 'o-', label=f'#{scan}')
            self.plot_gate.plot(np.arange(self.ntimes), self.gate_data[scan, :, 0], 'o-', label=f'#{scan}')
        self.plot_pump.set_xlim(0, self.ntimes - 1)
        
        # tick locations
        if self.ntimes <= 10:
            tickstep = 1
        elif self.ntimes <= 20:
            tickstep = 2
        elif self.ntimes <= 50:
            tickstep = 5
        elif self.ntimes <= 100:
            tickstep = 10
        else:
            tickstep = 20
        ticks_pos = np.arange(0, self.ntimes, tickstep)
        self.plot_pump.xaxis.set_major_locator(mplFixedLocator(ticks_pos))
        ticks_time = ['{time:.3f}'.format(time=self.times[x]) for x in ticks_pos]
        self.plot_pump.xaxis.set_major_formatter(mplFixedFormatter(ticks_time))
        self.plot_pump.xaxis.set_minor_locator(mplMultipleLocator(1))
        
        # pseudo-tick at time zero
        self.plot_pump.axvline(x=self.i_zero, color='r', ls='-', lw=2)
        self.plot_gate.axvline(x=self.i_zero, color='r', ls='-', lw=2)
        
        # labels and legend
        self.plot_pump.set_xlabel(self.translator._('IMPORT_PD_PLOT_TIME_LABEL'))
        self.plot_gate.set_xlabel(self.translator._('IMPORT_PD_PLOT_TIME_LABEL'))
        self.plot_pump.xaxis.set_label_position('top')
        self.plot_pump.set_ylabel(self.translator._('IMPORT_PD_PLOT_PUMP_VALUE_LABEL'))
        self.plot_gate.set_ylabel(self.translator._('IMPORT_PD_PLOT_GATE_VALUE_LABEL'))
        self.figure.align_ylabels()
        legend = self.plot_gate.legend(loc='upper left', bbox_to_anchor=(1, 2),
                              title=self.translator._('IMPORT_PD_PLOT_LEGEND_TITLE'))
        legend.set_draggable(True)
        
        # ticks and grid
        axes = [self.plot_pump, self.plot_gate]
        for ax in axes:
            ax.tick_params(which='both', direction='in', top=True, bottom=True, left=True, right=True)
            ax.grid(which='major', axis='both', color='gray', ls='--', lw=0.75)
            ax.grid(which='minor', axis='both', color='gray', ls=':', lw=0.5)
        self.plot_pump.tick_params(labelbottom=False, labeltop=True)
        self.plot_gate.tick_params(labelbottom=True, labeltop=False)
    
    def plot_combined (self):
        """ Plot both photodiodes together with all scans as subsequent timeline. """
        # initialize
        self.plot = self.figure.add_subplot()
        self.figure.set_tight_layout('True')
        
        # plot data
        n = self.nscans * self.ntimes
        x = np.arange(1, n+1, 1)
        self.plot.plot(x, self.pump_data.flatten(), 'o', color='cornflowerblue',
                       label=self.translator._('IMPORT_PD_PLOT_PUMP_LEGEND_LABEL'))
        self.plot.plot(x, self.gate_data.flatten(), 'o', color='crimson',
                       label=self.translator._('IMPORT_PD_PLOT_GATE_LEGEND_LABEL'))
        self.plot.set_xlim(1, n)
        
        # tick locations
        self.plot.set_xticks([self.ntimes * scan + self.i_zero for scan in range(self.nscans)])
        scans_1 = np.arange(1, self.nscans + 1, 1)
        self.plot.set_xticklabels(scans_1)
        
        # highlight scan ranges
        scans_2 = np.arange(1, self.nscans + 1, 2)
        for i, scan in enumerate(scans_2, 0):
            self.plot.axvspan(xmin=self.ntimes*scan, xmax=self.ntimes*(scan+1), color='0.9')
        
        # labels and legend
        self.plot.set_xlabel(self.translator._('IMPORT_PD_PLOT_SCAN_LABEL'))
        self.plot.set_ylabel(self.translator._('IMPORT_PD_PLOT_VALUE_LABEL'))
        legend = self.plot.legend(loc='upper left')
        legend.set_draggable(True)
        
        # ticks and grid
        self.plot.grid(which='major', axis='x', color='k', ls='--', lw=1)
        self.plot.grid(which='major', axis='y', color='gray', ls='--', lw=0.75)
        self.plot.grid(which='minor', axis='y', color='gray', ls=':', lw=0.5)
        self.plot.tick_params(which='minor', direction='in', top=True, bottom=False, left=True, right=True)
    
    def plot_values (self, pump_data, gate_data, times, layout='separated', **kwargs):
        """
        Plot photodiode values based on user settings.
        
        Plots photodiodes either in a single graph (`'combined'`) or in two graphs
        stacked above each other (`'separated'`). For the latter, one might provide
        a subset of scans as keyword argument `scans`, to which the plot will be
        restricted. In combined layout, all scans will be plotted as a flattened
        time trace.
        
        Parameters
        ----------
        pump_data, gate_data : numpy.ndarray
            photodiode datasets
        
        times : numpy.ndarray
            delay times
        
        layout : ['separated', 'combined']
            whether to plot photodiodes together or independent
        """
        if pump_data.shape == gate_data.shape:
            self.pump_data = pump_data
            self.gate_data = gate_data
            self.nscans, self.ntimes, _ = pump_data.shape
        else:
            return
        
        if len(times) == self.ntimes:
            self.times = times
            self.i_zero = pc.lookup_index(0.0, self.times)
        else:
            return
        
        self.figure.clear()
        tabsize = self.parent().sizeHint()
        if layout == 'separated':
            scans = kwargs.get('scans', np.arange(self.nscans+1))
            self.plot_separated(scans)
            self.canvas.setMinimumSize(int(tabsize.width() * 0.5), int(tabsize.height() * 0.5))
        elif layout == 'combined':
            self.plot_combined()
            self.canvas.setMinimumWidth(2 * self.nscans * self.ntimes)
        else:
            return
        self.scroll_area.setMinimumSize(int(tabsize.width() * 0.5+50), int(tabsize.height() * 0.5 + 50))
    
    def export_plot (self):
        """ Export photodiode plot to file at 300dpi. """
        fn = self.working_dir + '/' + self.parent_tabname + ' ' + self.translator._('IMPORT_PD_PLOT_EXPORT_TITLE')
        file_name, file_filter = QFileDialog.getSaveFileName(self, self.translator._('FILE_EXPORT'), fn, pc.FILE_TYPE_FILTER_PLOT)
        if file_name == '':
            return
        
        if file_filter == pc.FILE_TYPE_PNG:
            f = 'png'
        elif file_filter == pc.FILE_TYPE_PDF:
            f = 'pdf'
        elif file_filter == pc.FILE_TYPE_SVG:
            f = 'svg'
        elif file_filter == pc.FILE_TYPE_EPS:
            f = 'eps'
        elif file_filter == pc.FILE_TYPE_JPG:
            f = 'jpg'
        elif file_filter == pc.FILE_TYPE_TIF:
            f = 'tiff'
        else:
            e = pcerror.OutputError('ERROR_IMPORT_PD_PLOT_EXPORT_FILE_FORMAT_ERROR', output=file_filter)
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.output), self.translator._(e.title))
            dlg.exec()
            return
        
        try:
            self.figure.savefig(file_name, format=f, dpi=300, bbox_inches='tight')
        except:
            e = pcerror.OutputError('ERROR_IMPORT_PD_PLOT_EXPORT_ERROR')
            dlg = ErrorDialog(self, self.translator._(e.msg), self.translator._(e.title))
            dlg.exec()
    
# -----------------------------------------------------------------
#   SENSITIVITY PLOT DIALOG
# -----------------------------------------------------------------

class HDFSensitivityPlotDialog (QDialog):
    """
    Dialog that displays the real and ideal lamp spectra as well as the
    resulting sensitivity curve.
    
    Attributes
    ----------
    wlns : numpy.ndarray
        wavelength data
        
    lamp : numpy.ndarray
        (real) lamp spectrum
    
    bb : numpy.ndarray
        (ideal) black body curve
    
    plot_sens, plot_lamp : matplotlib.subplot
        subplots used for sensitivity curve and lamp spectrum with twin x axes
    
    translator : i18n.Translator
        reference to the applications' translator instance
    """
    def __init__(self, parent, translatorhandle, *args, **kwargs):
        super(HDFSensitivityPlotDialog, self).__init__(parent, *args, **kwargs)
        self.translator = translatorhandle
        
        self.figure = mplFigure()
        self.canvas = mplCanvas(self.figure)
        self.canvas.setMinimumHeight(250)
        self.plot_sens = self.figure.add_subplot()
        self.plot_lamp = self.plot_sens.twinx()
        self.figure.set_tight_layout('True')
        
        lay = QVBoxLayout()
        lay.addWidget(self.canvas)
        self.setLayout(lay)
        self.setWindowTitle(self.translator._('IMPORT_BB_PLOT_TITLE'))
        self.setModal(False)
        
    def update_lamp_data (self, lamp_data, lamp_wlns):
        """ Update lamp spectrum data and wavelength axis. """
        self.lamp = lamp_data
        self.wlns = lamp_wlns
    
    def show_preview (self, wln_min, wln_max, temp, offset):
        """ Plot lamp spectrum, real and ideal, as well as calculated sensitivity. """
        self.plot_sens.clear()
        self.plot_lamp.clear()
        
        wln_min_i = pc.lookup_index(wln_min, self.wlns)
        wln_max_i = pc.lookup_index(wln_max, self.wlns)
        if not wln_min_i < wln_max_i:
            raise pcerror.RangeError(self.translator._('ERROR_IMPORT_INVALID_LAMP_WLN_RANGE_ERROR'))
        
        self.bb = pc.black_body_curve(temp, self.wlns)
        bb_norm = self.bb / self.bb[wln_min_i:wln_max_i].max() * self.lamp[wln_min_i:wln_max_i].max()
        sens = (self.lamp - offset) / self.bb
        sens_norm = sens / sens.max()
        
        self.plot_lamp.plot(self.wlns[wln_min_i:wln_max_i], self.lamp[wln_min_i:wln_max_i] - offset,
                            '-', color=pc.mpl_color(pcgui.ORANGE))
        self.plot_lamp.plot(self.wlns[wln_min_i:wln_max_i], bb_norm[wln_min_i:wln_max_i], '-', color='m')
        self.plot_sens.plot(self.wlns[wln_min_i:wln_max_i], sens_norm[wln_min_i:wln_max_i], '-.', color='k')
        
        self.plot_sens.set_xlim(wln_min, wln_max)
        self.plot_sens.axhline(0, 0, 1, color='k')
        self.plot_lamp.yaxis.set_major_locator(mplNullLocator())
        
        self.plot_sens.set_xlabel(self.translator._('IMPORT_BB_WLN_LABEL'))
        legend_handles = [mplLine([], [], ls='-.', color='k'),
                          mplLine([], [], color=pc.mpl_color(pcgui.ORANGE)),
                          mplLine([], [], color='m')]
        legend_labels = [self.translator._('IMPORT_BB_SENS_LABEL'),
                         self.translator._('IMPORT_BB_LAMP_LABEL'),
                         self.translator._('IMPORT_BB_BB_LABEL')]
        self.plot_sens.legend(legend_handles, legend_labels, bbox_to_anchor=(0, 1.02, 1, .1), loc='lower left',
                         mode='expand', borderaxespad=0,  ncol=4, frameon=True)
        self.plot_sens.tick_params(which='both', direction='in', top=True, bottom=True, left=True, right=True)
        self.plot_sens.grid(which='major', axis='both', color='gray', ls='--', lw=0.75)
        self.plot_sens.grid(which='minor', axis='both', color='gray', ls=':',  lw=0.5)
        
        self.canvas.draw()
    