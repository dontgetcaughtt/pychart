import numpy as np

from matplotlib.figure import Figure as mplFigure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as mplCanvas

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QWhatsThis
from PyQt5.QtWidgets import QLabel, QLineEdit, QPushButton, QCheckBox, QComboBox, QRadioButton, QTableWidget, QTableWidgetItem, QAbstractScrollArea
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout, QFormLayout
from PyQt5.QtGui import QIcon

from . import common as pcgui
from .ErrorDialog import ErrorDialog
from ..core import pychart as pc
from ..core import error as pcerror

# -----------------------------------------------------------------
#   AXES LABEL DIALOG
# -----------------------------------------------------------------

class AxesLabelDialog(QDialog):
    """
    Input dialog for axes labels.
    
    Dialog receives user inputs for axes labels. It is non-modal and updated if
    visible while units are changed.
    
    Attributes are set via `set_current_labels` and can be get from getter
    functions for each attribute.
    
    Attributes
    ----------
    wln_contour : str
        label for contour's wavelength axis
    
    wln_transient : str
        label for spectra's wavelength axis
    
    time : str
        label for time axis
    
    time_dec : str
        whether or not the time axis should be decimal
    
    abs_traces : str
        label for trace's absorption axis
    
    colorbar : str
        label for colorbar
    
    abs_spectra : str
        label for spectra's absorption axis if in L-type layout
    
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    """
    
    def __init__(self, parent, *args, **kwargs):
        super(AxesLabelDialog, self).__init__(parent, *args, **kwargs)
        
        self.le1, self.le2, self.le3 = QLineEdit(), QLineEdit(), QLineEdit()
        self.le4, self.le5, self.le6 = QLineEdit(), QLineEdit(), QLineEdit()
        self.rb1 = QRadioButton(self.parent().translator._('PLOT_DECIMAL'))
        self.rb2 = QRadioButton(self.parent().translator._('PLOT_SCIENTIFIC'))
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.apply_btn = QPushButton(self.parent().translator._('APPLY'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        lay3 = QHBoxLayout()
        lay3.addWidget(self.rb1)
        lay3.addWidget(self.rb2)
        lay1 = QFormLayout()
        lay1.addRow(QLabel(self.parent().translator._('PLOT_LABELING_DLG_WLN_CONTOUR')), self.le1)
        lay1.addRow(QLabel(self.parent().translator._('PLOT_LABELING_DLG_WLN_SPECTRA')), self.le2)
        lay1.addRow(pcgui.qSeparatorLine())
        lay1.addRow(QLabel(self.parent().translator._('PLOT_LABELING_DLG_TIME')), self.le3)
        lay1.addRow(QLabel(), lay3)
        lay1.addRow(pcgui.qSeparatorLine())
        lay1.addRow(QLabel(self.parent().translator._('PLOT_LABELING_DLG_AMP_TRACES')), self.le4)
        lay1.addRow(QLabel(self.parent().translator._('PLOT_LABELING_DLG_COLORBAR')), self.le5)
        lay1.addRow(QLabel(self.parent().translator._('PLOT_LABELING_DLG_AMP_SPECTRA')), self.le6)
        lay4 = QHBoxLayout()
        lay4.addWidget(self.ok_btn)
        lay4.addWidget(self.apply_btn)
        lay4.addWidget(self.cancel_btn)
        lay2 = QGridLayout()
        lay2.addLayout(lay1, 0, 0, 7, 2)
        lay2.addWidget(QLabel(), 7, 0)
        lay2.addLayout(lay4, 8, 0, 1, 2)
        self.setLayout(lay2)
        self.cancel_btn.clicked.connect(self.reject)
        self.setWindowTitle(self.parent().translator._('PLOT_LABELING_DLG_HEADER'))
        self.setModal(False)
    
    def set_current_labels(self, wln_contour, wln_transient, time, time_dec, amp_traces, colorbar, amp_spectra):
        self.le1.setText(wln_contour)
        self.le2.setText(wln_transient)
        self.le3.setText(time)
        self.le4.setText(amp_traces)
        self.le5.setText(colorbar)
        self.le6.setText(amp_spectra)
        self.rb1.setChecked(time_dec)
        self.rb2.setChecked(not time_dec)
    
    def set_time_dec_notation(self, time_dec):
        self.rb1.setEnabled(time_dec == 'both' or time_dec == 'dec')
        self.rb2.setEnabled(time_dec == 'both' or time_dec == 'log')
        self.rb1.setChecked(time_dec == 'dec')
        self.rb2.setChecked(time_dec == 'log')
    
    def set_available_spectra_label(self, layout):
        self.le2.setEnabled(layout == 'PLOT_LAYOUT_LINEAR')
        self.le6.setEnabled(layout != 'PLOT_LAYOUT_LINEAR')
    
    def get_wln_contour_label(self):
        return self.le1.text()
    
    def get_wln_transient_label(self):
        return self.le2.text()
    
    def get_time_label(self):
        return self.le3.text()
    
    def get_time_dec_notation(self):
        return self.rb1.isChecked()
    
    def get_amp_traces_label(self):
        return self.le4.text()
    
    def get_colorbar_label(self):
        return self.le5.text()
    
    def get_amp_spectra_label(self):
        return self.le6.text()

# -----------------------------------------------------------------
#   COLORBAR DIALOG
# -----------------------------------------------------------------

class ColorbarDialog(QDialog):
    """
    Input dialog for colorbar settings.
    
    Dialog receives user inputs regarding the colorbar display. It is
    modal and provides a preview of the colormap.
    
    Attributes are set via `set_current_labels` and can be get from getter
    functions for each attribute.
    
    Attributes
    ----------
    colormap : str
        name of the colormap
    
    minimum : float
        minimum amplitude value
    
    maximum : float
        maximum amplitude value
    
    color1 : tuple
        color for high out-of-range values
    
    color2 : tuple
        color for low out-of-range values
    
    over : bool
        whether to display color1
    
    under : bool
        whether to display color2
    
    style : str
        indicates to display out-of-range colors triangular ('tri') or
        rectangular ('rect') or not at all ('none')
    
    color3 : tuple
        color for undefined values
    
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    """
    
    def __init__(self, parent, *args, **kwargs):
        super(ColorbarDialog, self).__init__(parent, *args, **kwargs)
        
        self.colorbar_combo = QComboBox()
        self.colorbar_flip_check = QCheckBox(self.parent().translator._('PLOT_COLORBAR_FLIP_SYMBOL'))
        self.le1, self.le2 = pcgui.qRLineEdit80(), pcgui.qRLineEdit80()
        self.le3, self.le4 = pcgui.qRLineEdit80(), pcgui.qRLineEdit60()
        self.le5, self.le7 = pcgui.qRLineEdit60(), pcgui.qRLineEdit60()
        self.le6 = pcgui.qRLineEdit80(self.parent().translator._('PLOT_COLORBAR_NAN'))
        self.le4.setEnabled(False)
        self.le5.setEnabled(False)
        self.le6.setEnabled(False)
        self.check1, self.check2 = QCheckBox('⯇'), QCheckBox('⯈')
        self.check1.clicked.connect(self.enable_widgets)
        self.check2.clicked.connect(self.enable_widgets)
        self.label = QLabel('')
        self.rb1 = QRadioButton(self.parent().translator._('PLOT_COLORBAR_EXTEND_TRIANGLE'))
        self.rb2 = QRadioButton(self.parent().translator._('PLOT_COLORBAR_EXTEND_RECT'))
        self.rb3 = QRadioButton(self.parent().translator._('PLOT_COLORBAR_EXTEND_FALSE'))
        self.info_btn = pcgui.qInfoButton()
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.apply_btn = QPushButton(self.parent().translator._('APPLY'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        self.preview_btn = QPushButton(self.parent().translator._('PLOT_COLORBAR_RENEW_PREVIEW'))
        self.preview_btn.clicked.connect(self.update_preview)
        self.ok_btn.setDefault(True)
        self.figure = mplFigure(figsize=(1, 0.5))
        self.canvas = mplCanvas(self.figure)
        lay4 = QHBoxLayout()
        lay4.addWidget(self.colorbar_combo)
        lay4.addWidget(self.colorbar_flip_check)
        lay3 = QHBoxLayout()
        lay3.addStretch()
        lay3.addWidget(self.rb1)
        lay3.addWidget(self.rb2)
        lay3.addWidget(self.rb3)
        lay3.addStretch()
        lay2 = QHBoxLayout()
        lay2.addStretch()
        lay2.addWidget(self.preview_btn)
        lay2.addStretch()
        lay2.addWidget(self.ok_btn)
        lay2.addWidget(self.apply_btn)
        lay2.addWidget(self.cancel_btn)
        lay2.addStretch()
        lay1 = QGridLayout()
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_COLORBAR_VALUES')), 0, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_COLORBAR_COLORS')), 1, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_COLORBAR_PRESENTATION')), 2, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_COLORBAR_PREVIEW')), 4, 0)
        lay1.addWidget(pcgui.qDummyLabel(15),                   1,  1)
        lay1.addWidget(self.le4,                                0,  4, 1, 2)
        lay1.addWidget(pcgui.qDummyLabel(30),                   0,  6)
        lay1.addWidget(self.le5,                                0,  7, 1, 2)
        lay1.addWidget(self.le6,                                0, 12, 1, 2)
        lay1.addWidget(self.check1,                             1,  2)
        lay1.addWidget(self.le1,                                1,  3, 1, 2)
        lay1.addLayout(lay4,                                    1,  5, 1, 3)
        lay1.addWidget(self.le2,                                1,  8, 1, 2)
        lay1.addWidget(self.check2,                             1, 10)
        lay1.addWidget(pcgui.qDummyLabel(15),                   1, 11)
        lay1.addWidget(self.le3,                                1, 12, 1, 2)
        lay1.addLayout(lay3,                                    2,  2, 1, 9)
        lay1.addWidget(self.canvas,                             4,  2, 1, 9)
        lay1.addWidget(self.info_btn,                           4, 13)
        lay1.addWidget(QLabel(),                                3,  0)
        lay1.addWidget(QLabel(),                                5,  0)
        lay1.addLayout(lay2,                                    6,  0, 1, 14)
        self.setLayout(lay1)
        self.cancel_btn.clicked.connect(self.reject)
        self.info_btn.clicked.connect(
            lambda: QWhatsThis.showText(self.info_btn.pos(), self.parent().translator._('PLOT_INFOTEXT_COLORBAR'), self.info_btn))
        self.setWindowTitle(self.parent().translator._('PLOT_COLORBAR_DLG_HEADER'))
        self.setModal(True)
    
    def set_current_labels(self, colormap, flip, minimum, maximum, step,
                           color1, color2, over, under, style, color3):
        self.colorbar_combo.clear()
        self.colorbar_combo.addItem(colormap)
        self.colorbar_flip_check.setCheckState(Qt.Checked if flip else Qt.Unchecked)
        self.le1.setText(color2)
        self.le2.setText(color1)
        self.le3.setText(color3)
        self.le4.setText(minimum)
        self.le5.setText(maximum)
        self.le7.setText(step)  # currently only dummy, might be used later
        self.check1.setChecked(under)
        self.check2.setChecked(over)
        self.enable_widgets()
        self.rb1.setChecked(style == 'tri')
        self.rb2.setChecked(style == 'rect')
        self.rb3.setChecked(style == 'none')
        self.update_preview()
    
    def enable_widgets(self):
        self.le1.setEnabled(self.check1.isChecked())
        self.le2.setEnabled(self.check2.isChecked())
    
    def get_flip_policy(self):
        return self.colorbar_flip_check.isChecked()
    
    def get_style(self):
        if self.rb1.isChecked():
            return 'tri'
        elif self.rb2.isChecked():
            return 'rect'
        else:
            return 'none'
    
    def get_over(self):
        return self.le2.text()
    
    def get_under(self):
        return self.le1.text()
    
    def get_NaN(self):
        return self.le3.text()
    
    def get_over_policy(self):
        return self.check2.isChecked()
    
    def get_under_policy(self):
        return self.check1.isChecked()
    
    def update_preview(self):
        self.figure.clear()
        
        over  = self.check2.isChecked()
        under = self.check1.isChecked()
        rect  = self.rb2.isChecked()
        none  = self.rb3.isChecked()
        if over ^ under:
            extend = 'max' if over else 'min'
        else:
            extend = 'both' if over and under else 'neither'
        frac = 0 if none else None
        
        try:
            tmp1 = self.le2.text().strip()
            tmp2 = self.le1.text().strip()
            overcolor  = pc.mpl_color(tmp1) if (tmp1 and over)  else False
            undercolor = pc.mpl_color(tmp2) if (tmp2 and under) else False
            nancolor   = pc.mpl_color(self.le3.text())
        
        except:
            e = pcerror.RangeError('ERROR_PLOT_INVALID_INPUT_ERROR')
            dlg = ErrorDialog(self.parent(), self.parent().translator._(e.msg), self.parent().translator._(e.title))
            dlg.exec()
            return
        
        cm = pc.generate_custom_cmap(self.colorbar_combo.currentText(), self.colorbar_flip_check.isChecked(),
                                     overcolor, undercolor, nancolor)
        
        levels = np.arange(float(self.le4.text()),
                    float(self.le5.text()) + float(self.le7.text()),
                    float(self.le7.text()))
        gradient = np.vstack((np.linspace(0, 1, 256), np.linspace(0, 1, 256)))
        self.figure.subplots_adjust(wspace=0, hspace=0)
        subplot1 = self.figure.add_subplot(211)
        subplot2 = self.figure.add_subplot(111)
        plot = subplot1.contourf(gradient[0, :], gradient[:, 0], gradient, cmap=cm, levels=levels, extend=extend)
        self.figure.colorbar(plot, cax=subplot2, orientation='horizontal', extendrect=rect, extendfrac=frac)
        subplot1.set_visible(False)
        subplot2.set_axis_off()
        self.canvas.draw()

# -----------------------------------------------------------------
#   BASELINE DIALOG
# -----------------------------------------------------------------

class BaselineDialog(QDialog):
    """
    Input dialog for baseline settings.
    
    Dialog receives user inputs regarding the baseline(s). It is
    non-modal and updated if visible while absorption unit is changed.
    
    Attributes are set via `set_current_labels` and can be get from getter
    functions for each attribute.
    
    Attributes
    ----------
    show_policy : bool
        whether or not the baseline should be drawn
    
    value : float
        absorption at which level the baseline is drawn given in `unit_abs`
    
    color : tuple
        color of the baseline
    
    ls : ['-.', '--', ':', '-']
        baseline's line style
    
    lw : float
        baseline's line width
    
    zorder : int
        baseline's zorder
    
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    """
    
    def __init__(self, parent, *args, **kwargs):
        super(BaselineDialog, self).__init__(parent, *args, **kwargs)
        
        self.check1 = QCheckBox(self.parent().translator._('PLOT_TRACES'))
        self.check2 = QCheckBox(self.parent().translator._('PLOT_SPECTRA'))
        self.check1.clicked.connect(self.enable_widgets)
        self.check2.clicked.connect(self.enable_widgets)
        self.value1_edit, self.value2_edit = pcgui.qRLineEdit(), pcgui.qRLineEdit()
        self.color1_edit, self.color2_edit = pcgui.qRLineEdit(), pcgui.qRLineEdit()
        self.ls1_combo, self.ls2_combo = pcgui.qlsComboBox(), pcgui.qlsComboBox()
        self.lw1_edit, self.lw2_edit = pcgui.qRLineEdit(), pcgui.qRLineEdit()
        self.zorder1_edit, self.zorder2_edit = pcgui.qRLineEdit(), pcgui.qRLineEdit()
        self.unit_label = QLabel()
        self.zorder_info_button = pcgui.qInfoButton()
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.apply_btn = QPushButton(self.parent().translator._('APPLY'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        self.ok_btn.setDefault(True)
        lay2 = QHBoxLayout()
        lay2.addStretch()
        lay2.addWidget(self.ok_btn)
        lay2.addWidget(self.apply_btn)
        lay2.addWidget(self.cancel_btn)
        lay2.addStretch()
        lay1 = QGridLayout()
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_BASELINE_DLG_SHOW')), 0, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_BASELINE_DLG_VALUE')), 2, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_BASELINE_DLG_COLOR')), 3, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_BASELINE_DLG_LS')), 4, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_BASELINE_DLG_LW')), 5, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_BASELINE_DLG_ZORDER')), 6, 0)
        lay1.addWidget(self.check1,                           0, 1)
        lay1.addWidget(self.check2,                           0, 2)
        lay1.addWidget(QLabel(),                              1, 0)
        lay1.addWidget(self.value1_edit,                      2, 1)
        lay1.addWidget(self.value2_edit,                      2, 2)
        lay1.addWidget(self.color1_edit,                      3, 1)
        lay1.addWidget(self.color2_edit,                      3, 2)
        lay1.addWidget(self.ls1_combo,                        4, 1)
        lay1.addWidget(self.ls2_combo,                        4, 2)
        lay1.addWidget(self.lw1_edit,                         5, 1)
        lay1.addWidget(self.lw2_edit,                         5, 2)
        lay1.addWidget(self.zorder1_edit,                     6, 1)
        lay1.addWidget(self.zorder2_edit,                     6, 2)
        lay1.addWidget(self.unit_label,                       2, 3)
        lay1.addWidget(self.zorder_info_button,               6, 3)
        lay1.addWidget(QLabel(),                              7, 0)
        lay1.addLayout(lay2,                                  8, 0, 1, 4)
        self.setLayout(lay1)
        self.cancel_btn.clicked.connect(self.reject)
        self.zorder_info_button.clicked.connect(
            lambda: QWhatsThis.showText(self.zorder_info_button.pos(), self.parent().translator._('PLOT_INFOTEXT_ZORDER'),
                                        self.zorder_info_button))
        self.setWindowTitle(self.parent().translator._('PLOT_BASELINE_DLG_HEADER'))
        self.setModal(False)
    
    def set_current_labels(self, show_policy1, show_policy2, value1, value2,
                           color1, color2, ls1, ls2, lw1, lw2, zorder1, zorder2, unit_abs):
        self.check1.setChecked(show_policy1)
        self.check2.setChecked(show_policy2)
        self.enable_widgets()
        self.value1_edit.setText(str(value1))
        self.value2_edit.setText(str(value2))
        self.color1_edit.setText(color1)
        self.color2_edit.setText(color2)
        self.ls1_combo.setCurrentText(ls1)
        self.ls2_combo.setCurrentText(ls2)
        self.lw1_edit.setText(str(lw1))
        self.lw2_edit.setText(str(lw2))
        self.zorder1_edit.setText(str(zorder1))
        self.zorder2_edit.setText(str(zorder2))
        self.unit_label.setText(unit_abs)
    
    def get_show_policy(self):
        return self.check1.isChecked(), self.check2.isChecked()
    
    def get_value(self):
        return float(self.value1_edit.text()), float(self.value2_edit.text())
    
    def get_color(self):
        return self.color1_edit.text(), self.color2_edit.text()
    
    def get_ls(self):
        return self.ls1_combo.currentText(), self.ls2_combo.currentText()
    
    def get_lw(self):
        return float(self.lw1_edit.text()), float(self.lw2_edit.text())
    
    def get_zorder(self):
        return int(self.zorder1_edit.text()), int(self.zorder2_edit.text())
    
    def enable_widgets(self):
        state = self.check1.isChecked()
        self.value1_edit.setEnabled(state)
        self.color1_edit.setEnabled(state)
        self.ls1_combo.setEnabled(state)
        self.lw1_edit.setEnabled(state)
        self.zorder1_edit.setEnabled(state)
        
        state = self.check2.isChecked()
        self.value2_edit.setEnabled(state)
        self.color2_edit.setEnabled(state)
        self.ls2_combo.setEnabled(state)
        self.lw2_edit.setEnabled(state)
        self.zorder2_edit.setEnabled(state)
    
    def activate_widgets(self, ltype):
        self.check2.setEnabled(ltype)
        self.value2_edit.setEnabled(ltype)
        self.color2_edit.setEnabled(ltype)
        self.ls2_combo.setEnabled(ltype)
        self.lw2_edit.setEnabled(ltype)
        self.zorder2_edit.setEnabled(ltype)

# -----------------------------------------------------------------
#   SCALEBAR DIALOG
# -----------------------------------------------------------------

class ScaleBarDialog(QDialog):
    """
    Input dialog for scalebar settings.

    Dialog receives user inputs regarding the scalebar appearance. It is
    non-modal and updated if visible while units are changed.

    Attributes are set via `set_current_labels` and can be get from getter
    functions for each attribute.

    Attributes
    ----------
    show_policy : bool
        whether or not the scalebar should be shown

    value : float
        height of the scalebar given in `unit_abs`

    label : str
        label of the scalebar

    color : tuple
        color of the scalebar

    pos_wln : float
        position of the scalebar on the wavelength axis given in `unit_wln`

    pos_y : float
        position of the scalebar y direction
    
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    
    # TODO: Note that this dialog uses the 'a.u.' i18n identifier, which might be changed to `"ARBITRARY"` later.
    """
    
    def __init__(self, parent, *args, **kwargs):
        super(ScaleBarDialog, self).__init__(parent, *args, **kwargs)
        
        self.rb1 = QRadioButton(self.parent().translator._('PLOT_SCALEBAR_DLG_SHOW'))
        self.rb2 = QRadioButton(self.parent().translator._('PLOT_SCALEBAR_DLG_HIDE'))
        self.rb1.clicked.connect(self.enable_widgets)
        self.rb2.clicked.connect(self.enable_widgets)
        self.le1, self.le2 = pcgui.qRLineEdit(), pcgui.qRLineEdit()
        self.le3, self.le4 = pcgui.qRLineEdit(), pcgui.qRLineEdit()
        self.le5 = pcgui.qRLineEdit()
        self.l1, self.l2, self.l3 = QLabel(), QLabel(), QLabel(self.parent().translator._('a.u.'))
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.apply_btn = QPushButton(self.parent().translator._('APPLY'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        lay2 = QHBoxLayout()
        lay2.addStretch()
        lay2.addWidget(self.ok_btn)
        lay2.addWidget(self.apply_btn)
        lay2.addWidget(self.cancel_btn)
        lay2.addStretch()
        lay3 = QHBoxLayout()
        lay3.addStretch()
        lay3.addWidget(QLabel(self.parent().translator._('PLOT_SCALEBAR')))
        lay3.addWidget(self.rb1)
        lay3.addWidget(self.rb2)
        lay3.addStretch()
        lay1 = QGridLayout()
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_SCALEBAR_DLG_VALUE')), 2, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_SCALEBAR_DLG_LABEL')), 3, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_SCALEBAR_DLG_COLOR')), 4, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_SCALEBAR_DLG_POSWLN')), 5, 0)
        lay1.addWidget(QLabel(self.parent().translator._('PLOT_SCALEBAR_DLG_POSY')), 6, 0)
        lay1.addLayout(lay3,                                  0, 0, 1, 3)
        lay1.addWidget(QLabel(),                              1, 0)
        lay1.addWidget(self.le1,                              2, 1)
        lay1.addWidget(self.le2,                              3, 1)
        lay1.addWidget(self.le5,                              4, 1)
        lay1.addWidget(self.le3,                              5, 1)
        lay1.addWidget(self.le4,                              6, 1)
        lay1.addWidget(self.l1,                               2, 2)
        lay1.addWidget(self.l2,                               5, 2)
        lay1.addWidget(self.l3,                               6, 2)
        lay1.addWidget(QLabel(),                              7, 0)
        lay1.addLayout(lay2,                                  8, 0, 1, 3)
        self.setLayout(lay1)
        self.cancel_btn.clicked.connect(self.reject)
        self.setWindowTitle(self.parent().translator._('PLOT_SCALEBAR_DLG_HEADER'))
        self.setModal(False)
    
    def set_current_labels(self, show_policy, value, label, color, pos_wln, pos_y, unit_abs, unit_wln):
        self.rb1.setChecked(show_policy)
        self.rb2.setChecked(not show_policy)
        self.enable_widgets()
        self.le1.setText(str(value))
        self.le2.setText(label)
        self.le5.setText(color)
        self.le3.setText(str(pos_wln))
        self.le4.setText(str(pos_y))
        self.l1.setText(unit_abs)
        self.l2.setText(unit_wln)
    
    def get_show_policy(self):
        return self.rb1.isChecked()
    
    def get_value(self):
        return float(self.le1.text())
    
    def get_color(self):
        return self.le5.text()
    
    def get_label(self):
        return self.le2.text()
    
    def get_pos_wln(self):
        return float(self.le3.text())
    
    def get_pos_y(self):
        return float(self.le4.text())
    
    def enable_widgets(self):
        state = self.rb1.isChecked()
        self.le1.setEnabled(state)
        self.le2.setEnabled(state)
        self.le3.setEnabled(state)
        self.le4.setEnabled(state)
        self.le5.setEnabled(state)

# -----------------------------------------------------------------
#   ARTISTS DIALOG
# -----------------------------------------------------------------

""" List of indices for QTableWidgetItems in artists table """
ARTISTS_COMBO_WIDGET_INDICES = [0, 3, 7]
ARTISTS_FLOAT_WIDGET_INDICES = [1, 2, 4, 5, 8, 10]
ARTISTS_RECT_WIDGET_INDICES  = [9]

class ArtistsDialog (QDialog):
    """
    Input dialog for additional artists.
    
    Dialog is based on a table, allowing the user to define an unlimited number
    of additional artists to draw. For each artist the coordinates of start and
    end positions have to be defined as well as the axis, which these values
    belong to. Color, line width, line style and zorder can be set independently.
    
    Artists are set via `set_current_artists` and can be get from `get_artists`.
    
    Attributes
    ----------
    ax1, ax2 : str
        axis of start / end point
    
    x1, x2 : float
        x-value (wavelength or amplitude or ...) of start / end point
    
    y1, y2 : float
        y-value (time or amplitude or ...) of start / end point
    
    color : tuple
        color of the artist
    
    ls : ['-.', '--', ':', '-']
        artists' line style
    
    lw : float
        artists' line width
    
    zorder : int
        artists' zorder
    
    parent : QWidget
        reference to parent widget which features a `translator` attribute
        to call `_()` for translation
    """
    
    def __init__ (self, parent, *args, **kwargs):
        super(ArtistsDialog, self).__init__(parent, *args, **kwargs)
        
        self.ok_btn = QPushButton(self.parent().translator._('OK'))
        self.apply_btn = QPushButton(self.parent().translator._('APPLY'))
        self.cancel_btn = QPushButton(self.parent().translator._('CANCEL'))
        self.ok_btn.setDefault(True)
        lay2 = QHBoxLayout()
        lay2.addStretch()
        lay2.addWidget(self.ok_btn)
        lay2.addWidget(self.apply_btn)
        lay2.addWidget(self.cancel_btn)
        lay2.addStretch()
        
        self.table = QTableWidget(0, 11)
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_AX1')))
        self.table.setHorizontalHeaderItem(1, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_X1')))
        self.table.setHorizontalHeaderItem(2, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_Y1')))
        self.table.setHorizontalHeaderItem(3, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_AX2')))
        self.table.setHorizontalHeaderItem(4, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_X2')))
        self.table.setHorizontalHeaderItem(5, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_Y2')))
        self.table.setHorizontalHeaderItem(6, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_COLOR')))
        self.table.setHorizontalHeaderItem(7, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_LS')))
        self.table.setHorizontalHeaderItem(8, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_LW')))
        self.table.setHorizontalHeaderItem(9, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_FILL')))
        self.table.setHorizontalHeaderItem(10, QTableWidgetItem(self.parent().translator._('PLOT_ARTISTS_DLG_ZORDER')))
        self.table.verticalHeader().setVisible(True)
        
        self.table_add_line_button = pcgui.qPushButton30()
        self.table_add_rect_button = pcgui.qPushButton30()
        self.table_remove_button = pcgui.qPushButton30()
        self.table_up_button = pcgui.qPushButton30()
        self.table_down_button = pcgui.qPushButton30()
        self.table_info_button = pcgui.qInfoButton()
        self.table_add_line_button.setIcon(QIcon('../images/icons/add.svg'))
        self.table_add_rect_button.setIcon(QIcon('../images/icons/add-ext.svg'))
        self.table_remove_button.setIcon(QIcon('../images/icons/remove.svg'))
        self.table_up_button.setIcon(QIcon('../images/icons/up.svg'))
        self.table_down_button.setIcon(QIcon('../images/icons/down.svg'))
        self.table_add_line_button.setToolTip(self.parent().translator._('PLOT_ARTISTS_DLG_ADD_LINE'))
        self.table_add_rect_button.setToolTip(self.parent().translator._('PLOT_ARTISTS_DLG_ADD_RECT'))
        self.table_remove_button.setToolTip(self.parent().translator._('PLOT_ARTISTS_DLG_DEL'))
        self.table_up_button.setToolTip(self.parent().translator._('PLOT_ARTISTS_DLG_MOVE_UP'))
        self.table_down_button.setToolTip(self.parent().translator._('PLOT_ARTISTS_DLG_MOVE_DOWN'))
        
        self.table_add_line_button.clicked.connect(self.add_line_row)
        self.table_add_rect_button.clicked.connect(self.add_rect_row)
        self.table_remove_button.clicked.connect(self.remove_row)
        self.table_down_button.clicked.connect(self.move_row_down)
        self.table_up_button.clicked.connect(self.move_row_up)
        self.table_info_button.clicked.connect(
            lambda: QWhatsThis.showText(self.table_info_button.pos(),
                                        self.parent().translator._('PLOT_INFOTEXT_ARTISTS'), self.table_info_button))
        
        lay3 = QVBoxLayout()
        lay3.addWidget(self.table_add_line_button)
        lay3.addWidget(self.table_add_rect_button)
        lay3.addWidget(self.table_remove_button)
        lay3.addWidget(self.table_up_button)
        lay3.addWidget(self.table_down_button)
        lay3.addWidget(self.table_info_button)
        
        lay1 = QGridLayout()
        lay1.addWidget(self.table, 0, 0)
        lay1.addLayout(lay3,       0, 1)
        lay1.addLayout(lay2,       1, 0)
        self.setLayout(lay1)
        self.cancel_btn.clicked.connect(self.reject)
        self.setWindowTitle(self.parent().translator._('PLOT_ARTISTS_DLG_HEADER'))
        self.setModal(False)
    
    def remove_row (self):
        self.table.removeRow(self.table.currentRow())
    
    def add_line_row (self):
        row = self.table.currentRow() + 1
        self.table.insertRow(row)
        for col in range(self.table.columnCount()):
            if col in ARTISTS_COMBO_WIDGET_INDICES:
                combo = pcgui.qSubplotComboBox() if not col == 7 else pcgui.qlsComboBox()
                self.table.setCellWidget(row, col, combo)
            else:
                self.table.setItem(row, col, QTableWidgetItem())
        item = self.table.item(row, 9)
        item.setFlags(item.flags() & (not Qt.ItemIsEditable))
    
    def add_rect_row (self):
        row = self.table.currentRow() + 1
        self.table.insertRow(row)
        for col in range(self.table.columnCount()):
            if col in ARTISTS_COMBO_WIDGET_INDICES:
                combo = pcgui.qSubplotComboBox() if not col == 7 else pcgui.qlsComboBox()
                self.table.setCellWidget(row, col, combo)
            else:
                self.table.setItem(row, col, pcgui.qYellowTableItem())
    
    def move_row (self, inc):
        row = self.table.currentRow()
        ncols = self.table.columnCount()
        # store current row
        temp = []
        for col in range(ncols):
            if col in ARTISTS_COMBO_WIDGET_INDICES:
                temp.append(self.table.cellWidget(row, col).currentText())
            else:
                temp.append(self.table.takeItem(row, col))
        # override current row and restore in new row
        for col in range(ncols):
            if col in ARTISTS_COMBO_WIDGET_INDICES:
                self.table.cellWidget(row, col).setCurrentText(self.table.cellWidget(row + inc, col).currentText())
                self.table.cellWidget(row + inc, col).setCurrentText(temp[col])
            else:
                self.table.setItem(row, col, self.table.takeItem(row + inc, col))
                self.table.setItem(row + inc, col, temp[col])
        self.table.setCurrentCell(row + inc, self.table.currentColumn())
    
    def move_row_down (self):
        if self.table.currentRow() != self.table.rowCount() - 1:
            self.move_row(1)
    
    def move_row_up (self):
        if self.table.currentRow() != 0:
            self.move_row(-1)
    
    def get_artists (self):
        artists = []
        ncols = self.table.columnCount()
        for row in range(self.table.rowCount()):
            artist = []
            for col in range(ncols):
                if col in ARTISTS_COMBO_WIDGET_INDICES:
                    artist.append(self.table.cellWidget(row, col).currentText())
                elif col in ARTISTS_RECT_WIDGET_INDICES:
                    if self.table.item(row, col).flags() & Qt.ItemIsEditable:
                        artist.append(self.table.item(row, col).text())
                    else:
                        artist.append(None)
                else:
                    artist.append(self.table.item(row, col).text())
            artists.append(artist)
        return artists
    
    def set_current_artists (self, artists):
        self.table.setRowCount(0)
        self.table.setRowCount(len(artists))
        ncols = self.table.columnCount()
        for row in range(len(artists)):
            if artists[row][9] is None:
                twi = lambda t: QTableWidgetItem(t)
            else:
                twi = lambda t: pcgui.qYellowTableItem(t)
            for col in range(ncols):
                if col in ARTISTS_COMBO_WIDGET_INDICES:
                    combo = pcgui.qSubplotComboBox() if not col == 7 else pcgui.qlsComboBox()
                    combo.setCurrentText(artists[row][col])
                    self.table.setCellWidget(row, col, combo)
                elif col in ARTISTS_RECT_WIDGET_INDICES:
                    item = twi(str(artists[row][col]))
                    if artists[row][9] is None:
                        item.setText('')
                        item.setFlags(item.flags() & (not Qt.ItemIsEditable))
                    self.table.setItem(row, col, item)
                else:
                    self.table.setItem(row, col, twi(str(artists[row][col])))
        self.table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.table.resizeColumnsToContents()
    