from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QFileDialog
from PyQt5.QtWidgets import QLabel, QPushButton, QLineEdit, QTreeWidget, QTreeWidgetItem, QHeaderView
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout
from PyQt5.QtGui import QIcon, QBrush, QColor

from . import common as pcgui
from .ErrorDialog import ErrorDialog
from ..core import pychart as pc
from ..core import error as pcerror
from ..core import netcdfimport as pcnetcdf

class NetCDFMergeFilesDialog (QDialog):
    """
    Dialog providing user interface for merging multiple NetCDF raw data
    files into one.
    
    Widget proivdes input fields to choose input and output data files.
    Input files are displayed in a tree widget along with their meta data.
    Meta data (axis shapes) are check for consistency. Scans of input files
    might be selected individually for merging.
    
    Attributes
    ----------
    working_dir : str
        working directory to open file dialogs at
    
    translator : i18n.Translator
        reference to the applications translator instance
        
    wln_tol : float
        absolute tolerance of wavelength axes values
    
    time_tol : float
        relative tolerance of time axes values
    
    master : dict
        meta data of first input file to compare other files against
    
    array_type :
    """
    
    def __init__(self, translatorhandle, working_dir, wln_tol, time_tol, *args, **kwargs):
        super(NetCDFMergeFilesDialog, self).__init__(*args, **kwargs)
        
        self.translator = translatorhandle
        self.working_dir = working_dir
        self.wln_tol = wln_tol
        self.time_tol = time_tol
        
        self.master = {}
        
        self.setup_gui()
        
        self.input_file_dlg_button.clicked.connect(self.update_input_raw_data_file)
        self.output_file_dlg_button.clicked.connect(self.update_export_raw_data_file)
        self.add_button.clicked.connect(self.load_raw_data_file)
        self.export_btn.clicked.connect(self.merge_raw_data)
        self.cancel_btn.clicked.connect(self.reject)
    
    def setup_gui (self):
        """ Setup graphical user interface. """
        
        # input
        self.input_line_edit = QLineEdit()
        self.input_file_dlg_button = pcgui.qPushButton30('...')
        self.add_button = pcgui.qPushButton30()
        self.add_button.setIcon(QIcon('../images/icons/add.svg'))
        self.add_button.setToolTip(self.translator._('IMPORT_MERGE_ADD_FILE'))
        lay_input = QHBoxLayout()
        lay_input.addWidget(QLabel(self.translator._('IMPORT_MERGE_INPUT_FILE')))
        lay_input.addWidget(self.input_line_edit)
        lay_input.addWidget(self.input_file_dlg_button)
        lay_input.addWidget(self.add_button)
        
        # file list
        self.file_tree = QTreeWidget()
        self.file_tree.setColumnCount(5)
        headers = ['IMPORT_MERGE_FILE','IMPORT_MERGE_WLNS','IMPORT_MERGE_TIMES','IMPORT_MERGE_SHOTS','IMPORT_MERGE_MODE']
        self.file_tree.setHeaderLabels([self.translator._(header) for header in headers])
        headeritem = self.file_tree.header()
        headeritem.setStretchLastSection(False)
        headeritem.setDefaultAlignment(Qt.AlignCenter)
        headeritem.setSectionResizeMode(QHeaderView.ResizeToContents)
        self.file_tree.headerItem().setTextAlignment(0, Qt.AlignLeft)
        self.file_tree.setMinimumWidth(800)
        self.file_tree.setMinimumHeight(400)
        
        # output
        self.output_line_edit = QLineEdit()
        self.output_file_dlg_button = pcgui.qPushButton30('...')
        lay_output = QHBoxLayout()
        lay_output.addWidget(QLabel(self.translator._('IMPORT_MERGE_OUTPUT_FILE')))
        lay_output.addWidget(self.output_line_edit)
        lay_output.addWidget(self.output_file_dlg_button)
        
        # buttons
        self.export_btn = QPushButton(self.translator._('IMPORT_MERGE_EXPORT'))
        self.cancel_btn = QPushButton(self.translator._('IMPORT_MERGE_CLOSE'))
        lay1 = QHBoxLayout()
        lay1.addStretch()
        lay1.addWidget(self.export_btn)
        lay1.addWidget(self.cancel_btn)
        lay1.addStretch()
        
        # main layout
        lay = QVBoxLayout()
        lay.addLayout(lay_input)
        lay.addWidget(self.file_tree)
        lay.addLayout(lay_output)
        lay.addLayout(lay1)
        
        self.setLayout(lay)
        self.setWindowTitle(self.translator._('IMPORT_MERGE_DLG_TITLE'))
        self.setModal(True)
    
    def add_file (self, filename, fileinfo):
        """ Add file meta data to tree view. """
        if not self.master:
            self.master['wlns'] = fileinfo['wlns']
            self.master['times'] = fileinfo['times']
            self.master['nshots'] = fileinfo['nshots']
            self.master['nchopper'] = fileinfo['nchopper']
        
        checks = {
            'wlns':    pc.consistent_axes(fileinfo['wlns'], self.master['wlns'], atol=self.wln_tol),
            'times':   pc.consistent_axes(fileinfo['times'], self.master['times'], rtol=self.time_tol),
            'shots':   fileinfo['nshots'] == self.master['nshots'],
            'chopper': fileinfo['nchopper'] == self.master['nchopper']
        }
        
        red = QBrush(QColor.fromRgb(*pcgui.RED))
        green = QBrush(QColor.fromRgb(*pcgui.GREEN))
        item = QTreeWidgetItem([
            filename, str(fileinfo['nwlns']), str(fileinfo['ntimes']), str(fileinfo['nshots']), str(fileinfo['nchopper'])
        ])
        item.setForeground(1, green if checks['wlns'] else red)
        item.setForeground(2, green if checks['times'] else red)
        item.setForeground(3, green if checks['shots'] else red)
        item.setForeground(4, green if checks['chopper'] else red)
        for col in range(1,5):
            item.setTextAlignment(col, Qt.AlignCenter ^ Qt.AlignVCenter)
        
        checked = Qt.Checked if all(checks.values()) else Qt.Unchecked
        for scan in range(fileinfo['nscans']):
            scanitem = QTreeWidgetItem()
            scanitem.setText(0, self.translator._('IMPORT_MERGE_SCAN_LABEL').format(scan=scan+1))
            scanitem.setIcon(0, QIcon('../images/icons/single-spectrum.svg'))
            scanitem.setCheckState(0, checked)
            item.addChild(scanitem)
        
        self.file_tree.addTopLevelItem(item)
    
    def update_input_raw_data_file (self):
        """ Get raw data file path from open dialog for input. """
        file_name, file_filter = QFileDialog.getOpenFileName(self,self.translator._('FILE_OPEN'),self.working_dir,pc.FILE_TYPE_NCDF)
        if file_name != '':
            self.input_line_edit.setText(file_name)
    
    def load_raw_data_file (self):
        """ Open raw data file and extract meta data for display. """
        fileinfo = pcnetcdf.retrieve_netcdf_information(self.input_line_edit.text())
        if fileinfo:
            self.add_file(self.input_line_edit.text(), fileinfo)
        else:
            e = pcerror.InputError('ERROR_IMPORT_IMPORT_DATA_ERROR', input=self.input_line_edit.text())
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
    
    def update_export_raw_data_file (self):
        """ Get raw data file path from save dialog for export. """
        file_name, file_filter = QFileDialog.getSaveFileName(self,self.translator._('FILE_EXPORT'),self.working_dir,pc.FILE_TYPE_NCDF)
        if file_name != '':
            self.output_line_edit.setText(file_name)
    
    def merge_raw_data (self):
        """ Generate list of user-selected scans, merge and export them into new file. """
        if self.output_line_edit.text() == '':
            file_name, _ = QFileDialog.getSaveFileName(self,self.translator._('FILE_EXPORT'),self.working_dir,pc.FILE_TYPE_NCDF)
            if file_name == '':
                return
            else:
                self.output_line_edit.setText(file_name)
        else:
            file_name = self.output_line_edit.text()
        
        filelist = []
        for i_file in range(self.file_tree.topLevelItemCount()):
            file = self.file_tree.topLevelItem(i_file).text(0)
            scans = []
            for i_scan in range(self.file_tree.topLevelItem(i_file).childCount()):
                if self.file_tree.topLevelItem(i_file).child(i_scan).checkState(0) == Qt.Checked:
                    scans.append(i_scan)
            if scans:
                filelist.append((file,scans))
        
        try:
            ds = pcnetcdf.merge_netcdf_files(filelist, wln_tol=self.wln_tol, time_tol=self.time_tol)
        except pcerror.RangeError as e:
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input,ranges=e.ranges), self.translator._(e.title))
            dlg.exec()
            return
        except:
            e = pcerror.InputError('ERROR_IMPORT_MERGE_FAILED_MERGE_ERROR')
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
            return
        
        try:
            ds.to_netcdf(file_name)
        except:
            e = pcerror.InputError('ERROR_IMPORT_MERGE_FAILED_EXPORT_ERROR', input=file_name)
            dlg = ErrorDialog(self, self.translator._(e.msg).format(input=e.input), self.translator._(e.title))
            dlg.exec()
        