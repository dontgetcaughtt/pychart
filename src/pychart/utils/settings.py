import json

import matplotlib as mpl

from PyQt5.QtCore import QDir

from . import i18n
from ..core import error as pcerror
from ..version import PYCHART_VERSION_NAME

class PyChartSettings (object):
    """
    A class which stores all application wide settings. Settings are
    stored to and read from file and remain active after program restart.
    
    Attributes
    ----------
    language : i18n.LanguageCode
        i18n language code specifying the current localisation
    
    startup_tab : str
        tab which is loaded at program start and after closing of all tabs
    
    default_dir : str
        default directory for all file dialogs
    
    default_plot_settings : str
        file path to a JSON file to load its plot settings from for new PlotTabs
    
    export_resolution : int
        resolution used when exporting plots as image in dpi
    
    rcparams : dict
        dictionary controlling matplotlib layout settings
    
    pychart_factor : dict of float or None
        factor which is applied prior to exporting data as a pychart file
        dict contains individual factors for NetCDF and HDF exports
        if None, data is exported as-is (treated as factor 1)
    
    z20_factor : dict of float or None
        factor which is applied prior to exporting data as a Z20 file
        dict contains individual factors for NetCDF and HDF exports
        if None, data is exported as-is (treated as factor 1)
    
    wln_tolerance : dict of float
        absolute tolerance of wavelength axes values to be considered identical
        dict contains individual values for NetCDF and HDF imports
    
    time_tolerance : dict of float
        relative tolerance of time axes values to be considered identical
        dict contains individual values for NetCDF and HDF imports
    
    gate_time_range : tuple of float
        start and end values of the time range considered in the determination
        of time zeros for gate correction
    
    pd_plot_layout : str
        layout used for plotting HDF photodiode values
    
    translator : i18n.Translator
        reference to the applications translator instance
    """
    
    def __init__(self, translator):
        self._language = None
        self._startup_tab = ''
        self._default_dir = ''
        self._default_plot_settings = ''
        self._export_resolution = 0
        self._rcparams = {}
        self._pychart_factor = {}
        self._z20_factor = {}
        self._wln_tolerance = {}
        self._time_tolerance = {}
        self._gate_time_range = ()
        self._pd_plot_layout = ''
        
        self.translator = translator
        
        self.default_settings()
    
    def default_settings(self):
        self.language = i18n.LanguageCode.de
        self.startup_tab = 'PlotTab'
        self.default_dir = QDir.currentPath()
        
        self.default_plot_settings = '../settings/default_fsTA.json'
        self.export_resolution = 300
        self._rcparams = {'figure.titlesize': 15, 'xtick.labelsize': 11, 'ytick.labelsize': 11, 'xtick.major.size': 5,
            'ytick.major.size': 5, 'xtick.minor.size': 3, 'ytick.minor.size': 3, 'xtick.major.width': 1,
            'ytick.major.width': 1, 'axes.labelsize': 12, 'axes.linewidth': 1.25, 'lines.linewidth': 1.25,
            'font.family': 'sans-serif'}
        
        self._pychart_factor = {'NetCDF': 1000, 'HDF': None}
        self._z20_factor = {'NetCDF': None, 'HDF': None}
        self._wln_tolerance = {'NetCDF': 0.01, 'HDF': 0.01}
        self._time_tolerance = {'NetCDF': 0.005, 'HDF': 0.005}
        self.gate_time_range = (-1.2, 1.2)
        self._pd_plot_layout = 'separated'
    
    # -----------------------------------------------------------------
    #   PROPERTIES
    # -----------------------------------------------------------------
    
    @property
    def language (self):
        if self._language is i18n.LanguageCode.de:
            return self.translator._('de')
        elif self._language is i18n.LanguageCode.en:
            return self.translator._('en')
        else:
            return self.translator._('de')
    
    @language.setter
    def language (self, lang):
        if (lang == self.translator._('de')) or (lang == 'de') or (lang is i18n.LanguageCode.de):
            self._language = i18n.LanguageCode.de
        elif (lang == self.translator._('en')) or (lang == 'en') or (lang is i18n.LanguageCode.en):
            self._language = i18n.LanguageCode.en
        else:
            self._language = i18n.LanguageCode.de
    
    def language_i18n_identifier (self):
        return self._language
    
    @property
    def startup_tab (self):
        if self._startup_tab == 'PlotTab':
            return self.translator._('SETTINGS_TAB_PLOT')
        elif self._startup_tab == 'TD-Fit':
            return self.translator._('SETTINGS_TAB_TDFIT')
        elif self._startup_tab == 'NetCDF':
            return self.translator._('SETTINGS_TAB_NETCDF')
        elif self._startup_tab == 'HDF':
            return self.translator._('SETTINGS_TAB_HDF')
        elif self._startup_tab == 'ChooseNew':
            return self.translator._('SETTINGS_TAB_CHOOSE')
        else:
            return self.translator._('SETTINGS_TAB_PLOT')
    
    @startup_tab.setter
    def startup_tab (self, tab):
        if (tab == self.translator._('SETTINGS_TAB_PLOT')) or (tab == 'PlotTab'):
            self._startup_tab = 'PlotTab'
        elif (tab == self.translator._('SETTINGS_TAB_TDFIT')) or (tab == 'TD-Fit'):
            self._startup_tab = 'TD-Fit'
        elif (tab == self.translator._('SETTINGS_TAB_NETCDF')) or (tab == 'NetCDF'):
            self._startup_tab = 'NetCDF'
        elif (tab == self.translator._('SETTINGS_TAB_HDF')) or (tab == 'HDF'):
            self._startup_tab = 'HDF'
        elif (tab == self.translator._('SETTINGS_TAB_CHOOSE')) or (tab == 'ChooseNew'):
            self._startup_tab = 'ChooseNew'
        else:
            self._startup_tab = 'PlotTab'
    
    @property
    def default_dir (self):
        return self._default_dir
    
    @default_dir.setter
    def default_dir (self, directory):
        self._default_dir = directory
    
    @property
    def default_plot_settings (self):
        return self._default_plot_settings
    
    @default_plot_settings.setter
    def default_plot_settings (self, path):
        self._default_plot_settings = path
    
    @property
    def export_resolution (self):
        return self._export_resolution
    
    @export_resolution.setter
    def export_resolution (self, dpi):
        try:
            self._export_resolution = int(dpi)
        except ValueError:
            raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_DPI_ERROR')
    
    @property
    def rcparams (self):
        return self._rcparams
    
    @rcparams.setter
    def rcparams (self, params):
        for key in params.keys():
            try:
                self._rcparams[key] = params[key]
                mpl.rcParams[key] = params[key]
            except KeyError:
                pass
    
    @property
    def netcdf_to_pychart_factor (self):
        return self._pychart_factor['NetCDF']
    
    @netcdf_to_pychart_factor.setter
    def netcdf_to_pychart_factor (self, factor):
        if not factor:
            self._pychart_factor['NetCDF'] = None
        else:
            try:
                self._pychart_factor['NetCDF'] = float(factor)
            except ValueError:
                raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR')
    
    @property
    def netcdf_to_z20_factor (self):
        return self._z20_factor['NetCDF']
    
    @netcdf_to_z20_factor.setter
    def netcdf_to_z20_factor (self, factor):
        if not factor:
            self._z20_factor['NetCDF'] = None
        else:
            try:
                self._z20_factor['NetCDF'] = float(factor)
            except ValueError:
                raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR')
    
    @property
    def hdf_to_pychart_factor (self):
        return self._pychart_factor['HDF']
    
    @hdf_to_pychart_factor.setter
    def hdf_to_pychart_factor (self, factor):
        if not factor:
            self._pychart_factor['HDF'] = None
        else:
            try:
                self._pychart_factor['HDF'] = float(factor)
            except ValueError:
                raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR')
    
    @property
    def hdf_to_z20_factor (self):
        return self._z20_factor['HDF']
    
    @hdf_to_z20_factor.setter
    def hdf_to_z20_factor (self, factor):
        if not factor:
            self._z20_factor['HDF'] = None
        else:
            try:
                self._z20_factor['HDF'] = float(factor)
            except ValueError:
                raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR')
    
    @property
    def netcdf_wln_tolerance (self):
        return self._wln_tolerance['NetCDF']
    
    @netcdf_wln_tolerance.setter
    def netcdf_wln_tolerance (self, tolerance):
        try:
            self._wln_tolerance['NetCDF'] = float(tolerance)
        except ValueError:
            raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR')
    
    @property
    def hdf_wln_tolerance (self):
        return self._wln_tolerance['HDF']
    
    @hdf_wln_tolerance.setter
    def hdf_wln_tolerance (self, tolerance):
        try:
            self._wln_tolerance['HDF'] = float(tolerance)
        except ValueError:
            raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR')

    @property
    def netcdf_time_tolerance (self):
        return self._time_tolerance['NetCDF']
    
    @netcdf_time_tolerance.setter
    def netcdf_time_tolerance (self, tolerance):
        try:
            self._time_tolerance['NetCDF'] = float(tolerance)
        except ValueError:
            raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR')
    
    @property
    def hdf_time_tolerance (self):
        return self._time_tolerance['HDF']
    
    @hdf_time_tolerance.setter
    def hdf_time_tolerance (self, tolerance):
        try:
            self._time_tolerance['HDF'] = float(tolerance)
        except ValueError:
            raise pcerror.RangeError('ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR')
    
    @property
    def gate_time_range (self):
        return self._gate_time_range
    
    @gate_time_range.setter
    def gate_time_range (self, ranges):
        if (type(ranges) == tuple) and (len(ranges) == 2):
            self._gate_time_range = ranges
    
    @property
    def pd_plot_layout (self):
        return self._pd_plot_layout
    
    @pd_plot_layout.setter
    def pd_plot_layout (self, layout):
        if layout in ['separated', 'combined']:
            self._pd_plot_layout = layout
    
    # -----------------------------------------------------------------
    #   LOAD AND SAVE
    # -----------------------------------------------------------------
    
    def load_settings_from_file(self, filepath):
        """ Load settings from a JSON file in pychart's settings directory. """
        # TODO: Settings are applied until errors might occur. In case of failures,
        #   they are, thus, partially changed to user requests. In the future,
        #   files should be checked to be valid first,and only then *all* settings
        #   changes may be applied.
        try:
            with open(filepath, 'r', encoding='utf-8') as file:
                settings = json.load(file)
            
            if 'HDF' not in settings.keys():
                settings['HDF'] = {}
            
            self.language    = settings['general'].get('language',    self.language)
            self.startup_tab = settings['general'].get('startup_tab', self.startup_tab)
            self.default_dir = settings['general'].get('default_dir', self.default_dir)
            
            self.default_plot_settings = settings['PlotTab'].get('default_settings',  self.default_plot_settings)
            self.export_resolution     = settings['PlotTab'].get('export_resolution', self.export_resolution)
            self.rcparams              = settings['PlotTab'].get('rcParams',          self.rcparams)
            
            self.netcdf_to_pychart_factor = settings['NetCDF'].get('pychart_factor', self.netcdf_to_pychart_factor)
            self.netcdf_to_z20_factor     = settings['NetCDF'].get('z20_factor',     self.netcdf_to_z20_factor)
            self.netcdf_wln_tolerance     = settings['NetCDF'].get('wln_tolerance',  self.netcdf_wln_tolerance)
            self.netcdf_time_tolerance    = settings['NetCDF'].get('time_tolerance', self.netcdf_time_tolerance)
            self.hdf_to_pychart_factor    = settings['HDF'].get('pychart_factor',    self.hdf_to_pychart_factor)
            self.hdf_to_z20_factor        = settings['HDF'].get('z20_factor',        self.hdf_to_z20_factor)
            self.hdf_wln_tolerance        = settings['HDF'].get('wln_tolerance',     self.hdf_wln_tolerance)
            self.hdf_time_tolerance       = settings['HDF'].get('time_tolerance',    self.hdf_time_tolerance)
            
            self.gate_time_range = settings['NetCDF'].get('gate_lower_limit', self.gate_time_range[0]), \
                                        settings['NetCDF'].get('gate_upper_limit', self.gate_time_range[1])
            self.pd_plot_layout = settings['HDF'].get('pd_plot_layout', self.pd_plot_layout)
        
        except json.JSONDecodeError:
            raise pcerror.InputError('ERROR_PYCHART_SETTINGS_JSON_ERROR', input=filepath)
        
        except:
            raise pcerror.InputError('ERROR_PYCHART_SETTINGS_LOAD_ERROR', input=filepath)
    
    def save_settings_to_file (self, filepath):
        """ Save settings to a JSON file in pychart's settings directory. """
        settings = {
            'general': {
                'version':     PYCHART_VERSION_NAME,
                'language':    self._language.value,
                'startup_tab': self._startup_tab,
                'default_dir': self.default_dir
            },
            'PlotTab': {
                'default_settings':  self.default_plot_settings,
                'export_resolution': self.export_resolution,
                'rcParams':          self.rcparams
            },
            'NetCDF': {
                'pychart_factor':   self.netcdf_to_pychart_factor,
                'z20_factor':       self.netcdf_to_z20_factor,
                'gate_lower_limit': self.gate_time_range[0],
                'gate_upper_limit': self.gate_time_range[1],
                'wln_tolerance':    self.netcdf_wln_tolerance,
                'time_tolerance':   self.netcdf_time_tolerance
            },
            'HDF': {
                'pychart_factor': self.hdf_to_pychart_factor,
                'z20_factor':     self.hdf_to_z20_factor,
                'wln_tolerance':  self.hdf_wln_tolerance,
                'time_tolerance': self.hdf_time_tolerance,
                'pd_plot_layout': self.pd_plot_layout
            }
        }
        
        try:
            with open(filepath, 'w', encoding='utf-8') as file:
                json.dump(settings, file, indent=4, ensure_ascii=False)
        
        except json.JSONDecodeError:
            raise pcerror.OutputError('ERROR_PYCHART_SETTINGS_JSON_ERROR', output=filepath)
        
        except:
            raise pcerror.OutputError('ERROR_PYCHART_SETTINGS_SAVE_ERROR', output=filepath)
    