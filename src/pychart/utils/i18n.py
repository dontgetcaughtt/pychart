import yaml
from enum import Enum

from PyQt5.QtCore import QDir

from ..core import error as pcerror

# TODO: consider switching literals to status.import.infReplaced

# -----------------------------------------------------------------
#   SUPPORTED LANGUAGES
# -----------------------------------------------------------------

class LanguageCode (Enum):
    """ Enumeration for supported languages. """
    de = 'de'
    en = 'en'

# -----------------------------------------------------------------
#   INTERNATIONALIZATION HANDLER CLASS
# -----------------------------------------------------------------

class Translator (object):
    """
    A Class for translating string literals into a certain language.
    
    As part of its initialization procedure, German is loaded as the
    default language. It also serves as a fallback, if the language,
    which is attempted to load on `load_lang` call, can not be
    loaded.
    
    For each language, translations for the literals used by pychart
    have to be provided in a valid YAML file. For more information
    on translating please refer to TRANSLATING.md in the i18n directory.
    
    Attributes
    ----------
    lang : LanguageCode
        currently loaded language
    """
    
    def __init__ (self, lang=LanguageCode.de):
        self._lang = None
        self._strings = None
        self.load_lang(lang)
    
    @property
    def lang (self):
        return self._lang
    
    @property
    def available_languages (self):
        raise NotImplementedError
        # TODO: improve way of loading / adding languages
        #   register available languages from *.yaml files in /i18n/
        #   clean up determination (ugly if-elif chain) of language in settings.py
        #   get rid of redundant "translated" language strings (eg. 'de') in every translation file
    
    def load_lang_from_file (self, filepath):
        """
        Loads translation strings from a YAML file found at `filepath`.
        
        Parameters
        ----------
        filepath : str
            filepath of a valid YAML language file
        
        Returns
        -------
        bool
            True, if file was successfully loaded, False otherwise
        """
        try:
            with open(filepath, 'r', encoding='utf-8') as file:
                self._strings = yaml.safe_load(file)
            return True
        except yaml.YAMLError:
            return False
    
    def load_lang (self, lang):
        """
        Load language for translation.
        
        Loads translation strings for language `lang` from corresponding YAML file.
        If it fails to load the given language, German is loaded as a fallback. In
        that case an `InputError` is raised for notification after the fallback
        language was loaded.
        
        Parameters
        ----------
        lang : LanguageCode
            language to be loaded
        
        Raises
        ------
        pcerror.InputError
            if the chosen language could not be loaded (invalid or missing)
        
        pcerror.InternalError
            if no language (including the fallback language) could be loaded at all
        """
        if lang == self._lang:
            return
        
        i18n_dir = QDir.current()
        i18n_dir.cdUp()
        i18n_dir.cd('i18n')
        fallback = not i18n_dir.exists(lang.value + '.yaml')
        
        if not fallback:
            if self.load_lang_from_file(i18n_dir.path() + '/' + lang.value + '.yaml'):
                self._lang = lang
            else:
                fallback = True
        
        if fallback: # No if-else here as fallback can change in if-block.
            if self.load_lang_from_file(i18n_dir.path() + '/de.yaml'):
                self._lang = LanguageCode.de
                raise pcerror.InputError('ERROR_PYCHART_LANG_NOT_FOUND_ERROR', input=lang)
            else:
                raise pcerror.InternalError('ERROR_PYCHART_NO_LANG_LOADED_ERROR') from None
    
    def _ (self, s):
        """
        Return the translation for `s`.
        
        Returns the translated string for the literal `s` in the language currently loaded.
        If the corresponding translation can not be found, `s` is returned.
        
        Note that `Translator._()` and `Translator.translate()` are identical.
        
        Parameters
        ----------
        s: str
            literal to be translated
        
        Returns
        -------
        str
            translated string
        """
        try:
            return self._strings[s]
        except KeyError:
            return s
    
    def translate (self, s):
        return self._(s)
    
    def retranslate (self, s):
        """
        Return the translation for `s`.
        
        Returns the literal for the translated string `s`. If no corresponding
        literal can not be found for the translation to the currently loaded
        language, `s` is returned.
        
        Parameters
        ----------
        s: str
            translated string
        
        Returns
        -------
        str
            universal literal
        """
        try:
            return list(self._strings.keys())[list(self._strings.values()).index(s)]
        except (KeyError, AttributeError):
            return s
