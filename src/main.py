import os
import sys

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon

from pychart.gui import MainWindow

os.environ["QT_ENABLE_HIGHDPI_SCALING"] = "1"

def main (argv):
    
    if not QApplication.instance():
        app = QApplication(argv)
    else:
        app = QApplication.instance()
    
    app.setWindowIcon(QIcon('../images/logo/icon.png'))
    
    win = MainWindow.MainWindow()
    win.showMaximized()
    
    sys.exit(app.exec())

if __name__ == '__main__':
    main(sys.argv)
