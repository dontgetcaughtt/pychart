#
# Script used to generate a colormap reference.
#
# This script's output is a visual overview of
# all available colormaps as pre-defined by
# matplotlib. They are grouped to help the user
# to find a colormaps suitable for their specific
# purposes.
#
# The script works in conjunction with the
# `predefined.txt` file. Changes in that file's
# syntax need to be adjusted here.
#

import re
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict

# load and categorize known colormaps
cmaps = OrderedDict()

with open('../cmaps/predefined.txt', 'r') as file:
    lines = file.read().splitlines()

category = []
category_name = 'Unsorted Colormaps'
for line in lines:
    if line.startswith('## '):
        cmaps.update({category_name: category})
        category_name = line[3:]
        category = []
    elif line.startswith('#####'):
        continue
    else:
        category += [line.strip(' #')]
cmaps.update({category_name: category})

# check for other colormaps that are available but not categorized
regex = re.compile('.*_r')
available_cmaps = set([cmap for cmap in plt.colormaps() if not regex.match(cmap)])

categorized_cmaps = set()
for maps in cmaps.values():
    categorized_cmaps |= set(maps)

unsorted_cmaps = available_cmaps - categorized_cmaps
if unsorted_cmaps:
    cmaps['Unsorted Colormaps'] = list(unsorted_cmaps)
    cmaps.move_to_end('Unsorted Colormaps')
else:
    del cmaps['Unsorted Colormaps']

# book keeping
n_cmaps = sum([len(cmaps[key]) for key in cmaps])
n_categories = len(cmaps)

# function that plots each colormap of a given category into a subfigure
def plot_color_gradients (cmap_category, cmap_list, subfig):
    axes = subfig.subplots(nrows=len(cmap_list)+1, squeeze=True)
    subfig.subplots_adjust(top=1, bottom=0, left=0.12, right=1)
    axes[0].set_axis_off()
    axes[0].text(0.5, 0.25, cmap_category, va='center', ha='center', fontsize=18, fontstyle='italic')
    
    for ax, cmap in zip(axes[1:], cmap_list):
        ax.imshow(gradient, aspect='auto', cmap=plt.get_cmap(cmap))
        l, b, w, h = list(ax.get_position().bounds)
        x_text = l - 0.01
        y_text = b + 0.5*h
        subfig.text(x_text, y_text, cmap, va='center', ha='right', fontsize=12)
        ax.set_axis_off()

# set up figure
fig = plt.figure(figsize=(12, 0.6*(n_cmaps+n_categories)))

hr = [(len(category) + 1) for category in cmaps.values()]
subfigs = fig.subfigures(n_categories, 1, height_ratios=hr, wspace=0, hspace=0)

gradient = np.vstack((np.linspace(0, 1, 256), np.linspace(0, 1, 256)))

# plot
for i, key in enumerate(cmaps):
    plot_color_gradients(key, cmaps[key], subfigs[i])

plt.savefig('../cmaps/cmap-reference.png', format='png', bbox_inches='tight')
plt.show()
