#
# Script used to generate the pychart icon.
#
# A circle is drawn as a contour by means of Gaussian functions
# in polar coordinates. The letter 'C' ist extracted by using
# just a subset of angles. To better mimic real contour plots,
# a meaningless function is overlayed and random noise added.
#

import numpy
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

def gaussian (x, amp, center, w, offset):
    return amp * np.exp(- ((x - center)**2) / (2 * w**2)) + offset

# create colormap; identical to bwgyr_Kristoffer
bwgyr_lut = [
    ( 85,   0, 214),
    ( 43,   0, 233),
    (  0,   0, 255),
    (  0, 114, 251),
    (  0, 230, 255),
    (255, 255, 255),
    (  0, 255,   0),
    (255, 255,   0),
    (241, 208,   0),
    (228, 161,   0),
    (253, 131,   0),
    (237,  66,   0),
    (255,   0,   0),
    (178,   0,   0),
    (137,   0,   0),
    ( 80,   0,   0)
]

bwgyr = LinearSegmentedColormap.from_list('bwgyr',
        [ tuple(map(lambda x: float(x) / 255, color)) for color in bwgyr_lut])

# function to determine if angle is part of letter 'C'
# letter at angles within [70°,305°]
# raising edges within [45°,70°) and (305°,325°]
def amplitude (angle):
    if angle < 45:
        return 0
    elif angle < 70:
        span = 70 - 45
        diff = 70 - angle
        return (span - diff) / span
    elif angle <= 305:
        return 1
    elif angle <= 325:
        span = 325 - 305
        diff = 325 - angle
        return diff / span
    else:
        return 0

# set up coordinate system
angles = np.radians(np.linspace(0, 360, 360))
distance = np.linspace(0, 4, 400)
r, theta = np.meshgrid(distance, angles)

# initialize values
values = np.zeros((angles.size, distance.size))
values += (np.random.rand(angles.size, distance.size) * 0.3)
values += numpy.nan_to_num( (0.4 * np.sin(r))**2 ) - numpy.nan_to_num( (0.4 * np.cos(theta))**2 )

# generate letter with Gaussian within angle range
# radius = 0.6; width = 0.15; offset = 0
for angle in range(360):
    values[angle,:] += gaussian(distance, amplitude(angle), 0.6, 0.15, 0)

# set up plot
mpl.rcParams['axes.linewidth'] = 0

fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))

fig.set_size_inches(22.2, 12.6, forward=True)
ax.set_xticks([])
ax.set_yticks([])

levels = np.linspace(np.min(values), 1.3, 50)

ax.contourf(theta, r, values, cmap=bwgyr, levels=levels)

plt.savefig('pychart-icon.png', format='png', bbox_inches='tight')
plt.show()
