"""
Script used to generate the NetCDF test files for `tests/test_netcdf.py`.

Detailed information on the tests themselves can be found in the test file. A
general overview on the files generated here and their rough purpose can be found
in `TESTING.md`.

This script should not be executed on a regular basis. Once the - partially
randomly filled - arrays are saved and initially tested, they are guaranteed to
work. Re-execution might introduce new errors.
"""

def create_arrays ():
    datasets = {}
    
    # axes arrays
    wlns = np.arange(32, dtype='int32')
    times = np.arange(7, dtype='int32')
    
    # array for testing correct chopper indexing
    # difference absorption is all positive
    arr_4cycle = np.random.rand(3,7,5,1,4,32).astype(dtype='float64')
    arr_4cycle[1,:,:,:,0,:] = 14.4  # pumpprobe
    arr_4cycle[1,:,:,:,1,:] =  3.6  # pump
    arr_4cycle[1,:,:,:,2,:] = 16.4  # probe
    arr_4cycle[1,:,:,:,3,:] = 0.02  # bg
    
    # array for testing bg corr
    # difference absorption is all positive
    arr_2cycle = np.empty((1,7,5,1,2,32), dtype='float64')
    arr_2cycle[:,:,:,:,0,:] = np.random.rand(1,7,5,1,32)*200 - 30000  # pumpprobe
    arr_2cycle[:,:,:,:,1,:] = np.random.rand(1,7,5,1,32)*200 - 25000  # probe
    
    # array for testing proper scan selection handling
    # average is 2 for [0,1,2,3,4], [0,2], [0,2,4], [1,3] and [1,3,4]
    arr_5scans = np.random.rand(5,7,5,1,4,32).astype(dtype='float64')
    arr_5scans[0,:,:,:,2,:] = 0
    arr_5scans[1,:,:,:,2,:] = 1
    arr_5scans[2,:,:,:,2,:] = 4
    arr_5scans[3,:,:,:,2,:] = 3
    arr_5scans[4,:,:,:,2,:] = np.nan
    
    # array for testing different shot counts
    arr_nshots = np.random.rand(4,7,20,1,4,32).astype(dtype='float64')
    arr_nshots[1,:,:,:,0,:] = 14.4  # pumpprobe
    arr_nshots[1,:,:,:,1,:] =  3.6  # pump
    arr_nshots[1,:,:,:,2,:] = 16.4  # probe
    arr_nshots[1,:,:,:,3,:] = 0.02  # bg
    
    # 1d-array for dimension tests
    arr_onedim = np.empty((1,1,1,1,1,1), dtype='float64')
    
    # array for use as bg file along `arr_2cycle`
    arr_bg_2cycle = np.random.rand(5,1,2,32).astype('float64')*50 - 32000
    
    # array for use as bg file or single file
    arr_bg_4cycle = np.random.rand(5,1,4,32).astype('float64')
    arr_bg_4cycle[:,:,0,:] = 36  # pumpprobe
    arr_bg_4cycle[:,:,1,:] = 11  # pump
    arr_bg_4cycle[:,:,2,:] = 42  # probe
    arr_bg_4cycle[:,:,3,:] =  1  # bg
    
    # array with invalid chopper mode
    arr_bg_3cycle = np.random.rand(5,1,3,32).astype('float64')
    
    # create xarray objects from arrays
    datasets['4cycle'] = xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_4cycle),
        ccd_pixel_var=(['ccd_row'], wlns), scan_var=(['delay_time'], times))
    )
    datasets['2cycle'] = xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_2cycle),
        ccd_pixel_var=(['ccd_row'], wlns), scan_var=(['delay_time'], times))
    )
    datasets['5scans'] = xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_5scans),
        ccd_pixel_var=(['ccd_row'], wlns), scan_var=(['delay_time'], times))
    )
    datasets['20shots'] = xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_nshots),
        ccd_pixel_var=(['ccd_row'], wlns), scan_var=(['delay_time'], times))
    )
    datasets['altered-axes'] = xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_4cycle),
        ccd_pixel_var=(['ccd_row'], wlns+0.2), scan_var=(['delay_time'], times*1.5))
    )
    datasets['one-dimensional'] = xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_onedim),
        ccd_pixel_var=(['ccd_row'], [0]), scan_var=(['delay_time'], [0]))
    )
    datasets['missing-axes'] = xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'exp_step_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_4cycle))
    )
    datasets['wrong-identifier'] = xarray.Dataset(dict(
        ccd_data=(['scan_nr', 'delay_time', 'shot_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_2cycle),
        ccd_pixel_var=(['ccd_row'], wlns), scan_var=(['delay_time'], times))
    )
    datasets['wrong-dimensions'] = xarray.Dataset(dict(
        ccd_data=(['ccd_scan', 'ccd_line', 'ccd_row'], arr_2cycle.reshape((14,32,5))))
    )
    datasets['bg-2cycle'] = xarray.Dataset(dict(
        ccd_data=(['ccd_mess', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_bg_2cycle),
        ccd_pixel_var=(['ccd_row'], wlns))
    )
    datasets['bg-4cycle'] = xarray.Dataset(dict(
        ccd_data=(['ccd_mess', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_bg_4cycle),
        ccd_pixel_var=(['ccd_row'], wlns))
    )
    datasets['bg-3cycle'] = xarray.Dataset(dict(
        ccd_data=(['ccd_mess', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_bg_3cycle),
        ccd_pixel_var=(['ccd_row'], wlns))
    )
    datasets['bg-wrong-identifier'] = xarray.Dataset(dict(
        ccd_data=(['shot_nr', 'ccd_scan', 'ccd_line', 'ccd_row'], arr_bg_4cycle),
        ccd_pixel_var=(['ccd_row'], wlns))
    )
    
    return datasets

if __name__ == '__main__':
    import numpy as np
    import xarray
    
    datasets = create_arrays()
    for name, ds in datasets.items():
        ds.to_netcdf('../tests/test-data/netcdf/' + name + '.arr')
        ds.close()
    del datasets
