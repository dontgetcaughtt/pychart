#
# Script used to generate an explanatory plot for documentation.
#
# Subplots for contour, traces, spectra and colorbar are plotted
# for all possible positions. An additional mesh grid is drawn
# to explain the user-adjustable width and height ratios.
#
# It is necessary to plot two different versions (settings A and B)
# to display all available options: (Un)comment definitions in lines
# 19 or 20, respectively.
#

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec as mplGridSpec
from matplotlib.patches import ConnectionPatch as mplConnectionPatch
from matplotlib.patches import Circle as mplCirclePatch

linlog, Ltype, widths = True, False, [50,150,80] # Settings A
#linlog, Ltype, widths = False, True, [60,195,25] # Settings B
heights = [5,15,55,15,20,15]

shift = 5 # shift of elucidating mesh grid
ticks = 2 # mesh grid's tick length

fig = plt.figure(figsize=(16,8))
grid = mplGridSpec(6, 3, width_ratios=widths, height_ratios=heights)

axes = {
    'contour_lin':     [None, 'tab:cyan',   '4,2',   'contour lin part'],
    'contour_log':     [None, 'tab:blue',   '3,2',   'contour log part'],
    'contour':         [None, 'tab:blue',   '3-4,2', 'contour'],
    'traces_lin':      [None, 'tab:orange', '4,1',   'traces lin part'],
    'traces_log':      [None, 'tab:red',    '3,1',   'traces log'],
    'traces':          [None, 'tab:red',    '3-4,1', 'traces'],
    'spectra_angled':  [None, 'tab:green',  '1-2,2', 'spectra (L-type)'],
    'spectra_linear':  [None, 'tab:green',  '2-4,3', 'spectra (linear)'],
    'colorbar_top':    [None, 'tab:pink',   '1-2,2', 'colorbar (top)'],
    'colorbar_bottom': [None, 'tab:purple', '6,2',   'colorbar (bottom)'],
    'colorbar_right':  [None, 'tab:pink',   '2-3,3', 'colorbar (right)']
}

if linlog:
    axes['contour_log'][0] = fig.add_subplot(grid[2,1])
    axes['contour_lin'][0] = fig.add_subplot(grid[3,1])
    axes['traces_log'][0]  = fig.add_subplot(grid[2,0])
    axes['traces_lin'][0]  = fig.add_subplot(grid[3,0])
else:
    axes['contour'][0] = fig.add_subplot(grid[2:4,1])
    axes['traces'][0]  = fig.add_subplot(grid[2:4,0])

if Ltype:
    axes['spectra_angled'][0]  = fig.add_subplot(grid[:2,1])
    axes['colorbar_right'][0]  = fig.add_subplot(grid[1:3,2])
    axes['colorbar_bottom'][0] = fig.add_subplot(grid[5,1])
else:
    axes['spectra_linear'][0]  = fig.add_subplot(grid[1:4,2])
    axes['colorbar_top'][0]    = fig.add_subplot(grid[:2,1])
    axes['colorbar_bottom'][0] = fig.add_subplot(grid[5,1])

for ax in axes.values():
    if ax[0] is None:
        continue
    ax[0].set(xlim=(0,1), ylim=(0,1))
    ax[0].fill([0,1,1,0],[0,0,1,1], ax[1])
    ax[0].tick_params(axis='both', which='both', direction='in', labelbottom=False, labelleft=False)
    ax[0].text(0.5, 0.5, '{name}\n[{grid}]'.format(name=ax[-1],grid=ax[2]), va="center", ha="center", fontdict={'color':'w'})

mesh = fig.add_subplot(grid[:, :])
mesh.axis('off')
mesh.set_xlim(0, sum(widths))   # origin (0,0) is in the top left corner;
mesh.set_ylim(-sum(heights), 0) # mesh displayed as 4th quadrant
w = np.insert(widths,0,0).cumsum()
h = -np.insert(heights,0,0).cumsum()

patches = [
    mplConnectionPatch((0, shift), (max(w), shift), 'data', 'data', mesh, mesh), # x-axis
    mplConnectionPatch((-shift, 0), (-shift, min(h)), 'data', 'data', mesh, mesh) # y-axis
]
patches += [mplConnectionPatch((pt,shift-ticks), (pt,shift+ticks), 'data', 'data', mesh, mesh) for pt in w] # x-ticks
patches += [mplConnectionPatch((-shift+ticks,pt), (-shift-ticks,pt), 'data', 'data', mesh, mesh) for pt in h] # y-ticks
for patch in patches:
    patch.set_annotation_clip(False)
    mesh.add_patch(patch)

for i, width in enumerate(widths):
    mesh.add_patch(mplCirclePatch((sum(widths[:i]) + 0.5*width, shift+5*ticks), ticks, edgecolor='k', facecolor='none', clip_on=False))
    mesh.text(sum(widths[:i]) + 0.5*width, shift+5*ticks, str(i), weight='bold', va='center', ha='center')
    mesh.text(sum(widths[:i]) + 0.5*width, shift+2*ticks, str(width), va='center', ha='center')

for i, height in enumerate(heights):
    mesh.add_patch(mplCirclePatch((-(shift+6*ticks),-(sum(heights[:i])+0.5*height)), ticks, edgecolor='k', facecolor='none', clip_on=False))
    mesh.text(-(shift+6*ticks), -(sum(heights[:i]) + 0.5*height), str(i), weight='bold', va='center', ha='center')
    mesh.text(-(shift+3*ticks), -(sum(heights[:i]) + 0.5*height), str(height), va='center', ha='center')

plt.subplots_adjust(top=0.865, bottom=0.02, left=0.072, right=0.99, hspace=0.03, wspace=0.008)
plt.savefig("../docs/images/plot-layout-type-scale.png", format='png', dpi=120, bbox_inches='tight')
plt.show()
