# v5.2

_June 06, 2022_

pychart now requires Python 3.10.

The online documentation has been expanded and now contains detailed user guides for NetCDF and HDF imports.

**New Features**
- Plot photodiode values from Kerr gate measurements.
- Additional rectangles can be drawn on plots.
- Time traces or transient spectra can now be scaled with an arbitrary factor.

**Other Changes**
- Improved user interface for Sellmeier corrections.
- Preview for spectral sensitivity correction has been moved into independent window.
- The online documentation is opened in the web browser on help requests.
- Backwards compatibility for general settings files (`pychart-settings.json`) since their introduction in 
v4.0 is now guaranteed.

# v5.1

_May 19, 2022_

**New Features**
- multiple NetCDF files can be merged into a single file for easy averaging over many scans
- numbers of scans for NetCDF imports is not limited to four anymore

**Major Changes**
- for NetCDF imports solvent measurements may now contain only a subset of times
- dpi resolution of exported plots is not fixed anymore but might be adjusted in the GeneralSettingsDialog

**Other Improvements**
- imported NetCDF and HDF datasets are checked for consistency
- user guide for plotting feature
- test suite for NetCDF imports

# v5.0

_April 15, 2022_

**New Highlight Feature**
- import single spectra from the fsTA setup, which are supplied as "background" files

**Major Changes**
- new icon set
- many improvements of the gate correction feature:
  - higher precision in Sellmeier fit results (now three decimals) and "convey to input" button
  - restriction of time trace's time range as part of the GeneralSettingsDialog
  - additional time shift (for convenience only)

**Other Improvements**
- more materials added to Sellmeier correction
- Test suite including unittests
- improved documentation

# v4.0

_September 03, 2021_

**New Highlight Features**
- import and correct HDF data from Kerr-Gate
- internationalization
- progress and status bar now indicate current progress

**New Features**
- fully functional Sellmeier time zero correction 
- improved gate correction algorithms
- optional solvent time shift
- optional fixed gate fit parameters
- Z20 compatible data export
- go directly to any wavelength in imported traces previews

# v3.0

_January 04, 2021_

First version to include options to import and manipulate raw data from fsTA setup.

**New Highlight Feature:**
- import and correct NetCDF data

**Major Changes:**
- add lines as free artists to plots
- general settings dialog implemented
- overhauled exception handling

A complete code review was undertaken and many small changes have been applied.

# v2.0

_March 23, 2020_

This version is the first to include a fitting routine for single-channel data. 
However, this feature is still experimental and may not work properly.

Most importantly many small changes and functions have been implemented. Many 
more things can now be specified, like colors for labels (different from their 
corresponding plot lines), baselines for time traces and transient spectra, the 
data multiply factor and much more!

**New Highlight Features:**

- time-dependent fitting for single-wavelength data (experimental)
- saving and loading of plot settings to / from file

# v1.0

_August 28, 2019_

This is the very first stable release of pyChart for productive purposes.
Currently, only plotting is possible, fitting routines will be added in later versions.