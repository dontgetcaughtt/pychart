# Contour Plots

*Last updated: May 31, 2022 by K.A.T.*

The Contour Plot module allows users to create contour representations of time-resolved, spectroscopic datasets 
accompanied by time traces and transient spectra of interest with just one click. Many details can be user-defined and 
settings stored to file, so that creation of publication-ready plots is fast and easy.  

_**Table of Contents**_
<!-- Manual TOC is very ugly, but sections are unlikely to change, nor are additional ones expected. -->
- [**General**](#general)
  - [Data format requirements](#data-format-requirements)
  - [Units](#units)
  - [Shifting the data's order of magnitude](#shifting-the-datas-order-of-magnitude)
  - [Restoring user settings; default settings](#restoring-user-settings-default-settings)
- [**Aufbauprinciple: Composition and Layout Structure of Plots**](#aufbauprinciple-composition-and-layout-structure-of-plots)
  - [Plot Layout](#plot-layout-aka-shared-axes)
  - [Plot Type](#plot-type-aka-time-axis-scaling)
  - [Plot Widths and Heights](#plot-widths-and-heights)
  - [General Scaling](#general-scaling)
  - [Ticks](#ticks)
  - [Axes Label](#axes-label)
- [**Colors, Colorbar and Colormaps**](#colors-colorbar-and-colormaps)
  - [Defining Colors](#defining-colors)
  - [Selection and Definition of Colormaps](#selection-and-definition-of-colormaps)
  - [Adjusting Colorbars](#adjusting-colorbars)
- [**Time Traces and Transient Spectra**](#time-traces-and-transient-spectra)
  - [Customization](#customization)
  - [Scaling and Scalebar](#scaling-and-scalebar)
  - [Baselines](#baselines)
  - [External Traces/Spectra](#external-tracesspectra)
- [**Additional Lines and Rectangles**](#additional-lines-and-rectangles)
- [**Implementation Details**](#implementation-details)

![](../images/screenshots/plot-screenshot.png)  
<small>_Figure 1. Screenshot of the PlotTab module of pyChart for the generation of contour plots._</small>

## General

<small>_Table 1. Available tool bar actions._</small>

| Icon                                                     | Description                                                                                                                                                                                                                                                                                                                                     |
|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <img src="../images/icons/plot.svg" height="48"/>        | Plot data according to settings.<br /><small>The duration of this process is influenced by several parameters. It mostly depends on the amplitude step size, which thus should not be less than 1/1000 of the data's amplitude range. Multiplying the data with a factor also increases the time of the plotting process significantly.</small> |
| <img src="images/plot-json-icons.svg" height="48"/>      | [Import/Save](#restoring-user-settings-default-settings) settings from/to json file.                                                                                                                                                                                                                                                            |
| <img src="../images/icons/save.svg" height="48"/>        | Export plot as image.<br /><small>Plot will be saved exactly as displayed in the preview section.<br />Available file formats are png, pdf, svg, eps, jpg and tiff.</small>                                                                                                                                                                     |
| <img src="../images/icons/colorbar.svg" height="48"/>    | Open [Colorbar Dialog](#adjusting-colorbars).                                                                                                                                                                                                                                                                                                   |
| <img src="../images/icons/scalebar.svg" height="48"/>    | Open [Scalebar Dialog](#scaling-and-scalebar).                                                                                                                                                                                                                                                                                                  |
| <img src="../images/icons/autoscale.svg" height="48"/>   | Apply autoscale settings.<br /><small>Triggering this function will set the plot ranges of the contour representation to reasonable values. Scaling of traces or spectra plots will not be affected.<br />Currently, this feature works rather provisional than professional.</small>                                                           |
| <img src="../images/icons/axislabel.svg" height="48"/>   | Open [Axes Label Dialog](#axes-label).                                                                                                                                                                                                                                                                                                          |
| <img src="../images/icons/baseline.svg" height="48"/>    | Open [Baseline Dialog](#baselines).                                                                                                                                                                                                                                                                                                             |
| <img src="../images/icons/add-artists.svg" height="48"/> | Open [Additional Lines Dialog](#additional-lines-and-rectangles).                                                                                                                                                                                                                                                                               |

### Data format requirements

Datasets have to be provided as ASCII text files with wavelengths, wavenumbers or any other energy axis in columns, and 
times in rows. In other words: one spectrum in one row for each time point. The first column serves as a header 
defining the time axis, while the first row is a header defining the wavelength axis. To normalize the data matrix an 
entry at position (0,0) is required. This stand-in data point can be anything, preferably "0", but not empty. It will be 
ignored during processing.

Values can be integers or floating point numbers in decimal or scientific notation. Both, full stop (".") and comma 
(",") are allowed as decimal marks. Values need to be separated by whitespace (spaces ("˽") or tabstops).

| Example of a valid input                                                                                                                                                                       | Equivalent to and parsed by pychart as                                                                                                                                                                                                                                                                                                                                                                                                                              |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `0 300 301 302 303 304`<br />`-0.1 123.4 567.8 912.3 456.7 890.1`<br />`0.0 912.3 456.7 890.1 123.4 567.8`<br />`0.1 567 912 456 123 890`<br />`0.2 123.4e0 4.567e2 8.901E2 5.678E-2 -91.23e1` | <table><tr><th></th><th>300</th><th>301</th><th>302</th><th>303</th><th>304</th></tr><tr><th>-0.1</th><td>123.4</td><td>567.8</td><td>912.3</td><td>456.7</td><td>890.1</td></tr><tr><th>0.0</th><td>912.3</td><td>456.7</td><td>890.1</td><td>123.4</td><td>567.8</td></tr><tr><th>0.1</th><td>567</td><td>912</td><td>456</td><td>123</td><td>890</td></tr><tr><th>0.2</th><td>123.4</td><td>456.7</td><td>890.1</td><td>0.05678</td><td>-912.3</td></tr></table> |

### Units

Units in which the data are given can be set right behind the dataset input field via selection boxes. Currently, 
nanometer (nm), wavenumber (cm<sup>-1</sup>) and electronvolt (eV) are possible units of energy, while optical density 
(mOD, OD), counts and arbitrary units (a.u.) are possible as units of amplitude. For the time axis, units from 
femtosecond (fs) to minutes (min) are available. Note that the selection of units has no effect on the data plotting 
itself. It only simplifies the correct labeling of axes, which can be set [manually](#axes-label), too.

![](images/plot-units.png)

### Shifting the data's order of magnitude

For the ease of data handling it is possible to multiply the entire dataset with a constant factor prior plotting. 
This keeps very low or large numbers in a reasonable range. It can be set via the input field labeled "ΔA·". Please 
note that for large datasets this process can slow down the plotting process significantly. 

![](images/plot-multiply.png)

### Restoring user settings; default settings

| Icon                                                     | Description                     |
|----------------------------------------------------------|---------------------------------|
| <img src="../images/icons/import-json.svg" height="48"/> | Import settings from json file. |
| <img src="../images/icons/export-json.svg" height="48"/> | Save settings to json file.     |

Settings currently applied can be stored to file and recalled for later use. This enables the user to continue with 
previous works or to change previously generated plots without repeating all setting changes already made. Json settings 
files are also intended to be used as blueprints for the generation of plots with similar look, e.g. to plot several 
datatsets for one publication in the same manner.

PyChart provides several default settings with reasonable parameters for different experiments. They account 
for their respective axis values (e.g. picosecond range for fsTA experiments), typical amplitude magnitude (e.g. thousands of 
counts for Kerr Gate measurements), plotting type (e.g. logarithmic for nsIR experiments) and other. These settings can 
be easily loaded from the menu bar via  _Contour Plot_ 🠖 _Default Settings_.

The settings which are loaded at program startup or when a new Contour PlotTab is opened, can be set in the 
[General Settings Dialog](GeneralSettings.md). Any json settings file can be chosen to account for individual preferences 
and, for example, direct plotting of certain time traces, which are often of interest for the user.

## Aufbauprinciple: Composition and Layout Structure of Plots

In general, all plots consist of three parts: a central **contour** representation, selected time **traces** at the 
left-hand side and selected transient **spectra** at two possible positions. The interplay between these (i.e. shared 
axes, relative positioning) is defined by the plot's layout and type. Both affect the overall design and, thus, the 
appearance of the final plot. 

### Plot Layout (aka Shared Axes)

Regarding the spectra, users might choose between two positions:
- on the right-hand side of the contour (_linear layout_)
- on top of the contour (_L-type layout_)

Contour and traces always share a common time axis, spectra in linear layout as well. On the other hand, with L-type 
layout the spectra share their wavelength axis with the contour. Their amplitude axis is always independent (cf. 
[scaling](#scaling-and-scalebar)).

![](images/plot-layout.png)

Depending on the chosen layout, different options are available for the position of the colorbar:
- at the bottom
- at the top (linear layout only)
- on the right-hand side (L-type layout only)

### Plot Type (aka Time Axis Scaling)

The time axis can be plotted in three different types:
- linear scaling (_lin_)
- logarithmic scaling (_log_)
- linear and logarithmic scaling with a break in between (_lin-log_) 

The plot type is indicated in the very top right corner by colored buttons and can be selected by pressing on one of these. 
Alternatively, it can be chosen in the menu bar via _Contour Plot_ 🠖 _{type}_.

| Plot Type                        | Description                                                         |
|----------------------------------|---------------------------------------------------------------------|
| ![](images/plot-linlog-type.png) | Linear scaling followed by logarithmic scaling after an axis break. |
| ![](images/plot-lin-type.png)    | Full linear scaling.                                                |
| ![](images/plot-log-type.png)    | Full logarithmic scaling                                            |

### Plot Widths and Heights

The sizes of the individual parts are set relative to each other. To this end, the _General_ section features input fields 
for three widths and six heights. Note that due to the relative specification the values entered do not yield lengths of 
that measure. In fact, inputs [50,150,100] and [1,3,2] are identical. However, it is recommended to use rather high 
values, as they allow for finer adjustments.

![](images/plot-widths-heights.png)

The widths define the horizontal space of three consecutive parts (Table 2). While the first (left) and second (center) 
always correspond to the traces and contour respectively, the last (right) part defines the space reserved for either 
the spectra plot (in linear layout) or might be used for the colorbar (in L-type layout).   

<small>_Table 2. Overview of most important uses for plot widths._</small>

| Width | reserves space for            |
|-------|-------------------------------|
| ⓪     | Traces                        |
| ①     | Contour; colorbar (optional). |
| ②     | Spectra; colorbar (optional). |

The heights define the vertical space reserved for six parts stacked above each other, starting with the uppermost one 
(Table 3). The space for the traces and contour plots is reserved by ② and ③. In lin-log mode these correspond to the 
logarithmic and linear parts, respectively. Thus, changing their values can alter the height of the linear part in 
comparison to the logarithmic one. In case of unique scaling (either lin or log) these two heights are added to form 
combined plots (cf. Figure 3).

For parts in which nothing is drawn, e.g. height ⑤ if in linear layout the colorbar is at the top or width ② if in 
L-type layout the colorbar is at the bottom, values might be set to 0.

<small>_Table 3. Overview of most important uses for plot heights._</small>

| Height | reserves space for                                                                                                                                                                                                                                                       |
|--------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ⓪      | Additional share to ① for spectra or colorbar at the top.                                                                                                                                                                                                                |
| ①      | Overhang of the spectra plot above the upper end of the contour.<br /><small>This becomes crucial if spectra at late delay times are plotted, as they would otherwise be truncated. I.e. the offset spectrum would only show its negative part if ① is set to 0.</small> |
| ②      | Logarithmic part of traces and contour; share of spectra in linear layout.<br /><small>Combined with ③ to form traces and contour with unique scaling.</small>                                                                                                           |
| ③      | Linear part of traces and contour; share of spectra in linear layout.<br /><small>Combined with ② to form traces and contour with unique scaling.</small>                                                                                                                |
| ④      | Free space needed for bottom axes labels and ticks.                                                                                                                                                                                                                      |
| ⑤      | Colorbar, if displayed at the bottom.                                                                                                                                                                                                                                    |

Figure 2 shows an example of the linear layout with lin-log scaling and width of [50,150,80]; Figure 3 shows 
an example of L-type layout with unique scaling and widths of [60,195,25].

![](images/plot-layout-linear-linlog.png)  
<small>_Figure 2. Example of linear layout with lin-log scaling._</small>  
![](images/plot-layout-Ltype-single.png)  
<small>_Figure 3. Example of L-type layout with unique scaling._</small>

### General Scaling

Axis ranges for the contour representation can be set above the preview section at the top right of the window. They are 
the most important axis settings as they define how the central contour is displayed. For the time axis a start and end 
time need to be given; in _lin-log_ mode a cut time (_-//-_) as well at which the axis breaks between linear and logarithmic. 
As amplitude values are visualized by color coding, an amplitude step (Δ) is required in addition to the start and end 
values. The given wavelength range (start and end wavelength) not only corresponds to the contour but to the transient 
spectra too, even in linear mode.

![](images/plot-axes-ranges.png)

| Icon                                                   | Description               |
|--------------------------------------------------------|---------------------------|
| <img src="../images/icons/autoscale.svg" height="48"/> | Apply autoscale settings. |

Triggering the autoscale function will set the plot ranges of the contour representation to reasonable values. Attempts 
are made to determine appropriate amplitude steps as well as the cut times for axis breaks in lin-log mode. The scaling 
of traces or spectra plots will not be affected. For changing their scaling see [below](#scaling-and-scalebar). 
Currently, this feature works rather provisional than professional.

### Ticks

Ticks displayed for the contour's axes are set next to its scaling ranges. For wavelength and time axes all ticks which 
should be displayed need to be defined as a comma separated list. Regarding the time axis it is necessary to attribute 
the tick values to the linear or logarithmic part. In _lin_ and _log_ mode only the respective part is enabled, but in 
_lin-log_ mode values can be entered to either of these. Special interest should be taken on the cut time, which could 
be reasonably attributed to the linear part (prior to the axis break) as well as to the logarithmic part (after the axis 
break).

Ticks of the amplitude axis, which is displayed as a colorbar (see below), are not given as discrete values. Instead,
a step width needs to be given. Starting with 0, all multiples of the step width will be displayed in both, the positive 
and negative, directions of the axis. In the example shown in the screenshot below, the amplitude axis ticks will cover 
the range from -35 to +100 in steps of 15: -30, -15, 0, 15, 30, 45, 60, 75, 90.

![](images/plot-axes-ticks.png)

### Axes Label

| Icon                                                   | Description             |
|--------------------------------------------------------|-------------------------|
| <img src="../images/icons/axislabel.svg" height="48"/> | Open Axes Label Dialog. |

A final plot has five axes, which are labeled. These are
- wavelength axis of contour representation ①, _and_
- amplitude axis of traces plot ②, _and_
- time axis for traces and contour plot ③, _and_
- amplitude axis of the color bar ④, _and_
- wavelength axis for spectra plot ⑤ _or_
- amplitude axis for spectra plot ⑥.

![](images/plot-axeslabel.png)  
<small>_Figure 7. Example plot marking all available axis labels by red circled numbers as enumerated above. Note that 
for explanatory reasons only, this plot features transient spectra at precluding positions 5 and 6 simultaneously._</small>

The label texts can be defined in the Axes Label Dialog. Special characters such as Greek ones can be inserted via LaTeX 
syntax (`$...$`). Additionally, the literal `$UNIT$` will be replaced by the axes unit set in the top right corner of 
the main window. Note, that changing the x-axis unit to "cm<sup>-1</sup>" or "eV" will not change the default label texts to 
"wavenumber" or "energy". 

![](images/plot-axeslabel-dlg.png)  
<small>_Figure 8: Screenshot of the Axes Labels Dialog with values used in Figure 7._</small>

The appearance of the axes labels and numbering (e.g. font size) can be changed in the 
[General Settings Dialog](GeneralSettings.md). For the numbers of the time axis, users can choose between numeric 
(1, 10, 100, ...) and scientific notation (10<sup>0</sup>, 10<sup>1</sup>, 10<sup>2</sup>, ...).

## Colors, Colorbar and Colormaps

### Defining Colors

Any required color can be defined as an RGB tuple, e.g. `128,255,0` or an RGBA tuple, where the fourth number specifies 
the transparency (maximal at 0, full opacity at 255). Additionally, some frequently used colors can be identified by a 
name. The set of pre-defined colors include `b` (blue), `g` (green), `r` (red), `c` (cyan), `m` (magenta), `y` (yellow), 
`k` (black), `w` (white). Other named colors as specified in [this list](https://matplotlib.org/stable/gallery/color/named_colors.html) 
might be used as well. A full list of possible color inputs can be found 
[here](https://matplotlib.org/3.5.0/tutorials/colors/colors.html).

### Selection and Definition of Colormaps

Colormaps are simply lists of colors. A lot of pre-defined ones are available, however, only a few are loaded at program 
start as the list is overwhelming and many of these are rather ugly or useless. Which colormaps are loaded is 
determined in the [predefined.txt](../cmaps/predefined.txt) file. Only those not lead by an `#` (comment) are included 
in the list in the colormap combobox. [This page](ColormapReference.md) gives a visual impression of the 
colormaps.

Users might also define individual colormaps. To this end, one text file per colormap containing a list of RGB tuples 
(see above) has to be stored in the _cmaps_ directory. All files with a `.cmap` extension are added to the list of 
available colormaps. The files need to specify one RGB tuple per line, surrounded by parentheses; lines starting with `#`
will be considered as comments and, thus, ignored. [Examples](../cmaps/bwgyr_Kristoffer.cmap) for valid cmap files can 
be found in the _cmap_ directory.

### Adjusting Colorbars

| Icon                                                  | Description           |
|-------------------------------------------------------|-----------------------|
| <img src="../images/icons/colorbar.svg" height="48"/> | Open Colorbar Dialog. |

Users might choose a colormap to visually indicate amplitude values in the Axes Section next to the axes ranges and 
axes ticks input fields. The drop-down menu only includes those colormaps selected as described in the paragraph above. 
When a colormap is chosen, it will be used as pre-defined without further adjustments. Those can be made in the Colorbar 
Dialog.

![](images/plot-colormap.png)

Colormaps define a range of colors, which is broadcast to a domain of values. For data values subceeding the lowest or 
exceeding the highest domain value, the boundary colors are used by default. For example, if a colormap going from blue 
via green and yellow to red ("jet") is applied to a value domain from -35 to 100, values below -35 are displayed in blue, 
values above 100 in red. This behaviour is indicated by activated tick boxes in the Colorbar Dialog. The 
input fields on the left and right of the colormap drop-down menu define the colors for out-of-domain values. If a field 
is left empty, although the tick box is checked, the boundary color is used. Alternatively, any other color might be 
entered. If a tick box is not checked, no colors are assigned for subceeding (⯇) or exceeding (⯈) values, respectively. 
I.e. values are not displayed at all and will probably appear white (depending on the background color).

A graphical illustration can be set via _Style_. If disabled, the colorbar is drawn only in the defined color range. For 
triangular or rectangular styles, the out-of-bound colors are indicated by triangles or rectangles at the ends 
of the colorbar.

The color assigned to non-real values (NaN, pos. and neg. infinity) is set at the very top right corner of the 
Dialog. The colormaps can be reversed using the 🔁 check box. By clicking the _Update Preview_ button, an instant 
feedback on the final appearance of the colorbar is provided.

![](images/plot-colorbar-dlg.png)  
<small>_Figure 4: Screenshot of the Colorbar Dialog. For demonstration purposes, the color for values subceeding the 
lower bound was set to gray. Exceeding values are not be displayed at all; NaN and infinity values in white._</small>

## Time Traces and Transient Spectra

Wavelengths (or wavenumbers or energies) for which time traces shall be plotted left to the contour representation can 
be defined in the section _Time Traces_. Times for which transient spectra shall be plotted right to or above the 
contour representation can be defined in the section _Transient Spectra_. Both sections contain a table to independently 
adjust the appearance of the additional traces/spectra; the time traces table and transient spectra table, respectively. 
Each row in these tables defines one trace/spectrum.

<small>_Table 4. Time traces and transient spectra table buttons._</small>

| Icon                                 | Description                                                                                                           |
|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| ![](../images/icons/add.svg)         | Add one entry (time trace or transient spectrum) to the table.                                                        |
| ![](../images/icons/add-ext.svg)     | Add one entry for external data to the table.                                                                         |
| ![](../images/icons/remove.svg)      | Remove entry from table.                                                                                              |
| ![](../images/icons/up.svg)          | Move entry one row up. Entries at the top of the list will be drawn first and might be covered by the following ones. |
| ![](../images/icons/down.svg)        | Move entry one row down. Entries at the bottom of the list will be drawn last and on top of the previous ones.        |
| ![](../images/icons/information.svg) | Provides additional information about the table.                                                                      |

### Customization

On user request, labels can be shown at two different parts of the overall plot: Within the contour representation and/or 
the traces plot or spectra plot, respectively. Corresponding check boxes are part of the tables and can 
be set separately. Additional fields allow the precise positioning of the labels as described in detail below.

The label's position can be set via distinct absorption, wavelength and time coordinates. User input is only required 
for those labels which are actually drawn.

<small>_Table 5: Columns of the time traces setup table._</small>

| Table column   | Description                                                                                                                                                                                                                                                                                                                                         | Unit of input value                                                                    |
|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------|
| Wavelength     | Wavelength (or wavenumber or energy), which shall be plotted.<br /><small>In case of external traces, filepath to external data.</small>                                                                                                                                                                                                            | [wavelength] or [wavenumber] or [energy]<br /><small>full or relative filepath</small> |
| Color          | Color of the traces, the labels as well as the wavelength line within the contour representation.                                                                                                                                                                                                                                                   | RGB tuple                                                                              |
| Label          | Label's text of the time trace and the wavelength line. `auto` generates labels following "{_wavelength_}˽{_unit_}".                                                                                                                                                                                                                                |                                                                                        |
| _{check box}_  | First check box enables/disables the label within time traces plot; the second box within contour representation.                                                                                                                                                                                                                                   |                                                                                        |
| ΔA<sub>1</sub> | Label position in x-direction of time traces plot.                                                                                                                                                                                                                                                                                                  | [absorption]                                                                           |
| t<sub>1</sub>  | Label position in y-direction of time traces plot.<br /><small>For lin-//-log plots the time axis coordinates refer to the linear part. If the label is to be drawn within the logarithmic part, its value on the (imaginaryly extended) linear axis needs to be determined by the user via trial-and-error or an educated guess.</small>           | [time]                                                                                 |
| λ<sub>2</sub>  | Label position in x-direction of the contour representation.                                                                                                                                                                                                                                                                                        | [wavelength] or [wavenumber] or [energy]                                               |
| t<sub>2</sub>  | Label position in y-direction of the contour representation.<br /><small>For lin-//-log plots the time axis coordinates refer to the linear part. If the label is to be drawn within the logarithmic part, its value on the (imaginaryly extended) linear axis needs to be determined by the user via trial-and-error or an educated guess.</small> | [time]                                                                                 |
| ↺              | Rotation of the corresponding label.                                                                                                                                                                                                                                                                                                                | °                                                                                      |
| int/ext        | Indicates whether entry is used for internal or external traces (not editable).                                                                                                                                                                                                                                                                     |                                                                                        |
| ΔA (max)       | Upper bound for scaling of external traces.                                                                                                                                                                                                                                                                                                         | [wavelength] or [wavenumber] or [energy]                                               |
| ΔA (min)       | Lower bound for scaling of external traces.                                                                                                                                                                                                                                                                                                         | [wavelength] or [wavenumber] or [energy]                                               |

<small>_Table 6: Columns of the transient spectra setup table._</small>

| Table column  | Description                                                                                                                                                                                                                                                                                                                                         | Unit of input value                      |
|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------|
| Time          | Time, which shall be plotted.                                                                                                                                                                                                                                                                                                                       | [time]                                   |
| Color         | Color of the spectrum and the time line within the contour representation.                                                                                                                                                                                                                                                                          | RGB tuple                                |
| Label         | Label's text of the transient spectrum and the time line. `auto` generates labels following "{_time_}˽{_unit_}".                                                                                                                                                                                                                                    |                                          |
| _{check box}_ | Enables/disables the label within the contour plot.                                                                                                                                                                                                                                                                                                 |                                          |
| λ             | Label position in x-direction of the contour representation.                                                                                                                                                                                                                                                                                        | [wavelength] or [wavenumber] or [energy] |
| t             | Label position in y-direction of the contour representation.<br /><small>For lin-//-log plots the time axis coordinate refers to the linear part. If the label is to be drawn within the logarithmic part, its value on the (imaginaryly extended) linear axis needs to be determined by the user via trial-and-error or an educated guess.</small> | [time]                                   |
| Color         | Color of the label.                                                                                                                                                                                                                                                                                                                                 | RGB tuple                                |
| int/ext       | Indicates whether entry is used for internal or external spectra (not editable).                                                                                                                                                                                                                                                                    |                                          |
| ΔA (max)      | Upper bound for scaling of external spectra.                                                                                                                                                                                                                                                                                                        | [wavelength] or [wavenumber] or [energy] |
| ΔA (min)      | Lower bound for scaling of external spectra.                                                                                                                                                                                                                                                                                                        | [wavelength] or [wavenumber] or [energy] |

The font size of all labels are set below these tables.

For each time trace a vertical wavelength line is drawn within the contour representation to mark the wavelength which 
trace is plotted. Analogous, for each transient spectrum a horizontal time line is drawn to mark the time that's spectrum 
is plotted. Their line widths and line styles can be set below the tables for all wavelength lines or all time lines 
together.

Vertical wavelength lines can be expanded to the transient spectra plot (if it is drawn on top of the countour) by 
activating the _Expand lines to spectra_ check box. Horizontal time lines can be expanded to the traces plot by 
activating the _Expand lines to traces_ check box. The [nsIR benchmark dataset](../tests/benchmark-data/plotting/nsIR_data.png) 
shows an example of that behaviour.

![](images/plot-timelines.png)

### Scaling and Scalebar

The time traces' time axis is synchronous to the contour ones. Therefore, the scaling can not be set separately. However, 
the absorption axis' limits can be defined below the time traces table at _Scaling_ as a tuple of two numbers 
("{_upper bound_},{_lower bound_}"). The axis' ticks are set at _Ticks_ as a comma separated list.

![](images/plot-scaling.png)

If the transient spectra are displayed on the right of the countour representation, the time lines are extended into the 
spectra plot and serve as baseline to mark each spectrum's difference absorption ΔA = 0. I.e. the spectra do not share a 
common amplitude axis and, thus, no absorption values or ticks are given on the y-axis. Instead, a scaling factor has to 
be defined in _Scaling_ below the transient spectra table to change the spectra's heights. This scaling factor must not 
be zero.

If the transient spectra are displayed above the contour representation, no vertical shift happens. Instead, a scaling 
range needs to be defined by "{_upper bound_},{_lower bound_}", just as for the time traces.

| Icon                                                  | Description           |
|-------------------------------------------------------|-----------------------|
| <img src="../images/icons/scalebar.svg" height="48"/> | Open Scalebar Dialog. |

The button _Scalebar_ opens a dialog to adapt a scaling bar within the spectra plot as a compensation for the 
missing axis label. The dialog allows to set the height, labeling and position of the scale bar.

The position of the Scalebar in vertical direction is set in arbitrary units as every spectrum has its own unique y-axis.
The scaling factor is defined in a way that spectra with amplitudes in the order of several tens or a few hundreds 
are displayed properly with a scaling factor of around 1. In other cases it may need to be adjusted 
accordingly. It might also happen, that the scalebar can not be seen, although it is set to _show_, if its position 
value is much larger than the scaling factor. For example, with a scaling factor set to 0.5, the visibility window of 
the scalebar is between -50 and 100 a.u. Therefore, a position of 120 a.u. will technically display the scalebar, but 
outside the plot and invisible for the user. It is guaranteed that the scalebar is visible for a position of 0 a.u.

![](images/plot-scalebar-dlg.png)  
<small>_Figure 5: Screenshot of the Scalebar Dialog with default values._</small>

Sometimes it might be necessary to scale a time trace or transient spectrum independently of the others, e.g. if 
traces shall be plotted, which amplitudes are orders of magnitude apart. In those cases no scaling will be found that 
displays traces with higher and lower amplitudes satisfactorily. Thus, a trace or spectrum can be multiplied with an 
arbitrary factor. That factor needs to be specified in the first column of the setup table as 
"{_time or wavelength_}˽*˽{_factor_}" and will also be considered if auto labeling is turned on. An example is shown in the 
screenshot below; its resulting output can be seen [here](images/plot-multiply-traces-output.png).

![](images/plot-multiply-traces-input.png)  
<small>_Figure 6: Screenshot of the setup table with a wavelength trace using arbitrary scaling (upper row) and no scaling
(lower row)._</small>

Note that for transient spectra this feature is only available in L-type layout. In linear layout transient spectra 
will not be multiplied with any factor specified. 

### Baselines

| Icon                                                  | Description           |
|-------------------------------------------------------|-----------------------|
| <img src="../images/icons/baseline.svg" height="48"/> | Open Baseline Dialog. |

The traces as well as spectra plot (only available when plotted above the contour) can be extended 
by a baseline, typically drawn at ΔA = 0. The Baseline Dialog allows for detailed adjustment of the baselines regarding 
line style, width, etc. By adjusting their _zorder_ the baselines can be shifted to lower layers (traces and spectra are 
plotted above and might cover the baseline) or to upper layers (baseline plotted above traces or spectra).

Note that the amplitude value, at which a baseline is drawn, defaults to 0, but can be set to any value. Both, time 
traces and transient spectra plots, allow only one baseline each. If further lines are required, consider using the 
[Additional Lines Dialog](#additional-lines-and-rectangles).

![](images/plot-baseline-dlg.png)  
<small>_Figure 9: Screenshot of the Baseline Dialog with default values and the baseline activated for the traces plot._</small>

### External Traces/Spectra

The time traces and transient spectra plots are supposed to display parts of the data in more detail. In some cases one 
might wish to additionally include external datasets into the plot, e.g. to compare the time traces at one wavelength with the 
one of another sample. To do so, the user has to add a row to the corresponding table via the yellow add button (cf. 
Table 4). Most columns of the table remain their effect. However, the first column now does not take a wavelength or 
time, but a filepath to the destination of the external data file (e.g. `C:\data\trace.dat`). That file needs 
to be a text file containing exactly two data columns. Several [examples](../tests/benchmark-data/plotting/fsTA_data_1_1MN.dat) 
can be found in the benchmark data of the test directory.

![](images/plot-external-spectra.png)

Scaling of the external datasets needs to be set independently via the _ΔA (max)_ and _ΔA (min)_ columns. They 
correspond to a regular scaling range with _ΔA (max)_ being the value of the upper bound of the plot range and _ΔA (min)_ 
the lowest displayed value. If external time traces should be scaled identical to the internal ones, both ranges have 
to be set manually to the same bounds as in the _Scaling_ field below the time traces table. Note that external transient 
spectra can only be scaled identical to the internal ones, if the plot layout is set to L-type: Only then have the 
spectra a proper and common amplitude axis. Flipping _ΔA (max)_ and _ΔA (min)_ will lead to the data being displayed 
inverse, which might be of special interest when comparing transient difference absorption spectra with ground state 
spectra, for example.

## Additional Lines and Rectangles

| Icon                                                     | Description                   |
|----------------------------------------------------------|-------------------------------|
| <img src="../images/icons/add-artists.svg" height="48"/> | Open Additional Lines Dialog. |

The Additional Objects Dialog allows drawing lines and (possibly filled) rectangles anywhere to the overall plot without 
restrictions. Their main purpose is to connect two points in two different parts of the overall plot.
[This example](images/plot-artists-example.png) shows several possible use cases:
- Drawing a line from the contour representation to the transient spectra plot to mark a delay time, which is outside 
the plotting ranges (cyan line at '14.7 ns'). Without the additional line, the external spectrum would have no 
baseline for ΔA = 0.
- Marking a specific wavelength within the transient spectra plot (magenta line at 525 nm).
- Highlighting a region within the traces plot by drawing a rectangular margin around it (cyan box around signal increase at long delay times).
- Marking a wavelength range in the transient spectra plot as stimulated emission (blue bar from 445 nm to 510 nm) 

![](images/plot-artists-dlg.png)  
<small>_Figure 10. Screenshot of the Additional Lines Dialog as it was used to generate the example mentioned in the text
above._</small>

The Additional Lines and Rectangles Dialog consists of a table with one object per row. For lines (blue add button), x 
and y coordinates need to be given as well as the plot these coordinates refer to. In case of rectangles (yellow add 
button), these points mark two opposing corners of the rectangle. Table 7 gives an overview about all required columns.

<small>_Table 7: Columns of the additional lines setup table._</small>

| Table column                   | Description                                                                                                                                             |
|--------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| Subplot #<i></i>1              | Subplot for coordinates with index 1.                                                                                                                   |
| λ<sub>1</sub> / ΔA<sub>1</sub> | x coordinate for start point, either wavelength (contour, spectra) or amplitude (traces).                                                               |
| t<sub>1</sub> / ΔA<sub>1</sub> | y coordinate for start point, either time (contour, traces, spectra) or amplitude (spectra).                                                            |
| Subplot #<i></i>2              | Subplot for coordinates with index 2.                                                                                                                   |
| λ<sub>2</sub> / ΔA<sub>2</sub> | x coordinate for end point, either wavelength (contour, spectra) or amplitude (traces).                                                                 |
| t<sub>2</sub> / ΔA<sub>2</sub> | y coordinate for end point, either time (contour, traces, spectra) or amplitude (spectra).                                                              |
| Line Color                     | Line color. For rectangles, this is the margin's color.                                                                                                 |
| Linestyle                      | Line style.                                                                                                                                             |
| Linewidth                      | Line width. Might be 0 to draw rectangles without a margin.                                                                                             |
| Fill Color                     | Fill color for rectangles; might be blank. For lines, this column is not used.                                                                          |
| zorder                         | Stacking position in case the line overlaps with other lines, labels, etc. Line will be on top with high values or might be hidden, when zorder is low. |

## Implementation Details

_To be expanded in the future. Until now, please refer to the docstrings in the source code files._
_Main resources:_ [`plot.py`](../src/pychart/core/plot.py) [`PlotTab.py`](../src/pychart/gui/PlotTab.py)