# Importing and Manipulating fsTA Data from NetCDF Files

*Last updated: June 05, 2022 by K.A.T.*
<!-- IF YOU INTEND TO UPDATE THIS FILE: Please consider to also update the other ImportTab's documentations as they have 
many parts (regarding Data Table, Time Traces and some paragraphs about corrections, scans etc.) in common. -->

The ImportNetCDF module allows users to import raw data from the Gilch group's fsTA experiment and apply fundamental 
correction and manipulation routines. The resulting two-dimensional data is immediately displayed as graphical traces 
and in an editable spreadsheet.

This documents aims at describing _how_ to apply corrections to fsTA data. For information on the data acquisition and 
the scientific background of these routines see, for example, 
[this thesis](https://docserv.uni-duesseldorf.de/servlets/DerivateServlet/Derivate-62905/Dissertation-Thom-final-pdfa.pdf). 

_**Table of Contents**_
<!-- Manual TOC is very ugly, but sections are unlikely to change, nor are additional ones expected. -->
- [**General**](#general)
  - [Data format requirements](#data-format-requirements)
  - [Exporting corrected data](#exporting-corrected-data)
- [**Importing Sample Data and Calculation of Difference Absorption**](#importing-sample-data-and-calculation-of-difference-absorption)
  - [Chopper Modes](#chopper-modes)
  - [Selecting Scans](#selecting-scans)
- [**Data Manipulation**](#data-manipulation)
  - [Background Correction](#background-correction)
  - [Solvent Subtraction](#solvent-subtraction)
  - [Gate Correction](#gate-correction)
  - [Sellmeier Correction](#sellmeier-correction)
- [**Data Table**](#data-table)
- [**Time Traces**](#time-traces)
- [**Import Single Spectra**](#import-single-spectra)
- [**Merge Raw Data Files**](#merge-raw-data-files)
- [**Implementation Details**](#implementation-details)

![](../images/screenshots/netcdf-import-screenshot.png)  
<small>_Figure 1. Screenshot of the ImportNetCDF module of pyChart for import and manipulation 
of NetCDF data from fsTA experiments._</small>

## General

<small>_Table 1. Available tool bar actions._</small>

| Icon                                                         | Description                                                                                                                                                                      |
|--------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <img src="../images/icons/calculate.svg" height="48"/>       | Calculate data according to settings.<br /><small>This process might take some time, depending of the size of the input data sets and the corrections which are applied.</small> |
| <img src="../images/icons/open-as-plot.svg" height="48"/>    | Open data in a new [PlotTab](Plotting.md).<br /><small>To this end, the current data table values are exported as ASCII file in the application's `temp` directory.</small>      |
| <img src="../images/icons/export-data.svg" height="48"/>     | Export data table to ASCII file.<br /><small>Output file can be compatible to pychart or Z20 requirements.</small>                                                               |
| <img src="../images/icons/single-spectrum.svg" height="48"/> | Open [Single Spectra Dialog](#import-single-spectra).                                                                                                                            |
| <img src="../images/icons/merge-files.svg" height="48"/>     | Open [Merge Raw Data Files Dialog](#merge-raw-data-files).                                                                                                                       |

### Data format requirements

Raw data files for main, solvent, gate and background data need to be given as NetCDF files. The exact structure has to 
follow those described in more detail [here](fsTA.md). Note that the file structure of background files differs from the 
other's.

If more than one file is imported, i.e. as part of the correction routines, the wavelength and time axes need to match 
– otherwise corrections could not be applied. Thus, the axes of the input files are checked for consistency. These checks 
are subject to tolerances, which can be set independently for wavelength and time axes in the 
[General Settings Dialog](GeneralSettings.md#axes-tolerances-at-consistency-checks). In particular, the following 
consistency checks are conducted:
- Wavelength axes of main, solvent and gate datasets need to be identical.
- Time axes of main and solvent need to be identical. However, the solvent's time axis might just be subset of the main 
data's axis. In that case time axis values until the last time at which the solvent was measured require identity. For 
all following delay times of the main data, the solvent data is subtracted as 0.
- Wavelength axes of background files, if any, need to be identical to their major data wavelength axis. 

### Exporting corrected data

| Icon                                                     | Description                      |
|----------------------------------------------------------|----------------------------------|
| <img src="../images/icons/export-data.svg" height="48"/> | Export data table to ASCII file. |

The data currently displayed in the data table can be exported as ASCII file, which is guaranteed to be compatible with 
pycharts [PlotTab](Plotting.md) module. The data table is exported "as is": Changes made to the data after calculation 
(e.g. replacing NaNs) will be incorporated. To multiply all values with a fixed factor to shift the order of magnitude 
to manageable values, refer to the [General Settings Dialog](GeneralSettings.md#multiplying-data-at-export).

Exporting the data using the toolbar icon is equivalent to use _Import_ 🠖 _Export Data_. Additionally, the data can be 
exported with a header as required by Z20 via  _Import_ 🠖 _Export Data (Z20)_.

## Importing Sample Data and Calculation of Difference Absorption

The experiment's main dataset and related correction routines can be set in the corresponding _Main Data_ group. After 
loading a data file, the number of wavelength channels, delay time positions, single measurement per data point, chopper 
positions and available scans are displayed. 

![](images/netcdf-main.png)

### Chopper Modes

Depending on the number of chopper positions, the sample's difference absorption ΔA might be calculated from 
_Four Cycle Mode_ or _Two Cycle Mode_ data. If four chopper positions were used during the data acquisition, the 
calculation can still be limited to the two cycle mode.

Instead of calculating the difference absorption, users can also determine a single chopper position's data, i.e. probe,
pump + probe and possibly pump and background data. These data is averaged over all single measurements and selected scans, but can 
not be corrected any further, However, it might be used in the data table, as time traces or opened as plot as usual.  

### Selecting Scans

By default, all measurement scans are considered during the calculation process by averaging over them. In case users 
wish to exclude certain scans due to quality detriments or would like to inspect them individually, scans can be selected 
and deselected independently. To this end, toggling the _Selection_ button will enable the drop-down-menu.

![](images/import-scans.png)

## Data Manipulation

The sample's data gathered from fsTA experiments can and needs to be corrected in various ways. The corrections can 
be divided into groups: Corrections, which are applied as part of a correction chain in a certain order to the sample 
data and that might require additional input, and pre-corrections, which are intrinsic to the single corrections of 
the chain and applied to the data that is used for that correction.

The correction chain consists of the following parts in the given order:
1. [**Solvent Correction**](#solvent-subtraction): Subtraction of solvent from the sample. This requires an additional 
solvent data file as well as a scaling factor.
2. One of the following corrections of different time zero points due to dispersion:
   - [**Gate Correction**](#gate-correction): This requires an additional gate measurement data file.
   - [**Sellmeier Correction**](#sellmeier-correction): This requires additional user inputs about the experimental setup.

The sample (main) data as well as the solvent and gate data used in any of the correction chain steps can be subject to 
the following pre-corrections:
- [**Background Correction**](#background-correction): A background spectrum is subtracted from the dataset.
_(Only available in Two Chopper Mode.)_ 
- **Reference Correction**: Uses the very first time point as a reference measurement, at which the signal should be 
exactly 0. The measured value at this point is _set to_ zero by subtracting it from all values. This correction only
works properly, if the delay time is considerably negative. _(Not available for gate measurements.)_

![](images/netcdf-bg-ref-corr.png)

All corrections (chain corrections and pre-corrections) are independent of each other and might be selected by the user 
as needed.

### Background Correction

While in Four Cycle Mode background spectra are an intrinsic part of the experiment's operation, in Two Cycle Mode no such 
spectra are measured. Thus, in this (and only this) mode, an additional background file might be loaded and subtracted from 
their major data. Although such a correction is eligible for scientific reasons, the performance of the calculation process 
is significantly affected and on low-end computers it might fail due to scarce memory. For quick-and-dirty measurements, 
it is therefore recommended to forgo the background correction and to use the Four Cycle Mode for all "production" measurements.

### Solvent Subtraction

To subtract properly weighted solvent measurements from the main data, the corresponding group box needs to be activated. 
Prior to the subtraction, the same settings regarding ΔA calculation and pre-corrections as for the main data might 
be applied to the solvent data. Also, the scans considered in the averaging process, can be selected as usual. Refer to 
the explanations in the previous paragraphs for more details.

The scaling factor with which the solvent data is weighted during the subtraction can be set in four different ways as 
listed in Table 1. Most methods use the sample's absorption _A_ (with respect to the path length effectively employed in 
the experimental setup) to calculate the scaling factor _f_. This calculation is done internally by pychart. 

![](images/netcdf-solvent-factor.png)

<small>_Table 2. Available methods for the determination of solvent factors._</small>

| Method         | Formula                                | Notes                                                    |
|----------------|----------------------------------------|----------------------------------------------------------|
| _Absorption_   | f = 1 - 10<sup>-A</sup>                |                                                          |
| _Transmission_ | f = 10<sup>-A</sup>                    |                                                          |
| _Factor_       | f = (1 - 10<sup>-A</sup>) / (ln(10)⋅A) | [Lorenc _et al._](https://doi.org/10.1007/s003400100750) |
| _free_         | f                                      | arbitrary factor as entered by the user                  |

Additionally, a constant time shift might be specified to account for drifts arising during the measurement. Checks for 
time axes consistency as describe [above](#data-format-requirements) are not affected by this shift.

![](images/netcdf-solvent-shift.png)

### Gate Correction

One possible method to correct time-resolved data for dispersion is based on a Kerr Gate measurement. To this end, the 
corresponding group box needs to be activated and necessary data files been loaded. Note that Gate and Sellmeier 
corrections can not be applied together.

![](images/netcdf-gate.png)

During the Gate correction, the time zero point for every wavelength is derived from the maximum signal intensity 
observed in the gate measurement. The determination of the signal intensity can rely on the calculation of a 
signal _difference_ or a difference absorption (signal _quotient_); as usual either four or two cycle modes might be 
used for that calculation. 

Pressing the _Setup_ button will open the Gate Correction Setup Dialog, which allows to refine the resulting time zero 
points. However, it is not necessary to use that dialog, as the default settings will apply a straightforward and purely 
data based correction. Just like for the solvent correction, the time zeros points resulting from the gate correction 
can be shifted in time relatively to the sample. 

![](images/netcdf-gate-dlg.png)  
<small>_Figure 2. Screenshot of the Gate Correction Setup Dialog with default settings._</small>

Within the _Determination of time zero point_ section, users might further define the signal processing. From 
the calculated signal difference or difference absorption the maximum signal is interpret as the time zero point _t_<sub>0</sub>. This 
maximum might be determined in two ways: be means of a Gaussian fit or a simple search for the maximal value. If set to
_Gaussian_, the time traces for every wavelength will be fitted with a standard Gauss function using the parameter 
settings given by the user. The center of that Gaussian is considered to be the time of the maximum signal intensity and 
set as time zero point. The _Maxima_ option provides a more trivial alternative. Therein, the times featuring the highest 
signal values are considered to be the time zeros for their respective wavelengths. As this might lead to problems 
arising at the end points of the time traces due to measurement artifacts, the method relying on a Gauss fit should be 
preferred. 

The time traces considered in the analysis of time zero points can be restricted in the 
[General Settings Dialog](GeneralSettings.md#gate-correction-time-range). Additionally, if using Gaussians, the range 
for the center fit parameter can be restricted as well. Table 3 gives an impression on the differences occurring from 
the two different methods of time zero determination.

<small>_Table 3. Methods for the determination of time zero points from gate measurements._</small>

| Method          | Result                               |
|-----------------|--------------------------------------|
| _Gaussian_      | ![](images/netcdf-gate-gaussian.png) |
| _Maxima_        | ![](images/netcdf-gate-maxima.png)   |

After the determination of _t_<sub>0</sub>, the results might be post-processed to flatten the dispersion curve. A 
moving average (_Smooth_) over up to 20 data points allows for a very quick correction process. More fine results can be achieved 
using a Sellmeier fit. To this end, the dispersion curve is fitted to a Sellmeier function using the settings as 
given by the user. The Sellmeier coefficients are fixed to arbitrary but reasonable values. Within the specified 
wavelength range (highlighted in red) the data is fitted, yielding a thickness _d_ and an _offset_. If the wavelengths 
are confined to a range in which the dispersion curve is rather well-formed, the resulting fit curve will provide a neat 
extrapolation for time zero points outside that wavelength range. Thus, the ends of the dispersion curve, for which 
usually no time zero determination is feasible, do not need to be excluded from all following data correction and 
interpretation. Table 4 gives an overview on the different post-processing methods and their potential.

<small>_Table 4. Post-processing methods for time zero points from gate measurements._</small>

| Post-Processing | Result                                   |
|-----------------|------------------------------------------|
| _None_          | ![](images/netcdf-gate-none.png)         |
| _Smooth_        | ![](images/netcdf-gate-smooth.png)       |
| _Sellmeier Fit_ | ![](images/netcdf-gate-sellmeierfit.png) |

### Sellmeier Correction

To apply a correction for dispersion based on the Sellmeier (or Cauchy) equation, the corresponding group box 
needs to be activated. A list of materials causing dispersion can be generated by selecting materials from the drop-down 
menu and entering corresponding thicknesses. Additionally, an additive _offset_ time can be defined.

For a detailed description of the correction itself and a full list of available materials, see 
[here](SellmeierCorrection.md).

![](images/import-sellmeier-widget.png)

<small>_Table 5. Sellmeier widget buttons._</small>

| Icon                                               | Description                                |
|----------------------------------------------------|--------------------------------------------|
| <img src="../images/icons/add.svg" height="24">    | Add material with given thickness to list. |
| <img src="../images/icons/check.svg" height="24">  | Update selected material to new thickness. |
| <img src="../images/icons/remove.svg" height="24"> | Remove material from list.                 |

## Data Table

The data resulting from the calculation process is displayed in a table with its wavelengths' data in columns and time 
data in rows. In principle, the data is read-only, but might be edited using the buttons on the right side of the table.
Data can be deleted or added as full columns or rows. Additionally, NaN or infinity entries can be replaced by user 
defined scalar values.

<small>_Table 6. Data table edit buttons._</small>

| Icon                                                    | Description                                                                                                                              |
|---------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|
| <img src="../images/icons/add-col.svg" height="24">     | Add column for a specified wavelength and fill with specified values. The new column is inserted in front of the currently selected one. |
| <img src="../images/icons/remove-col.svg" height="24">  | Delete currently selected wavelength column.                                                                                             |
| <img src="../images/icons/add-row.svg" height="24">     | Add row for a specified time and fill with specified values. The new column is inserted above of the currently selected one.             |
| <img src="../images/icons/remove-row.svg" height="24">  | Delete currently selected time row.                                                                                                      |
| <img src="../images/icons/replace-nan.svg" height="24"> | Replace `NaN` with specified value.                                                                                                      |
| <img src="../images/icons/replace-inf.svg" height="24"> | Replace positive and/or negative `inf` with specified value.                                                                             |
| <img src="../images/icons/edit.svg" height="24">        | Toggle between read-only and manual-edit mode.                                                                                           |

| Add wavelength dialog              | Add time dialog                     | Replace NaN dialog                     | Replace inf dialog                     |
|------------------------------------|-------------------------------------|----------------------------------------|----------------------------------------|
| ![](images/import-add-wln-dlg.png) | ![](images/import-add-time-dlg.png) | ![](images/import-replace-nan-dlg.png) | ![](images/import-replace-inf-dlg.png) |

Triggering the edit button will switch the table from read-only to edit mode. Theoretically, any value might be 
changed to any other value by hand. Please check the necessity of this feature prior using...

## Time Traces

As soon as a data calculation is complete for the first time, it is possible to display the data as independent time 
traces for each wavelength. To this end, the dataset to be displayed needs to be selected using the colored 
buttons for data, sample and solvent. Regarding the time traces for sample and solvent, the corresponding 
pre-corrections (e.g. averaging over certain scans, reference point correction, etc.) are applied – this includes the 
solvent factor. The data channel will show the calculation result as displayed in the data table after applying all 
user-defined corrections.

Scaling of time traces can be adjusted manually or set to autoscaling. In addition to plot ranges the time axis scale 
might be set to linear or (symmetrical) logarithmic.

![](images/import-traces-scaling.png)

It is possible to show several time traces simultaneously in layouts with one or two columns and up to five rows. The 
consecutive time traces plotted in such a grid is referred to as a set of traces. Using the arrow buttons allows to move 
through the wavelengths of the data set.

![](images/import-traces-gridset.png)

<small>_Table 7. Time traces control buttons._</small>

| Icon                                 | Description                                               |
|--------------------------------------|-----------------------------------------------------------|
| ![](../images/icons/go-first.svg)    | Go to first wavelength traces.                            |
| ![](../images/icons/go-back.svg)     | Move currently visible set of traces to the previous set. |
| ![](../images/icons/go-previous.svg) | Move time traces one wavelength back.                     |
| ![](../images/icons/go-next.svg)     | Move time traces one wavelength forward.                  |
| ![](../images/icons/go-forward.svg)  | Move currently visible set of traces to the next set.     |
| ![](../images/icons/go-last.svg)     | Go to last wavelength traces.                             |
| ![](../images/icons/go-to.svg)       | Go to time trace for the specified wavelength.            |

The time traces are only updated if changes to the data table are made. In all other cases, such as re-calculation or
selection/deselection of data channels, the _Update_ button has to be triggered manually to review changes. 

## Import Single Spectra

| Icon                                                         | Description                 |
|--------------------------------------------------------------|-----------------------------|
| <img src="../images/icons/single-spectrum.svg" height="48"/> | Open Single Spectra Dialog. |

Background files, as specified [here](fsTA.md), were intended to serve as background spectra for the correction of 
two cycle mode measurements. In this context, they can be seen as single spectra files, that were measured at one delay 
time for only one scan. Yet they contain a regular wavelength axis, two or four different chopper positions and a single  
measurements dimension. It is therefore possible to measure single spectra at any delay time and to store them as "background" 
file without the need for full delay line scans.

In fact for measurements at 14.7 ns or 18.1 ns this is the only way to measure a spectrum. Furthermore, if spectra at 
specific delay times are required with high quality, this can be used to accumulate a very large number of spectra for 
averaging (500 measurements in almost three minutes). Especially for samples with very low signal intensities, this might 
be helpful, as full delay line scans are limited to 20 single measurements in four scans due to software restrictions 
– and if they wouldn't the measurement time for such an effort was not justifiable.

![](images/netcdf-single-spectrum-dlg.png)  
<small>_Figure 3. Screenshot of the Import Single Spectra Dialog._</small>

In general, the settings available in the Single Spectrum Dialog are analogous to those for the regular NetCDF import 
described in this document. For information on the data table and possible manipulations, refer to the respective 
[section](#data-table) and Table 6; for possible adjustments of the displayed preview spectrum, refer to the 
[time traces sectinon](#time-traces).

The available corrections are limited for obvious reasons. In particular, the sample data is not up for a reference 
correction. This can be compensated using a correction measurement. That can either contain a solvent spectrum (and 
scaled with an appropriate scaling factor, see [above](#solvent-subtraction)) or a dark background spectrum, that is 
used as a reference point (at which the difference absorption should be exactly 0). Note that the option _as Reference 
Correction_ is equivalent to choosing _as Solvent Correction_ with the solvent factor set to _f_ = 1 in _free_ mode.  

## Merge Raw Data Files

| Icon                                                     | Description                       |
|----------------------------------------------------------|-----------------------------------|
| <img src="../images/icons/merge-files.svg" height="48"/> | Open Merge Raw Data Files Dialog. |

The Merge Files Dialog allows users to combine scans from different raw data files into one. This makes it possible to 
average over scans, which have been originally stored in different files. Possible use cases include:
- Increase data quality by averaging over more than four scans, which is the maximum number of scans that can be stored 
into one file by the experiment's software.
- Repeating the measurement for multiple aliquots of the sample (e.g. because of its photoreactivity) and treating them as one.
- Duplicating certain scans by adding them to the list more them once. In doing so, weighting factors can be introduced 
into the averaging process.

![](images/netcdf-merge-dlg.png)  
<small>_Figure 4. Screenshot of the Merge Raw Data Files Dialog. In this example, files can not be merged with each 
other, as the second file mismatches regarding the number of delay times and chopper positions as well as wavelength 
axis values._</small>

To merge files, they need to be selected through the open file dialog and added to the list using the blue add button. 
Each file will appear with its dimensions regarding wavelengths, delay times, single measurements and chopper positions. All 
dimensions need to match exactly, to facilitate merging them. The first file that was added to the list is considered 
as master file, that defines the required dimensions. If other files match, their data is printed in green, while 
mismatching files are printed in red. In case of wavelength and time dimensions, not only the number but also their axis 
values need to match.

Files on the list can be expanded to individually select or deselect scans. With clicking on the _Merge_ button, all 
scans that are selected and match the master file are merged into the outfile as chosen by the user. That outfile is 
guaranteed to be compatible with pychart's ImportNetCDF module.

## Implementation Details

_To be expanded in the future. Until now, please refer to the docstrings in the source code files._
_Main resources:_ [`netcdfimport.py`](../src/pychart/core/netcdfimport.py) [`NetCDFImportTab.py`](../src/pychart/gui/NetCDFImportTab.py)
