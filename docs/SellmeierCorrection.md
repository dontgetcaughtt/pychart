# Correction of Time Zero Shifts Caused by Dispersion

The time zero correction of Kerr-Gate measurements always and of fsTA measurements might rely on a calculation of the
expected time shifts caused by the materials present in the set-up. To this end, information about the refractive
indices of the materials and their (effective) thickness is needed. The latter is entered by the user and is subject to
changes based on the dataset. The former is provided by pychart in terms of parametrization coefficients as described
in the following.

## Available Data Sets

### Sellmeier coefficients

In the first place, calculation of time shifts via the Sellmeier equation is attempted. Therein, the wavelength-dependent
refractive index of a material is calculated based on Sellmeier coefficients _B<sub>i</sub>_ and _C<sub>i</sub>_. They 
are provided in the `data/sellmeier-coefficients.json` file. When adding new materials to this file, note that 
coefficients _B_ are dimensionless, while _C_ needs to be given in nm<sup>2</sup>.

### Cauchy coefficients

In case no Sellmeier coefficients can be found for a given material, they might be estimated based on Cauchy's 
equation. Corresponding coefficients _A_, _B_, _C_, ... are searched for in 
`data/cauchy-coefficients.json`. When adding new Cauchy coefficients to this file, they need to be provided in 
magnitudes of nm (e.g. [_B_] = nm<sup>2</sup>, [_C_] = nm<sup>4</sup>) 

### Refractive indices

If no Sellmeier or Cauchy coefficients for the estimation of _n_ are available, refractive indices at a single
wavelength can be used. Values in `data/refractive-indices.json` should  correspond to those measured at the D-line of 
sodium (589 nm). In most cases differences within the visible region are rather small. For the UV, a time zero 
correction simply based on this single value is probably not appropriate. Thus, efforts to provide Sellmeier or Cauchy 
coefficients should always be made.

### Permittivities

Relative permittivities given in `data/relative-permittivities.json` provide a last fall-back to estimate the refractive 
index, if it can not be measured directly. As this attempt does not account for dispersion and results in a 
wavelength-independent value for _n_, remarks from the previous paragraph still apply. Furthermore, deviation resulting 
from this kind of estimation are rather large.

## Notes

### Difference Between fsTA and Kerr Gate Measurements

The dispersion curves observed in Kerr Gate measurements match the well known banana-shaped curves for any kind of 
dispersion: The red parts of the spectrum arrive earlier at the detector than the blue components. However, in fsTA 
experiments dispersion curve is flipped horizontally, with blue spectral components seemingly faster than the red ones.

An often misunderstanding is that this is caused by the fact that the pump gets delayed within our experimental set-up. 
Actually this doesn't matter, as the relative (!) delay time already accounts for this. From the view of the laser 
pulses, no differences can be seen in the delay time between pump and probe, regardless which beam takes "extra rounds". 
In fact, the flipped dispersion curve is a result of the experimental design, which is a race experiment. As the blue 
components are slower than the red ones, they are caught up by the pump first. At this time the red components are still 
ahead of the pump and thus seen at longer delay times.

### Details of Calculation

The calculation of time zero is based on the time the light needs to travel through a material of a certain thickness. 
As the speed of light depends on the refractive index, different materials cause different time shifts. Furthermore, 
the refractive index depends on the wavelength (aka dispersion), thus, the time shift also differs for different 
wavelengths. The time zero correction therefore requires knowledge of the material's refractive index _n_, which might 
be estimated based on the methods described above. From these (ideally wavelength-dependent) values, the group velocity 
index is calculated. Using the speed of light in vacuum, time shifts are yielded.

These time shifts are absolute values, typically in the range of tens of picoseconds. In our set-ups the experimental 
time zero is usually set to a value close to the actual one, by shifting the delay line to reasonable values of a few 
picoseconds. To account for this in the analysis, time shifts calculated by the sellmeier module of pychart need to be 
extended by a constant offset. 

For more detailed information on the algorithms please refer to the documentation in 
the implementation (`src/core/sellmeier.py`). 

## References

**Sellmeier Coefficients**
- CaF<sub>2</sub>: [1]
- Silica: [2]
- 1737F
- Water
- Tolulene (Tol)

**Cauchy Coefficients**
- Acetone: [3]
- Acetonitrile (MeCN): [4]
- Cyclohexane (CX): [3]
- Ethanol (EtOH): [4]
- _n_-Hexane (nHex): [4]
- Isopropanol (iPrOH): [4]
- Methanol (MeOH): [4]

**Refractive Indices**
- Benzene: [5]
- Chloroform (CHCl3): [5]
- Dichloromethane (DCM): [5]
- Dimethyl Sulfoxide (DMSO): [5]
- Ethyl Acetate (EtOAc): [5]

**Relative Permittivities**

_(no data)_

---
[1] Appl. Opt. **1963**, _2_, 1103-1107
    [doi:10.1364/AO.2.001103](https://doi.org/10.1364/AO.2.001103)

[2] J. Opt. Soc. Am. **1965**, _55_, 1205-1209 
    [doi:10.1364/JOSA.55.001205](https://doi.org/10.1364/JOSA.55.001205)

[3] Meas. Sci. Technol. **1997**, _8_ 601-605
    [doi:10.1088/0957-0233/8/6/003](https://iopscience.iop.org/article/10.1088/0957-0233/8/6/003)

[4] J. Opt. Soc. Am. B **2005**, _22_, 1479-1485
    [doi:10.1364/JOSAB.22.001479](https://doi.org/10.1364/JOSAB.22.001479)

[5] CRC Handbook of Chemistry and Physics **2004** (85th edition)
