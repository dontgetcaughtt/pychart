# Kerr Gate

*Last updated: September 07, 2021 by K.A.T.*

Time-resolved fluorescence data recorded with Momo is stored in the Hierarchical 
Data Format (HDF5). Like NetCDF this is a binary format developed for large and 
complex data sets. One of the major users is - you name it - NASA.

HDF5 files can be loaded in Python with *h5py*. The program *HDF View* allows 
to read and copy (e.g. to Origin) the raw data tables.

## Raw Data Representation (.h5)

> `ccd_pixel_var:` wavelength axis values in nm (contains one row of '0.0' at the end)

> `data_corr:` for some reason identical with `reference`

> `data_raw:` raw spectra

> `delay_time_ps:` time axis values in ps

> `delay_time_steps:` position of the delay line 

> `reference:` photodiode values
> 
>      0   gate
>      1   gate background
>      2   pump
>      3   pump background
>      4   number of laser shots
>      5   '0.0'

> `scan_duration:` elapsed time in minutes (accumulated)

> `scan_temperature:` temperature of the CCD chip in °C

> `scan_thg:` spectra of a THG only scan (usually not used at all)

> `single_spectra:` single spectra for correction
> 
>      0   lamp (black body)
>      1   leak (pump only)
>      2   thg (gate only)
>      3   -∞
>      4   dark (closed)
>      5   background @ start
>      6   background @ end
>      7   wavelength in nm
> 
> For some reason the wavelength is stored as an integer and thus cut off.
> Just as in `ccd_pixel_var` one row of '0.0' is added to the table.

> `single_pd:` photodiode values of single spectra
> 
>      0   '0.0'
>      1   gate ?
>      2   pump ?
> 
> The entry at position 0 might also be '0', '-1', '1' or similar.

## Some Notes about the Algorithms and their Implementation

### Problems Regarding Missing Scans

As long as anyone can remember the very first spectrum of the first scan is likely to not be stored. In these cases the 
number of spectra is exactly one spectrum less than a multiple of the delay time steps. When loading the data, this 
condition is checked, and, if applicable, the first scan is discarded.

More recent problems showing complete mismatches between the number of spectra and the product of scans and time steps 
have been encountered. These are new and not fully understood, yet. As it is fully unknown for these datasets which 
spectra are missing, no genuine assumption on one spectrum's delay time can be done. These sets are unfortunately 
useless. Maybe mistakes on the user side in operating the momo software for recording are to be blamed...

### Determination and Usage of Photodiode Values

In earlier days (until July 2017) photodiode values were not stored correctly for THG and leak measurements. Thus, 
the average value for the pump and gate photodiodes over all single measurements of the dataset was calculated and used 
for scaling the THG and leak spectra prior to subtraction. Of course, this is not really appropriate as the laser 
intensity could have been completely different for these background spectra. A proper photodiode correction is therefore 
not possible with those datasets.

The problem originated probably because the recorded values were stored as integers instead of floats. When loading the 
files, the values are checked for being exactly 0 or 1. If so, the mean value is used as described above. In all other 
case, the values are assumed to be correct.
