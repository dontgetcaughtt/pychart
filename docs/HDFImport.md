# Importing and Manipulating Kerr Gate Data from HDF Files

*Last updated: June 04, 2022 by K.A.T.*
<!-- IF YOU INTEND TO UPDATE THIS FILE: Please consider to also update the other ImportTab's documentations as they have 
many parts (regarding Data Table, Time Traces and some paragraphs about corrections, scans etc.) in common. -->

The ImportHDF module allows users to import raw data from the Gilch group's Kerr Gate experiment and apply fundamental 
correction and manipulation routines. The resulting two-dimensional data is immediately displayed as graphical traces 
and in an editable spreadsheet.

This documents aims at describing _how_ to apply corrections to fsFl data. For information on the data acquisition and 
the scientific background of these routines see, 
[this thesis](https://docserv.uni-duesseldorf.de/servlets/DerivateServlet/Derivate-42710/dissertation_mundt.pdf). 

_**Table of Contents**_
<!-- Manual TOC is very ugly, but sections are unlikely to change, nor are additional ones expected. -->
- [**General**](#general)
  - [Data format requirements](#data-format-requirements)
  - [Exporting corrected data](#exporting-corrected-data)
- [**Importing Sample Data and Calculation of Fluorescence Intensity**](#importing-sample-data-and-calculation-of-fluorescence-intensity)
  - [Selecting Scans](#selecting-scans)
- [**Data Manipulation**](#data-manipulation)
  - [Background Correction](#background-correction)
  - [Solvent Subtraction](#solvent-subtraction)
  - [Sensitivity Correction](#sensitivity-correction)
  - [Sellmeier Correction](#sellmeier-correction)
- [**Data Table**](#data-table)
- [**Time Traces**](#time-traces)
- [**Photodiode Values Plot**](#photodiode-values-plot)
- [**Implementation Details**](#implementation-details)

![](../images/screenshots/hdf-import-screenshot.png)  
<small>_Figure 1. Screenshot of the ImportHDF module of pyChart for import and manipulation 
of HDF data from Kerr Gate experiments._</small>

## General

<small>_Table 1. Available tool bar actions._</small>

| Icon                                                      | Description                                                                                                                                                                      |
|-----------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <img src="../images/icons/calculate.svg" height="48"/>    | Calculate data according to settings.<br /><small>This process might take some time, depending of the size of the input data sets and the corrections which are applied.</small> |
| <img src="../images/icons/photodiodes.svg" height="48"/>  | Open [Photodiode Values Plot](#photodiode-values-plot).                                                                                                                          |
| <img src="../images/icons/open-as-plot.svg" height="48"/> | Open data in a new [PlotTab](Plotting.md).<br /><small>To this end, the current data table values are exported as ASCII file in the application's `temp` directory.</small>      |
| <img src="../images/icons/export-data.svg" height="48"/>  | Export data table to ASCII file.<br /><small>Output file can be compatible to pychart or Z20 requirements.</small>                                                               |

### Data format requirements

Raw data files for main and solvent data need to be given as HDF5 files. The exact structure has to follow those described 
in more detail [here](KerrGate.md). Note that data objects `single_spectra` and `single_pd` can be split into major and 
background files, or combined into one file (cf. [below](#data-manipulation)). The same applies analogously for the lamp file.

If more than one file is imported, i.e. as part of the correction routines, the wavelength and time axes need to match 
– otherwise corrections could not be applied. Thus, the axes of the input files are checked for consistency. These checks 
are subject to tolerances, which can be set independently for wavelength and time axes in the 
[General Settings Dialog](GeneralSettings.md#axes-tolerances-at-consistency-checks). In particular, the following 
consistency checks are conducted:
- Wavelength axes of main, solvent and lamp datasets need to be identical.
- Time axes of main and solvent datasets need to be identical.

### Exporting corrected data

| Icon                                                     | Description                      |
|----------------------------------------------------------|----------------------------------|
| <img src="../images/icons/export-data.svg" height="48"/> | Export data table to ASCII file. |

The data currently displayed in the data table can be exported as ASCII file, which is guaranteed to be compatible with 
pycharts [PlotTab](Plotting.md) module. The data table is exported "as is": Changes made to the data after calculation 
(e.g. replacing NaNs) will be incorporated. To multiply all values with a fixed factor to shift the order of magnitude 
to manageable values, refer to the [General Settings Dialog](GeneralSettings.md#multiplying-data-at-export).

Exporting the data using the toolbar icon is equivalent to use _Import_ 🠖 _Export Data_. Additionally, the data can be 
exported with a header as required by Z20 via  _Import_ 🠖 _Export Data (Z20)_.

## Importing Sample Data and Calculation of Fluorescence Intensity

The experiment's main dataset and related correction routines can be set in the corresponding _main data_ group. After 
loading a data file, the number of wavelength channels, delay time positions and available scans are displayed.

### Selecting Scans

By default, all measurement scans are considered during the calculation process by averaging over them. In case users 
wish to exclude certain scans due to quality detriments or would like to inspect them individually, scans can be selected 
and deselected independently. To this end, toggling the _Selection_ button will enable the drop-down-menu.

![](images/import-scans.png)

In was found, that some data files are inconsistent regarding the number of stored spectra _n_ compared to the expected 
number based on the product of delay times and scans _n'_ = _n_<sub>times</sub> ⋅ _n_<sub>scans</sub>. Obviously, sometimes not all spectra are 
saved properly. Thus, the following rules are implemented to determine the number of actually available scans:

1. If the number of stored spectra equals the product of delay times and scans (_n = n'_), the file is self-consistent and all scans are available.
2. If the number of stored spectra is exactly one spectrum short of the expected amount (_n = n'_ - 1), it is assumed that 
the very first spectrum was not stored. The remaining spectra of that first scan are discarded and the number of available scans is 
reduced by one. For the main data, this is indicated by `(-1)`.
3. If the number of stored spectra is more than one spectrum short of the expectation (_n = n' - x_), but is yet an integer 
multiple of the number of delay times (_x = k_ ⋅ _n_<sub>times</sub> with _k_ ∈ ℕ), it is assumed that only consecutive 
spectra constituting full scans were not stored. The number of available scans is reduced accordingly. For the main data, 
the initially expected number of scans is indicated by `(n_scans?)`.

## Data Manipulation

The sample's data gathered from Kerr Gate experiments can and needs to be corrected in various ways. The corrections can 
be divided into groups: Corrections, which are applied as part of a correction chain in a certain order to the sample 
data and that might require additional input, and pre-corrections, which are intrinsic to the single corrections of 
the chain and applied to the data that is used for that correction.

The correction chain consists of the following parts in the given order:
1. [**Solvent Correction**](#solvent-subtraction): Subtraction of solvent from the sample. This requires an additional 
solvent data file as well as a scaling factor. 
2. [**Sensitivity Correction**](#sensitivity-correction): Correction of the experiment's spectral sensitivity. This 
requires an additional lamp measurement as well as information about corresponding parameters.
3. [**Dispersion Correction**](#sellmeier-correction): Sellmeier correction of different time zero points due to
dispersion. This requires additional user inputs about the experimental setup.

The sample (main) data as well as the solvent data used in the first correction step can be subject to the following 
pre-corrections:
- [**Background Correction**](#background-correction): The dataset's correction spectra (dark, third harmonic and leak fluorescence) are 
subtracted from the dataset. These correction spectra are scaled using photodiode values. 
- **Reference Correction**: Uses the very first time point as a reference measurement, at which the signal should be 
exactly 0. The measured value at this point is _set to_ zero by subtracting it from all values. This correction only
works properly, if the delay time is considerably negative. 
- **Photodiode Correction**: Corrects the data for intensity fluctuations based on the photodiode values. This refers 
only to the intensity dependence of excitation and gate efficiency.

![](images/hdf-corrections.png)

All corrections (chain corrections and pre-corrections) are independent of each other and might be selected by the user 
as needed.

### Background Correction

Spectra required for the background correction can be included in the same data file as the major time-resolved spectra 
or in a stand-alone file. If the _vide supra_ check box is activated, pychart tries to extract the background spectra from 
the major data file specified above. If it is unchecked, an additional file has to be chosen. From that file only 
background spectra and corresponding photodiode values will be loaded.

![](images/hdf-background.png)

### Solvent Subtraction

To subtract properly weighted solvent measurements from the main data, the corresponding group box needs to be activated. 
Prior to the subtraction, the same set of corrections as for the main data might be applied to the solvent data. Also, 
the scans considered in the averaging process, can be selected as usual. Refer to the explanations in the previous 
paragraphs for more details.

The scaling factor with which the solvent data is weighted during the subtraction can be set in four different ways as 
listed in Table 2. Most methods use the sample's absorption _A_ (with respect to the path length effectively employed in 
the experimental setup) to calculate the scaling factor _f_. This calculation is done internally by pychart. 

![](images/hdf-solvent-factor.png)

<small>_Table 2. Available methods for the determination of solvent factors._</small>

| Method         | Formula                                | Notes                                                    |
|----------------|----------------------------------------|----------------------------------------------------------|
| _Absorption_   | f = 1 - 10<sup>-A</sup>                |                                                          |
| _Transmission_ | f = 10<sup>-A</sup>                    |                                                          |
| _Factor_       | f = (1 - 10<sup>-A</sup>) / (ln(10)⋅A) | [Lorenc _et al._](https://doi.org/10.1007/s003400100750) |
| _free_         | f                                      | arbitrary factor as entered by the user                  |

### Sensitivity Correction

To apply a correction for the spectral sensitivity of the instrument, the corresponding group box needs to be activated. 
From the imported data file only the lamp spectrum of the `single_spectra` data object will be considered. An additional 
_offset_ can be subtracted from the measured lamp spectrum. It is then compared to an ideal spectrum of a black body 
radiator of the given temperature. The sensitivity function is defined as the quotient of these two spectra.

![](images/hdf-sensitivity.png)

Clicking the _Update Preview_ button will open a window displaying the real and ideal spectra as well as the resulting 
sensitivity function. When changing correction parameters, such as the offset value, that button needs to be triggered 
again to refresh the plot; adjusting the preview wavelength ranges, allows to zoom into it.

![](images/hdf-sensitivity-plot.png)  
<small>_Figure 2. Scrrenshot of the sensitivity plot window._</small>

### Sellmeier Correction

To apply a correction for dispersion aka time zero correction aka Sellmeier correction, the corresponding group box 
needs to be activated. A list of materials causing dispersion can be generated by selecting materials from the drop-down 
menu and entering corresponding thicknesses. Additionally, an additive _offset_ time can be defined.

For a detailed description of the correction itself and a full list of available materials, see 
[here](SellmeierCorrection.md).

![](images/import-sellmeier-widget.png)

<small>_Table 3. Sellmeier widget buttons._</small>

| Icon                                               | Description                                |
|----------------------------------------------------|--------------------------------------------|
| <img src="../images/icons/add.svg" height="24">    | Add material with given thickness to list. |
| <img src="../images/icons/check.svg" height="24">  | Update selected material to new thickness. |
| <img src="../images/icons/remove.svg" height="24"> | Remove material from list.                 |

## Data Table

The data resulting from the calculation process is displayed in a table with its wavelengths' data in columns and time 
data in rows. In principle, the data is read-only, but might be edited using the buttons on the right side of the table.
Data can be deleted or added as full columns or rows. Additionally, NaN or infinity entries can be replaced by user 
defined scalar values.

<small>_Table 4. Data table edit buttons._</small>

| Icon                                                    | Description                                                                                                                              |
|---------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|
| <img src="../images/icons/add-col.svg" height="24">     | Add column for a specified wavelength and fill with specified values. The new column is inserted in front of the currently selected one. |
| <img src="../images/icons/remove-col.svg" height="24">  | Delete currently selected wavelength column.                                                                                             |
| <img src="../images/icons/add-row.svg" height="24">     | Add row for a specified time and fill with specified values. The new column is inserted above of the currently selected one.             |
| <img src="../images/icons/remove-row.svg" height="24">  | Delete currently selected time row.                                                                                                      |
| <img src="../images/icons/replace-nan.svg" height="24"> | Replace `NaN` with specified value.                                                                                                      |
| <img src="../images/icons/replace-inf.svg" height="24"> | Replace positive and/or negative `inf` with specified value.                                                                             |
| <img src="../images/icons/edit.svg" height="24">        | Toggle between read-only and manual-edit mode.                                                                                           |

| Add wavelength dialog              | Add time dialog                     | Replace NaN dialog                     | Replace inf dialog                     |
|------------------------------------|-------------------------------------|----------------------------------------|----------------------------------------|
| ![](images/import-add-wln-dlg.png) | ![](images/import-add-time-dlg.png) | ![](images/import-replace-nan-dlg.png) | ![](images/import-replace-inf-dlg.png) |

Triggering the edit button will switch the table from read-only to edit mode. Theoretically, any value might be 
changed to any other value by hand. Please check the necessity of this feature prior using...

## Time Traces

As soon as a data calculation is complete for the first time, it is possible to display the data as independent time 
traces for each wavelength. To this end, the dataset to be displayed needs to be selected using the colored 
buttons for data, sample and solvent. Regarding the time traces for sample and solvent, the corresponding 
pre-corrections (e.g. averaging over certain scans, reference point correction, etc.) are applied – this includes the 
solvent factor. The data channel will show the calculation result as displayed in the data table after applying all 
user-defined corrections.

Scaling of time traces can be adjusted manually or set to autoscaling. In addition to plot ranges the time axis scale 
might be set to linear or (symmetrical) logarithmic.

![](images/import-traces-scaling.png)

It is possible to show several time traces simultaneously in layouts with one or two columns and up to five rows. The 
consecutive time traces plotted in such a grid is referred to as a set of traces. Using the arrow buttons allows to move 
through the wavelengths of the data set.

![](images/import-traces-gridset.png)

<small>_Table 5. Time traces control buttons._</small>

| Icon                                 | Description                                               |
|--------------------------------------|-----------------------------------------------------------|
| ![](../images/icons/go-first.svg)    | Go to first wavelength traces.                            |
| ![](../images/icons/go-back.svg)     | Move currently visible set of traces to the previous set. |
| ![](../images/icons/go-previous.svg) | Move time traces one wavelength back.                     |
| ![](../images/icons/go-next.svg)     | Move time traces one wavelength forward.                  |
| ![](../images/icons/go-forward.svg)  | Move currently visible set of traces to the next set.     |
| ![](../images/icons/go-last.svg)     | Go to last wavelength traces.                             |
| ![](../images/icons/go-to.svg)       | Go to time trace for the specified wavelength.            |

The time traces are only updated if changes to the data table are made. In all other cases, such as re-calculation or
selection/deselection of data channels, the _Update_ button has to be triggered manually to review changes. 

## Photodiode Values Plot

| Icon                                                     | Description                  |
|----------------------------------------------------------|------------------------------|
| <img src="../images/icons/photodiodes.svg" height="48"/> | Open Photodiode Values Plot. |

Photodiode values recorded during the measurement can be used to correct the data as described above, but might also 
serve as a qualitative indicator for the measurement's laser fluctuations. To this end, photodiode values for the main 
data set can be plotted. Two layout types are available: separately per scan or as consecutive timeline. The layout can 
be set in the [General Settings Dialog](GeneralSettings.md#photodiodes-plot-layout).

When plotted per scan, values are depicted as a function of delay time. To maintain a clear view, both diodes, pump and 
gate, are displayed in two independent plots. In case of consecutive timelines, values are a function of data point 
indices with those data points constituting scans being highlighted.

![](images/hdf-photodiodes-separate.png)  
<small>_Figure 3. Photodiode values plotted separately for pump (top) and gate (bottom) diodes as a function of 
the delay time. The red line marks the time zero point. Values are identical to those in Figure 4._</small>

![](images/hdf-photodiodes-timeline.png)  
<small>_Figure 4. Photodiode values plotted as consecutive timeline for pump (blue) and gate (red). Scans of coherent 
data points are highlighted with alternating colors. Values are identical to those in Figure 3._</small>

Note that in both layouts the distances between data points do not resemble delay time differences but are linear to 
account for the fact that the actual (experimental) time difference between the measurements is almost equidistant.

To export the photodiode plot as an image, right-click on the plot to open a save dialog. The output file format can be 
chosen, while the resolution is fixed to 300 dpi.

## Implementation Details

_To be expanded in the future. Until now, please refer to the docstrings in the source code files._
_Main resources:_ [`hdfimport.py`](../src/pychart/core/hdfimport.py) [`HDFImportTab.py`](../src/pychart/gui/HDFImportTab.py)
