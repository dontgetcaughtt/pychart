# Authors

Pychart is maintained by Kristoffer Thom <kristoffer.thom@hhu.de>

Contributors, listed alphabetically, are:

- Janina Bertling