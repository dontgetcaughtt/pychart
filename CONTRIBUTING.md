# Contributing to pychart

If you like pychart and would like to contribute to it, there are several ways of doing so. Most importantly, any bug 
report would help us to improve pychart for everyone. As the main goal are the plotting and fitting features, code 
contributions are also very welcome. For those, please refer to the coding style conventions down below. Since the 
time-resolved spectroscopy community is a small one, do not hesitate to contact us directly when you are planning to 
contribute!

Keep in mind, that the import features are specifically designed for our own setups and are not indented to 
be adapted for other one. If you would like to use them too, you might fork the project and use it as a blueprint. 

**Note that by contributing, you agree that your contributions will be licensed under the [MIT License](LICENSE).**

## Bug Reports

All bugs can be reported either by using the [GitLab Issue Tracker](https://gitlab.com/dontgetcaughtt/pychart/issues)
(preferred) or via e-mail. 

## Translations

To translate pychart's user interface into another language, copy one of the existing `i18n/*.yaml` language files 
you are familiar with and translate _all_ strings provided in there into the new language. Afterwards you can create a 
merge request and send us your file via e-mail.

As pychart aims at scientific users, english is likely an appropriate language for all users. The desire for translations 
is therefore considered to be rather low. Also, pychart is an evolving application and new string literals will be 
included in future versions. To keep the overall quality high we will, thus, only include new translations made by 
native-speakers who are willing to keep their translations updated.

## Code Contributions

To keep the code consistent, please follow the existing coding style, or – in doubt – follow 
[PEP8](https://www.python.org/dev/peps/pep-0008/). 