# -----------------------------------------------------------------
#   UNITS STRINGS
# -----------------------------------------------------------------

#   TIME
# ---------------------------------------------------------------
fs: fs
ps: ps
ns: ns
µs: µs
ms: ms
s: s
min: min

#   ENERGY EQUIVALENT
# ---------------------------------------------------------------
nm: nm
cm⁻¹: cm⁻¹
eV: eV

#   ABSORPTION
# ---------------------------------------------------------------
mOD: mOD
OD: OD

#   OTHER
# ---------------------------------------------------------------
a.u.: a.u.
counts: counts



# -----------------------------------------------------------------
#   LANGUAGE STRINGS (Nicht übersetzen!)
# -----------------------------------------------------------------
de: Deutsch
en: English



# -----------------------------------------------------------------
#   GENERAL I/O AND DIALOG STRINGS
# -----------------------------------------------------------------
FILE_OPEN: Datei öffnen
FILE_SAVE: Datei speichern
FILE_EXPORT: Datei exportieren
DIRECTORY_OPEN: Verzeichnis öffnen
APPLY: Übernehmen
CANCEL: Abbrechen
OK: OK



# -----------------------------------------------------------------
#   MENU ENTRIES
# -----------------------------------------------------------------
MENU_FILE: Datei
MENU_NEW: Neu...
MENU_NEW_PLOT: Neuer Konturplot
MENU_NEW_TD: Neuer TD-Fit
MENU_NEW_IMPORT: Neuer Import
MENU_IMPORT_NETCDF: NetCDF (fsTA)
MENU_IMPORT_HDF: HDF (Kerr-Gate)
MENU_IMPORT_SINGLE: Einzelspektrum (fsTA)
MENU_IMPORT_MERGE: Dateien zusammenfügen (fsTA)
MENU_CLOSE: Schließen
MENU_CLOSE_ALL: Alle schließen
MENU_EXIT: Beenden
MENU_PLOT: Konturplot
MENU_IMPORT: Importieren
MENU_OPT: Optionen
MENU_SET: Einstellungen...
MENU_HELP_M: Hilfe
MENU_HELP: Hilfe...
MENU_ABOUT: Über...



# -----------------------------------------------------------------
#   GENERAL SETTINGS DIALOG
# -----------------------------------------------------------------
SETTINGS_DLG_TITLE: Einstellungen
SETTINGS_DLG_RESTORE: Auf Standardwerte zurücksetzen

SETTINGS_GENERAL: Allgemeines
SETTINGS_LANGUAGE: 'Sprache:'
SETTINGS_DEFAULT_DIR: 'Standardverzeichnis:'
SETTINGS_DEFAULT_TAB: 'Bei Programmstart öffnen:'
SETTINGS_TAB_CHOOSE: Auswahldialog
SETTINGS_TAB_PLOT: Konturplot
SETTINGS_TAB_TDFIT: TD-Fit
SETTINGS_TAB_NETCDF: NetCDF Import
SETTINGS_TAB_HDF: HDF Import

SETTINGS_PLOT: Konturplots
SETTINGS_DEFAULT_PLOT_SETTINGS: 'Initialisiere mit diesen Einstellungen:'
SETTINGS_EXPORT_RESOLUTION: 'Auflösung beim Export:'
SETTINGS_EXPORT_DPI: dpi
SETTINGS_RCPARAMS_GROUP: Matplotlib
SETTINGS_RCPARAMS_WARNING: >-
  Hinweis: Diese Einstellungen wirken sich auch <br /> auf die Darstellung
  anderer Grafiken aus.

SETTINGS_NETCDF: NetCDF Import
SETTINGS_NETCDF_GATE_GROUP: Gatekorrektur
SETTINGS_NETCDF_GATE_CONFINE: Beschränke Zeitspuren auf
SETTINGS_NETCDF_GATE_RANGE: ≤ t ≤

SETTINGS_EXPORT_GROUP: Export importierter Daten
SETTINGS_FACTOR: 'Multipliziere Daten beim Export:'
SETTINGS_PC_FACTOR: pychart-Dateien
SETTINGS_Z20_FACTOR: Z20-Dateien

SETTINGS_TOLERANCE_GROUP: Achsentoleranz
SETTINGS_WLN_TOLERANCE: 'Toleranz bzgl. Wellenlängenachsen (absolut):'
SETTINGS_TIME_TOLERANCE: 'Toleranz bzgl. Zeitachsen (relativ):'

SETTINGS_HDF: HDF Import
SETTINGS_HDF_PLOT_PD_GROUP: Photodioden
SETTINGS_HDF_PLOT_PD:  Zeige Photodiodenwerte
SETTINGS_HDF_PLOT_PD_SEPARATED: einzeln für jeden Scan
SETTINGS_HDF_PLOT_PD_COMBINED: als durchgehender Zeitstrahl



# -----------------------------------------------------------------
#   ABOUT DIALOG
# -----------------------------------------------------------------
ABOUT_TITLE: About
ABOUT_CAPTION: <b>PyChart {version}</b><br />CC-BY 4.0
ABOUT_TEXT: >-
  © Kristoffer Thom 2019-2022<br />
  kristoffer.thom@hhu.de<br /><br />
  Heinrich-Heine-Universität Düsseldorf, Germany<br />
  Institut für Physikalische Chemie II<br />
  Arbeitskreis Femtosekundenspektroskopie<br />
  Prof. Dr. Peter Gilch



# -----------------------------------------------------------------
#   NEW DIALOG
# -----------------------------------------------------------------
NEW_DLG_TITLE: Neu
NEW_PLOT_TAB: Neuer Kontourplot
NEW_IMPORT: Neuer Import...
NEW_NETCDF_TAB: Import vom fsTA-Experiment
NEW_HDF_TAB: Import vom Kerr-Gate
NEW_TDFIT_TAB: Neuer TD-Fit



# -----------------------------------------------------------------
#   PLOT TAB
# -----------------------------------------------------------------
PLOT_CONTOUR: Konturplot
PLOT_TRACES: Zeitspuren
PLOT_SPECTRA: Transiente Spektren
PLOT_COLORBAR: Colorbar
PLOT_BASELINE: Baselines
PLOT_SCALEBAR: Scalebar

PLOT_TAB_NAME: Konturplot
PLOT_DATAFILE: 'Datensatz:'
PLOT_UNITS: 'Einheiten:'
PLOT_ABS_FACTOR: ΔA·
PLOT_LINLOG: lin -//- log
PLOT_LIN: lin
PLOT_LOG: log

PLOT_GROUP_PREVIEW: Vorschau
PLOT_GROUP_GENERAL: Allgemeines
PLOT_NAME: 'Titel:'
PLOT_SHOW_TITLE: Titel anzeigen
PLOT_LAYOUT: 'Layout:'
PLOT_LAYOUT_LINEAR: linear (Zeitspuren - Konturplot - transiente Spektren)
PLOT_LAYOUT_L_TYPE: rechtwinklig (Zeitspuren - Konturplot -| transiente Spektren)
PLOT_LAYOUT_T_TYPE: linear mit DAS (Zeitspuren - Konturplot -| DAS - transiente Spektren)
PLOT_LAYOUT_COLORBAR_TOP: oben
PLOT_LAYOUT_COLORBAR_BOTTOM: unten
PLOT_LAYOUT_COLORBAR_RIGHT: rechts
PLOT_LAYOUT_HEIGHTS: 'Höhe:'
PLOT_LAYOUT_WIDTHS: 'Breite:'

PLOT_GROUP_AXES: Achsen
PLOT_AXES_WLN: 'Wellenlänge:'
PLOT_AXES_TIME: 'Zeit:'
PLOT_AXES_ABS: 'Amplitude:'
PLOT_AXES_START: von
PLOT_AXES_STOP: bis
PLOT_AXES_CUT: '-//-'
PLOT_AXES_STEP: Δ
PLOT_AXES_TICKS: Ticks

PLOT_GROUP_TRACES: Zeitspuren
PLOT_GROUP_SPECTRA: Transiente Spektren
PLOT_WAVELENGTH: Wellenlänge
PLOT_WAVENUMBER: Wellenzahl
PLOT_ENERGY: Energie
PLOT_COLOR: Farbe
PLOT_LABEL: Beschriftung
PLOT_TIME: Zeit
PLOT_POS_X1: ΔA₁
PLOT_POS_Y1: t₁
PLOT_POS_X2: λ₂
PLOT_POS_Y2: t₂
PLOT_INT_EXT: int/ext
PLOT_INTERNAL: int
PLOT_EXTERNAL: ext
PLOT_LIM_A_MAX: ΔA (max)
PLOT_LIM_A_MIN: ΔA (min)
PLOT_POS_X: λ
PLOT_POS_Y: t
PLOT_ROTATION: ↺
PLOT_EXPAND_WAVELENGTH_LINES: Linien auf Spektren ausdehnen
PLOT_EXPAND_SPECTRA_LINES: Linien auf Zeitspuren ausdehnen
PLOT_SCALING: 'Skalierung:'
PLOT_TICKS_λ: 'Ticks (λ):'
PLOT_TICKS_ΔA: 'Ticks (ΔA):'
PLOT_LABELSIZE: 'Schriftgröße:'
PLOT_LINESTYLE: 'Linienstil:'
PLOT_LINEWIDTH: 'Linienstärke:'

#   TRACES / SPECTRA LISTS
# ---------------------------------------------------------------
PLOT_MOVE_UP: aufwärts
PLOT_MOVE_DOWN: abwärts

PLOT_ADD_INT_TRACE: Zeitspur einfügen
PLOT_ADD_EXT_TRACES: Externe Zeitspur einfügen
PLOT_DEL_TRACE: Zeitspur entfernen

PLOT_ADD_INT_SPECTRUM: Spektrum einfügen
PLOT_ADD_EXT_SPECTRUM: Externes Spektrum einfügen
PLOT_DEL_SPECTRUM: Spektrum entfernen

#   MENU
# ---------------------------------------------------------------
PLOT_MENU_PLOT: Plotten
PLOT_MENU_LOAD: Einstellungen laden...
PLOT_MENU_SAVE: Einstellungen speichern...
PLOT_MENU_DEFAULT: Standardeinstellungen
PLOT_MENU_DEFAULT_MORE: Mehr...
PLOT_MENU_EXPORT: Grafik exportieren...
PLOT_MENU_COLORBAR: Colorbar anpassen...
PLOT_MENU_SCALEBAR: Scalebar anpassen...
PLOT_MENU_AUTOSCALE: Autoskalierung
PLOT_MENU_BASELINE: Baselines...
PLOT_MENU_AXESLABEL: Achsenbeschriftungen...
PLOT_MENU_ARTISTS: Zusätzliche Objekte...
PLOT_MENU_FSTA: fsTA
PLOT_MENU_KERR: fsFl
PLOT_MENU_NSTA: nsTA
PLOT_MENU_NSIR: nsIR
PLOT_MENU_HPLC: HPLC
PLOT_MENU_PUMP: 'fsTA: pump'
PLOT_MENU_PROBE: 'fsTA: probe'
PLOT_MENU_BG: 'fsTA: bg'
PLOT_MENU_LINLOG: lin -//- log
PLOT_MENU_LIN: linear
PLOT_MENU_LOG: logarithmisch

#   AXES LABEL DIALOG
# ---------------------------------------------------------------
PLOT_LABELING_DLG_HEADER: Achsenbeschriftungen
PLOT_LABELING_DLG_WLN_CONTOUR: 'Wellenlängenachse (Kontourplot):'
PLOT_LABELING_DLG_WLN_SPECTRA: 'Wellenlängenachse (transiente Spektren):'
PLOT_LABELING_DLG_TIME: 'Zeitachse (Zeitspuren):'
PLOT_LABELING_DLG_AMP_TRACES: 'Amplitudenachse (Zeitspuren):'
PLOT_LABELING_DLG_COLORBAR: 'Amplitudenachse (Colorbar):'
PLOT_LABELING_DLG_AMP_SPECTRA: 'Amplitudenachse (transiente Spektren):'
PLOT_DECIMAL: dezimal
PLOT_SCIENTIFIC: wissenschaftlich

#   COLORBAR DIALOG
# ---------------------------------------------------------------
PLOT_COLORBAR_DLG_HEADER: Colorbar
PLOT_COLORBAR_FLIP_SYMBOL: 🔁
PLOT_COLORBAR_VALUES: 'Wertebereich:'
PLOT_COLORBAR_COLORS: 'Farbbereich:'
PLOT_COLORBAR_PRESENTATION: 'Darstellung:'
PLOT_COLORBAR_PREVIEW: 'Vorschau:'
PLOT_COLORBAR_RENEW_PREVIEW: Vorschau aktualisieren
PLOT_COLORBAR_NAN: NaN ∨ inf
PLOT_COLORBAR_EXTEND_TRIANGLE: dreieckig
PLOT_COLORBAR_EXTEND_RECT: viereckig
PLOT_COLORBAR_EXTEND_FALSE: verborgen

#   BASELINE DIALOG
# ---------------------------------------------------------------
PLOT_BASELINE_DLG_HEADER: Baseline
PLOT_BASELINE_DLG_SHOW: Baseline in
PLOT_BASELINE_DLG_VALUE: 'Wert:'
PLOT_BASELINE_DLG_COLOR: 'Farbe:'
PLOT_BASELINE_DLG_LS: 'Linienstil:'
PLOT_BASELINE_DLG_LW: 'Linienstärke:'
PLOT_BASELINE_DLG_ZORDER: 'zorder:'

#   SCALEBAR DIALOG
# ---------------------------------------------------------------
PLOT_SCALEBAR_DLG: Scalebar...
PLOT_SCALEBAR_DLG_HEADER: Scalebar
PLOT_SCALEBAR_DLG_SHOW: anzeigen
PLOT_SCALEBAR_DLG_HIDE: verbergen
PLOT_SCALEBAR_DLG_VALUE: 'Wert:'
PLOT_SCALEBAR_DLG_LABEL: 'Beschriftung:'
PLOT_SCALEBAR_DLG_COLOR: 'Farbe:'
PLOT_SCALEBAR_DLG_POSWLN: 'Position x-Achse:'
PLOT_SCALEBAR_DLG_POSY: 'Position y-Achse:'

#   ADDITIONAL ARTISTS DIALOG
# ---------------------------------------------------------------
PLOT_ARTISTS_DLG_HEADER: Zusätzliche Linien und Rechtecke

PLOT_ARTISTS_DLG_AX1: 'Subplot #1'
PLOT_ARTISTS_DLG_X1: λ₁ / ΔA₁
PLOT_ARTISTS_DLG_Y1: t₁ / ΔA₁
PLOT_ARTISTS_DLG_AX2: 'Subplot #2'
PLOT_ARTISTS_DLG_X2: λ₂ / ΔA₂
PLOT_ARTISTS_DLG_Y2: t₂ / ΔA₂
PLOT_ARTISTS_DLG_COLOR: Linienfarbe
PLOT_ARTISTS_DLG_LW: Linienstärke
PLOT_ARTISTS_DLG_LS: Linienstil
PLOT_ARTISTS_DLG_FILL: Füllfarbe
PLOT_ARTISTS_DLG_ZORDER: zorder

PLOT_ARTISTS_DLG_ADD_LINE: Zusätzliche Linie
PLOT_ARTISTS_DLG_ADD_RECT: Zusätzliches Rechteck
PLOT_ARTISTS_DLG_DEL: Zusätzliches Objekt entfernen
PLOT_ARTISTS_DLG_MOVE_UP: aufwärts
PLOT_ARTISTS_DLG_MOVE_DOWN: abwärts

#   INFO TEXTS
# ---------------------------------------------------------------
PLOT_INFOTEXT_FILE: >-
  <b>Datentabelle</b> (ohne Header)<br /><br />
  Erste Zeile: Wellenlängen<br />Erste Spalte: Zeiten<br /><br />
  <i>Hinweis:</i> führende 0 zum Ausgleichen der Matrix nötig
PLOT_INFOTEXT_COLORBAR: >-
  <b>Darstellung von Werten über-, unter- und außerhalb des Darstellungsbereichs</b>
  <br /><br />Die Angabe der Farben erfolgt als RGB-Tupel, z.B. <i>102,204,102</i>.
  Ebenfalls möglich sind <i>b</i> (blau), <i>g</i> (grün), <i>r</i> (rot), <i>c</i> (cyan),
  <i>m</i> (magenta), <i>y</i> (gelb), <i>k</i> (schwarz) oder <i>w</i> (weiß).<br /><br />
  Sollen die jeweils letzten definierten Werte der Colorbar verwendet werden (Standard)
  müssen die entsprechenden Eingabefelder leer sein.
PLOT_INFOTEXT_TRACES: >-
  <b>Wellenlängen, für die Zeitspuren dargestellt werden sollen</b><br />
  <br /><i>auto</i> generiert automatische Beschriftung aus Wellenlänge und
  Einheit.<br /><br />ΔA₁ und t₁ beschreiben die Position im linearen
  Zeitspurplot; λ₂ und t₂ die Position im linearen Teil der Konturdarstellung; ↺
  gibt an, um wie viel Grad die Beschriftung gedreht werden soll.<br /><br />Bei
  aus externen Dateien eingebundenen Zeitspuren erfolgt die Angabe des
  Dateipfades in der Spalte Wellenlänge. Eigene Skalierungsgrenzen lassen sich
  unter ΔA(max) und ΔA(min) festlegen.<br /><br />Die Angabe der Farbe erfolgt
  als RGB-Tupel, z.B. <i>102,204,102</i>. Ebenfalls möglich sind <i>b</i>
  (blau), <i>g</i> (grün), <i>r</i> (rot), <i>c</i> (cyan), <i>m</i> (magenta),
  <i>y</i> (gelb), <i>k</i> (schwarz) oder <i>w</i> (weiß).
PLOT_INFOTEXT_SPECTRA: >-
  <b>Zeiten, für die Transiente Spektren dargestellt werden sollen</b><br />
  <br /><i>offset</i> als Platzhalter für die letzte gemessene Zeit möglich.
  <i>auto</i> generiert automatische Beschriftung aus Zeit und Einheit.<br />
  <br />Bei aus externen Dateien eingebundenen Spektren erfolgt die Angabe des
  Dateipfades in der Spalte Zeit. Eigene Skalierungsgrenzen lassen sich unter
  ΔA(min) und ΔA(max) festlegen. <br /><br />Die Angabe der Farbe erfolgt als
  RGB-Tupel, z.B. <i>102,204,102</i>. Ebenfalls möglich sind <i>b</i> (blau),
  <i>g</i> (grün), <i>r</i> (rot), <i>c</i> (cyan), <i>m</i> (magenta), <i>y</i>
  (gelb), <i>k</i> (schwarz) oder <i>w</i> (weiß).
PLOT_INFOTEXT_ZORDER: >-
  zorder gibt die (relative) Lage aller gezeichneten Ebenen an. Ein Wert von 0
  entspricht der internen Voreinstellung von matplotlib. Mit übermäßig großen
  Werten von z. B. 10 lassen sich die Baselines in den Vordergrund zwingen.
PLOT_INFOTEXT_ARTISTS: >-
  <b>Zusätzliche Linien und Rechtecke</b><br /><br />Objekte werden von (x₁,y₁) bzgl. Subplot
  #1 bis (x₂,y₂) bzgl. Subplot #2 gezeichnet und werden stets Subplot #2
  hinzugefügt. Daher sollte der Ranghöchste Plot (contour &lt; traces &lt;
  spectra) als Subplot #2 gewählt werden.<br /><br />Die Angabe der Farbe
  erfolgt als RGB-Tupel, z.B. <i>102,204,102</i>. Ebenfalls möglich sind
  <i>b</i> (blau), <i>g</i> (grün), <i>r</i> (rot), <i>c</i> (cyan), <i>m</i>
  (magenta), <i>y</i> (gelb), <i>k</i> (schwarz) oder <i>w</i> (weiß).<br />
  <br />zorder gibt die (relative) Lage aller gezeichneten Ebenen an. Ein Wert von 0
  entspricht der internen Voreinstellung von matplotlib. Mit übermäßig großen
  Werten von z. B. 10 lassen sich die Objekte in den Vordergrund zwingen.



# -----------------------------------------------------------------
#   IMPORT NETCDF / HDF TAB
# -----------------------------------------------------------------
IMPORT_NETCDF_TAB_NAME: NetCDF Import
IMPORT_HDF_TAB_NAME: HDF Import

IMPORT_RAWDATA: 'Rohdatensatz:'
IMPORT_BGDATA: 'Background:'
IMPORT_USE_SAME: s.o.

IMPORT_BG_CORR: Untergrundkorrektur
IMPORT_REF_CORR: Referenzpunktkorrektur
IMPORT_PD_CORR: Photodiodenkorrektur
IMPORT_CORR_ON: an
IMPORT_CORR_OFF: aus

IMPORT_WLN_COUNT: 'Wellenlängen:'
IMPORT_TIME_COUNT: 'Verzögerungszeiten:'
IMPORT_SHOT_COUNT: 'Einzelmessungen:'
IMPORT_SCAN_COUNT: 'Scans:'
IMPORT_MODE_COUNT: 'Chopperpositionen:'
IMPORT_SCAN_DISCARDED: ' (-1)'
IMPORT_SCANS_EXPECTED: ' ({nscans}?)'

IMPORT_SCANS: 'Scans:'
IMPORT_SCANS_ALL: alle
IMPORT_SCANS_SELECT: Auswahl
IMPORT_SCANS_LABEL: 'Scan #{scan}'

IMPORT_PUMP: pump
IMPORT_PROBE: probe
IMPORT_PUMPPROBE: pump + probe
IMPORT_BACKGROUND: background
IMPORT_MODE4: ΔA (4er Modus)
IMPORT_MODE2: ΔA (2er Modus)

IMPORT_DATA_GROUP: Hauptdaten

#   SOLVENT CORRECTION
# ---------------------------------------------------------------
IMPORT_SOLV_GROUP: Lösungsmittel
IMPORT_SOLV_SUBSTRACT: 'Lösungsmittelfaktor f:'
IMPORT_SOLV_ABSORPTION: Absorption
IMPORT_SOLV_TRANSMISSION: Transmission
IMPORT_SOLV_FACTOR: Faktor
IMPORT_SOLV_FREE: frei
IMPORT_SOLV_ABS_LABEL: 'A ='
IMPORT_SOLV_FREE_LABEL: 'f ='
IMPORT_SOLV_PATHLENGTH: (bzgl. effektiver Schichtdicke)
IMPORT_SOLV_SHIFT_LABEL: Verschiebe um
IMPORT_SOLV_SHIFT_UNIT: ps relativ zur Probe

#   SENSITIVITY CORRECTION
# ---------------------------------------------------------------
IMPORT_SENSITIVITY_GROUP: Sensitivitätskorrektur
IMPORT_BB_TEMPERATURE: 'Temperatur T:'
IMPORT_BB_OFFSET: 'offset:'
IMPORT_BB_PREVIEW_RANGE: 'Vorschaubereich:'
IMPORT_BB_WLN_RANGE: ≤ λ ≤
IMPORT_BB_PREVIEW: Vorschau aktualisieren

IMPORT_BB_PLOT_TITLE: Sensitivitätskurve
IMPORT_BB_WLN_LABEL: Wellenlänge / nm
IMPORT_BB_SENS_LABEL: Sensitivität
IMPORT_BB_LAMP_LABEL: Lampenspektrum
IMPORT_BB_BB_LABEL: Schwarzer Strahler

#   GATE CORRECTION
# ---------------------------------------------------------------
IMPORT_GATE_GROUP: Gatemessung
IMPORT_GATE_DLG: Setup...
IMPORT_MODE4_GATE: 4er Modus
IMPORT_MODE2_GATE: 2er Modus
IMPORT_SIGDIFF: Signaldifferenz
IMPORT_DIFFABS: Differenzabsorption
IMPORT_GATE_SHIFT_LABEL: Verschiebe Zeitnullpunkte um
IMPORT_GATE_SHIFT_UNIT: ps

IMPORT_GATE_DLG_TITLE: Gatekorrektur

IMPORT_GATE_DLG_FILE_GROUP: Datengrundlage
IMPORT_GATE_DLG_CHOPPERMODE: 'Choppermodus:'
IMPORT_GATE_DLG_SIGNALTYPE: 'Signaltyp:'
IMPORT_GATE_DLG_WAVELENGTHS: 'Wellenlängen:'
IMPORT_GATE_DLG_TIMES: 'Verzögerungszeiten:'
IMPORT_GATE_DLG_SCANS: 'Scans:'

IMPORT_GATE_DLG_PROCESSING_GROUP: Zeitnulllpunktsbestimmung
IMPORT_GATE_DLG_GAUSS: 'Gaussfit (t<sub>0</sub> : center)'
IMPORT_GATE_DLG_MAXIMA: 'Maxima (t<sub>0</sub> : t(ΔA<sub>max</sub>))'
IMPORT_GATE_DLG_AMP: 'amp:'
IMPORT_GATE_DLG_CENTER: 'center:'
IMPORT_GATE_DLG_WIDTH: 'width:'
IMPORT_GATE_DLG_OFFSET: 'offset:'
IMPORT_GATE_DLG_CENTER_RANGE: ≤ t<sub>0</sub> / ps ≤
IMPORT_GATE_DLG_AMP_AUTO: ΔA<sub>max</sub>
IMPORT_GATE_DLG_CENTER_AUTO: t(ΔA<sub>max</sub>)

IMPORT_GATE_DLG_POSTPROCESSING_GROUP: Dispersionskurvenglättung
IMPORT_GATE_DLG_NO_SMOOTH: keine
IMPORT_GATE_DLG_SMOOTH: Smooth
IMPORT_GATE_DLG_SELLMEIER: Sellmeier-Fit
IMPORT_GATE_DLG_WLN: 'λ:'
IMPORT_GATE_DLG_THICKNESS: 'd:'
IMPORT_GATE_DLG_WLN_RANGE: ≤ λ ≤
IMPORT_GATE_DLG_FIX: Fix
IMPORT_GATE_DLG_CONVEY: Übertrage Fitergebnis in Eingabefeld

IMPORT_GATE_DLG_RESULT_GROUP: Vorschau
IMPORT_GATE_DLG_PREVIEW: Anwenden
IMPORT_GATE_DLG_WLN_LABEL: Wellenlänge / nm
IMPORT_GATE_DLG_TIME_LABEL: Zeitnullpunkt / ps
IMPORT_GATE_DLG_LEGEND_GATE: Gatemessung
IMPORT_GATE_DLG_LEGEND_SMOOTH: Smooth
IMPORT_GATE_DLG_LEGEND_FIT: Sellmeier-Fit
IMPORT_GATE_DLG_LEGEND_RECENT: aktuell angewendet

#   SELLMEIER CORRECTION
# ---------------------------------------------------------------
IMPORT_SELLMEIER_GROUP: Sellmeierkorrektur

IMPORT_SELLMEIER_ADD: Hinzufügen
IMPORT_SELLMEIER_UPDATE: Aktualisieren
IMPORT_SELLMEIER_DEL: Entfernen

SELLMEIER_MM: mm
SELLMEIER_PS: ps
SELLMEIER_OFFSET: 'offset:'
SELLMEIER_MATERIAL_HEADER: Material
SELLMEIER_THICKNESS_HEADER: 'd / mm'

#   SINGLE IMPORT DIALOG
# ---------------------------------------------------------------
IMPORT_SINGLE_DLG_TITLE: Importiere fsTA-Einzelspektrum

IMPORT_SINGLE_CORR_GROUP: Korrekturmessung
IMPORT_SINGLE_CORR_REF: als Referenzpunktkorrektur
IMPORT_SINGLE_CORR_SOLV: als Lösungsmittelmessung
IMPORT_SINGLE_CALCULATE: Daten berechnen
IMPORT_SINGLE_EXPORT: Spektrum exportieren...
IMPORT_SINGLE_PREVIEW_GROUP: Vorschau

IMPORT_SINGLE_PLOT_WLN: 'Wellenlängenachse:'
IMPORT_SINGLE_PLOT_AMP: 'Signalachse:'
IMPORT_SINGLE_PLOT_AUTO: auto
IMPORT_SINGLE_PLOT_MIN: von
IMPORT_SINGLE_PLOT_MAX: bis
IMPORT_SINGLE_PLOT_MULTIPLY: ΔA·1000
IMPORT_SINGLE_PLOT_UPDATE: Aktualisieren
IMPORT_SINGLE_PLOT_DATA: Daten
IMPORT_SINGLE_PLOT_MAIN: Probe
IMPORT_SINGLE_PLOT_CORR: Referenz
IMPORT_SINGLE_PLOT_WLN_LABEL: Wellenlänge / nm
IMPORT_SINGLE_PLOT_AMP_LABEL: Differenzabsorption / OD
IMPORT_SINGLE_PLOT_AMP_LABEL_MULTIPLY: Differenzabsorption / mOD
IMPORT_SINGLE_PLOT_AMP_LABEL_RAW: Signalintensität / a.u.

IMPORT_SINGLE_TABLE_ADD_ROW: Wellenlänge einfügen
IMPORT_SINGLE_TABLE_DEL_ROW: Wellenlänge entfernen
IMPORT_SINGLE_TABLE_REPLACE_NAN: NaNs ersetzen
IMPORT_SINGLE_TABLE_REPLACE_INF: Inf ersetzen
IMPORT_SINGLE_TABLE_ENABLE_EDIT: Manuell bearbeiten

#   MERGE NETCDF FILES DIALOG
# ---------------------------------------------------------------
IMPORT_MERGE_DLG_TITLE: NetCDF Rohdateien zusammenfügen

IMPORT_MERGE_INPUT_FILE: 'Eingabedatei:'
IMPORT_MERGE_OUTPUT_FILE: 'Ausgabedatei:'
IMPORT_MERGE_ADD_FILE: Rohdatei hinzufügen
IMPORT_MERGE_EXPORT: Export
IMPORT_MERGE_CLOSE: Schließen

IMPORT_MERGE_FILE: Datei
IMPORT_MERGE_WLNS: Wellenlängen
IMPORT_MERGE_TIMES: Zeiten
IMPORT_MERGE_MODE: Chopperpositionen
IMPORT_MERGE_SHOTS: Einzelmessungen
IMPORT_MERGE_SCAN_LABEL: Scan {scan}

#   DATA TABLE
# ---------------------------------------------------------------
IMPORT_TABLE_GROUP: Daten
IMPORT_TABLE_ADD_COL: Wellenlänge einfügen
IMPORT_TABLE_DEL_COL: Wellenlänge entfernen
IMPORT_TABLE_ADD_ROW: Zeit einfügen
IMPORT_TABLE_DEL_ROW: Zeit entfernen
IMPORT_TABLE_REPLACE_NAN: NaNs ersetzen
IMPORT_TABLE_REPLACE_INF: Inf ersetzen
IMPORT_TABLE_ENABLE_EDIT: Manuell bearbeiten

#   DATA PLOTTING
# ---------------------------------------------------------------
IMPORT_PLOT_ROWS: 'Reihen:'
IMPORT_PLOT_COLS: 'Spalten:'
IMPORT_PLOT_MIN: von
IMPORT_PLOT_CUT: '-//-'
IMPORT_PLOT_MAX: bis
IMPORT_PLOT_AMP: 'Signalachse:'
IMPORT_PLOT_TIME: 'Zeitachse:'
IMPORT_PLOT_AUTO: auto
IMPORT_PLOT_MULTIPLY: ΔA·1000

IMPORT_PLOT_CORR: Daten
IMPORT_PLOT_MAIN: Probe
IMPORT_PLOT_SOLV: Lösungsmittel
IMPORT_PLOT_GATE: Gatemessung

IMPORT_PLOT_UPDATE: Aktualisieren
IMPORT_PLOT_GOTO_FIRST: Anfang
IMPORT_PLOT_GOTO_LAST: Ende
IMPORT_PLOT_GOTO_PREV: zurück
IMPORT_PLOT_GOTO_NEXT: weiter
IMPORT_PLOT_GOTO_BACK: zurück (mehrfach)
IMPORT_PLOT_GOTO_FRWD: weiter (mehrfach)
IMPORT_PLOT_GOTO_GOTO: gehe zu

#   PHOTODIODE PLOTTING
# ---------------------------------------------------------------
IMPORT_PD_PLOT_DLG_TITLE: Kerr-Gate Photodiodenwerte
IMPORT_PD_PLOT_EXPORT_ACTION: Grafik exportieren...
IMPORT_PD_PLOT_EXPORT_TITLE: Photodioden

IMPORT_PD_PLOT_VALUE_LABEL: Photodiodenwerte
IMPORT_PD_PLOT_PUMP_VALUE_LABEL: Pump-Diodenwert
IMPORT_PD_PLOT_GATE_VALUE_LABEL: Gate-Diodenwert
IMPORT_PD_PLOT_TIME_LABEL: Verzögerungszeit / ps
IMPORT_PD_PLOT_SCAN_LABEL: 'Scans (markiert am Zeitnullpunkt)'
IMPORT_PD_PLOT_LEGEND_TITLE: Scan
IMPORT_PD_PLOT_PUMP_LEGEND_LABEL: Pumpdiode
IMPORT_PD_PLOT_GATE_LEGEND_LABEL: Gatediode

#   MENU
# ---------------------------------------------------------------
IMPORT_MENU_SHOW: Daten berechnen
IMPORT_MENU_PLOT_PD: Photodiodenwerte anzeigen
IMPORT_MENU_PLOT: Als Konturplot öffnen
IMPORT_MENU_EXPORT: Daten exportieren...
IMPORT_MENU_EXPORT_Z20: Daten exportieren (Z20)...

#   INF / NAN REPLACEMENT DIALOGS
# ---------------------------------------------------------------
IMPORT_REPLACE_NAN_TITLE: NaN ersetzen
IMPORT_REPLACE_NAN_TEXT: NaN (Not A Number) durch Gleitkommazahl ersetzen
IMPORT_REPLACE_NAN_VALUE: 'Wert:'
IMPORT_REPLACE_INF_TITLE: Inf ersetzen
IMPORT_REPLACE_INF_TEXT: Infinite Werte durch Gleitkommazahl ersetzen
IMPORT_REPLACE_INF_VALUE: 'Wert:'
IMPORT_REPLACE_INF_APPLY: 'Anwenden auf:'

#   INSERT COLUMN / ROW DIALOG
# ---------------------------------------------------------------
IMPORT_INSERT_DLG_TITLE: Daten hinzufügen
IMPORT_INSERT_COLUMN_TEXT: Datenspalte mit festgelegtem Wert einfügen
IMPORT_INSERT_ROW_TEXT: Datenzeile mit festgelegtem Wert einfügen
IMPORT_INSERT_WAVELENGTH: 'Wellenlänge:'
IMPORT_INSERT_TIME: 'Zeit:'
IMPORT_INSERT_INI_VALUE: 'Initialisiere mit: '

#   INFO TEXTS
# ---------------------------------------------------------------
IMPORT_INFOTEXT_SOLV: >-
  <b>Methode zur Bestimmung des Lösungsmittelfaktors</b><br /><br
  /><i>Absorption:</i> f = 1 - 10<sup>-A</sup><br /><i>Transmission:</i> f =
  10<sup>-A</sup><br /><i>Faktor:</i> f = (1 - 10<sup>-A</sup>)/(ln(10) &#x2219;
  A)<br /><i>frei:</i> beliebiger Faktor



# -----------------------------------------------------------------
#   STATUS MESSAGES
# -----------------------------------------------------------------

#   PLOTTING
# ---------------------------------------------------------------
STATUS_PLOT_SETTINGS_LOADED: Einstellungen aus {file} wiederhergestellt.
STATUS_PLOT_DEFAULT_SETTINGS_LOADED: Standardeinstellungen für {setup} geladen.
STATUS_PLOT_SETTINGS_SAVED: Einstellungen unter {file} gespeichert.
STATUS_PLOT_IMAGE_SAVED: Plot nach {file} erfolgreich exportiert.

STATUS_PLOT_LOAD_DATA: Lade Daten...
STATUS_PLOT_INITIALIZE: Initialisieren...
STATUS_PLOT_DRAW_CONTOUR: Zeichne Contour...
STATUS_PLOT_DRAW_TRACES: Zeichne Zeitspuren...
STATUS_PLOT_DRAW_SPECTRA: Zeichne Spektren...
STATUS_PLOT_DRAW_COLORBAR: Erstelle Colorbar...
STATUS_PLOT_DRAW_BASELINES: Zeichne Baselines...
STATUS_PLOT_DRAW_ARTISTS: Zeichne zusätzliche Linien...
STATUS_PLOT_FINALIZE: Fertigstellen...
STATUS_PLOT_FAILED: Plotten fehlgeschlagen.

#   IMPORTING
# ---------------------------------------------------------------
STATUS_IMPORT_INF_REPLACED: '{number} Inf-Werte ersetzt.'
STATUS_IMPORT_NAN_REPLACED: '{number} NaN ersetzt.'

STATUS_IMPORT_DATA_EXPORTED: Daten nach {file} exportiert.
STATUS_IMPORT_DATA_EXPORTED_Z20: Daten nach {file} exportiert (Z20-Kompabilitätsmodus).

STATUS_IMPORT_PREPARE_CORRECTION: Initialisiere...
STATUS_IMPORT_CALC_MAIN: Korrigiere Hauptdaten, berechne Differenzabsorption...
STATUS_IMPORT_CORR_MAIN: Korrigiere Fluoreszenzdaten...
STATUS_IMPORT_CALC_SOLV: Berechne Lösungsmittel...
STATUS_IMPORT_CORR_SOLV: Korrigiere Lösungsmittelmessung...
STATUS_IMPORT_PREPARE_TIMEZERO: Initialisiere Zeitnullpunktskorrektur...
STATUS_IMPORT_GATE_CORR: Gatekorrektur...
STATUS_IMPORT_SELLMEIER_CORR: Sellmeier-Korrektur...
STATUS_IMPORT_SUBSTRACT_SOLV: Berechnungen werden abgeschlossen...
STATUS_IMPORT_FAILED: Fehlgeschlagen.



# -----------------------------------------------------------------
#   ERROR MESSAGES
# -----------------------------------------------------------------
ERROR_ERROR: Fehler
ERROR_INTERNAL: Internal Error
ERROR_INPUT: Input Error
ERROR_OUTPUT: Output Error
ERROR_RANGE: Range Error

#   SUSPICIOUS ERRORS
# ---------------------------------------------------------------
ERROR_UNKOWN_ERROR: Ein unbekannter Fehler ist aufgetreten.
ERROR_PLOT_INTERNAL_ERROR: Exception caught at plot.py -> draw_plot()
ERROR_NCDF_INTERNAL_ERROR_1: Exception caught at netcdfimport.py -> extract_main_data()
ERROR_NCDF_INTERNAL_ERROR_2: Exception caught at netcdfimport.py -> correct_main_data()
ERROR_HDF_INTERNAL_ERROR: Exception caught at hdfimport.py -> correct_main_data()

#   PLOT TAB ERRORS
# ---------------------------------------------------------------
ERROR_PLOT_IMPORT_DATA_ERROR: Datei {input} weist ein falsches Format auf.
ERROR_PLOT_EXT_IMPORT_DATA_ERROR: Datei {input} weist ein falsches Format auf (exakt zwei Spalten benötigt).
ERROR_PLOT_WLN_RANGE_ERROR: Startwellenlänge darf nicht gleich Endwellenlänge sein.
ERROR_PLOT_TIME_RANGES_ERROR: 'Ungültige Zeitgrenzen: Startzeit ≤ Achsenbruch ≤ Endzeit'
ERROR_PLOT_TIME_CUT_ERROR: Achsenunterbrechung darf nicht bei null sein.
ERROR_PLOT_ABS_RANGE_ERROR: 'Ungültige Absorptionsgrenzen: Mindestens zwei Absorptionsstufen nötig (min + 2·intervall ≤ max).'
ERROR_PLOT_ABS_STEP_ERROR: Absorptionsintervall darf nicht null sein.
ERROR_PLOT_SCALING_ERROR: Skalierungsfaktor darf nicht null sein.
ERROR_PLOT_EXPORT_ERROR: Der Plot konnte nicht erfolgreich exportiert werden.
ERROR_PLOT_EXPORT_FILE_FORMAT_ERROR: Das gewählte Dateiformat ({input}) wird nicht unterstützt.
ERROR_PLOT_NO_CMAPS_LOADED_ERROR: Keine Colormaps geladen.
ERROR_PLOT_JSON_SETTINGS_ERROR: JSON-Datei {input} konnte nicht geöffnet werden.
ERROR_PLOT_IMPORT_SETTINGS_ERROR: Fehler beim Lesen der Einstellungen von {input}.
ERROR_PLOT_EXPORT_SETTINGS_ERROR: Fehler beim Schreiben der Einstellungen nach {output}.
ERROR_PLOT_MISSING_DATA_ERROR: 'Autoskalierung nicht möglich: keine Daten geladen.'
ERROR_PLOT_INVALID_INPUT_ERROR: Ungültige Eingabe.

#   IMPORT TABS ERRORS
# ---------------------------------------------------------------
ERROR_IMPORT_MEMORY_ERROR: Nicht genügend Speicher vorhanden.
ERROR_IMPORT_IMPORT_DATA_ERROR: Datei {input} konnte nicht importiert werden.
ERROR_IMPORT_INVALID_MODE_ERROR: 'Modus mit {input} Chopperpositionen wird nicht unterstüzt (zulässige Modi: {ranges}).'
ERROR_IMPORT_UNKNOWN_MODE_ERROR: 'Modus <i>{input}</i> wird nicht unterstützt (zulässige Modi: <i>{ranges}</i>).'
ERROR_IMPORT_MISSING_MAIN_ERROR: Keine Messdaten geladen.
ERROR_IMPORT_MISSING_SOLV_ERROR: Keine Lösungsmittelmessung geladen.
ERROR_IMPORT_MISSING_GATE_ERROR: Keine Gatemessung geladen.
ERROR_IMPORT_MISSING_CORR_ERROR: Keine Korrekturmessung geladen.
ERROR_IMPORT_MISSING_LAMP_ERROR: Kein Lampenspektrum geladen.
ERROR_IMPORT_MISSING_BG_ERROR: Keine Backgroundmessung geladen ({input}).
ERROR_IMPORT_MODE_UNAVAILABLE_ERROR: 4er Modus steht bei diesem Datensatz nicht zur Verfügung.
ERROR_IMPORT_INVALID_NUMBER: <i>{input}</i> ist keine gültige Zahl.
ERROR_IMPORT_INVALID_ABSORPTION_ERROR: <i>{input}</i> ist keine gültige Absorption.
ERROR_IMPORT_INVALID_TIME_SHIFT_ERROR: 'Ungültiger Zeit-Shift: <i>{input}</i>'
ERROR_IMPORT_UNKNOWN_FIT_ERROR: 'Modus <i>{input}</i> wird nicht unterstützt (zulässige Modi: <i>{ranges}</i>).'
ERROR_IMPORT_INVALID_PLOT_RANGE_ERROR: Ungültige Eingabe bei Plotgrenzen.
ERROR_IMPORT_INVALID_THICKNESS_ERROR: 'Ungültige Eingabe bei Sellmeier-Parametern: <i>{input}</i> ist keine gültige Schichtdicke.'
ERROR_IMPORT_INVALID_TIMEOFFSET_ERROR: 'Ungültige Eingabe bei Sellmeier-Parametern: <i>{input}/<i> ist kein gültiger Offset.'
ERROR_IMPORT_CORRUPTED_FILE_ERROR: >-
  Datei {input} ist fehlerhaft und konnte nicht richtig geladen werden: Die Anzahl der Spektren muss ein ganzzahliges
  Vielfaches der Anzahl an Verzögerungszeiten sein.
ERROR_IMPORT_INCOMPLETE_BG_ERROR: Backgrounddatei ist unvollständig. Daten werden deshalb möglicherweise fehlerhaft korrigiert.
ERROR_IMPORT_INVALID_OFFSET_ERROR: 'Ungültiger offset: <i>{input}</i>'
ERROR_IMPORT_INVALID_LAMP_WLN_RANGE_ERROR: 'Ungültiger Wellenlängenbereich: λ₁ < λ₂'
ERROR_IMPORT_INVALID_GATE_WLN_RANGE_ERROR: 'Ungültiger Wellenlängenbereich: λ₁ < λ₂'
ERROR_IMPORT_SMALL_GATE_WLN_RANGE_ERROR: Wellenlängenbereich muss mindestens drei Wellenlängen beinhalten (n > 2).
ERROR_IMPORT_INCOMPATIBLE_WLNS_NETCDF_ERROR: 'Inkompatible Datensätze: Wellenlängenachsen von Probe, Lösungsmittel und Gatemessung müssen übereinstimmen.'
ERROR_IMPORT_INCOMPATIBLE_TIMES_NETCDF_ERROR: 'Inkompatible Datensätze: Zeitachse des Lösungsmittels muss eine Untermenge der Probenachse sein.'
ERROR_IMPORT_INCOMPATIBLE_BG_NETCDF_ERROR: 'Inkompatible Datensätze: Backgrounddateien müssen die gleiche Anzahl an Wellenlängen aufweise wie ihre Hauptdaten ({input}).'
ERROR_IMPORT_INCOMPATIBLE_WLNS_HDF_ERROR: 'Inkompatible Datensätze: Wellenlängenachsen von Probe und Lösungsmittel müssen übereinstimmen.'
ERROR_IMPORT_INCOMPATIBLE_WLNS_BG_HDF_ERROR: 'Inkompatible Datensätze: Wellenlängenachsen der Untergrundmessungen müssen mit jener der Stammdaten übereinstimmen.'
ERROR_IMPORT_INCOMPATIBLE_WLNS_LAMP_HDF_ERROR: 'Inkompatible Datensätze: Wellenlängenachse der Lampenmessung muss mit jener von Probe und Lösungsmittel übereinstimmen.'
ERROR_IMPORT_INCOMPATIBLE_TIMES_HDF_ERROR: 'Inkompatible Datensätze: Zeitachse des Lösungsmittels muss mit jener der Probe übereinstimmen.'
ERROR_IMPORT_MERGE_INCOMPATIBLE_WLNS_ERROR: 'Inkompatible Datensätze: Wellenlängenachsen aller Dateien müssen übereinstimmen.'
ERROR_IMPORT_MERGE_INCOMPATIBLE_TIMES_ERROR: 'Inkompatible Datensätze: Zeitachsen aller Dateien müssen übereinstimmen.'
ERROR_IMPORT_MERGE_INCOMPATIBLE_SHOTS_ERROR: 'Inkompatible Datensätze: Anzahl der Einzelmessungen muss in allen Datein {ranges} betragen.'
ERROR_IMPORT_MERGE_INCOMPATIBLE_MODE_ERROR: 'Inkompatible Datensätze: Anzahl der Chopperpositionen muss in allen Datein identisch sein.'
ERROR_IMPORT_MERGE_INVALID_SCAN_ERROR: 'Ungültige Eingabe: {input} hat keine {ranges} Scans zum zusammenfügen.'
ERROR_IMPORT_MERGE_FAILED_MERGE_ERROR: Dateien konnten nicht zusammengefügt werden.
ERROR_IMPORT_MERGE_FAILED_EXPORT_ERROR: 'Export der zusammengefügten Dateien nach {input} fehlgeschlagen.'
ERROR_IMPORT_PD_PLOT_EXPORT_ERROR: Der Photodiodenplot konnte nicht erfolgreich exportiert werden.
ERROR_IMPORT_PD_PLOT_EXPORT_FILE_FORMAT_ERROR: Das gewählte Dateiformat ({input}) wird nicht unterstützt.

#   GENERAL PYCHART UTILITIES ERRORS
# ---------------------------------------------------------------
ERROR_PYCHART_IMPORT_DATA_ERROR: Datei {input} konnte nicht geladen werden.
ERROR_PYCHART_EXPORT_DATA_ERROR: Datei {output} konnte nicht gespeichert werden.
ERROR_PYCHART_CMAP_IMPORT_ERROR: Colormap {input} konnte nicht registriert werden.
ERROR_PYCHART_INVALID_ABSORPTION_ERROR: Absorption darf nicht < 0 sein.
ERROR_PYCHART_UNKNOWN_ABS_FACTOR_ERROR: >-
  Lösungsmittelfaktor <i>{input}</i> wird nicht unterstützt (zulässige Eingaben:
  <i>{ranges}</i>).
ERROR_PYCHART_INVALID_SMOOTH_ERROR: Ein gleitender Durchschnitt kann nur über n > 1 Werte berechnet werden.
ERROR_PYCHART_INVALID_MPL_COLOR_ERROR: Nicht erkannte matplotlib-Farbe <i>{input}</i>. Siehe Dokumentation (F1) für mehr Informationen.
ERROR_PYCHART_SELLMEIER_JSON_ERROR: >-
  Datei {input} ist fehlerhaft. Der Funktionsumfang der Zeitnullpunktskorrektur
  ist dadurch beeinträchtigt.
ERROR_PYCHART_SELLMEIER_LOAD_ERROR: >-
  Datei {input} konnte nicht geladen werden. Der Funktionsumfang der
  Zeitnullpunktskorrektur ist dadurch beeinträchtigt.
ERROR_PYCHART_INVALID_TEMPERATURE_ERROR: Temperatur kann nicht ≤ 0K sein.

#   GENERAL PYCHART APPLICATION ERRORS
# ---------------------------------------------------------------
ERROR_PYCHART_SETTINGS_JSON_ERROR: JSON-Datei {input} konnte nicht geöffnet werden.
ERROR_PYCHART_SETTINGS_LOAD_ERROR: Fehler beim Lesen der Einstellungen von {input}.
ERROR_PYCHART_SETTINGS_SAVE_ERROR: Fehler beim Schreiben der Einstellungen nach {output}.
ERROR_PYCHART_SETTINGS_INVALID_DPI_ERROR: 'Ungültige Export-Auflösung: {input} ist keine Zahl.'
ERROR_PYCHART_SETTINGS_INVALID_FACTOR_ERROR: 'Ungültiger Export-Faktor: {input} ist keine Zahl.'
ERROR_PYCHART_SETTINGS_INVALID_TOLERANCE_ERROR: 'Ungültige Achsentoleranz: {input} ist keine Zahl.'

ERROR_PYCHART_LANG_NOT_FOUND_ERROR: Übersetzungen für <i>{input}</i> konnten nicht geladen werden. Rückfall auf Deutsch.
ERROR_PYCHART_NO_LANG_LOADED_ERROR: Es konnte keine Sprache geladen werden. Anwendung wird nun beendet.





# -----------------------------------------------------------------
#   TIME-DEPENDENT FIT TAB (temporary)
# -----------------------------------------------------------------
TDFIT_PLOT_XLABEL: 'Zeit t / '
TDFIT_PLOT_YLABEL: Signalintensität I(t)
TDFIT_PLOT_LOG: logarithmisch
TDFIT_PLOT_LIN: linear
TDFIT_PLOT_XAXIS: 'Zeitachse:'
TDFIT_PLOT_YAXIS: 'Signalachse:'
TDFIT_GENERAL_GROUP: Allgemeines
TDFIT_GENERAL_FIT: 'Methode:'
TDFIT_GENERAL_FIT_t: I(t) multiexponentiell
TDFIT_GENERAL_FIT_λ: I(λ,t) multiexponentiell
TDFIT_GENERAL_NAME: 'Name:'
TDFIT_GENERAL_UNITS: 'Einheit:'
TDFIT_FILES_IRF: 'IRF:'
TDFIT_FILES_DATA: 'Datensatz:'
TDFIT_FILES_OPEN: Datei öffnen
TDFIT_CONV_GROUP: Faltung
TDFIT_CONV_IRF: IRF
TDFIT_CONV_GAUSS: Gauss
TDFIT_CONV_NONE: deaktiviert
TDFIT_CONV_FWHM: 'FWHM:'
TDFIT_PARAMS_GROUP: Parameter
TDFIT_PARAMS_BEGIN: 'Start:'
TDFIT_PARAMS_END: 'Ende:'
TDFIT_PARAMS_STEP: 'Schrittweite:'
TDFIT_PARAMS_START: Startwert
TDFIT_PARAMS_RESULT: Fit
TDFIT_PARAMS_FIX: Fix
TDFIT_PARAMS_TSHIFT: 't<sub>shift</sub>:'
TDFIT_PARAMS_BG: 'Untergrund:'
TDFIT_PARAMS_BGIRF: 'Untergrund<sub>IRF</sub>:'
TDFIT_PARAMS_CUTTOFF: 'Cuttoff:'
TDFIT_PARAMS_SCATTER: 'Streulicht:'
TDFIT_TIMES_GROUP: Zeitkonstanten
TDFIT_TIMES_TIMES: 'Anzahl:'
TDFIT_TIMES_MINIMUM: min.
TDFIT_TIMES_MAXIMUM: max.
TDFIT_TIMES_START: Startwert
TDFIT_TIMES_AMPLITUDE: Amplitude
TDFIT_TIMES_RESULT: Fit
TDFIT_TIMES_FIX: Fix
TDFIT_FIT_INI: Initialisieren
TDFIT_FIT_RUN: Fit
TDFIT_FIT_SAVE: Speichern...
TDFIT_FIT_GROUP: Fit
TDFIT_FIT_STATUS: 'Status: '
TDFIT_FIT_READY: Bereit.
TDFIT_FIT_RUNNING: Läuft.
TDFIT_FIT_DONE: Fertig.
TDFIT_FIT_ERROR: Fehler.
TDFIT_FIT_TIME: 'Abgelaufende Zeit: '
