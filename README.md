![](images/logo/logo.png)

**_pyChart_** is intended to be an extensive application for dealing with data 
generated from time-resolved spectroscopy.

- **Plotting:** Display time-resolved data as contour diagramms, highlight certain 
  spectra and time traces, and overlay other data, such as steady-state spectra.
- **Manipulation:** Apply correction routines, like solvent substraction prior to 
  plotting or fitting
- **Fitting:** Fit your data sets multi-exponentially.

pyChart is developed in the [Gilch group](http://www.gilch.hhu.de) at the 
University of Düsseldorf mainly to serve our very own requirements. It started 
with the need (or the PhD's desire) for an easy and fast tool to create 
publishable contour diagrams. From there on it will hopefully become a more 
powerful suite replacing old and antiquated software still in use.

pyChart consists of several modules for plotting, fitting and importing time-resolved data. The import and correction 
routines are designed specifically to deal with those raw datasets retrieved from our own experimental setups. They are, 
thus, not likely to be useful for anyone else. However, the most important features - plotting and fitting - are meant 
to work with data provided as plain text files and might be suitable for other scientists as well. Feel free to use 
these parts and simply ignore the rest!

## Usage

pyChart is written in Python. For installation follow the steps below:

1. Make sure you have Python 3.10 or later.
2. Install external dependencies by running `pip install -r requirements.txt`.
3. To start pyChart, execute the `main.py`.

Since version 5.0 pyChart provides the following modules:

|                                  | Feature       | Description                                              | experiments designed for                                      | File Format |
|----------------------------------|---------------|----------------------------------------------------------|---------------------------------------------------------------|-------------|
| ![](images/icons/new-plot.svg)   | Plot          | plotting multi-channel time-dependent data as 2D contour | any time-resolved experiment (e.g. fs/ns-TA, Step-Scan, HPLC) | ASCII       |
| ![](images/icons/new-import.svg) | NetCDF Import | importing and correction of certain raw data             | Gilch group's fsTA experiment                                 | NetCDF      |
| ![](images/icons/new-import.svg) | HDF Import    | importing and correction of certain raw data             | Gilch group's Kerr Gate                                       | HDF5        |
| ![](images/icons/new-fit.svg)    | TD-Fit        | fitting single-channel time-dependent data               | any experiment (e.g. TCSPC)                                   | ASCII       |

For a quick overview, you might want to have a look at the [Screenshots](images/screenshots); detailed information on how 
to use pyChart can be found in the [User Guides](docs).

## Contribution

If you would like to contribute to pyChart in any form, please refer to our [contribution guide lines](CONTRIBUTING.md). 
Any bugs found can be reported via the [GitLab Issue Tracker](https://gitlab.com/dontgetcaughtt/pychart/issues). If you 
want to contact us for discussion, making suggestions or whatever don't hesitate to write an email.

## License

pyChart is released under the [MIT License](LICENSE).

## Contact

Kristoffer Thom  
kristoffer.thom@hhu.de

Heinrich Heine University Düsseldorf  
Institute for Physical Chemistry II  
AK Femtosekundenspektroskopie  
Prof. Dr. Peter Gilch  
Universitätsstraße 1  
40225 Düsseldorf  
www.gilch.hhu.de