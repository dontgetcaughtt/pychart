_Full texts of all licences can be found in the [`images/licenses`](../licenses) subdirectory._

If not stated otherwise, icons in this directory are based on icon sets released into the public domain (e.g. 
[small-n-flat](http://paomedia.github.io/small-n-flat/), [Tango](http://tango.freedesktop.org/Tango_Desktop_Project)) 
or are own work. They are made available under the 
[Creative Commons CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) (CC0 1.0) Public Domain Dedication.

The following icons are part of the [Papirus icon theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
and distributed under the terms of the GNU General Public License (GPL), version 3:

- `colorbar.svg`
- `new.svg`

Icons derived from the Papirus icons and therefore also subject to the GPL: 

- `autoscale.svg`
- `add-artists.svg`
- `import-json.svg` `export-json.svg`
- `new-fit.svg` `new-fit-alt.svg`
- `new-plot.svg` `new-plot-alt.svg`
- `new-import.svg` `new-import-alt.svg`
- `calculate.svg`
- `open-as-plot.svg`
- `fit.svg`

The following icons are derived from the [Paper icon theme](https://github.com/snwh/paper-icon-theme) and, thus, distributed 
under the terms of the [Creative Commons Attribution-ShareAlike 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/) 
(CC-BY-SA 4.0):

- `edit.svg`
- `export-data.svg`
- `single-spectrum.svg` `merge-files.svg`

The following icons are derived from the [Antü Classic icon theme](https://github.com/fabianalexisinostroza/Antu-classic)
and, thus, distributed under the terms of the GNU Lesser General Public License (LGPL), version 2.1:

- `add-col.svg` `remove-col.svg`
- `add-row.svg` `remoce-row.svg`
- `scalebar.svg`
- `axislabel.svg`

The following icons are part of the [Bootstrap icon library](https://github.com/twbs/icons) and distributed under the 
terms of the MIT license, Copyright 2019-2020 The Bootstrap Authors:

- `go-first.svg` `go-back.svg` `go-previous.svg` `go-next.svg` `go-forward.svg` `go-last.svg`
- `go-to.svg`
- `update.svg`
- `convey.svg`